# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 14:57:33 2018

main author: Regina Beckmann
modified by: Michael Grund
"""

import os
import json
import math
import pickle

#import cartopy.crs as ccrs
#import matplotlib.pyplot as plt
import netCDF4 as nc4
import numpy as np
import pandas as pd
#from cartopy.io.img_tiles import GoogleTiles
from scipy import ndimage

from . import CFall3D


# class ShadedReliefESRI(GoogleTiles):

#     # shaded relief
#     def _image_url(self, tile):
#         x, y, z = tile
#         url = ('https://server.arcgisonline.com/ArcGIS/rest/services/' \
#                'World_Imagery/MapServer/tile/{z}/{y}/{x}.jpg').format(
#             z=z, y=y, x=x)
#         return url


class AshfallSimulation(dict):

    def __init__(self, name=None, path=None):

        self.name = name
        self.path = path

        self.readgran(path, name)
        self.setup(path, name)

    def get_dict(self):
        export = dict()
        for key in self.keys(): export[key] = self[key]
        return export

    def readgran(self, path, name):

        # read granulometry file from SetSRC

        file = os.path.join(os.getcwd(), "GranulometrySRC.txt")

        f = open(file, 'r+')
        self['tgsd'] = np.loadtxt(f, delimiter=' ', comments='Diameter(mm) Density(kg/m^3) Psi Mass Fraction(%)')

        self['diam'] = self['tgsd'][:, 0]
        self['rhop'] = self['tgsd'][:, 1]
        self['psi'] = self['tgsd'][:, 2]
        self['fc'] = self['tgsd'][:, 3]

        self['nc'] = len(self['diam'])

        return self.get_dict()

    def setup(self, path, name):

        # inp_file file path
        file = name

        json_file = open(file)
        f = json_file.read()
        inp_file = json.loads(f)

        # import source class object
        file = os.path.join(os.getcwd(), 'Source_Object.pkl')

        # import source object

        # read source as dict
        with open(file, 'rb') as f:
            source = pickle.load(f)

        # day of eruption
        self['ibyr'] = source['year_erupt']
        self['ibmo'] = source['month_erupt']
        self['ibdy'] = source['day_erupt']

        # set begin time of run (in h)
        if source['ndt'] == 1:
            self['beg_time'] = source['erupt_start']
        elif source['ndt'] > 1:
            if source['source_type'] == 'RESUSPENSION':
                self['beg_time'] = source['erupt_start']
            else:
                self['beg_time'] = source['erupt_start'][0]

        # set end time of run (in h)
        self['run_end'] = source['run_end']

        # number of eruptive phases
        self['ndt'] = source['ndt']

        # start and end of eruptive phases (in h)
        self['erupt_start'] = source['erupt_start']
        self['erupt_end'] = source['erupt_end']

        # source type
        self['source_type'] = source['source_type']

        # Analsysis type (NCEP/NCAR or ERA5)
        self['analysis_type'] = source['analysis_type']

        if source['source_type'] == 'RESUSPENSION' and np.size(source['erupt_start']) > 1:
            print('Please give only one value as eruption start for resuspension case.')

        if np.size(self['erupt_start']) != np.size(self['erupt_start']):
            print('Fall3D: Number of eruptive phases not in accordance. Please check input file.')
            quit()

        # specifications on computational grid    
        self['lonmin'] = source['lonmin']
        self['lonmax'] = source['lonmax']
        self['latmin'] = source['latmin']
        self['latmax'] = source['latmax']

        # horizontal resolution
        self['res'] = source['res']

        # vent position
        self['xvent'] = source['X0']
        self['yvent'] = source['Y0']
        self['zvent'] = source['Z0']

        # number of source points
        self['plume_ns'] = source['plume_ns']

        # import topography from source
        self['topog'] = source['topog']

        # check whether options are active
        self['gravity_current'] = inp_file["GRAVITY_CURRENT"]["GRAVITY_CURRENT"]
        self['aggregation'] = source['aggregation']

        if self['aggregation']:
            self['vset_aggr'] = source['vset_aggr']

        if self['gravity_current'] == True and source['source_type'] != 'PLUME':
            print(
                'Gravity current assumes a plume source. Please check consistency with SetSRC. Gravity current turned off.')
            self['gravity_current'] = False

        if self['gravity_current'] == True:
            self['c_flow_rate'] = inp_file["GRAVITY_CURRENT"]["C_FLOW_RATE"]
            self['lambda_grav'] = inp_file["GRAVITY_CURRENT"]["LAMBDA_GRAV"]
            self['k_entrain'] = inp_file["GRAVITY_CURRENT"]["K_ENTRAIN"]
            self['brunt_vaisala'] = inp_file["GRAVITY_CURRENT"]["BRUNT_VAISALA"]

            self['radial_wind_lon'] = self['xvent']
            self['radial_wind_lat'] = self['yvent']

            # Find velocity model number
        vel_model = inp_file["FALL3D"]["TERMINAL_VELOCITY_MODEL"]

        if vel_model == 'ARASTOPOOUR' or vel_model == 'ARASTOOPOUR':
            self['modv'] = 1
        elif vel_model == 'GANSER':
            self['modv'] = 2
        elif vel_model == 'WILSON':
            self['modv'] = 3
        elif vel_model == 'DELLINO':
            self['modv'] = 4
        elif vel_model == 'PFEIFFER':
            self['modv'] = 5
        elif vel_model == 'DIOGUARDI2017':
            self['modv'] = 6
        elif vel_model == 'DIOGUARDI2018':
            self['modv'] = 7
        else:
            print('Invalid settling model. Ganser model is applied.')
            self['modv'] = 2

        # Turbulence model    
        vert_turb = inp_file["FALL3D"]["VERTICAL_TURBULENCE_MODEL"]

        if vert_turb == 'CONSTANT':
            self['modkv'] = 0
            self['rkv0'] = inp_file["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"]
        elif vert_turb == 'SIMILARITY':
            self['modkv'] = 1
        elif vert_turb == 'SURFACE_LAYER':
            self['modkv'] = 2
        else:
            print('Invalid vertical turbulence model. Constant vertical turbulence is assumed.')
            self['modkv'] = 0

        hor_turb = inp_file["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"]

        if hor_turb == 'CONSTANT':
            self['modkh'] = 0
            self['rkh0'] = inp_file["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"]
        elif hor_turb == 'RAMS':
            self['Csh'] = inp_file["FALL3D"]["RAMS_CS"]
            # default value = 0.2275
            self['modkh'] = 1
        elif hor_turb == 'CMAQ':
            self['modkh'] = 2
        else:
            print('Invalid horizontal turbulence model. Constant horizontal turbulence is assumed.')
            self['modkh'] = 0

        self['wetdeposition'] = inp_file["FALL3D"]["WET_DEPOSITION"]

        # TODO: download and integrate precipiation data
        if self['wetdeposition']:
            # precipation rate in mm/h, constant for whole area 
            self['p_rate'] = inp_file["FALL3D"]["P_RATE"]

            # Aggregation
        if source['aggregation']:
            self['aggregationtime'] = True

            # index of first aggregating class
            # (Aggregation occurrs at classes from iaggr:np-1)
            iaggr = -1

            for ic in range(source['npart'] - 1):
                if (source['diam_aggr'] > self['diam'][ic]):
                    iaggr = ic

            if (iaggr == -1): iaggr = np - 2  # ensure at least 1 aggregating class

            self['Df'] = inp_file["AGGREGATION"]["FRACTAL_EXPONENT"]  # default = 3
            self['iaggr'] = iaggr
            self['ndtagr'] = 1  # default value
            self['vset_factor'] = inp_file["AGGREGATION"]["VSET_FACTOR"]

        else:
            self['aggregationtime'] = False

        # meteo data information (in h)
        self['ibdbs'] = source['meteo_start']
        self['iedbs'] = source['meteo_end']
        self['dtdbs'] = source['meteo_inc']

        # number of particles
        self['npart'] = source['npart']

        # zlayer information
        self['zmin'] = source['zmin']
        self['zmax'] = source['zmax']
        self['zinc'] = source['zinc']

        self['zlayer'] = source['zlayer']

        self['nx'] = source['nx']
        self['ny'] = source['ny']
        self['nz'] = source['nz']

        Rearth = 6356 * 10 ** 3  # earth´s radius (m)

        # horizontal increments (dX2 not scaled)
        self['dX1'] = Rearth * (source['dlon'] * math.pi / 180.0)  # in m
        self['dX2'] = Rearth * (source['dlat'] * math.pi / 180.0)

        self['dlon'] = source['dlon']
        self['dlat'] = source['dlat']

        # Compute array dz (vertical increments, in m)
        dz = np.zeros((source['nz']))

        for k in range(source['nz'] - 1):
            dz[k] = source['zlayer'][k + 1] - source['zlayer'][k]

        dz[source['nz'] - 1] = dz[source['nz'] - 2]

        self['dz'] = dz

        # calculate scaling factor
        self['Hm1'] = CFall3D.scalingfac(self, source)

        self['source_time'] = self['beg_time']
        self['data_type'] = inp_file["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"]

        if source['data_type'] == 'MONTHLY_MEAN':
            self['meteo_time'] = 0
            self['meteo_ind'] = 0
        else:
            self['meteo_time'] = self['ibdbs']
            self['meteo_ind'] = 0

        # initialize concentration and ground load
        self['c'] = np.zeros((self['nz'] + 2, self['ny'] + 2, self['nx'] + 2, source['nc']))

        self['cdry'] = np.zeros((self['ny'], self['nx'], source['nc']))
        self['cwet'] = np.zeros((self['ny'], self['nx'], source['nc']))

        # mass lost at boundaries
        self['outmass'] = 0

        # aggregation
        self['mclass'] = np.zeros((source['nc'], 2))
        self['Maggr'] = np.zeros((self['nz'], self['ny'], self['nx']))

        # specify how often images are produced
        self['dt_images'] = inp_file["OUTPUT"]["TIME_IMAGES"]  # in h

        # flight level concentration
        self['flight_level'] = inp_file["FALL3D"]["FLIGHT_LEVEL"]
        self['simulation_type'] = inp_file["FALL3D"]["SIMULATION"]

        # initialisations for plotting
        self['cday'] = self['ibdy']  # current day
        self['dn'] = 1

        return self.get_dict()

    # read source from pickle file

    def readsource(self, sind, path, name):

        # import source class object
        file = os.path.join(os.getcwd(), 'Source_Object.pkl')

        # read source as dict
        with open(file, 'rb') as f:
            source = pickle.load(f)

        tmrat = source['tmrat_%s' % sind]
        xsrc = source['xsrc_%s' % sind]
        ysrc = source['ysrc_%s' % sind]
        zsrc = source['zsrc_%s' % sind]
        nsou = source['nsrc_%s' % sind]

        if source['source_type'] != 'RESUSPENSION':
            self['zs'] = source['zs_%s' % sind]  # Zplum

        self['nc'] = source['nc']

        # total flow rate
        flowrate = np.sum(tmrat)

        self['flowrate'] = flowrate
        self['ngas'] = source['ngas']

        # flow rate particles
        flowrate_part = np.sum(tmrat[:, 0:source['nc'] - source['ngas']])

        print('Flow rate of eruptive phase (kg/s) \n Total: %s \n Particle: %s \n Gas: %s' \
              % (round(flowrate), round(flowrate_part), round(flowrate - flowrate_part)))

        # flow rate gas
        if (source['ngas'] > 0) and source['source_type'] != 'RESUSPENSION':
            flowrate_gas = np.sum(tmrat[:, source['nc'] - 1])
        else:
            flowrate_gas = 0

        if source['aggregation']:
            self['tplume'] = np.zeros((self['ny'], self['nx'], self['nz']))
            # TODO: settempplume fehlt (wegen plume)
            # aggregation geht auch ohne plume, trotzem braucht man tplume???

        # scale source term   
        for ns in range(nsou):
            ix = int(xsrc[ns])
            iy = int(ysrc[ns])

            tmrat[ns, :] = tmrat[ns, :] * self['Hm1'][iy, ix]

        # set to false so that not at every iteration the same soure term is read
        self['sourcetime'] = False

        if (self['aggregation']):
            self['aggregationtime'] = True

        self['tmrat'] = tmrat
        self['xsrc'] = xsrc
        self['ysrc'] = ysrc
        self['zsrc'] = zsrc
        self['nsou'] = nsou

        return self.get_dict()

    def readmeteo(self, mind, path, name):
        path_CDomain = os.path.join(os.getcwd(), "Fall3D")
        from Fall3D.Domain import Domain

        topopath = self.path + '/TsupyDEM/'

        sl = [[self['lonmin'], self['lonmax'], self['latmin'], self['latmax'], self['res'], 'VolcanoDomain']]
        d = Domain(sl, DEMfolder=topopath)

        # new structure, -MG- 02/2020
        if self['data_type'] == 'MONTHLY_MEAN':
            folder = path + '/MeteoDataBase/' + self['analysis_type'] + '/' + self['data_type']
        elif self['data_type'] == 'DAILY':
            folder = path + '/MeteoDataBase/' + self['analysis_type'] + '/' + self['data_type'] + '/' + str(
                self['ibyr'])

        data_meteo = d.AddMeteo(folder, source=self['analysis_type'], year=self['ibyr'], month=self['ibmo'], \
                                day=self['ibdy'], hour=self['beg_time'], run_end=self['run_end'], \
                                data_type=self['data_type'], topography=self['topog'], zlayer=self['zlayer'],
                                path_C=path_CDomain)

        # read monthly mean data
        if self['data_type'] == 'MONTHLY_MEAN':

            # extract meteorological data from dict
            vx = data_meteo['data']['u']  # eastward wind
            vy = data_meteo['data']['v']  # northward wind
            vz = data_meteo['data']['w']  # vertical wind from omega
            temp = data_meteo['data']['temp']  # temperature in K
            hum = data_meteo['data']['hum']
            rho = data_meteo['data']['ro']
            vptemp = data_meteo['data']['Tp']  # potential temperature
            pres = data_meteo['data']['p_lvls']
            qv = data_meteo['data']['qv']  # specific humidity

            # temperature at surface
            Tz0 = data_meteo['data_pressure']['t2m']

            # velocity at surface
            u10 = data_meteo['data_pressure']['u10']
            v10 = data_meteo['data_pressure']['v10']

            # read boundary layer height
            if self['analysis_type'] == 'ERA5':
                zi = data_meteo['data_pressure']['blh']
            else:
                print('No boundary layer information available. Assuming a height of 1500 m.')
                zi = np.ones((self['ny'], self['nx'])) * 1500



        elif self['data_type'] == 'DAILY' and self['analysis_type'] == 'NCAR':

            # extract meteorological data from dict    
            vx = data_meteo['data']['u'][mind, :, :, :]  # eastward wind
            vy = data_meteo['data']['v'][mind, :, :, :]  # northward wind
            vz = data_meteo['data']['w'][mind, :, :, :]  # vertical wind from omega
            temp = data_meteo['data']['temp'][mind, :, :, :]  # temperature in K
            hum = data_meteo['data']['hum'][mind, :, :, :]
            rho = data_meteo['data']['ro'][mind, :, :, :]
            vptemp = data_meteo['data']['Tp'][mind, :, :, :]  # potential temperature
            pres = data_meteo['data']['p_lvls'][mind, :,
                   :]  # debugged -MG- 2020/01, ori. by RB: pres = data_meteo['data']['p_lvls'][mind,:,:,:]
            qv = data_meteo['data']['qv'][mind, :, :, :]  # specific humidity

            # temperature at surface           
            Tz0 = data_meteo['data_pressure']['t2m'][mind, :, :]

            # velocity at surface
            u10 = data_meteo['data_pressure']['u10'][mind, :, :]
            v10 = data_meteo['data_pressure']['v10'][mind, :, :]

            # read boundary layer height
            if self['analysis_type'] == 'ERA5':
                zi = data_meteo['data_pressure']['blh'][mind, :, :]
            else:
                print('No boundary layer information available. Assuming a height of 1500 m.')
                zi = np.ones((self['ny'], self['nx'])) * 1500


        # -MG- made separation between NCAR DAILY and ERA DAILY since ERA DAILY data has same format like MONTHLY MEANS data
        elif self['data_type'] == 'DAILY' and self['analysis_type'] == 'ERA5':

            # extract meteorological data from dict
            vx = data_meteo['data']['u']  # eastward wind
            vy = data_meteo['data']['v']  # northward wind
            vz = data_meteo['data']['w']  # vertical wind from omega
            temp = data_meteo['data']['temp']  # temperature in K
            hum = data_meteo['data']['hum']
            rho = data_meteo['data']['ro']
            vptemp = data_meteo['data']['Tp']  # potential temperature
            pres = data_meteo['data']['p_lvls']
            qv = data_meteo['data']['qv']  # specific humidity

            # temperature at surface
            Tz0 = data_meteo['data_pressure']['t2m']

            # velocity at surface
            u10 = data_meteo['data_pressure']['u10']
            v10 = data_meteo['data_pressure']['v10']

            # read boundary layer height
            if self['analysis_type'] == 'ERA5':
                zi = data_meteo['data_pressure']['blh']
            else:
                print('No boundary layer information available. Assuming a height of 1500 m.')
                zi = np.ones((self['ny'], self['nx'])) * 1500

        # Flip meteo data so that iy = 0 is south and ix = 0 west (which is the case) 
        meteovar = [vx, vy, vz, temp, hum, rho, vptemp, pres, qv]

        for f in meteovar:
            f = np.flip(f, 1)  # axis 1 is latitude for 3D data

        meteovar2D = [Tz0, u10, v10, zi]

        for f in meteovar2D:
            f = np.flip(f, 0)  # axis 0 is latitude for 2D data

        ######################################################################
        # manipulate data (own topography and wind data)
        # self.artificial_topography(H = 2000, R = 3000)

        # vx,vy,temp,vptemp,pres,rho,hum = self.artificial_meteorology()

        # Tz0 = temp[0,:,:]
        ######################################################################

        # give information            
        nx = self['nx']
        ny = self['ny']
        nz = self['nz']

        # Give information on meteo data
        print('Westward wind speed(m/s) \n Max: %s \n Min: %s  \n Avg: %s' \
              % (round(np.amax(vx), 2), round(np.amin(vx), 2), round(np.sum(vx) / (nx * ny * nz), 2)))
        print('Northward wind speed(m/s) \n Max: %s \n Min: %s  \n Avg: %s' \
              % (round(np.amax(vy), 2), round(np.amin(vy), 2), round(np.sum(vy) / (nx * ny * nz), 2)))
        print('Vertical wind speed(m/s) \n Max: %s \n Min: %s  \n Avg: %s' \
              % (round(np.amax(vz), 2), round(np.amin(vz), 2), round(np.sum(vz) / (nx * ny * nz), 2)))

        print('Air temperature (K)  \n Max: %s \n Min: %s  \n Avg: %s' \
              % (round(np.amax(temp), 1), round(np.amin(temp), 1), round(np.sum(temp) / (nx * ny * nz), 1)))

        print('Air density (kg/m^3)  \n Max: %s \n Min: %s  \n Avg: %s' \
              % (round(np.amax(rho), 2), round(np.amin(rho), 2), round(np.sum(rho) / (nx * ny * nz), 2)))

        print('Air humidity (perc)  \n Max: %s \n Min: %s  \n Avg: %s' \
              % (round(np.amax(hum), 2), round(np.amin(hum), 2), round(np.sum(hum) / (nx * ny * nz), 2)))

        # wind at bottom is zero
        vx[0, :, :] = 0
        vy[0, :, :] = 0
        vz[0, :, :] = 0

        # get parameters of ABL

        u = vx[1, :, :]  # wind speeds at z1 (first level above ground)
        v = vy[1, :, :]

        Tz1 = temp[1, :, :]
        z0 = 1  # reference level z0 (bottom, 1m)
        z1 = self['zlayer'][1]  # reference level z1 > z0
        Pz0 = pres[0, :]  # pressure at levels, debugged by -MG- 2020/01, ori: Pz0 = pres[0,:,:]
        Pz1 = pres[1, :]  # debugged by -MG- 2020/01, ori: Pz1 = pres[1,:,:]

        # potential temperature at z1 and z0 (in K)
        Thz0 = Tz0 * (1.01 * 100000 / Pz0) ** (0.285)
        Thz1 = Tz1 * (1.01 * 100000 / Pz1) ** (0.285)

        rmonin, ustar = CFall3D.get_par_ABL(u, v, nx, ny, z1, z0, Tz1, Tz0, Thz0, Thz1)

        # calculate diffusion coefficients
        print('FALL3D: Calculate diffusion coefficients.')

        rkver = CFall3D.diff_coeff_v(self, self['modkv'], self['nx'], self['ny'], self['nz'], \
                                     vx, vy, vptemp, ustar, rmonin, self['zlayer'], self['dz'], zi)

        rkhor = CFall3D.diff_coeff_h(self, self['modkh'], self['nx'], self['ny'], self['nz'], \
                                     vx, vy, self['Hm1'], self['dX1'], self['dX2'], self['dlon'], self['dlat'])

        # write to dict
        self['rkhor'] = rkhor
        self['rkver'] = rkver
        self['vx'] = vx
        self['vy'] = vy
        self['vz'] = vz
        self['temp'] = temp
        self['hum'] = hum
        self['rho'] = rho
        self['Tz0'] = Tz0
        self['u10'] = u10
        self['v10'] = v10
        self['qv'] = qv
        self['pres'] = pres
        self['rmonin'] = rmonin
        self['ustar'] = ustar
        self['zi'] = zi
        self['vptemp'] = vptemp

        # set to true when new meteo data is read
        if (self['aggregation']):
            self['aggregationtime'] = True

        return self.get_dict()

    def calc_vset(self):

        # calculate settling velocities
        print('Calculate settling velocities of all particle classes (m/s) according to model %s' % self['modv'])

        vset = CFall3D.set_vsett(self, self['npart'], self['nx'], self['ny'], self['nz'], self['nc'])

        self['vset'] = vset

        return self.get_dict()

    def calc_vdry(self):
        # Dry deposition velocities to account for effects distinct from sedimentation velocity (setvdrydep.f90)

        # Computation is done only for particles smaller than 100 mic (the limit of the aerosol Giant mode) and for the second z-layer
        # (first is ground, where w=0)

        # The Feng model discriminates between 6 land-use categories:

        # 1 'Urban'
        # 2 'Remote continental'
        # 3 'Desert'
        # 4 'Polar'
        # 5 'Marine'
        # 6 'Rural'

        # diameter (mm)
        diam = self['tgsd'][:, 0]

        # convert to m
        diam = diam / 1000

        nc = self['nc']
        nx = self['nx']
        ny = self['ny']
        nz = self['nz']

        # modes
        imode = np.zeros((self['nc']))

        have_to_work = False
        ipass = 0

        # Loop over classes to averiguate the mode (including particles)
        for ic in range(nc):
            if diam[ic] > 10 ** -4:  # not considererd (> 100 micrometer, 0.1 mm)
                imode[ic] = 0
            elif diam[ic] > 10 ** -5:  # giant mode
                imode[ic] = 1
                have_to_work = True
            elif diam[ic] > 2.5 * 10 ** -6:  # coarse mode
                imode[ic] = 2
                have_to_work = True
            elif diam[ic] > 10 ** -7:  # accumulation mode
                imode[ic] = 3
                have_to_work = True
            else:
                imode[ic] = 4
                have_to_work = True

        if np.amax(imode) == 0:
            print('None of the particle classes is considered to calculate dry deposition velocity.')

        # initialize values at the first time
        if ipass == 0:
            a = np.zeros((6, 4))
            b = np.zeros((6, 4))

            ipass = 1

            a[0, 0] = 0.0048  # Mode 1
            a[1, 0] = 0.0037
            a[2, 0] = 0.0042
            a[3, 0] = 0.0032
            a[4, 0] = 0.0043
            a[5, 0] = 0.0045
            b[0, 0] = 1.0
            b[1, 0] = 1.0
            b[2, 0] = 1.0
            b[3, 0] = 1.0
            b[4, 0] = 1.0
            b[5, 0] = 1.0

            a[0, 1] = 0.0315  # Mode 2
            a[1, 1] = 0.0120
            a[2, 1] = 0.2928
            a[3, 1] = 0.1201
            a[4, 1] = 0.1337
            a[5, 1] = 0.0925
            b[0, 1] = 2.7925
            b[1, 1] = 2.2413
            b[2, 1] = 3.8581
            b[3, 1] = 3.4407
            b[4, 1] = 3.5456
            b[5, 1] = 3.2920

            a[0, 2] = 1.2891  # Mode 3
            a[1, 2] = 1.3977
            a[2, 2] = 1.3970
            a[3, 2] = 1.1838
            a[4, 2] = 1.2834
            a[5, 2] = 1.2654
            b[0, 2] = 2.6878
            b[1, 2] = 2.5838
            b[2, 2] = 2.5580
            b[3, 2] = 2.8033
            b[4, 2] = 2.7157
            b[5, 2] = 2.7227

            a[0, 3] = 1.0338  # Mode 4
            a[1, 3] = 1.0707
            a[2, 3] = 0.9155
            a[3, 3] = 1.0096
            a[4, 3] = 1.1595
            a[5, 3] = 1.0891
            b[0, 3] = 1.2644
            b[1, 3] = 1.3247
            b[2, 3] = 1.0364
            b[3, 3] = 1.2069
            b[4, 3] = 1.4863
            b[5, 3] = 1.4240

            self['a'] = a
            self['b'] = b

        if have_to_work:

            vdep = CFall3D.set_vdry(self, imode)

            print('Dry deposition velocity (m/s)  \n Max: %s \n Min: %s  \n Avg: %s' \
                  % (round(np.amax(vdep), 7), round(np.amin(vdep), 7), round(np.sum(vdep) / (nx * ny * nc), 7)))

            self['vdep'] = vdep

        else:
            self['vdep'] = np.zeros((ny, nx, nc))

        # apply scaling
        self['vx'], self['rho'], self['rkh1'] = CFall3D.apply_scaling(self)

        # correct vertical velocity (corvver.f90)
        self['vz'] = CFall3D.corvver(self)

        ####################################################
        # TODO: Guntur & Tavurvur
        # self['vz'] = np.zeros((nz,ny,nx))
        ####################################################

        return self.get_dict()

    def gravity_current(self, time):
        # TODO: not tested yet

        # add radial wind field(addradialwind.f90)   

        if self['gravity_current'] == True and self['source_type'] == 'PLUME':
            self['vx'], self['vy'] = CFall3D.add_radial_wind(self, time)

        # Compute the velocity field divergence (wind field plus possible gravity current) --> subroutine setdivu.f90
        self['divu'] = CFall3D.setdivu(self)

        # this will be needed to check for non-zero divergence wind fields --> neglect part in PDE

        return self.get_dict()

    def critical_dt(self):

        # calculate the critical time step (finddt.f90)
        dt = CFall3D.finddt(self)

        return dt

    def calc_aggr_par(self):

        # if necessary computes aggregation parameters and corrects settling velocity of the aggregates
        self['vset'], self['Alagr'], self['Abagr'], self['Asagr'], self['Adagr'], self['Atagr'] = CFall3D.agrpar(self)

        self['aggregationtime'] = False

        return self.get_dict()

    def setsrc(self, dt):

        # set the already scaled source term (setsrc.f90)

        Hm1 = self['Hm1']
        dX1 = self['dX1']
        dX2 = self['dX2']

        dz = self['dz']
        nc = self['nc']

        nsou = self['nsou']
        xsrc = self['xsrc']
        ysrc = self['ysrc']
        zsrc = self['zsrc']

        tmrat = self['tmrat']
        c = self['c']

        # concentration at source points
        for isou in range(nsou):
            ix = int(xsrc[isou])
            iy = int(ysrc[isou])
            iz = int(zsrc[isou])

            if iz == 0:
                dzeta = dz[0]
            else:
                dzeta = 0.5 * (dz[iz - 1] + dz[iz])

            vol0 = Hm1[iy, ix] * dX1 * dX2 * dzeta

            for ic in range(nc):
                c[iz, iy, ix, ic] = c[iz, iy, ix, ic] + dt * tmrat[isou, ic] / vol0

        # account for aggregation
        ipass = 0

        if self['aggregation']:

            ipass = ipass + 1

            if (ipass % self['ndtagr'] == 0):
                c, self['Maggr'], self['mclass'] = CFall3D.agrsrc(self, dt)

        # del self['c']
        self['c'] = c
        # print(np.shape(c))

        return self.get_dict()

    def solveads(self, ic, dt, iiter):

        # set boundary conditions
        c = CFall3D.setboundaries(self, ic)

        # advection in z-direction
        c = CFall3D.advectz(self, c, dt, ic)

        # print(np.amax(c-a))

        # advection in x and y direction

        if iiter % 2 == 0:
            c = CFall3D.advectx(self, c, dt)
            c = CFall3D.advecty(self, c, dt)

        else:
            c = CFall3D.advecty(self, c, dt)
            c = CFall3D.advectx(self, c, dt)

        # diffusion
        c = CFall3D.diffz(self, c, dt)

        # diffusion in x and y 
        if iiter % 2 == 0:
            c = CFall3D.diffx(self, c, dt)
            c = CFall3D.diffy(self, c, dt)

        else:
            c = CFall3D.diffy(self, c, dt)
            c = CFall3D.diffx(self, c, dt)

        # wet deposition
        if self['wetdeposition']:
            c, cwet = CFall3D.wetdep(self, c, ic, dt)
            self['cwet'] = cwet

            # sedimentation at groud
        cdry, c = CFall3D.accum(self, dt, ic, c)

        # clip negative concentration values
        c[c < 0] = 0

        self['cdry'] = cdry
        self['c'][:, :, :, ic] = c

        return self.get_dict()

    def lostmass(self, dt, erumass):

        ######################################################################
        # Compute the mass lost at the boundaries (cmass2d.f90)
        ######################################################################

        # Note that c and u hold the scaled quantities :

        # c* = c * Hm1
        # u* = u / Hm1

        # However, since dx = Hm1*dX1 = sin(gama)*Rearth*dgama the
        # terms cancel in the summ over the area. Consequently, for
        # spherical coordinates it is sufficient to call cmass2d with
        # dX1 and dX2 (without correcting for Hm1)

        mymass = CFall3D.cmass2d(self, dt)

        self['outmass'] = self['outmass'] + mymass

        # total ground load
        self['cload'] = self['cdry'] + self['cwet']

        # Correct the mass unbalance due to the nonnull divergence (divcorr.f90)
        if self['gravity_current']:
            conc = CFall3D.divcorr(self, erumass, dt)

            self['c'] = conc

        return self.get_dict()

    def calcmass(self, dt):

        c_classes = np.sum(self['c'], axis=3)

        # compute mass in the domain and in the deposit (writim.f90 --> cmass3d.f90)
        massvol, massdep, massdry, masswet = CFall3D.calcmass_c(self, dt, c_classes)

        self['massvol'] = massvol
        self['massdep'] = massdep
        self['massdry'] = massdry
        self['masswet'] = masswet

        return self.get_dict()

    def giveinfo(self, time, dt, iiter, erumass):

        # thickness of deposits (m)
        self['Thickdep'] = CFall3D.depthick(self)

        # first iteration
        if iiter == 2:
            # Cities (or other csv files)
            path_cities = self.path + '/Cartopy_Data/Cities.csv'
            cities = pd.read_csv(path_cities)
            # Name = np.asarray(cities['NAME'])

            lonc = cities['LONGITUDE']
            latc = cities['LATITUDE']
            # popc = np.asarray(cities['POP_MAX'])

            indc = np.where((latc <= self['latmax']) & (latc >= self['latmin']) & (lonc <= self['lonmax']) & (
                        lonc >= self['lonmin']))
            indc = np.asarray(indc)

            # ROADS (or other csv files)
            # fname = self.path + '/Cartopy_Data/ne_10m_roads.shp'
            # shape_feature = ShapelyFeature(Reader(fname).geometries(),
            #                     ccrs.PlateCarree(), edgecolor='red')

            # STATES
            # states_provinces = cfeature.NaturalEarthFeature(
            #            category='cultural',
            #            name='admin_1_states_provinces_lines',
            #            scale='10m',
            #            facecolor='none')       

        # months for labelling
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

        # find current day
        if time > 3600 * 24 * self['dn']:
            self['cday'] = self['cday'] + 1
            self['dn'] = self['dn'] + 1

            if self['cday'] > 31:
                print('Fall3D warning: Entering next month. Check weather data.')

        daytime = time - 24 * 3600 * (self['dn'] - 1)  # time in s after 00:00 of current day
        dayhour = int(daytime / 3600)  # time in h
        dayminute = int((daytime - 3600 * dayhour) / 60)  # time in min

        if dayminute < 10:
            minutestr = ('0%s' % dayminute)
        else:
            minutestr = dayminute

        beg_time = self['beg_time'] * 3600  # in s

        # correct masses for boundary condition effects        
        if self['massdep'] + self['massvol'] > erumass:
            self['massvol'] = erumass - self['massdep']

            # Information block
        print('Information on Iteration number %s' % iiter)

        print('Time: \n  Time step(s): %s \n  Elapsed time (min): %s \n  Current day: %s %s %s \n  Current time : %s:%s' \
              % (round(dt, 2), round((time - beg_time) / 60, 2), self['cday'], months[self['ibmo'] - 1], self['ibyr'],
                 dayhour, minutestr))

        print(
            'Mass: \n  Total ejected (t): %s \n  Inside domain (t): %s (%s %%) \n  Outside domain (t): %s (%s %%) \n  Deposit(t) \n  (1) Wet: %s \n  (2) Dry: %s \n  (3) Total: %s (%s %%)' \
            % (round(erumass / 1000, 0), round(self['massvol'] / 1000, 1), round(self['massvol'] * 100 / erumass, 1), \
               round((erumass - self['massdep'] - self['massvol']) / 1000, 1),
               round((erumass - self['massdep'] - self['massvol']) * 100 / erumass, 1),
               round(self['masswet'] / 1000, 1), \
               round(self['massdry'] / 1000, 1), round(self['massdep'] / 1000, 1),
               round(self['massdep'] * 100 / erumass, 1)))

        # Plotting
        ground_load = np.sum(self['cload'][:, :, 0:self['npart']], axis=2)
        concentration = self['c'][0:self['nz'], 0:self['ny'], 0:self['nx'], :]  # cut rim values

        # correct for Hm1        
        Hm1 = self['Hm1']

        for ix in range(self['nx']):
            for iy in range(self['ny']):
                ground_load[iy, ix] = ground_load[iy, ix] / Hm1[iy, ix]
                concentration[:, iy, ix, :] = concentration[:, iy, ix, :] / Hm1[iy, ix]

        #####################################################################
        # parameters etc. later required for plotting in the post-processing step
        #####################################################################

        # meshgrid for countour plot
        lon = np.linspace(self['lonmin'], self['lonmax'], self['nx'])
        lat = np.linspace(self['latmin'], self['latmax'], self['ny'])

        xx, yy = np.meshgrid(lon, lat)
        # resolution of background image
        if abs(self['lonmin'] - self['lonmax']) > 5:
            tile_res = 7
        elif abs(self['lonmin'] - self['lonmax']) < 1:
            tile_res = 11
        else:
            tile_res = 10

            ############## 1. GROUND LOAD ######################

        # interpolation
        nx_inter = 1000
        f_inter = int(nx_inter / self['nx'])
        ground_load = ndimage.zoom(ground_load, f_inter)

        # make copy of ground_load here to save non-normed matrix
        ground_load_cp = ground_load.copy()

        ##### save for each time step all grids (groundload, deposit thickness etc.) in a single
        ##### netcdf file to process it outside of the main program
        # ==========================================================================================
        # helper function to prepare all variables for the netcdf file write first time snapshot [0]
        def prep_nc_data(f, group, varname, xdim, ydim, data, dto):

            # create different data groups based on group and varname input
            data_grp = f.createGroup(group)

            data_grp.createDimension(xdim['var'], len(xdim['vals']))
            data_grp.createDimension(ydim['var'], len(ydim['vals']))
            data_grp.createDimension('time', None)

            # define attributes to describe data
            f.title = 'Results_data_WILDCARD'
            f.nx = self['nx']
            f.ny = self['ny']

            xvals = data_grp.createVariable(xdim['full'], 'f4', xdim['var'])
            yvals = data_grp.createVariable(ydim['full'], 'f4', ydim['var'])
            vargrid = data_grp.createVariable(varname, 'f4', ('time', xdim['var'], ydim['var']))
            time = data_grp.createVariable('Time', 'i4', 'time')

            xvals[:] = xdim['vals']  # The "[:]" at the end of the variable instance is necessary
            yvals[:] = ydim['vals']
            vargrid[0, :, :] = data

            time_num = dto.timestamp()
            time[0] = time_num

            f.close()

        # ==========================================================================================

        # ==========================================================================================
        # helper function to update/add new time slices

        def mod_nc_data(f, group, varname, data, dto):

            # get current size of file and add afterwards
            vargrid = f.groups[group][varname]
            currsize = len(vargrid)  # no +1 is required since python always starts to count with 0
            vargrid[currsize, :, :] = data

            vartime = f.groups[group]['Time']
            time_num = dto.timestamp()
            vartime[currsize] = time_num

            f.close()

        # ==========================================================================================

        # ==========================================================================================
        # preallocate dict including plotting dimensions (x-y) info
        xdim = {}
        ydim = {}
        xdim['var'] = []
        xdim['vals'] = []
        xdim['full'] = []
        ydim['var'] = []
        ydim['vals'] = []
        ydim['full'] = []

        # current time step
        from datetime import datetime
        dto = datetime.strptime((str(self['cday']) + ' ' + months[self['ibmo'] - 1] + ' ' + str(
            self['ibyr']) + ' ' + str(dayhour) + ':' + str(minutestr)), '%d %b %Y %H:%M')

        # ==========================================================================================
        file = os.path.join("/home/mpiuser/sfs/", os.environ["USERNAME"], "uploads", "simres_grids.nc")

        # coordinates modified for map view grid  
        lonmod = np.arange(0, np.shape(ground_load_cp)[0], 1)
        latmod = np.arange(0, np.shape(ground_load_cp)[1], 1)

        group = 'Groundload_data'
        varname = 'Ground_load'
        xdim['var'] = 'lon'
        xdim['vals'] = lonmod
        xdim['full'] = 'Longitude'
        ydim['var'] = 'lat'
        ydim['vals'] = latmod
        ydim['full'] = 'Latitude'
        data = ground_load_cp  # ground_load_cp is unnormalized (is done in external plotting routine)

        if iiter == 2:

            # initialize all variables to save    
            f = nc4.Dataset(file, 'w', format='NETCDF4')  # 'w' stands for write, only use 'w' here
            prep_nc_data(f, group, varname, xdim, ydim, data, dto)

        elif iiter > 2:

            f = nc4.Dataset(file, 'a', format='NETCDF4')  # 'a' stands for append
            mod_nc_data(f, group, varname, data, dto)

        ###############################################################################################################################################
        ###############################################################################################################################################        

        # North-South CROSSLINE at vent + ground concentration 

        crossline_vent = self['c'][0:self['nz'], 0:self['ny'], int(self['xsrc'][0]), 0:self['nc']]
        crossline_tot = np.sum(crossline_vent, axis=2)  # all particle classes
        conc_ground = np.sum(self['c'][0, 0:self['ny'], 0:self['nx'], 0:self['nc']], axis=2)

        # MAP VIEW
        # coordinates map view
        loncross = np.arange(0, np.shape(conc_ground)[0], 1)
        latcross = np.arange(0, np.shape(conc_ground)[1], 1)

        group = 'Concentration_Info_1'
        varname = 'Ground_conc'
        xdim['var'] = 'lon'
        xdim['vals'] = loncross
        xdim['full'] = 'Longitude'
        ydim['var'] = 'lat'
        ydim['vals'] = latcross
        ydim['full'] = 'Latitude'
        data = conc_ground

        if iiter == 2:

            # initialize all variables to save    
            f = nc4.Dataset(file, 'a', format='NETCDF4')  # use also 'a' here to append since file was created above
            prep_nc_data(f, group, varname, xdim, ydim, data, dto)

        elif iiter > 2:

            f = nc4.Dataset(file, 'a', format='NETCDF4')  # 'a' stands for append
            mod_nc_data(f, group, varname, data, dto)

        # LAT-VERTICAL VIEW    
        # coordinates modified for cross section
        vertcross = np.arange(0, np.shape(crossline_tot)[0], 1)
        latcross = np.arange(0, np.shape(crossline_tot)[1], 1)

        group = 'Concentration_Info_2'
        varname = 'Ash_conc'
        xdim['var'] = 'z'
        xdim['vals'] = vertcross
        xdim['full'] = 'Vertical'
        ydim['var'] = 'lat'
        ydim['vals'] = latcross
        ydim['full'] = 'Latitude'
        data = crossline_tot

        if iiter == 2:

            # initialize all variables to save    
            f = nc4.Dataset(file, 'a', format='NETCDF4')  # use also 'a' here to append since file was created above
            prep_nc_data(f, group, varname, xdim, ydim, data, dto)

        elif iiter > 2:

            f = nc4.Dataset(file, 'a', format='NETCDF4')  # 'a' stands for append
            mod_nc_data(f, group, varname, data, dto)

            ###############################################################################################################################################
        ###############################################################################################################################################

        ###################################
        # plot current deposit thickness
        ###################################       

        thickness = self['Thickdep']

        # interpolation
        thickness = ndimage.zoom(thickness, f_inter)  # in m
        thickness = thickness * 1000  # in mm

        # make copy of thickness here to save non-normed matrix
        thickness_cp = thickness.copy()

        # assign nan values
        thickmax = np.amax(thickness)
        thickmin = min(thickmax * 0.005, 0.001)  # in m
        thickness[thickness < thickmin] = float('nan')

        bounds_cb = np.linspace(0, thickmax, 5)

        group = 'Depothick_data'
        varname = 'Depo_thick'
        xdim['var'] = 'lon'
        xdim['vals'] = lonmod
        xdim['full'] = 'Longitude'
        ydim['var'] = 'lat'
        ydim['vals'] = latmod
        ydim['full'] = 'Latitude'
        data = thickness_cp

        if iiter == 2:

            # initialize all variables to save    
            f = nc4.Dataset(file, 'a', format='NETCDF4')  # use also 'a' here to append since file was created above
            prep_nc_data(f, group, varname, xdim, ydim, data, dto)

        elif iiter > 2:

            f = nc4.Dataset(file, 'a', format='NETCDF4')  # 'a' stands for append
            mod_nc_data(f, group, varname, data, dto)

        ###############################################################################################################################################
        ###############################################################################################################################################

        ############ total avergae concentration above each ground point (to see ash spreading)

        conc = self['c'][0:self['nz'], 0:self['ny'], 0:self['nx'], 0:self['nc']]
        conc = np.sum(conc, axis=3)  # sum over nc
        conc = np.sum(conc, axis=0) / self['nz']  # average over height, in kg/m^3

        # transform to g/m^3
        conc = conc * 1000

        # interpolation
        conc = ndimage.zoom(conc, f_inter)

        # make copy of conc here to save non-normed matrix
        conc_cp = conc.copy()
        # ==============================================================================

        # min/max values
        mmin = 10 ** (-5)  # 0.01 g/m^3
        mmax = np.amax(conc)

        # assign Nan value so it becomes transparent
        conc[conc < mmin] = float('nan')

        bounds_cb = np.linspace(mmin, mmax, 6)

        group = 'Airborne_data'
        varname = 'Airborne_mass'
        xdim['var'] = 'lon'
        xdim['vals'] = lonmod
        xdim['full'] = 'Longitude'
        ydim['var'] = 'lat'
        ydim['vals'] = latmod
        ydim['full'] = 'Latitude'
        data = conc_cp

        if iiter == 2:

            # initialize all variables to save    
            f = nc4.Dataset(file, 'a', format='NETCDF4')  # use also 'a' here to append since file was created above
            prep_nc_data(f, group, varname, xdim, ydim, data, dto)

        elif iiter > 2:

            f = nc4.Dataset(file, 'a', format='NETCDF4')  # 'a' stands for append
            mod_nc_data(f, group, varname, data, dto)

        ###############################################################################################################################################
        ###############################################################################################################################################

        #################### Concentration at flight level ####################
        # del conc

        # conc = self['c'][0:self['nz'], 0:self['ny'], 0:self['nx'], 0:self['nc']]

        # # user-defined flight level
        # flightlevel = self['flight_level']

        # zlayer = self['zlayer']
        # dz = self['dz']

        # hgt_above_terrain = np.ones((self['ny'],self['nx']))*flightlevel - self['topog']

        # # concentration array (kg/m^3)
        # c_plane = np.zeros((self['ny'],self['nx']))

        # # find value at height level
        # for ix in range(self['nx']):
        #     for iy in range(self['ny']):
        #         for iz in range(self['nz']-1):

        #             if hgt_above_terrain[iy,ix] > self['zmax']:
        #                 print('FALL3D Warning: Flight level higher than top of computational domain.')
        #                 c_plane[iy,ix] = np.sum(conc[self['nz']-1,self['ny']-1,self['nx']-1,:])

        #             elif hgt_above_terrain[iy,ix] >= zlayer[iz] and hgt_above_terrain[iy,ix] < zlayer[iz+1]:                        
        #                 s = (hgt_above_terrain[iy,ix] - zlayer[iz])/dz[iz]                        
        #                 c_plane[iy,ix] = s*np.sum(conc[iz,iy,ix,:]) + (1.0-s)*np.sum(conc[iz+1,iy,ix,:])              

        # # convert to g/m^3
        # c_plane = 1000*c_plane

        # # interpolation
        # c_plane = ndimage.zoom(c_plane,f_inter)

        # # set small values to Nan
        # cmin = 2 # 2 g/m^3 (surely causes damage to turbines)       
        # cmax = np.amax(c_plane)       
        # c_plane[c_plane < cmin] = float('nan')

        # bounds_cb = np.linspace(0,cmax,5)
        # bounds = np.linspace(1.1*thickmin,thickmax,5)

        #        if iiter == 2:
        #            fig5 = plt.figure(num = 5,figsize =figsize,dpi=dpi,facecolor='w',edgecolor='k')
        #            ax5=fig5.add_axes([0.15,0.1,0.65,0.8],projection=ccrs.PlateCarree())
        #            ax5.set_extent([long[0], long[1], lati[0], lati[1]])
        #
        #            if self['simulation_type'] == 'real':
        #                # background
        #                ax5.add_image(tiler,tile_res,zorder=1,interpolation='bicubic')
        #
        #            # features
        #            ax5.add_feature(states_provinces,zorder=11, edgecolor='gray')
        #            #ax5.coastlines(resolution='10m', color='black', linewidth=1)
        #            ax5.plot(self['xvent'], self['yvent'], 'r^', markersize=2, transform=ccrs.Geodetic(),zorder=14)
        #
        #            ax5.set_xlabel('Longitude (deg)')
        #            ax5.set_ylabel('Latitude (deg)')
        #            ax5.set_yticks(np.linspace(self['latmin'],self['latmax'],6))
        #            ax5.set_xticks(np.linspace(self['lonmin'],self['lonmax'],6))
        #            ax5.set_xlim([self['lonmin'],self['lonmax']])
        #            ax5.set_ylim([self['latmin'],self['latmax']])
        #            ax5.set_title('%s %s %s at %s:%s Concentration %s km' % (self['cday'], months[self['ibmo']-1], self['ibyr'], dayhour, minutestr,self['flight_level']/1000))
        #
        #
        #            for i in range(np.size(indc,axis=1)):
        #
        #                ax5.scatter(lonc[indc[0,i]],latc[indc[0,i]],c='white',edgecolor='black',s=10,zorder=70)
        #                ax5.annotate(Name[int(indc[0,i])],xy=(lonc[indc[0,i]],latc[indc[0,i]]),
        #                     xytext=(1,1),textcoords='offset points',style='italic',color='w',fontweight='bold',zorder=45,
        #                     path_effects=[PathEffects.withStroke(linewidth=1,foreground="k")])

        #            self['img5'] = ax5.imshow(c_plane,zorder = 12,transform=ccrs.PlateCarree(),cmap = ccolor,alpha = alpha, \
        #                              extent = [self['lonmin'], self['lonmax'], self['latmin'], self['latmax']],\
        #                              vmax=cmax,vmin=0)

        #            axb5=fig5.add_axes([0.85,0.1,0.025,0.8])

        #            self['ax5'] = ax5
        #            self['fig5']= fig5
        #            self['axb5']  = axb5

        #            cb5=self['fig5'].colorbar(self['img5'], cax=self['axb5'] ,ticks = bounds_cb,extend='max',format='%.0f')
        #            cb5.set_label('Concentration (g/m$^3)$')
        #            plt.locator_params(nbins=6)
        #            tick_locator = ticker.MaxNLocator(nbins=6)
        #            cb5.locator = tick_locator
        #            cb5.update_ticks()
        #
        #
        #        elif iiter > 2:
        #
        #            img5 = self['img5']
        #            img5.remove()
        #
        #            self['ax5'].set_title('%s %s %s at %s:%s Concentration %s km' % (self['cday'], months[self['ibmo']-1], self['ibyr'], dayhour, minutestr,self['flight_level']/1000))
        #
        #            self['img5'] = self['ax5'].imshow(c_plane,zorder=12,transform=ccrs.PlateCarree(),cmap = ccolor,alpha = alpha, \
        #                              extent = [self['lonmin'], self['lonmax'], self['latmin'], self['latmax']],\
        #                              vmax=cmax,vmin=0)
        #
        #        #self['fig5'].savefig('%s/Simulations/%s/Figures/Flightlevel_conc_%s%s%s_%sh_%smin.png' \
        #        #             %(self.path,self.name, self['cday'],months[self['ibmo']-1], self['ibyr'],dayhour,minutestr),format = 'png',dpi = 'figure')
        #
        #        plt.pause(1)

        #plt.close('all')

    def saveres(self):

        # calculate are covered with certain ash amount
        ash_thresholds = [200, 500, 750, 800]  # in kg/m^2

        areas = np.zeros((len(ash_thresholds)))

        for ix in range(self['nx']):
            for iy in range(self['ny']):
                for i in range(len(ash_thresholds)):

                    if np.sum(self['cload'][iy, ix, :]) >= ash_thresholds[i]:
                        areas[i] = areas[i] + self['dX1'] * self['dX2'] * self['Hm1'][iy, ix]

        for i in range(len(ash_thresholds)):
            self['area_%s' % ash_thresholds[i]] = areas[i]

            # save result to pickle file
        f = open(os.path.join(os.getcwd(), "Results.pkl"), 'wb')
        pickle.dump(self, f)
        f.close()

        # TODO: for restart option end time of run is needed

    def accumulation_time(self, path, name, iiter, time):
        #################################
        # -MG- 2020/04 this step is not done so far 
        # is called in Fall3D_Master.py but was commented out by R. Beckmann
        #################################

        # accumulation over time at points specified in input file

        if iiter == 2:

            # find vent position
            tol = 10 ** -10

            nx = self['nx']
            ny = self['ny']

            # find index of vent position
            for i in range(nx):
                if abs(self['lonmin'] + i * self['dlon'] - self['xvent']) <= self['dlon'] / 2 + tol:
                    indx_vent = i

            for j in range(ny):
                if abs(self['latmin'] + j * self['dlat'] - self['yvent']) <= self['dlat'] / 2 + tol:
                    indy_vent = j

            self['indx_vent'] = indx_vent
            self['indy_vent'] = indy_vent

            file = name

            json_file = open(file)
            f = json_file.read()
            inp_file = json.loads(f)

            # initialization
            number_points = inp_file["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"]

            for ip in range(number_points):
                self['p%s' % (ip + 1)] = []
                self['p%s' % (ip + 1)].append(0)

            self['time_array'] = []
            self['time_array'].append(0)

            coord_points = np.zeros((number_points, 2))
            coord2_points = np.zeros((number_points, 2))
            names_points = dict()

            # coordinates/names of points
            for ip in range(number_points):
                p_num = 'P' + str(ip + 1)
                p_name = p_num + '_NAME'
                lon_p = inp_file["OUTPUT"]["POINTS_LOAD"][p_num][0]
                lat_p = inp_file["OUTPUT"]["POINTS_LOAD"][p_num][1]

                coord2_points[ip, 0] = lon_p
                coord2_points[ip, 1] = lat_p

                coord_points[ip, 0] = self['indx_vent'] + int((lon_p - self['xvent']) / self['dlon'])
                coord_points[ip, 1] = self['indy_vent'] + int((lat_p - self['yvent']) / self['dlat'])

                names_points["P%s" % (ip + 1)] = inp_file["OUTPUT"]["POINTS_LOAD"][p_name]

            self['coord_points'] = coord_points
            self['coord2_points'] = coord2_points
            self['name_points'] = names_points
            self['number_points'] = number_points

        gload = np.sum(self['cload'], axis=2)

        current_time = (time - self['beg_time'] * 3600) / 3600  # in h

        # update array with values
        for ip in range(self['number_points']):
            self['p%s' % (ip + 1)].append(gload[int(self['coord_points'][ip, 1]), int(self['coord_points'][ip, 0])])

        self['time_array'].append(current_time)

        # plotting
        #if iiter % 50 == 0:

            # fig6 = plt.figure(num=6, dpi=180, facecolor='w', edgecolor='k')
            # plt.clf()

            # axis1 = fig6.add_axes([0.15, 0.1, 0.65, 0.8])

            # for ip in range(self['number_points']):
            #     axis1.plot(self['time_array'], self['p%s' % (ip + 1)], label=self['name_points']["P%s" % (ip + 1)])
            # axis1.legend(loc='upper left')

            # axis1.set_xlabel('Time after eruption start (h)')
            # axis1.set_ylabel(r'Ground load (kg/m$^2$)')
            # plt.pause(0.1)

        #if iiter == 2:
            # show location of points

            # Background image
            # tiler = ShadedReliefESRI()  # Object of class
            # tile_res = 11

            # fig7 = plt.figure(num=7, dpi=180, facecolor='w', edgecolor='k')

            # self['fig7'] = fig7  # -MG-

            # axis2 = fig7.add_axes([0.15, 0.1, 0.65, 0.8], projection=ccrs.PlateCarree())
            # axis2.add_image(tiler, tile_res, zorder=1, interpolation='bicubic')

            # axis2.set_xlabel('Longitude (deg)')
            # axis2.set_ylabel('Latitude (deg)')
            # axis2.set_yticks(np.linspace(self['latmin'], self['latmax'], 5))
            # axis2.set_xticks(np.linspace(self['lonmin'], self['lonmax'], 5))
            # axis2.set_xlim([self['lonmin'], self['lonmax']])
            # axis2.set_ylim([self['latmin'], self['latmax']])

            # for ip in range(self['number_points']):
            #     axis2.scatter(self['coord2_points'][ip, 0], self['coord2_points'][ip, 1], c='r', edgecolor='r', s=10,
            #                   zorder=70)
            #     axis2.annotate(self['name_points']["P%s" % (ip + 1)],
            #                    (self['coord2_points'][ip, 0], self['coord2_points'][ip, 1]), \
            #                    xytext=(1, 1), textcoords='offset points', fontsize=5)

            # plt.pause(0.1)

            # ##### -MG- 
            # self['fig7'].savefig(os.path.join(os.getcwd(), "PLOTTING_TESTS.png"), format='png', dpi='figure')

        # return self.get_dict()

    def artificial_topography(self, H, R):
        # replaces topography by one mountain of relative height H with Radius R at vent position
        # has to be enabled in ashfall simulation

        print('FALL3D: Replacing topography by single volcano at vent position of relative height %s km, Radius %s km.' \
              % (round(H / 1000, 1), round(R / 1000, 1)))

        nx = self['nx']
        ny = self['ny']

        dX1 = self['dX1']
        dX2 = self['dX2']

        h_0 = 100  # fixed value
        topog = np.ones((ny, nx)) * h_0

        # find vent position
        tol = 10 ** -10

        for i in range(nx):
            if abs(self['lonmin'] + i * self['dlon'] - self['xvent']) <= self['dlon'] / 2 + tol:
                x_vent = i

        for j in range(ny):
            if abs(self['latmin'] + j * self['dlat'] - self['yvent']) <= self['dlat'] / 2 + tol:
                y_vent = j

        grad = H / R  # gradient of slope

        for ix in range(nx):
            for iy in range(ny):

                dist_x = abs(x_vent - ix) * dX1  # in m
                dist_y = abs(y_vent - iy) * dX2

                if ix == x_vent and iy == y_vent:
                    topog[iy, ix] = h_0 + H

                elif dist_x ** 2 + dist_y ** 2 <= R ** 2:
                    topog[iy, ix] = h_0 + H - grad * np.sqrt(dist_x ** 2 + dist_y ** 2)

                elif np.sqrt(dist_x ** 2 + dist_y ** 2) > R:
                    topog[iy, ix] = h_0

        self['indx_vent'] = x_vent
        self['indy_vent'] = y_vent
        self['topog'] = topog

        return self.get_dict()

    def artificial_meteorology(self):

        print('FALL3D: Replacing real meteorological data by artificial data.')

        ########### Values to be changed ###########
        wind = 'profile'  # either constant or profile

        profile_option = 'linear'  # linear/manual possible

        vx_min = 0
        vx_max = 25  # linear for profile

        vy_min = 0
        vy_max = 0

        vz_min = 0
        vz_max = 0

        hum_c = 40  # relative humidity, in %

        #############################

        T_sea = 293.15  # sea level temperature

        # pressure
        p_sea = 101325  # normal pressure at sea level (Pa)

        nx = self['nx']
        ny = self['ny']
        nz = self['nz']

        zlayer = self['zlayer']
        topog = self['topog']

        # initialization of arrays
        vx = np.zeros((nz, ny, nx))
        vy = np.zeros((nz, ny, nx))
        vz = np.zeros((nz, ny, nx))

        T = np.zeros((nz, ny, nx))

        hum = np.ones((nz, ny, nx)) * hum_c

        pres = np.zeros((nz, ny, nx))
        rho = np.zeros((nz, ny, nx))

        if wind == 'profile':
            if profile_option == 'manual':
                # specify all vectors here 

                # Santiaguito, 30 km domain height (june)
                vx_prof = [1.12674, 2.00216, 2.82972, 2.70166, 2.27015, 2.05352, 2.08206, 2.0948, 1.99272, 1.6446,
                           1.12849, 0.556849, \
                           -0.257199, -0.890537, -0.827308, -0.123938, 0.835814, 1.87096, 2.94276, 3.94484, 4.79666,
                           5.67554, \
                           6.61577, 7.55601, 8.55538, 9.9654, 11.3754, 12.652, 13.8037, 14.9554, 15.0]

                vy_prof = [4.44089e-16, 0.242129, 0.62303, 1.05524, 0.997209, 1.12193, 1.25141, 1.43537, 1.5054,
                           1.55805, 1.77896, 2.16407, \
                           2.44892, 2.73468, 3.38121, 4.57879, 5.47152, 5.86161, 5.81361, 6.31589, 8.00379, 9.6081,
                           11.0229, 12.4376, 13.9674, \
                           16.2962, 18.625, 20.706, 22.5551, 24.4042, 25]

                # Merapi 30 km domain height (june)
                vx_prof = [1.69227, 3.02981, 1.5749, 1.00054, 1.03213, 1.40301, 1.75443, 2.44883, 2.85412, 2.43804,
                           1.29956, -0.225853, -1.04446, -0.466747, \
                           0.999637, 1.08464, -1.04343, -3.78775, -6.2303, -7.96678, -8.3561, -7.01615, -2.19852,
                           2.61911, 7.0937, 9.77301, 12.4523, \
                           13.5101, 13.3947, 13.2792, 13.0]

                vy_prof = [-9.99201e-16, 1.55086, 2.03708, 1.85102, 1.53091, 1.90721, 2.60883, 3.24388, 4.09768,
                           5.48478, 7.14492, 8.57974, 8.69537, \
                           6.97096, 3.2569, -0.418511, -3.40815, -6.25501, -9.2621, -11.362, -11.7309, -9.7248,
                           -2.94252, 3.83975, 10.0377, \
                           13.1774, 16.3171, 17.6898, 17.784, 17.8782, 17.9]

                # Vesuv 30 km domain height (june)
                vx_prof = [3.60822e-16, 1.49835, 2.26595, 2.44496, 3.45216, 4.68782, 5.35535, 5.95117, 6.64181, 7.41067,
                           8.24212, 9.00803, 9.16641, 8.64838, \
                           6.78534, 4.68359, 3.45014, 2.44425, 1.54294, 0.753117, 0.282863, -0.141108, -0.299743,
                           -0.458378, -0.617013, -0.731629, -0.840933, -0.937443, \
                           -0.995075, -1.05271, -1.0]

                vy_prof = [1.0621, 0.99652, 0.662772, 2.24834, 3.76753, 4.8447, 5.44664, 5.76302, 6.18553, 6.70823,
                           7.10802, 7.36903, 7.80962, 8.48534, 7.91214, 6.96386, \
                           5.44786, 3.74124, 1.92051, 0.102186, -1.70921, -3.36856, -4.15616, -4.94376, -5.73136,
                           -5.9063, -6.00733, -6.17198, -6.52998, -6.88799, 0.00114808]

                # Calbuco 30 km domain height (june)
                vx_prof = [1.35967, 4.21171, 6.9869, 6.72275, 6.71042, 7.43104, 8.25314, 9.02992, 9.61376, 10.1452,
                           10.5767, 11.0551, 11.5352, 11.7558, 11.7557, 11.5271, 11.2846, \
                           10.7281, 10.158, 9.62843, 9.11383, 9.01957, 9.16638, 9.3132, 9.96814, 11.1943, 12.4205,
                           14.7992, 17.2196, 19.64, 20.0]

                vy_prof = [-1.77636e-15, 2.02101, 7.92565, 12.2211, 15.1271, 17.3199, 19.2264, 21.0466, 23.02, 25.2384,
                           27.1537, 28.0781, 27.0447, 25.4666, 24.2314, \
                           23.3287, 22.6229, 22.0181, 21.4177, 21.2814, 21.3172, 21.976, 22.9921, 24.0082, 25.6932,
                           28.1302, 30.5671, 34.2236, 37.9241, 41.6247, 42.0]

                vz_prof = np.zeros((nz))

                # mean global wind december 2017 (increment 1000 m, max = 40 km)
                global_wspd = [0, 5.92657, 6.39148, 7.07537, 7.85909, 8.87582, 10.1315, 11.5014, 12.9244, 14.3285,
                               15.6062, 16.6444, 17.3735, 17.6025, 17.2002, \
                               16.1516, 14.5921, 12.9324, 11.3736, 10.3588, 10.0927, 10.6706, 12.2431, 13.8257, 15.3495,
                               16.9126, 18.4166, 18.9808, 19.2846, \
                               19.581, 19.7778, 20.0218, 20.8583, 21.6635, 22.5171, 23.3494, 24.2819, 25.3286, 26.3529,
                               27.3732, 28]

                vy_prof = np.zeros((nz))

            elif profile_option == 'linear':
                vx_prof = np.linspace(vx_min, vx_max, nz)
                vy_prof = np.linspace(vy_min, vy_max, nz)
                vz_prof = np.linspace(vz_min, vz_max, nz)

        elif wind == 'constant':
            vx_prof = np.ones((nz)) * vx_min
            vy_prof = np.ones((nz)) * vy_min
            vz_prof = np.ones((nz)) * vz_min

        # Variables
        grad_T = 0.5  # K/100m
        M = 0.02896  # molar mass
        R = 8.314  # gas constant
        R_a = 287.1
        R_d = 461.5
        g = 9.8066

        ############################################
        # Guntur wind data
        # vx_volcano = [0,-4.05,-5.03,-6.58,-8.72,-10.85,-10.01,-9.17,-8.3326,-7.49,-6.65]
        # vy_volcano = [0,-4.83,-4.48,-5.4,-7.59,-9.77,-9.79,-9.81,-9.83,-9.85,-9.87]

        # temp_volcano = [15.18 ,4.0,-7.87,-17.42,-24.66,-31.9, -35.98,-40.06,-44.14,-48.22,-52.3] # in deg C

        # Tavurvur wind data
        vx_volcano = [0, -4, -6, -8, -10, -12, -14, -16]
        vy_volcano = [0, 6.928203, 10.392305, 13.856406, 17.320508, 20.784610, 24.248711, 27.712813]

        temp_volcano = [30, 20, 10, 5, 0, -10, -20, -25]

        vx_prof = np.asarray(vx_volcano)
        vy_prof = np.asarray(vy_volcano)
        t_prof = np.asarray(temp_volcano)
        ##########################################       
        # calculate temperature, pressure and density

        for ix in range(nx):
            for iy in range(ny):

                vx[:, iy, ix] = vx_prof
                vy[:, iy, ix] = vy_prof
                vz[:, iy, ix] = vz_prof

                T_max = T_sea - 0.5 * topog[iy, ix] / 100  # ground temperature = 20°C

                for iz in range(nz):
                    # pressure (barometric formula)
                    T[iz, iy, ix] = T_max - grad_T * zlayer[iz] / 100  # dry cooling

                    altitude = topog[iy, ix] + zlayer[iz]

                    # saturation pressure
                    pd = math.exp(-6094.5 * 1 / T[iz, iy, ix] + 21.1 - 0.027 * T[iz, iy, ix] \
                                  + 0.000017 * T[iz, iy, ix] ** 2 + 2.46 * math.log(T[iz, iy, ix]))

                    if 1 - grad_T / 100 * altitude / T_sea <= 0:
                        print('Fall3D: Failed at calculating artificial meteorology.')

                    # barometric formula for linear temperature gradient
                    pres[iz, iy, ix] = p_sea * math.exp(
                        M * g / (R * grad_T / 100) * np.log(1 - grad_T / 100 * altitude / T_sea))

                    # gas constant of wet air
                    R_f = R_a / (1 - hum_c / 100 * pd / pres[iz, iy, ix] * (1 - R_a / R_d))

                    rho[iz, iy, ix] = pres[iz, iy, ix] / (R_f * T[iz, iy, ix])

        vptemp = T * (p_sea / pres) ** 0.286

        # transform to K
        for ix in range(nx):
            for iy in range(ny):
                T[:, iy, ix] = t_prof + 273.15  # in K

        return (vx, vy, T, vptemp, pres, rho, hum)

    def correct_divergence(self):

        # correct horizontal velocities for fixed vertical velocity so that a 
        # zero-divergence wind field is achieved that fulfils the continuity equation
        # the procedure is taken from the CALMET processor

        # TODO: test functionality

        vz = self['vz']
        vx = self['vx']
        vy = self['vy']

        nx = self['nx']
        ny = self['ny']
        nz = self['nz']

        dX1 = self['dX1']
        dX2 = self['dX2']
        Hm1 = self['Hm1']

        dz = self['dz']

        # apply cython function
        vx, vy = CFall3D.correct_divergence(vx, vy, vz, nx, ny, nz, dX1, dX2, Hm1, dz)

        self['vx'] = vx
        self['vy'] = vy

        return self.get_dict()
