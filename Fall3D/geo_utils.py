# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 09:19:39 2018

@author: schae
"""

import numpy as np

   
def AzimuthCalc(p1,p2):
    """
    Calculates the azimuth (angle clock-wise from north) between to p1 to p2
    p=(x,y)
    """
    dx=(p2[0]-p1[0])
    dy=(p2[1]-p1[1])
    if dy==0:
        azimuth=0
    else:
        azimuth = np.arctan((dx)/ (dy)) * 180.0 / np.pi
    
    if ((dy<=0)):
        azimuth+=180
    if ((dy>0) &  (dx<0)):
        azimuth+=360
    return azimuth

def grid2vec(grid,x_space,y_space):
    """
    Transfers geometric data from a 2D-array to a xyz-Vector
    x_space: coordinates along columns
    y_space: coordinates along rows
    """
    #Create xyz-vector
    vec=np.zeros((len(x_space)*len(y_space),3))
    #Assign Coordiantes
    for i in range(len(x_space)):
        vec[i*len(x_space):(i+1)*len(x_space),0]=x_space[i] #x
        vec[i*len(x_space):(i+1)*len(x_space),1]=y_space #y 
        vec[i*len(x_space):(i+1)*len(x_space),2]=grid[:,i] #z
        
    return vec
    
def vec2grid(vec,verbose=False):
    """
    Transfers geometric data from a 2D-array to a xyz-Vector
    x_space: coordinates along columns
    y_space: coordinates along rows
    """
    #Create xyz-vector
    #Check along which axis was dominated
    ix=np.where(vec[:,0]==vec[0,0])[0]
    iy=np.where(vec[:,1]==vec[0,1])[0]
                
    
    if np.abs(ix[0]-ix[1])==1:
        if verbose: print('grid was row dominant')
        x_space=np.sort(np.unique(vec[:,0]),order='ascending')
        y_space=vec[iy[0]:iy[1],1]
    elif np.abs(iy[0]-iy[1])==1:
        y_space=np.sort(np.unique(vec[:,1]),order='descending')
        x_space=vec[ix[0]:ix[1],0]
    
    vec=np.zeros((len(x_space)*len(y_space),3))
    #Assign Coordiantes
    for i in range(len(x_space)):
        vec[i*len(x_space):(i+1)*len(x_space),0]=x_space[i] #x
        vec[i*len(x_space):(i+1)*len(x_space),1]=y_space #y 
        vec[i*len(x_space):(i+1)*len(x_space),2]=grid[:,i] #z
        
    return vec

