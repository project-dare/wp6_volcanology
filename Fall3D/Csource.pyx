# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 10:01:11 2018

@author: Regina
"""
import numpy as np
import scipy as sp
cimport numpy as np
import math
cimport cython
#from cython.parallel import prange


#  python setup.py build_ext --inplace

def percentage(source,fc,diam):
    
    # Computes aggregation according to a percentage.
    # All classes below diam_aggr are reduced with
    # a fixed user-defined percentage
    
    cdef float fca = 0.0           # fraction of mass in the aggregate class
    cdef int iaggr = -1           # index of the first aggregating class
    
    for ic in range(source['npart']-1):
            if(source['diam_aggr'] > diam[ic]*10**-3):   # compare in m
                fca    = fca + source['perc']*fc[ic]
                fc[ic] = (1.0-source['perc'])*fc[ic]
                
                if iaggr == -1:
                    iaggr = ic
                    
                    
    fc[source['npart']-1] = fca  

    if iaggr == -1:
            print('SetSRC: No aggregated class found. Please choose diameter of aggregates larger.')
    else:
        return fc
        
            
            
            
def cornell(source,fc,diam):
    
    #Computes aggregation according to the Cornell model

    #The aggregate class is made of: 
    #            50% of particles with 3<phi<4
    #            75% of particles with 4<phi<5
    #            90% of particles with phi>5
    
    cdef int iaggr = -1 
    
    for ic in range(source['npart']-1):
        
            # convert diameter to m
            diam = diam*10**-3
            
            if(diam[ic] <= 0.000125 ) and (diam[ic] > 0.0000625):         #3<phi<4
                fc[source['npart']-1] = fc[source['npart']-1] + 0.5*fc[ic]
                fc[ic] = 0.5*fc[ic]
           
                if iaggr == -1:
                    iaggr = ic           
    
            elif(diam[ic] <= 0.0000625) and (diam[ic] >= 0.00003125):     #4<phi<5
                fc[source['npart']-1] = fc[source['npart']-1] + 0.75*fc[ic]
                fc[ic] = 0.25*fc[ic]
           
                if iaggr == -1:
                    iaggr = ic     
                
                
            elif (diam[ic] < 0.00003125):                                   #phi>5
                fc[source['npart']-1] = fc[source['npart']-1] + 0.9*fc[ic]
                fc[ic] = 0.1*fc[ic]
           
                if iaggr == -1:
                    iaggr = ic        
           
            
    if iaggr == -1:
        print('SetSRC: No aggregated class found according to the Cornell aggregation model.')  
    else: 
        return fc 


def costa(source,fc,diam):
    # Computed later depending on the plume model (time dependent)
    
    #TODO: not implemented yet
    
    for ic in range(source['npart']-1):
            if (source['diam_aggr'] > diam[ic]):
           
                if iaggr == -1:
                    iaggr = ic  
       

    if iaggr == -1:
        print('SetSRC: No aggregated class found. Please choose diameter of aggregates bigger.')
          
    print('Costa aggregation model not implemented yet.')
    
    
    
def get_par_ABL(u,v,nx,ny,z1,z0,Tz1,Tz0,Thz0,Thz1):
    
    cdef double K = 0.4       # Karman constant
    cdef double g = 9.8066
    
    # Monin Obukhov length
    cdef double  [:,:] rmonin = np.zeros((ny,nx))
        
    #friction velocity
    cdef double [:,:] ustar  = np.zeros((ny,nx))
    
    # calculate friction velocity and Monin Obhukov length
    for ix in range(nx):
             for iy in range(ny):
                 umod = max(math.sqrt(u[iy,ix]*u[iy,ix]+ v[iy,ix]*v[iy,ix]),math.e**(-4))
                 # bulk Richardson number
                 Ri = g*(z1-z0)*(Thz1[iy,ix]-Thz0[iy,ix])/(umod*umod*0.5*(Tz0[iy,ix]+Tz1[iy,ix]))
                 
                 # stable/unstable ABL
                  
                    
                 if Ri >= 0:          # stable
                     Gm = 1 + 4.7*Ri
                     Gm = 1/(Gm*Gm)
                     Gh = Gm
                     
                 else:                # unstable
                     Gm = math.log(z1/z0)
                     Gm = 1 + (70*K*K*math.sqrt(abs(Ri)*z1/z0))/(Gm*Gm)
                     Gh = 1 + (50*K*K*math.sqrt(abs(Ri)*z1/z0))/(Gm*Gm)
                     Gm = 1 - ((9.4*Ri)/Gm)
                     Gh = 1 - ((9.4*Ri)/Gh)  
                     
                 # friction velocity    
                 ustar1 = math.log(z1/z0)
                 ustar1 = K*umod*math.sqrt(Gm)/ustar1 
                 
                 # thstar (Pr=1 assumed)
                 thstar = math.log(z1/z0)
                 thstar1 = K*K*umod*(Thz1[iy,ix]-Thz0[iy,ix])*Gh/(ustar1*thstar*thstar)
                 thstar = max(thstar1,math.e**(-4))
                 
                 # Monin Obukhow length
                 rmonin1 = ustar1*ustar1*0.5*(Thz0[iy,ix]+Thz1[iy,ix])/(K*g*thstar)
                
                 # write to 2D array
                 rmonin[iy,ix] = rmonin1
                 ustar[iy,ix]  = ustar1
                 
    
    return (rmonin,ustar)
    
    

def thres_ust(source,npar_dep):
    
    cdef int rho_water = 1000    # kg/m^3
    cdef double porosity = 0.4
    cdef double g_cm = 981
    cdef double g = 9.8066
    cdef double gamma = 3*10**-4 # kg/s^2
    
    cdef double [:,:,:] rho = source['rho']
    cdef double [:] diam = source['tgsd'][:,0] # in mm
    cdef double [:] rhop = source['tgsd'][:,1]
    
    cdef int ny = source['ny']
    cdef int nx = source['nx']              
          
    # Computes the threshold friction velocity of a dry bare surface (solvedust.f90)
    cdef double [:,:,:] tust = np.zeros((ny,nx,npar_dep))
    
    
    
    if source['emission_scheme'] == 'WESTPHAL':
        
        for iy in range(ny):
                 for ix in range(nx):
                     for ic in range(npar_dep):
                         tust[iy,ix,ic] = source['tust_cte']
                         
    elif source['emission_scheme'] == 'MARTICORENA':
                                       
             for ix in range(nx):
                 
                 for iy in range(ny):
                     
                     for ic in range(npar_dep):
                         
                         d_cm    = diam[ic]/10          # in cm
                         rhoa_cm = rho[0,iy,ix]/1000    # g/cm^3 
                         rhop_cm = rhop[ic]/1000
                          
                         Re      = 1331*(d_cm**1.56) + 0.38
                         K       = math.sqrt((rhop_cm*g_cm*d_cm/rhoa_cm)* (1.0 + (0.006/(rhop_cm*g_cm*(d_cm**2.5)))))
                          
                         if Re < 0.03:
                             tust[iy,ix,ic] = (0.129*K)/math.sqrt((1.928*(0.03**0.092))-1.0)
                         elif Re <= 10:
                             tust[iy,ix,ic] = (0.129*K)/math.sqrt( (1.928*(Re**0.092))-1.0)
                         else:
                             tust[iy,ix,ic] = 0.129*K*(1.0-0.0858*math.exp(-0.0617*(Re-10.0)))
                              
                         tust[iy,ix,ic] = tust[iy,ix,ic]/100        # in m/s                         
                         
                         
    elif source['emission_scheme'] == 'SHAO':

        for ix in range(nx):
            
                 for iy in range(ny):
                     
                     for ic in range(npar_dep):
                          
                         d_m = diam[ic]*10**-3      # in m
                         tust[iy,ix,ic] = math.sqrt(0.0123*((rhop[ic]*g*d_m/rho[0,iy,ix]) + (gamma/(rho[0,iy,ix]*d_m))))
                                   
        
    return tust    
    
    
def moist_corr(source,gload,cload,tust,npar_dep):
    
    #TODO: Implement as matrix from weather data
    cdef double smoi = 0.05   # soil moisture
    
    cdef int w_threshold = 0  # assumed threshold value of 0% for ash
    cdef int ny = source['ny']
    cdef int nx = source['nx'] 
    cdef double porosity = 0.4
    cdef int rho_water = 1000 # kg/m^3
    
    cdef double [:] diam = source['tgsd'][:,0] # in mm
    cdef double [:] rhop = source['tgsd'][:,1]
                           
                          
    for ix in range(nx):
        for iy in range(ny):
                      
            if gload[iy,ix] > source['mindep']:
                         
                rho_part = 0
                         
                for ic in range(npar_dep):
                    # mean density of deposited material
                    rho_part = rho_part + rhop[ic]*cload[iy,ix,ic]/gload[iy,ix]
                              
            else:
                # default value
                rho_part = 2650
                    
            wv = smoi
                 
            rho_bulk=(1-porosity)*rho_part       # Soil Bulk Density
                      
            # exclude water bodies
            if wv <= 0.9:
                wg = 100*wv*rho_water/rho_bulk     # Gravimetric soil moisture
                    
            else:
                wg = 0    

                
            # compute factor correction
            if wg <= w_threshold:
                f_w = 1
                          
            else:
                f_w = math.sqrt(1.0 + 1.21*((wg-w_threshold)**(0.68)))
                          
                          
            for ic in range(npar_dep):
                tust[iy,ix,ic] = f_w*tust[iy,ix,ic]
                    
    return tust                
    
    
def calc_emis(source,tust,gload,cload,npar_dep,nc):
    # computes the source term depending on the emission scheme (in kg m-2 s-1 )
    
    cdef int ny = source['ny']
    cdef int nx = source['nx']
    cdef double K = 5.4*10**-4  # m-1
    cdef double alfa = 0.1      # radial entrainment coefficient
    cdef double g = 9.8066
    
    #TODO: Implement as matrix from weather data
    cdef double smoi = 0.05   # soil moisture
    
    cdef double [:,:,:] emis = np.zeros((ny,nx,nc))           # emission
    cdef double [:,:,:] emis_mas = np.zeros((ny,nx,nc))       # total emitted mass
    
    cdef double [:,:] ustar = source['ustar']
    cdef double [:,:,:] rho = source['rho']
    cdef double [:] diam = source['tgsd'][:,0] # in mm
    cdef double [:] F_H = np.zeros((nc))
    cdef double [:] p = np.zeros((nc))
    
    
    
    if source['emission_scheme'] == 'WESTPHAL':
        
        for iy in range(ny):
                 for ix in range(nx):
                     
                     wv = smoi
                     if wv <= 0.9:
                         if gload[iy,ix] > source['mindep']:
                             
                             for ic in range(nc):
                                 ic = npar_dep-nc+ic
                                 
                                 if cload[iy,ix,ic] > emis_mas[iy,ix,ic]:
                                     if ustar[iy,ix] >= tust[iy,ix,ic]:
                                         
                                         emis[iy,ix,ic]= 10**-5*(ustar[iy,ix]**4.0)            #  kg m-2 s-1
                                          



    if source['emission_scheme'] == 'MARTICORENA':
        
        for iy in range(ny):
                 for ix in range(nx):
                     
                     wv = smoi
                     
                     if wv <= 0.9:
                         if gload[iy,ix] > source['mindep']:
                             
                             # loop over all particle classes
                             for ic in range(nc):
                                 ic = npar_dep-nc+ic
                                 
                                 if cload[iy,ix,ic] > emis_mas[iy,ix,ic]:
                                     if ustar[iy,ix] >= tust[iy,ix,ic]:
                                                                                   
                                         emis[iy,ix,ic] = K*rho[0,iy,ix]*ustar[iy,ix]* (ustar[iy,ix]*ustar[iy,ix]-tust[iy,ix,ic]*tust[iy,ix,ic])/g    #   kg m-2 s-1
                                          



    if source['emission_scheme'] == 'SHAO':
        
        
        for iy in range(ny):
            for ix in range(nx):
                     
                wv = smoi
                     
                if wv <= 0.9:
                    if gload[iy,ix] > source['mindep']:
                             
                        # Horizontal flux of saltating particles (all deposit classes)
                             
                        for ic in range(npar_dep):
                                 
                            # Weight of each saltating class
                            if ustar[iy,ix] >= tust[iy,ix,ic]:
                                     
                                F_H[ic] = rho[0,iy,ix]*ustar[iy,ix]*ustar[iy,ix]*ustar[iy,ix]/g * ( 1 - (tust[iy,ix,ic]*tust[iy,ix,ic]/(ustar[iy,ix]*ustar[iy,ix])))
                                     
                            else:
                                F_H[ic] = 0
                                 
                            # weight of class relative to total ground load  
                            p[ic] = cload[iy,ix,ic]/gload[iy,ix]
    
                        # vertical flux
                        for ic in range(nc):
                            ic = npar_dep-nc+ic
                                 
                            if cload[iy,ix,ic] > emis_mas[iy,ix,ic]:
                                     
                                for ic2 in range(ic):
                                          
                                    alfa = (0.6 * math.log10(diam[ic2]) + 1.6)* math.exp(-140*diam[ic])         # d in mm
                                    
                                    emis[iy,ix,ic] = emis[iy,ix,ic] + alfa * p[ic2] * p[ic]*F_H[ic2]/(tust[iy,ix,ic] * tust[iy,ix,ic])
                                           

    # correct emission by a factor
    for ix in range(nx):
        for iy in range(ny):
            for ic in range(nc):
                emis[iy,ix,ic] = emis[iy,ix,ic]*source['emission_factor'] # kg s-1 m-2
        
    # total emitted mass
    for iy in range(ny):
        for ix in range(nx):
                  
            for ic in range(nc):
                      
                # monthly mean data
                if source['data_type'] == 'MONTHLY_MEAN':
                         
                    emis_step = source['runtime']*3600*emis[iy,ix,ic]  # kg/m^2
                    
                #TODO: Implementierung für wechselnde Wetterbedingungen
                     
                if cload[iy,ix,ic] < emis_mas[iy,ix,ic] + emis_step:
                          
                    emis_step = cload[iy,ix,ic] - emis_mas[iy,ix,ic]
                    emis[iy,ix,ic] = emis_step/(source['runtime']*3600)
                    emis_mas[iy,ix,ic] = cload[iy,ix,ic]
                          
                else:
                    emis_mas[iy,ix,ic] = emis_mas[iy,ix,ic] + emis_step
        
    # scale source term
        
    # compute scaling factors for Lon-Lat data (see Toon et al. 1988)
    cdef double [:,:] Hm1 = np.zeros((ny,nx))
    cdef double Rearth = 6356*10**3               # earth´s radius (m)
        
    dX1 = Rearth*(source['dlon']*math.pi/180.0)  # Scaled (dx,dy)
    dX2 = Rearth*(source['dlat']*math.pi/180.0)
        
    for j in range(ny):
        lat   = source['latmin'] + j*source['dlat']        # index 0 is also southernmost point
        colat = (90-lat)*math.pi/180                       # colatitude in rad
        Hm1[j,:] = math.sin(colat)  
          
    for ix in range(nx):
        for iy in range(ny):
            sup0 = Hm1[iy,ix]*dX1*dX2
                
            for ic in range(nc):
                emis[iy,ix,ic] = emis[iy,ix,ic]*sup0  # kg/s
                
    return (emis,emis_mas)                
        
        
        


                 
                    
# for interpolation

def interpolate1D(data_matrix, data_height_matrix, zlayer,f):
    #print(f)
    
    a = np.shape(data_height_matrix)
    #print(a)
    #type(data_matrix)
    #type(data_height_matrix)
    
    
    if (len(a)) == 4:
        
        returnval = np.zeros((int(a[0]),len(zlayer),int(a[2]),int(a[3])))
        
        if f != 'p_lvls':
            for it in range(np.shape(data_matrix)[0]): # loop over nmet
                returnval[it,:,:,:] = interpolate1D_c(data_matrix[it,:,:,:], data_height_matrix[it,:,:,:],zlayer)
#                
#                if f == 'qv':
#                    print(it)
        else: 
            for it in range(np.shape(data_height_matrix)[0]): # loop over nmet
                
                returnval[it,:,:,:] = interpolate1D_c(data_matrix[:,:,:], data_height_matrix[it,:,:,:],zlayer)
                
    else:
        
        returnval = interpolate1D_c(data_matrix,data_height_matrix,zlayer)
    
    #print('%s finished wit max %s' % (f,np.amax(np.asarray(returnval))))    
    return np.asarray(returnval)    
        
        
        
        
    
cdef double [:,:,:] interpolate1D_c(double [:,:,:] data, double [:,:,:] datapoints, long [:] newpoints):
    
    cdef double [:,:,:] interpol = np.zeros((len(newpoints),np.shape(data)[1],np.shape(data)[2]))
    
    cdef int ix,iy,i,j
    
    for ix in range(np.shape(data)[2]):
        for iy in range(np.shape(data)[1]):
            
            interp = np.zeros((len(newpoints)))
            
            values = data[:,iy,ix]
            pts    = datapoints[0:len(values),iy,ix]
            
            for i in range(len(newpoints)):
                for j in range(len(pts)-1):
                    
                    if newpoints[i] == pts[j]:
                        interp[i] = values[j]
                        
                    elif pts[j] <= newpoints[i] and pts[j+1] >= newpoints[i]:
                        dx1 = newpoints[i] - pts[j]
                        dx2 = pts[j+1] - newpoints[i]
                        interp[i] = (values[j]*dx2 + values[j+1]*dx1)/(dx1 + dx2) 
                        
                    # extrapolation    
                    elif (newpoints[i] >= pts[len(pts)-1]):
                        interp[i] = values[len(values)-1]
                    elif (newpoints[i] <= pts[0]):
                        interp[i] = values[0]    
            
            for iz in range(len(newpoints)):
                interpol[iz,iy,ix] = interp[iz]
            
    return interpol                           
                            
        
        
        
        
        
        

    