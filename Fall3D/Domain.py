# -*- coding: utf-8 -*-
"""
Created on Wed May 27 14:34:04 2015

@author: aschaefer
"""
from os.path import join

import numpy as np
import pandas as pd

from scipy import ndimage
from scipy.interpolate import griddata
from scipy.interpolate import interp1d
from netCDF4 import Dataset

from Fall3D import GeoCompile as gc

try:
    from Fall3D import CFall3D as C
except ModuleNotFoundError:
    from Fall3D import Csource as C      



def createDomain(config):
    
    d=Domain(config['spatial_list'],args=config)
    
    #If Vs30 is needed, add it to the domain
    if 'AddVs30' in config:
        if config['AddVs30']:
            d.AddVs30
        


class Domain(list):
    """
    Spatial domain object, which stores geospatial information about a specific 
    area. Simply starting with elevation data, but extendable with landcover,
    vs30, population, exposure, etc.

    """
    def __init__(self,spatial_list,**args):
        """
        Initialize Seismic Source by importing shapefile with respective data

        :type  spatial_list: list [[longitude min, longitude max, latitude min, latitude max, resolution (km), name, raster-fid (optional)]]
        :param spatial_list: List of coordinate containers (lists) for each subdomain
        
        """
        self.datajump=False
        self.NumDomain=0
        self.AddDomain(spatial_list,**args)
        
    def AddDomain(self,spatial_list,kind='DEM',DEMfolder='D:\\TsupyDEM\\',**args): 
        """
        Adds a group of additional domains references in spatial_list
        :type  spatial_list: list [[longitude min, longitude max, latitude min, latitude max, resolution (km), name, raster-fid (optional)]]
        :param spatial_list: List of coordinate containers (lists) for each subdomain
        
        :type  resize: bool 
        :param resize: binary identificator if resolution change is enforced
        
        :type  DEMfolder: str
        :param DEMfolder: local address to the folder which contains digital elevation models
        
        :type  kind: str
        :param kind: defines name under which the domain data is stored
        """
        if type(spatial_list[0])!=list:
            numDomain=1
            spatial_list=[spatial_list]
        else:
            numDomain=len(spatial_list)

        for i in range(numDomain):
            self._AddDomain(spatial_list[i],kind,DEMfolder,**args)
            self.NumDomain+=1
            
    def _AddDomain(self,spatial_list,kind='DEM',DEMfolder='D:\\TsupyDEM\\',**args):
        """
        Add Spatial region to the Domain container.

        :type  shapefile_list: list 
        :param shapefile_list: List of shapefile which contain the source zone data

        """
        print('Importing Grid')
            
        #Import spatial extent
        longitude=spatial_list[0:2]
        latitude=spatial_list[2:4]
        
        if len(spatial_list)<=6:
            resolution=spatial_list[4]
            DEMmeta=pd.read_csv(DEMfolder + 'meta.csv',index_col=None)
            rindex=np.where((DEMmeta['resolution'].values<=resolution))[0]
            if len(rindex)>0:
                dist=np.abs(resolution-DEMmeta['resolution'].values[rindex])
                target=np.min(rindex[np.min(np.where(dist==np.min(dist))[0])])
            else:
                dist=np.abs(resolution-DEMmeta['resolution'].values)
                        
                target=np.min(np.where(dist==np.min(dist))[0])
    
            if 'resize' in args:
                resize=args['resize']
                
            else:
                if np.min(dist)>0.1:
                    resize=True
                else:
                    resize=False
            
            #print('Given resolution ' + str(resolution) + 'km , applied ' \
            #      + str(DEMmeta['resolution'][target]))
                
            if type(DEMmeta['Folder'][target])==type('test'):
                fid=DEMfolder + DEMmeta['Folder'][target]+DEMmeta['Filename'][target]
            else:
                fid=DEMfolder + DEMmeta['Filename'][target]            
            if DEMmeta['Filetype'].iloc[target]=='tif':
                Domain=gc.ImportGeoTiff(fid,kind=kind)
                Domain['Resolution0']=spatial_list[4]
            elif DEMmeta['Filetype'].iloc[target]=='csv':
                DEMlist=pd.DataFrame.from_csv(fid,index_col=None)
                in_list=np.zeros(len(DEMlist))
                        
                for j in range(len(in_list)):
                    #North-West Point inside                    
                    x,y=longitude[0],latitude[0]
                            
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    #North-East Point inside
                    x,y=longitude[1],latitude[0]
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    #South-West Point inside
                    x,y=longitude[0],latitude[1]
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    #South-East Point inside
                    x,y=longitude[1],latitude[1]
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    x0=np.mean([DEMlist['minlong'][j],DEMlist['maxlong'][j]])
                    y0=np.mean([DEMlist['minlat'][j],DEMlist['maxlat'][j]])
                                                
                    if ((x0>=np.min(longitude)) & (x0<=np.max(longitude)) &\
                        (y0>=np.min(latitude)) & (y0<=np.max(latitude))):
                        in_list[j]=1
                        
                index=np.where(in_list==1)[0]
                if len(index)==1:
                    print('Single Tile imported')
                    fid=DEMfolder + DEMmeta['Folder'][target] +  DEMlist['fid'][index].values[0] + '.tif'
                    Domain=gc.ImportGeoTiff(fid,kind=kind)
                    Domain['Resolution0']=spatial_list[4]

                if len(index)>=2:
                    #Import all tiles individually                    
                    Domain_list=list()
                    coords=np.zeros((len(index),2))
                    for k in range(len(index)):
                        x0=np.mean([DEMlist['minlong'][index[k]],DEMlist['maxlong'][index[k]]])
                        y0=np.mean([DEMlist['minlat'][index[k]],DEMlist['maxlat'][index[k]]])
                        coords[k,:]=x0,y0
                        fid=DEMfolder + DEMmeta['Folder'][target] +  DEMlist['fid'][index[k]] + '.tif'
                        Domain_list.append(gc.ImportGeoTiff(fid,kind=kind))
        
                    Domain=dict()
                    Domain['Longitude']=np.max(DEMlist['maxlong'][index]),np.min(DEMlist['minlong'][index])
                    Domain['Latitude']=np.max(DEMlist['maxlat'][index]),np.min(DEMlist['minlat'][index])
                    Domain['Spacing']=Domain_list[0]['Spacing']
                    Domain['grids']=dict()
                    Domain['Resolution0']=spatial_list[4]
                    #maximum extent
                    x_space=np.unique(coords[:,0])
                    y_space=np.unique(coords[:,1])
                    dim=[len(Domain_list[0]['grids'][kind][0,:]),len(Domain_list[0]['grids'][kind][:,0])]

                    Domain['grids'][kind]=np.zeros((len(y_space)*dim[0],len(x_space)*dim[1]))
                    index_grid=np.zeros((len(y_space),len(x_space)))
                                                
                    y_space=y_space[np.argsort(y_space)[::-1]]
                    
                    for k in range(len(y_space)):
                        for l in range(len(x_space)):
                            ix=np.where((coords[:,0]==x_space[l]) & (coords[:,1]==y_space[k]))[0]
                            if len(ix)>0:                            
                                index_grid[k,l]=ix
                                Domain['grids'][kind][int(dim[0]*k):int(dim[0]*(k+1)),int(dim[1]*l):int(dim[1]*(l+1))]=Domain_list[int(ix)]['grids'][kind]
        
            if ((resize==True) & (DEMmeta['resolution'][target]!=resolution)):
                ratio=DEMmeta['resolution'][target]/resolution
            
                # print('Resizing at ratio ' + str(ratio))
                Domain['grids'][kind]=ndimage.interpolation.zoom(Domain['grids'][kind],ratio)
                Domain['Spacing']=(np.max(Domain['Longitude'])-np.min(Domain['Longitude']))/len(Domain['grids'][kind][0,:]), \
                    (np.max(Domain['Latitude'])-np.min(Domain['Latitude']))/len(Domain['grids'][kind][:,0])                  

        elif len(spatial_list)>6:
            resolution=spatial_list[4]

            if 'tif' in spatial_list[6][-3::]:
                Domain=gc.ImportGeoTiff(spatial_list[6],kind=kind)
                
                Domain['Resolution0']=spatial_list[4]
            else:
                DEMlist=pd.DataFrame.from_csv(spatial_list[6]+'\\meta.csv',index_col=None)
                in_list=np.zeros(len(DEMlist))
                        
                for j in range(len(in_list)):
                    #North-West Point inside                    
                    x,y=longitude[0],latitude[0]
                            
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    #North-East Point inside
                    x,y=longitude[1],latitude[0]
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    #South-West Point inside
                    x,y=longitude[0],latitude[1]
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    #South-East Point inside
                    x,y=longitude[1],latitude[1]
                    if (x>DEMlist['minlong'][j]) & (x<DEMlist['maxlong'][j]) \
                        & (y<DEMlist['maxlat'][j]) & (y>DEMlist['minlat'][j]):
                        in_list[j]=1
                    x0=np.mean([DEMlist['minlong'][j],DEMlist['maxlong'][j]])
                    y0=np.mean([DEMlist['minlat'][j],DEMlist['maxlat'][j]])
                                                
                    if ((x0>=np.min(longitude)) & (x0<=np.max(longitude)) &\
                        (y0>=np.min(latitude)) & (y0<=np.max(latitude))):
                        in_list[j]=1
                        
                index=np.where(in_list==1)[0]
                if len(index)==1:
                    print('Single Tile imported')
                    fid=spatial_list[6] + '\\' +  DEMlist['fid'][index].values[0] + '.tif'
                    Domain=gc.ImportGeoTiff(fid,kind=kind)
                    Domain['Resolution0']=spatial_list[4]
                if len(index)>=2:
                    #Import all tiles individually                    
                    Domain_list=list()
                    coords=np.zeros((len(index),2))
                    for k in range(len(index)):
                        x0=np.mean([DEMlist['minlong'][index[k]],DEMlist['maxlong'][index[k]]])
                        y0=np.mean([DEMlist['minlat'][index[k]],DEMlist['maxlat'][index[k]]])
                        coords[k,:]=x0,y0
                        fid=spatial_list[6]+'\\' +  DEMlist['fid'][index[k]] + '.tif'
                        Domain_list.append(gc.ImportGeoTiff(fid,kind=kind))
        
                    Domain=dict()
                    Domain['Longitude']=np.max(DEMlist['maxlong'][index]),np.min(DEMlist['minlong'][index])
                    Domain['Latitude']=np.max(DEMlist['maxlat'][index]),np.min(DEMlist['minlat'][index])
                    Domain['Spacing']=Domain_list[0]['Spacing']
                    Domain['grids']=dict()
                    Domain['Resolution0']=spatial_list[4]
                    #maximum extent
                    x_space=np.unique(coords[:,0])
                    y_space=np.unique(coords[:,1])
                    dim=[len(Domain_list[0]['grids'][kind][0,:]),len(Domain_list[0]['grids'][kind][:,0])]
                    Domain['grids'][kind]=np.zeros((len(y_space)*dim[0],len(x_space)*dim[1]))
                    index_grid=np.zeros((len(y_space),len(x_space)))
                                                
                    y_space=y_space[np.argsort(y_space)[::-1]]
                    
                    for k in range(len(y_space)):
                        for l in range(len(x_space)):
                            ix=np.where((coords[:,0]==x_space[l]) & (coords[:,1]==y_space[k]))[0]
                            if len(ix)>0:                            
                                index_grid[k,l]=ix
                                Domain['grids'][kind][int(dim[0]*k):int(dim[0]*(k+1)),int(dim[1]*l):int(dim[1]*(l+1))]=Domain_list[int(ix)]['grids'][kind]     
                

            dist=np.abs(spatial_list[4]-Domain['Spacing'][0]*100)
            if 'resize' in args:
                resize=args['resize']
            else:
                if np.min(dist)>0.1:
                    resize=True
                else:
                    resize=False

            if ((resize==True) & (Domain['Spacing'][0]*100!=resolution)):
                ratio=Domain['Spacing'][0]*100/resolution
    
                print('Resizing at ratio ' + str(ratio))
                Domain['grids'][kind]=ndimage.interpolation.zoom(Domain['grids'][kind],ratio)
                Domain['Spacing']=(np.max(Domain['Longitude'])-np.min(Domain['Longitude']))/len(Domain['grids'][kind][0,:]), \
                                  (np.max(Domain['Latitude'])-np.min(Domain['Latitude']))/len(Domain['grids'][kind][:,0])                  

        if 'Domain' in locals():
            resolution=np.round(float(Domain['Spacing'][0]*100.0),decimals=8)
                #Define if there's a datejump or not
                
                #Crop selected Boundaries from Model
            numDomains=len(self)
            dx,dy=Domain['Spacing']
            x_space=np.arange(Domain['Longitude'][1]+dx/2,Domain['Longitude'][0]-dx/2,dx)
            y_space=np.arange(Domain['Latitude'][1]+dy/2,Domain['Latitude'][0]-dy/2,dy)
            domain_id=numDomains
            Domain['ID']=domain_id
            Domain['resolution']=resolution
            Domain['Name']=spatial_list[5]
            Domain['x_space']=x_space
            Domain['y_space']=y_space
            if longitude[0]>longitude[1]:
                Domain['datejump']=True
    
            else:
                Domain['datejump']=False
            
            
            Domain=GridCrop(Domain,longitude,latitude,Domain['datejump'],kind)
            self.append(Domain)
            self.UpdateGridLinks()
            if self[domain_id]['datejump']==True:
                self.SetDatejump(True)
        else:
            print(spatial_list[5] + ' does not exist in DEM repository')
            #Assign DomainNumber
        
#        print(self.data[domain_id]['x_space'])
        
#            if kind=='DEM':
#                Domain['WaterSurface']=np.zeros((len(Domain['y_space']),len(Domain['x_space'])))
#                Domain['SeaFloor']=np.zeros((len(Domain['y_space']),len(Domain['x_space'])))
            
  
    def SetDatejump(self,datejump):
        """
        Change coordinate system in case the specified domain stretches beyond
        the Pacific datajump

        :type  datejump: bool 
        :param datejump: binary identificator whether datejump is present or not

        """
        numDomains=len(self)
        self.datajump=datejump
        for i in range(numDomains):
            if ((self[i]['datejump']==True) & (np.max(self[i]['x_space'])<180)):
                x_space=self[i]['x_space']
                x_space[np.where(x_space<0)]=360+self[i]['Spacing'][0]+x_space[np.where(x_space<0)]
                self[i]['x_space']=x_space
    
    def UpdateGridLinks(self):
        #Link Bathymetric Data
        num_Domains=len(self)
        #Build parameter array of resolution and coordinate window
        res_list=np.zeros((num_Domains,5))
        for i in range(num_Domains):
            res_list[i,0]=self[i]['resolution']
            res_list[i,1]=np.min(self[i]['x_space'])
            res_list[i,2]=np.max(self[i]['x_space'])
            res_list[i,3]=np.min(self[i]['y_space'])
            res_list[i,4]=np.max(self[i]['y_space'])
            self[i]['Parent']=[]
            self[i]['Child']=[]
        #Build a sorted and unique list of all resolutions in the domain space (ascending order, e.g. [0.5, 1, 5, 25])
        resu_list=np.unique(res_list[::,0])
        resu_list=np.sort(resu_list)
        #Build linking grids
        for i in range(num_Domains):
            if self[i]['resolution']==np.max(res_list[::,0]):
                self[i]['Parent']=-1
            else:
                if ((res_list[i,0]!=np.max(res_list[::,0])) & (res_list[i,0]!=np.min(res_list[::,0]))):
                    #Find current resolution in resolution list               
                    resi=np.where(resu_list==res_list[i,0])[0]
                    p_res=resu_list[resi+1] #Find Parent resolution 
                    #Domain is both parent and child
                    for j in range(num_Domains):
                        if ((res_list[j,0]==p_res) & (i!=j) & 
                        (res_list[i,1]>res_list[j,1]) & (res_list[i,2]<res_list[j,2]) & 
                        (res_list[i,3]>res_list[j,3]) & (res_list[i,4]<res_list[j,4])):
                            #Find Parent Domain
                           self[i]['Parent']=j
                           self[j]['Child'].append(i)
                if ((res_list[i,0]!=np.max(res_list[::,0])) & (int(res_list[i,0])==int(np.min(res_list[::,0])))):
                    #Find current resolution in resolution list
                    resi=np.where(resu_list==res_list[i,0])[0]
                    p_res=resu_list[resi+1] #Find Parent resolution 
                    #Domain is just a child
                    for j in range(num_Domains):
                        if ((res_list[j,0]==p_res) & (i!=j) & 
                        (res_list[i,1]>res_list[j,1]) & (res_list[i,2]<res_list[j,2]) & 
                        (res_list[i,3]>res_list[j,3]) & (res_list[i,4]<res_list[j,4])):
                           self[i]['Parent']=j
                           self[j]['Child'].append(i)
    
        print('Spatial Import Complete')
        
        for i in range(num_Domains):
            self[i]['ratio']=[]
            #Search for parent domains with children
            if len(self[i]['Child'])>0:
                x_parent=self[i]['x_space']
                y_parent=self[i]['y_space']
                pnx=len(x_parent)
                pgrid=np.ones((len(self[i]['y_space']),len(self[i]['x_space'])))*(-1)
                for j in range(len(self[i]['Child'])):
                    c_id=self[i]['Child'][j]
                    #Initialize Linking-Grid
                    cgrid=np.ones((len(self[c_id]['y_space']),len(self[c_id]['x_space'])))*(-1)                
                    x_child=self[c_id]['x_space']
                    y_child=self[c_id]['y_space']
                    cnx=len(x_child) #length in x-direction
    
                    ratio=res_list[i,0]/res_list[c_id,0] #resolution ratio of parent to child
                    self[i]['ratio'].append(ratio)
                    i_x=np.where((x_parent>=np.min(x_child)-0.25) & (x_parent<=np.max(x_child)+0.25))[0] #length indizes of parent in x
                    i_y=np.where((y_parent>=np.min(y_child)-0.25) & (y_parent<=np.max(y_child)+0.25))[0] #length indizes of parent in y
                    for x in range(len(i_x)):
                        dx=np.abs(x_parent[i_x[x]]-x_child)*100*0.95
                        for y in range(len(i_y)):
                            dy=np.abs(y_parent[i_y[y]]-y_child)*100*0.95
    
                            i1=np.where((dx<=res_list[i,0]/2.0))[0]
                            i2=np.where((dy<=res_list[i,0]/2.0))[0]
                            if (len(i1)>0) & (len(i2)>0):
                                #Build Linking Grid of Child
                                cgrid[np.min(i2):np.max(i2)+1,np.min(i1):np.max(i1)+1]=i_x[x]+pnx*i_y[y]
                                #Build Linking Grid of Parent
                                if ((len(i1)>=int(ratio)) & (len(i2)>=int(ratio))):
                                    if ((np.min(i1)>=25) & (np.max(i1)<len(dx)-25) & (np.min(i2)>=25) & (np.max(i2)<len(dy)-25)):
                                        pgrid[i_y[y],i_x[x]]=np.min(i1)+cnx*np.min(i2)
    
                    self[c_id]['cgrid']=cgrid
    #                plt.figure()
    #                plt.imshow(cgrid)
                    del cgrid
                self[i]['pgrid']=pgrid
    #            plt.figure()
    #            plt.imshow(pgrid)
                del pgrid
        

    def AddData(self,domain_id,fid,kind='data',dummy=0.0,**args):
        """
        Adds additional data grids to the specified domain

        """
        #Retrieve spatial information from Domain
        x_space0=self[domain_id]['x_space']
        y_space0=self[domain_id]['y_space']
        res0=self[domain_id]['resolution']
        #Import linking-Grid-Data
        Domain1=gc.ImportGeoTiff(fid,kind=kind)
    
        #reduce Linking-Grid to area of interest
        datejump=False
        if ((Domain1['Longitude'][0]>0) & (Domain1['Longitude'][1]<0)):
                datejump=True
        Domain1=GridCrop(Domain1,(np.min(x_space0),np.max(x_space0)),(np.min(y_space0),np.max(y_space0)),datejump)
        #Retrieve spatial information from landuse data
        x_space1=Domain1['x_space']
        y_space1=Domain1['y_space']
        res1=np.round(float(Domain1['Spacing'][0]*100.0),decimals=8)
        new_grid=np.ones((len(y_space1),len(x_space1)))*dummy
        if 'conversion' in args:
            conversion=args['conversion']
            for i in range(len(conversion[:,0])):
                
                index=np.where(Domain1['Elevation']==conversion[i,0])
                new_grid[index]=conversion[i,1]
        
        Domain1['data']=new_grid
        r=res1/res0
        #Change resolution of landuse to Domain extent
        #Initialize GPU
        export=ndimage.zoom(new_grid,r)

        #Add landuse to Domain-data
        self[domain_id]['grids'][kind]=export
        
        
        
        
        
        
    def interpolation(self,data,datapoints,newpoints):
        # linear interpolation function (1D)
        
        newvalues = np.zeros((len(newpoints)))
        
        for i in range(len(newpoints)):
        
            if (newpoints[i] >= np.amax(datapoints)):
                ind = np.argmax(datapoints)
                newvalues[i] = data[ind]
            
            
            
            
            elif (newpoints[i] <= np.amin(datapoints)):
                ind = np.argmin(datapoints)
                newvalues[i] = data[ind]
           
        
            else:
            
                for j in range(len(datapoints)-1):
                        
                    if newpoints[i] == datapoints[j]:
                        newvalues[i] = data[j]                   
                    
                    
                    elif (datapoints[j] <= newpoints[i]) and (datapoints[j+1] >= newpoints[i]):
                        dx1 = abs(newpoints[i] - datapoints[j])
                        dx2 = abs(datapoints[j+1] - newpoints[i])
                        newvalues[i] = (data[j]*dx2 + data[j+1]*dx1)/(dx1 + dx2)
                    
                    
                        
                    elif (datapoints[j] >= newpoints[i]) and (datapoints[j+1] <= newpoints[i]): 
                        dx1 = abs(newpoints[i] - datapoints[j])
                        dx2 = abs(datapoints[j+1] - newpoints[i])
                        newvalues[i] = (data[j]*dx2 + data[j+1]*dx1)/(dx1 + dx2)
        
#        for i in range(len(newpoints)):
#            for j in range(len(datapoints)-1):
#                
#                if newpoints[i] == datapoints[j]:
#                    newvalues[i] = data[j]
#                    
#                elif datapoints[j] <= newpoints[i] and datapoints[j+1] >= newpoints[i]:
#                    dx1 = newpoints[i] - datapoints[j]
#                    dx2 = datapoints[j+1] - newpoints[i]
#                    newvalues[i] = (data[j]*dx2 + data[j+1]*dx1)/(dx1 + dx2)
#                    
#                # extrapolation    
#                elif (newpoints[i] >= datapoints[len(datapoints)-1]):
#                    newvalues[i] = data[len(data)-1]
#                elif (newpoints[i] <= datapoints[0]):
#                    newvalues[i] = data[0]
                    
                    
        return newvalues            
        
        

    def AddMeteo(self,fod,topography,zlayer,year,month,day,hour,run_end,data_type,path_C,source='NCEP'):
        """
        Adds meteorological data as monthly average including:
        windspeeds (x,y)
        temperature
        humidity
        pressure 
        """
        #print(path_C)
        #sys.path.insert(0, path_C)
        #import CDomain
        
        meteoRaw=dict() 
        
        varNames=['temp','hgt','hum','u','v','w']
        
        var2D  = ['u10' , 'v10' , 't2m']
        
        print('Importing meteorological data. Analysis type:')

        
        if (source=='NCEP' or source=='NCAR') and data_type == 'MONTHLY_MEAN':
            
            print('NCEP/NCAR, monthly mean')
            
            
            # 3D variables
            fileNames=['air','hgt','rhum','uwnd','vwnd','omega']
            for f in range(len(fileNames)):
#                print(Dataset(join(fod,fileNames[f]+'.mon.mean.nc'), mode='r'))
                meteoRaw[varNames[f]] = Dataset(join(fod,fileNames[f]+'.mon.mean.nc'), mode='r')
                
            
            
            # Surface variables
            surfvar = ['air.sig995.mon.mean','uwnd.sig995.mon.mean', 'vwnd.sig995.mon.mean']
            surfname = ['t2m', 'u10', 'v10']
            
            for f in range(len(surfvar)):
                filename = surfvar[f] + '.nc'
                meteoRaw[surfname[f]] = Dataset(join(fod,filename), mode = 'r')           
               
            
            
            start_day = np.zeros((12))
            start_day[0] = 0
            start_day[1] = start_day[0] + 31
            
            if year % 4 == 0:
                start_day[2] = start_day[1] + 29
            else:
                start_day[2] = start_day[1] + 28
            
            start_day[3] = start_day[2] + 31
            start_day[4] = start_day[3] + 30
            start_day[5] = start_day[4] + 31
            start_day[6] = start_day[5] + 30
            start_day[7] = start_day[6] + 31
            start_day[8] = start_day[7] + 31
            start_day[9] = start_day[8] + 30
            start_day[10] = start_day[9] + 31
            start_day[11] = start_day[10] + 30            
            
            
            #pressure levels
            level = meteoRaw['temp'].variables['level'][:]
            
            #coordinates
            lon = meteoRaw['temp'].variables['lon'][:]
            lat = meteoRaw['temp'].variables['lat'][:]
        
            # get index for climate data (monthly means)
            year_diff = year-1948
            data_ind = year_diff*12+month-1
        
        
            # read 4D variables
            for f in range(len(fileNames)):
                meteoRaw[varNames[f]] = meteoRaw[varNames[f]].variables[fileNames[f]][data_ind,:,:,:]

            var2D = ['air','uwnd','vwnd']
            
            # read surface variables
            for f in range(len(surfname)):
                meteoRaw[surfname[f]] = meteoRaw[surfname[f]].variables[var2D[f]][data_ind,:,:]

            
        elif (source=='NCEP' or source=='NCAR') and data_type == 'DAILY':
            # read 6 times daily meteo data
            
            # index of first dataset needed
            start_day = np.zeros((12))
            start_day[0] = 0
            start_day[1] = start_day[0] + 31
            
            if year % 4 == 0:
                start_day[2] = start_day[1] + 29
            else:
                start_day[2] = start_day[1] + 28
            
            start_day[3] = start_day[2] + 31
            start_day[4] = start_day[3] + 30
            start_day[5] = start_day[4] + 31
            start_day[6] = start_day[5] + 30
            start_day[7] = start_day[6] + 31
            start_day[8] = start_day[7] + 31
            start_day[9] = start_day[8] + 30
            start_day[10] = start_day[9] + 31
            start_day[11] = start_day[10] + 30
            
            
            ind_beg = (start_day[int(month)-1] + day)*4
            
            
            if hour >= 6 and hour < 12:
                ind_beg = ind_beg + 1
            elif hour >= 12 and hour < 18:
                ind_beg = ind_beg + 2
            elif hour >= 18 and hour < 24:
                ind_beg = ind_beg + 3
            
            ind_beg = int(ind_beg)
            
            
            ind_end = ind_beg + int((run_end-hour)/6)+2  # add one more data set at the end than necessary
            ind_end = int(ind_end)
            
            
            print('NCEP/NCAR, 6-time daily')            
            
            # 4D variables
            fileNames=['air','hgt','rhum','uwnd','vwnd','omega']
            
            
            # read 4D data to meteoRaw
            for f in range(len(fileNames)):
#                print(Dataset(join(fod,fileNames[f]+'.mon.mean.nc'), mode='r'))
                filename = fileNames[f] + '.' + str(year) + '.nc'
                meteoRaw[varNames[f]] = Dataset(join(fod,filename), mode='r')
                
                
            # Surface variables to meteoRaw
            surfvar = ['air.2m.gauss.','uwnd.10m.gauss.', 'vwnd.10m.gauss.']
            surfname = ['t2m', 'u10', 'v10']
            surfkeyname = ['air', 'uwnd', 'vwnd']
            
            for f in range(len(surfvar)):
                filename = surfvar[f] + str(year) + '.nc'
                meteoRaw[surfname[f]] = Dataset(join(fod,filename), mode = 'r')
                
             
            #pressure levels
            level = meteoRaw['temp'].variables['level'][:]
            
            #coordinates
            lon = meteoRaw['temp'].variables['lon'][:]
            lat = meteoRaw['temp'].variables['lat'][:]
                
            # read 4D variables
            for f in range(len(fileNames)):
                meteoRaw[varNames[f]] = meteoRaw[varNames[f]].variables[fileNames[f]][:]
            # read surface variables
            for f in range(len(surfvar)):
                meteoRaw[surfname[f]] = meteoRaw[surfname[f]].variables[surfkeyname[f]][:]
                #print(np.amax(meteoRaw[surfname[f]]))
            
            # extract data in meteoRaw
            # 4D data
            fileNames=['t','z','r','u','v','w']
            
            for f in range(len(fileNames)):
                # print(np.shape(meteoRaw[varNames[f]]))
                meteoRaw[varNames[f]]=meteoRaw[varNames[f]][ind_beg:ind_end+1,:,:,:]
            
            # 3D data
            for f in range(len(surfname)):
                meteoRaw[surfname[f]] = meteoRaw[surfname[f]][ind_beg:ind_end+1,:,:]
                #print(np.amax(meteoRaw[surfname[f]]))
            
            # weather data in meteoraw have four indices: time, pressure level, latitude and longitude
            

            
        elif source=='ERA5' and data_type == 'MONTHLY_MEAN': 
            print('ERA5 MONTHLY MEAN')
            
            print('Import file 1: ' + str(year) + '_' + str(month) + '_' + 'pressure_lvl.nc')
            print('Import file 2: ' + str(year) + '_' + str(month) + '_' + 'surface.nc')
            print('Import file 3: ' + str(year) + '_' + str(month) + '_' + 'invariant.nc')
            
            data = Dataset(join(fod,str(year) + '_' + str(month) + '_' + 'pressure_lvl.nc'), mode = 'r')
            surface =  Dataset(join(fod,str(year) + '_' + str(month) + '_' + 'surface.nc'), mode = 'r')
            invariant = Dataset(join(fod,str(year) + '_' + str(month) + '_' + 'invariant.nc'), mode = 'r') 
            fileNames=['t','z','r','u','v','w']
            
            for f in range(len(fileNames)):
                meteoRaw[varNames[f]]=data.variables[fileNames[f]][:]

            meteoRaw['blh'] = surface.variables['blh'][:]     # boundary layer height  
            meteoRaw['u10'] = surface.variables['u10'][:]     # 10 m wind
            meteoRaw['v10'] = surface.variables['v10'][:]
            meteoRaw['t2m'] = surface.variables['t2m'][:]     # 2 m temperature
            meteoRaw['lsm'] = invariant.variables['lsm'][0,:,:] 

            # extract data for the month  
            for f in range(len(fileNames)):
                #meteoRaw[varNames[f]]=meteoRaw[varNames[f]][month-1,:,:,:] # original by R. Beckmann
                meteoRaw[varNames[f]]=meteoRaw[varNames[f]][0] # -MG- for each month download only the required files, saves memory and time as well as organizes the data in a database like fashion
            
            # original by R. Beckmann
            #meteoRaw['blh']=meteoRaw['blh'][month-1,:,:]
            #meteoRaw['u10']=meteoRaw['u10'][month-1,:,:]
            #meteoRaw['v10']=meteoRaw['v10'][month-1,:,:]
            #meteoRaw['t2m']=meteoRaw['t2m'][month-1,:,:]
            
            # -MG- for each month download only the required files, saves memory and time as well as organizes the data in a database like fashion
            meteoRaw['blh']=meteoRaw['blh'][0,:,:]
            meteoRaw['u10']=meteoRaw['u10'][0,:,:]
            meteoRaw['v10']=meteoRaw['v10'][0,:,:]
            meteoRaw['t2m']=meteoRaw['t2m'][0,:,:]
            
            #meteoRaw['lsm']=meteoRaw['t2m'][0,:,:]
    
            surfname = ['t2m', 'u10', 'v10']
            
            # data dimensions
            level = data.variables['level'][:]
            lon = data.variables['longitude'][:]
            lat = data.variables['latitude'][:]
            
            data.close()
            surface.close()
            invariant.close()

        #===========================================================================================================================
        # DAILY is newly implemented and not fully tested yet, -MG- 06/04/2020
        elif source=='ERA5' and data_type == 'DAILY': 
            print('ERA5 DAILY')

            print('Import file 1: ' + str(year) + '_' + str(month) + '_' + str(day) + '_' + 'pressure_lvl.nc')
            print('Import file 2: ' + str(year) + '_' + str(month) + '_' + str(day) + '_' + 'surface.nc')
            print('Import file 3: ' + str(year) + '_' + str(month) + '_' + str(day) + '_' + 'invariant.nc')

            #data = Dataset(join(fod,str(year) + '/' + str(year) + '_' + str(month) + '_' + str(day) + '_' + 'pressure_lvl.nc'), mode = 'r')
            #surface =  Dataset(join(fod,str(year) + '/' + str(year) + '_' + str(month) + '_' + str(day) + '_' + 'surface.nc'), mode = 'r')
            #invariant = Dataset(join(fod,str(year) + '/' + str(year) + '_' + str(month) + '_' + str(day) + '_' + 'invariant.nc'), mode = 'r')

            data = Dataset(join(fod, str(year) + '_' + str(month) + '_' + str(day) + '_' + 'pressure_lvl.nc'), mode = 'r')
            surface =  Dataset(join(fod, str(year) + '_' + str(month) + '_' + str(day) + '_' + 'surface.nc'), mode = 'r')
            invariant = Dataset(join(fod, str(year) + '_' + str(month) + '_' + str(day) + '_' + 'invariant.nc'), mode = 'r')
            fileNames=['t','z','r','u','v','w']
            
            for f in range(len(fileNames)):
                meteoRaw[varNames[f]]=data.variables[fileNames[f]][:]

            meteoRaw['blh'] = surface.variables['blh'][:]     # boundary layer height  
            meteoRaw['u10'] = surface.variables['u10'][:]     # 10 m wind
            meteoRaw['v10'] = surface.variables['v10'][:]
            meteoRaw['t2m'] = surface.variables['t2m'][:]     # 2 m temperature
            meteoRaw['lsm'] = invariant.variables['lsm'][0,:,:] 

            # extract data for the month  
            for f in range(len(fileNames)):
                #meteoRaw[varNames[f]]=meteoRaw[varNames[f]][month-1,:,:,:] # original by R. Beckmann
                meteoRaw[varNames[f]]=meteoRaw[varNames[f]][0] # -MG- for each month download only the required files, saves memory and time as well as organizes the data in a database like fashion
            
            # original by R. Beckmann
            #meteoRaw['blh']=meteoRaw['blh'][month-1,:,:]
            #meteoRaw['u10']=meteoRaw['u10'][month-1,:,:]
            #meteoRaw['v10']=meteoRaw['v10'][month-1,:,:]
            #meteoRaw['t2m']=meteoRaw['t2m'][month-1,:,:]
            
            # -MG- for each month download only the required files, saves memory and time as well as organizes the data in a database like fashion
            meteoRaw['blh']=meteoRaw['blh'][0,:,:]
            meteoRaw['u10']=meteoRaw['u10'][0,:,:]
            meteoRaw['v10']=meteoRaw['v10'][0,:,:]
            meteoRaw['t2m']=meteoRaw['t2m'][0,:,:]
            
            #meteoRaw['lsm']=meteoRaw['t2m'][0,:,:]
    
            surfname = ['t2m', 'u10', 'v10']
            
            # data dimensions
            level = data.variables['level'][:]
            lon = data.variables['longitude'][:]
            lat = data.variables['latitude'][:]
            
            data.close()
            surface.close()
            invariant.close()

            #===========================================================================================================================

        #plt.figure()
        #plt.imshow(meteoRaw['temp'][0,:,:])
        
        # convert pressure levels from mb to Pa
        level = level*101.3
        
        print('2D horizontal interpolation')
        
        var2D = surfname
        
        for d in range(len(self)):
            nx=len(self[d]['x_space'])
            ny=len(self[d]['y_space'])
            
            dim_data=dict()
            # dimensions of parameter arrays
            for f in varNames:
                dim_data[f] = np.shape(meteoRaw[f])
                
            for f in var2D:
                dim_data[f] = np.shape(meteoRaw[f])
                
               
            
            if (source == 'ERA5'):
                dim_data['blh']=np.shape(meteoRaw['blh'])
            
            meteoInt=dict()
            # prealloate arrays
            
            if len(dim_data[varNames[0]]) > 3:
                n_met = dim_data[varNames[0]][0]  
                # number of time points where meteorological data is needed
                for f in varNames:
                    
                    a = dim_data[f][1]
                    #print(a)
                    #type(a)
                    meteoInt[f] = np.zeros((n_met,a,ny,nx))
                    
                meteoInt['u10']  = np.zeros((n_met,ny,nx))
                meteoInt['v10']  = np.zeros((n_met,ny,nx))
                meteoInt['t2m']  = np.zeros((n_met,ny,nx))
                
                
                if (source == 'ERA5'):
                    meteoInt['blh']  = np.zeros((n_met,ny,nx))
                    meteoInt['lsm']  = np.zeros((ny,nx))
            else: 
                n_met = 1
                for f in varNames:
                    meteoInt[f] = np.zeros((dim_data[f][1],ny,nx)) # müsste 0 sein
                    
                
                meteoInt['u10']  = np.zeros((ny,nx))
                meteoInt['v10']  = np.zeros((ny,nx))
                meteoInt['t2m']  = np.zeros((ny,nx))
                
                
                
                if (source == 'ERA5'):    
                    meteoInt['blh'] = np.zeros((ny,nx))
                    meteoInt['lsm']  = np.zeros((ny,nx)) 
             
            # convert to 0-360 deg longitudes    
            if self[d]['Longitude'][0] < 0:
                self[d]['Longitude'][0] = self[d]['Longitude'][0] + 360
            
            if self[d]['Longitude'][1] < 0:
                self[d]['Longitude'][1] = self[d]['Longitude'][1] + 360
            
                
        
            # longitude and latitude of climate data (Lon: 0-360, Lat: -90-90) 
            # in area of interest
            #if self[d]['datejump']==False:
            ix=np.where((lon>=self[d]['Longitude'][0]-4) & 
                        (lon<=self[d]['Longitude'][1]+4))[0]
            iy=np.where((lat>=self[d]['Latitude'][0]-4) & 
                        (lat<=self[d]['Latitude'][1]+4))[0]
#            else:
#                ix=np.where((lon<=np.min(self[d]['Longitude'])+2+360) | 
#                                   (lon>=np.max(self[d]['Longitude'])-2+360))[0]
#                iy=np.where((lat>=np.min(self[d]['Latitude'])-2) |
#                                   (lat<=np.max(self[d]['Latitude'])+2))[0]
                
            
            
            
            
            meteoCut=dict()
            for f in varNames:
                meteoCut[f] = meteoRaw[f]
                
            xSpaceMet=lon[ix]
            ySpaceMet=lat[iy]
            
            #print(self[d]['Latitude'])
            #print(self[d]['Longitude'])
            #print(xSpaceMet)
            #print(ySpaceMet)
            
             # print(np.shape(meteoRaw['blh']))
#            print(lat[iy])
#            print(len(ix),len(iy))
            
            

            if len(dim_data[varNames[0]])==4:
                for key in varNames:
                    meteoCut[key]=meteoRaw[key][:,:,iy,:]
                    meteoCut[key]=meteoCut[key][:,:,:,ix]
                    
                for key in var2D:
                    meteoCut[key]=meteoRaw[key][:,iy,:]
                    meteoCut[key]=meteoCut[key][:,:,ix]
                    #print(np.amax(meteoCut[key]))
                
                if (source == 'ERA5'):
                    meteoCut['lsm']=meteoRaw['lsm'][iy,:]  
                    meteoCut['lsm']=meteoRaw['lsm'][:,ix]
                    
                if (source == 'ERA5'):
                    meteoCut['blh']=meteoRaw['blh'][:,iy,:]
                    meteoCut['blh']=meteoCut['blh'][:,:,ix]
            else:
                for key in varNames:
                    meteoCut[key]=meteoRaw[key][:,iy,:]
                    meteoCut[key]=meteoCut[key][:,:,ix]
                    #print(np.amax(meteoCut[key]))
                
                for key in var2D:
                    meteoCut[key]=meteoRaw[key][iy,:]
                    meteoCut[key]=meteoCut[key][:,ix]
                    
                #print(np.shape(meteoRaw['lsm']))    
                
                if source == 'ERA5':
                    meteoCut['lsm']=meteoRaw['lsm'][iy,:]  
                    meteoCut['lsm']=meteoCut['lsm'][:,ix] 
                    meteoCut['blh']=meteoRaw['blh'][iy,:]
                    meteoCut['blh']=meteoCut['blh'][:,ix]
                    #print(np.shape(meteoCut['lsm']))  
                    
                
                    
                    
                    
            
                
            pts=np.zeros((len(xSpaceMet)*len(ySpaceMet),3))
            
            for i in range(len(ySpaceMet)):
                pts[i*len(xSpaceMet):(i+1)*len(xSpaceMet),0]=xSpaceMet
                pts[i*len(xSpaceMet):(i+1)*len(xSpaceMet),1]=ySpaceMet[i]
            
            
            meteoInt=dict()
            
            # calculate meshgrid
            lonmin = self[d]['Longitude'][0]
            lonmax = self[d]['Longitude'][1]
            dlon = (lonmax-lonmin)/(self[d]['Longitude'][2]-1)
            
            latmin = self[d]['Latitude'][0]
            latmax = self[d]['Latitude'][1]
            dlat = (latmax-latmin)/(self[d]['Latitude'][2]-1)
            
            x_space = np.arange(lonmin,lonmax+dlon,dlon)
            y_space = np.arange(latmin,latmax+dlat,dlat)
            
            x_space = np.linspace(lonmin,lonmax,num  = nx)
            y_space = np.linspace(latmin,latmax,num  = ny)
            
            #print(x_space)
            #print(y_space)
            #print(nx)
            #print(ny)
            
            Ln, Lt = np.meshgrid(x_space,y_space)
            #Ln, Lt = np.meshgrid(self[d]['x_space'], self[d]['y_space'])
            self[d]['meteo']=dict()
            
            
            
            
            ###################################################################
            #Perform Interpolation
            ###################################################################
            
            
            if len(dim_data[varNames[0]]) == 3:
                
                if source == 'ERA5':
                    
                    var2D  = ['u10' , 'v10' , 't2m', 'lsm']
                else: 
                    var2D  = ['u10' , 'v10' , 't2m']
                    
                for key in varNames:
                    
                    meteoInt[key]=np.zeros((dim_data[key][0],ny,nx))
                    
                    #print('Interpoliert wird : %s ' %np.amax(meteoCut[key]))
                    #print(np.shape(meteoCut[key]))
                    
                    for ip in range(dim_data[key][0]):
#                        
                        pts[:,2]=np.reshape(meteoCut[key][ip,:,:],(len(xSpaceMet)*len(ySpaceMet)))
                        
                        meteoInt[key][ip,:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
#                        
                    #print('%s Maximum: %s' %(key,np.amax(meteoInt[key])))
                    
                    
                # 2D variables
                for key in var2D:
                    # print(key)
                    meteoInt[key]=np.zeros((ny,nx))
                    pts[:,2]=np.reshape(meteoCut[key][:,:],(len(xSpaceMet)*len(ySpaceMet)))
                    meteoInt[key][:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
                        
#                print(np.shape(meteoCut['blh']))
                if source=='ERA5':
                    meteoInt['blh']=np.zeros((ny,nx))
                    pts[:,2]=np.reshape(meteoCut['blh'][:,:],(len(xSpaceMet)*len(ySpaceMet)))
                    meteoInt['blh'][:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
#                
                        
                    
                    
                    
                    
                    
                    
            elif len(dim_data[varNames[0]]) == 4:
                for key in varNames:
                    #print(key)
                    meteoInt[key]=np.zeros((dim_data[key][0],dim_data[key][1],ny,nx))
                    
                    for it in range(dim_data[key][0]):
                        for ip in range(dim_data[key][1]):
                            pts[:,2]=np.reshape(meteoCut[key][it,ip,:,:],(len(xSpaceMet)*len(ySpaceMet)))
                            meteoInt[key][it,ip,:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
                            
                # 2D variables
                for key in var2D:
                    #print(key)
                    meteoInt[key]=np.zeros((n_met,ny,nx))
                    
                    for it in range(dim_data[key][0]):
                        
                        
                        pts[:,2]=np.reshape(meteoCut[key][it,:,:],(len(xSpaceMet)*len(ySpaceMet)))
                        meteoInt[key][it,:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
                    #print(np.amax(meteoInt[key]))
                
                # land sea mask
                if source == 'ERA5':
                    meteoInt['lsm']=np.zeros((ny,nx))
                    pts[:,2]=np.reshape(meteoCut['lsm'],(len(xSpaceMet)*len(ySpaceMet)))
                    meteoInt['lsm'][:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
                
                if source=='ERA5':
                    meteoInt['blh']=np.zeros((n_met,ny,nx))
                    
                    for it in range(dim_data['blh'][0]):
                        pts[:,2]=np.reshape(meteoCut['blh'][it,:,:],(len(xSpaceMet)*len(ySpaceMet)))
                        meteoInt['blh'][it,:,:]=griddata(pts[:,0:2], pts[:,2], (Ln,Lt), method='linear')
                        
            


            
            ######################################################            
            # Virtual temperature, specific humidity, air density            
            ######################################################
            p_lvls = np.zeros((len(level),ny,nx))
            
            ## Original
            ## calculate only pressure levels where data is available
            #if data_type == 'DAILY'z:
            #    pmax = min(dim_data['temp'][1],dim_data['hum'][1]) 
            #else:
            #    pmax = min(dim_data['temp'][0],dim_data['hum'][0])
            #    #print(pmax)
                
            
            ######## TESTING modified -MG- to handle ERA DAILY data    
            # calculate only pressure levels where data is available
            if data_type == 'DAILY' and source == 'NCAR':
                pmax = min(dim_data['temp'][1],dim_data['hum'][1])
            
            elif data_type == 'DAILY' and source == 'ERA5': 
                 pmax = min(dim_data['temp'][0],dim_data['hum'][0])
                 
            else:
                pmax = min(dim_data['temp'][0],dim_data['hum'][0])
                #print(pmax)
            ############################    


            # pressure levels are always the same
            for indx in range(nx):
                for indy in range(ny):
                    # pressure in Pa
                    p_lvls[:,indy,indx] = level  
            
            
            
            # transform temperature to K if necessary
            if np.amax(meteoInt['temp']) < 150:
                meteoInt['temp'] =  meteoInt['temp'] + 273.15 # NCEP is in Kelvin from the beginning!
                
            
            
            #specific humidity
            if len(dim_data[varNames[0]]) == 3: 
                work = np.zeros((pmax,ny,nx))
                
                
                # for NCAR data the temperature is already given in deg C  (nope)              
                
                es = 17.67*(meteoInt['temp'][0:pmax,:,:]-273.15)  / (243.5+ (meteoInt['temp'][0:pmax,:,:]-273.15))
                
                #print(es)
                
                print(np.shape(es))
                print(np.shape(work))
                
                work[:,:,:] = 6.112*np.exp(es)  # Saturation Pressure in mb        
                work[:,:,:] = work[:,:,:]*meteoInt['hum'][0:pmax,:,:]/100   # vapor pressure in mb
                work[:,:,:] = 0.622*work[:,:,:]/((p_lvls[0:pmax,:,:]/101.3)-0.378*work[:,:,:]) # specific humidity in kg/kg, p in mb
                
            else:
                work = np.zeros((n_met,pmax,ny,nx))
                
                for it in range (n_met):
                    
                    if source == 'ERA5':
                        es =  (17.67*(meteoInt['temp'][it,0:pmax,:,:]-273.15))/(243.5+(meteoInt['temp'][it,0:pmax,:,:]-273.15))
                    elif source == 'NCEP' or 'NCAR':
                        es =  (17.67*(meteoInt['temp'][it,0:pmax,:,:]-273.15))/(243.5+(meteoInt['temp'][it,0:pmax,:,:]-273.15))
                        
                    work[it,:,:,:] = 6.112*np.exp(es)                                                                               # Saturation Pressure in mb        
                    work[it,:,:,:] = work[it,:,:,:]*meteoInt['hum'][it,0:pmax,:,:]/100                                              # vapor pressure in mb
                    work[it,:,:,:] = 0.622*work[it,:,:,:]/((p_lvls[0:pmax,:,:]/101.3)-0.378*work[it,:,:,:])                         # specific humidity in kg/kg, p in mb
    
                                        
            qv = work
            #print(np.shape(qv))
            #print('qv = %s '% np.amax(qv))
            
            
            del work
            
            
            
            # virtual temperature
            if len(dim_data[varNames[0]]) == 3: 
                Tv = np.zeros((pmax,ny,nx))    
     
                Tv[:,:,:]= qv[:,:,:]/(1.0-qv[:,:,:])                                                   # Mixing ratio (humidity ratio)        
                
                Tv[:,:,:] = meteoInt['temp'][0:pmax,:,:]*(1.0 + 1.6077*Tv[:,:,:])/(1.0+Tv[:,:,:])       # virtual temperature
                #print(np.shape(Tv))
                
            else:
                Tv = np.zeros((n_met,pmax,ny,nx))
    
                for it in range(n_met):

                    Tv[it,:,:,:]= qv[it,:,:,:]/(1.0-qv[it,:,:,:])                                      # Mixing ratio (humidity ratio)        
    
                    Tv[it,:,:,:] = meteoInt['temp'][it,0:pmax,:,:]*(1.0 + 1.6077*Tv[it,:,:,:])/(1.0+Tv[it,:,:,:]) 
            
            #print('Tv = %s '% np.amax(Tv))
            #print(np.shape(Tv))
            
            #potential temperature
            if len(dim_data[varNames[0]]) == 4:
                Tp = np.zeros((n_met,pmax,ny,nx))    
    
                for it in range(n_met):
                    Tp[it,:,:,:] =  meteoInt['temp'][it,0:pmax,:,:]*(10**(5)/p_lvls[0:pmax,:,:])**(0.285) 

            else:
                Tp = np.zeros((pmax,ny,nx))
                Tp[:,:,:] = meteoInt['temp'][0:pmax,:,:]*(10**(5)/p_lvls[0:pmax,:,:])**(0.285) 
            
            #print(np.shape(Tp))
            
            
            # air density
            if len(dim_data[varNames[0]]) == 4:
                ro = np.zeros((n_met,pmax,ny,nx))
    
                for it in range(n_met): 
                    ro[it,:,:,:] = p_lvls[0:pmax,:,:]/(287.06*Tv[it,:,:,:])
                    
            else:
                ro = np.zeros((pmax,ny,nx))
                ro[:,:,:] = p_lvls[0:pmax,:,:]/(287.06*Tv[:,:,:]) 
                
            #print('Rhomax = %s '% np.amax(ro))
                
            # vertical velocity from omega --> w = - omega/(rho*g) = - omega*R*Tv/(P*g)    
     
            if len(dim_data[varNames[0]]) == 4:
                w = np.zeros((n_met,pmax,ny,nx))
    
                for it in range (n_met):
                    #w[it,:,:,:] =  - meteoInt['w'][it,0:pmax,:,:]*287.06*Tv[it,:,:,:]/9.81 # here pressure is missing
                    w[it,:,:,:] =  - meteoInt['w'][it,0:pmax,:,:]/(9.81*ro[it,0:pmax,:,:])
            else:
                w = np.zeros((pmax,ny,nx)) 
                #w[:,:,:] =  - meteoInt['w'][0:pmax,:,:]*287.06*Tv[:,:,:]/9.81
                w[:,:,:] =  - meteoInt['w'][0:pmax,:,:]/(9.81*ro[0:pmax,:,:])
                
            #print('Omega_max = %s ' % np.amax(w))    
            #print('Omega_min = %s ' % np.amin(w))    
                
            meteoInt['w'] = w
            meteoInt['ro'] = ro
            meteoInt['Tp'] = Tp
            meteoInt['p_lvls'] = p_lvls
            meteoInt['qv'] = qv
            
            
            
            ##########################
            # 1D-height interpolation
            ##########################
            print('1D height interpolation of climate data')
            
            #variables to interpolate
            varint = ['temp','hum','u','v','p_lvls','w','ro','Tp','qv']
            
            # transform to geopotential height for ERA5 data
            if source == 'ERA5':
                meteoInt['hgt'] = meteoInt['hgt']/9.8066
                
            # transform geopotential to terrain following coordinate system
            if len(dim_data[varNames[0]]) == 3: 
                for ip in range(dim_data['hgt'][0]):
                    meteoInt['hgt'][ip,:,:] = meteoInt['hgt'][ip,:,:] - topography
            elif len(dim_data[varNames[0]]) == 4: 
                for it in range(dim_data['hgt'][0]):
                    for ip in range(dim_data['hgt'][1]):
                        meteoInt['hgt'][it,ip,:,:] = meteoInt['hgt'][it,ip,:,:] - topography
            
            #print('Maximum value geop:')
            #print(np.matrix.max(meteoInt['hgt']))
            
            meteoInt2 = dict()
            
            for f in varint:
                if len(dim_data[varNames[0]]) == 3 or f == 'p_lvls':
                    meteoInt2[f] = np.zeros((len(zlayer),ny,nx))
                else:
                    meteoInt2[f] = np.zeros((n_met,len(zlayer),ny,nx))
            
            
            #interpolation with C function
#            for f in varint:
#                meteoInt2[f] = C.interpolate1D(meteoInt[f],meteoInt['hgt'],zlayer,f)
#            
            
            
            if len(dim_data[varNames[0]]) == 3:
                for ix in range(nx):
                    for iy in range(ny):
                        pts = meteoInt['hgt'][:,iy,ix]
                        
                        for f in varint:
                            values = meteoInt[f][:,iy,ix]
                            pts = meteoInt['hgt'][0:len(values),iy,ix]
                            
                            #print(values)
                            #print(pts)
                            # use own function
                            meteoInt2[f][:,iy,ix] = self.interpolation(values,pts,zlayer)
#                            F = interp1d(pts,values,kind='linear')
#                            meteoInt2[f][:,iy,ix] = F(zlayer)
                            #meteoInt2[f][:,iy,ix] = griddata(pts,values,zlayer,method='linear')
                            
            
            if len(dim_data[varNames[0]]) == 4:
                for it in range(n_met):
                    for ix in range(nx):
                        for iy in range(ny):
                            #pts = meteoInt['hgt'][it,:,iy,ix]
                        
                            for f in varint:
                                #print(np.shape( meteoInt[f]))
                                
                                if f == 'p_lvls':
                                    values =  meteoInt[f][:,iy,ix]
                                else:
                                    values = meteoInt[f][it,:,iy,ix]
                                    
                                pts = meteoInt['hgt'][it,0:len(values),iy,ix]
                                #TODO: für 4D Daten muss hier noch die eigene Interpolation angewendet werden
                                #print(f)
                                if f == 'p_lvls':
                                    meteoInt2[f][:,iy,ix] = self.interpolation(values,pts,zlayer)
                                else:    
                                    meteoInt2[f][it,:,iy,ix] = self.interpolation(values,pts,zlayer)
           
           
            self[d]['meteo']['geop'] = meteoInt['hgt']
            
            
            
            
            if source == 'ERA5':
                 self[d]['meteo']['blh'] = meteoInt['blh']
                 
            self[d]['meteo']['geop']          = meteoInt['hgt']
            self[d]['meteo']['levels']        = level
            self[d]['meteo']['data']          = meteoInt2
            self[d]['meteo']['data_pressure'] = meteoInt
            
            print('3D Interpolation finished')
            
        return self[d]['meteo']









    def AddVs30(self,regime='active',Hmin=0.0,**args):
        """
        Adds Vs30 data to all available subdomains
        :type  regime: str
        :param regime: Defines type of vs30 regime [active | stable]
        
        :type  Hmin: float
        :param Hmin: Minimum elevation for which vs30 is computed
        """
        
        for i in range(self.NumDomain):
            self[i]['grids']['Vs30']=self.GetVs30(i,regime,Hmin,args)['grid']

    def GetVs30(self,did,regime='active',lowCoast=False,Hmin=0.0,**args):
        i=did
        if 'DEM' in self[i]['grids']:

            dx=np.mean(self[i]['Spacing'])*100*1000
            grid=self[i]['grids']['DEM']
            
            if lowCoast==True:
                #Get WaterMask
                sl=[[self[did]['Longitude'][0],self[did]['Longitude'][1],
                    self[did]['Latitude'][0],self[did]['Latitude'][1],32199,'WaterMask']]
                wm=Domain(sl,resize=False)[0]
                mask=wm['grids']['DEM']
                #resize watermask
                ratio1=len(grid[0,:])/len(mask[0,:])
                ratio2=len(grid[:,0])/len(mask[:,0])
    
                mask=ndimage.zoom(mask,(ratio2,ratio1))
                index0=np.where(mask>=0.5)
            else:
                index0=np.where(grid<Hmin)

            grid[index0]=0
#            if 'mask' in args:
#                index=np.where(args['mask']==0)
#                grid[index]=0
            vs30_grid=grid/dx
            grad=np.gradient(vs30_grid)
            grad=np.sqrt(grad[0]**2+grad[1]**2)
            #Boundaries of vs30 values
            bound=[180,240,300,360,490,620,760]
            #Tectonic regimes after Allen and Wald 2009: 
                    #On the Use of High-Resolution Topographic Data as a Proxy
                    #for Seismic Site Conditions (VS30)
            if regime=='active':
                conv=[0.0003,0.0035,0.01,0.018,0.05,0.10,0.14]
            elif regime=='stable':
                conv=[0.000006,0.002,0.004,0.0072,0.013,0.018,0.025]
            
            index=np.where(grad<=conv[0])
            vs30_grid[index]=180
            index=np.where(grad>=conv[6])
            vs30_grid[index]=760
            for j in range(len(bound)-1):
                index=np.where((grad>conv[j]) & (grad<=conv[j+1]))
                vs30_grid[index]=bound[j]+(grad[index]-conv[j])/(conv[j+1]-conv[j])*(bound[j+1]-bound[j])
            
            #Smooth vs30-grid
            vs30_grid=ndimage.filters.gaussian_filter(vs30_grid,1)
            vs30_grid[index0]=179

            self[i]['grids']['Vs30']=vs30_grid
            dx=np.mean(self[i]['Spacing'])
            x_space=np.linspace(self[i]['Longitude'][0]+dx/2,self[i]['Longitude'][1]-dx/2,len(vs30_grid[0,:]))
            y_space=np.flipud(np.linspace(self[i]['Latitude'][0]+dx/2,self[i]['Latitude'][1]-dx/2,len(vs30_grid[:,0])))
            
            
            vs30_vec=np.zeros((len(x_space)*len(y_space),3))
            for j in range(len(x_space)):
                vs30_vec[j*len(y_space):(j+1)*len(y_space),0]=x_space[j]
                vs30_vec[j*len(y_space):(j+1)*len(y_space),1]=y_space
                vs30_vec[j*len(y_space):(j+1)*len(y_space),2]=vs30_grid[:,j]
        
            print('Vs30-grid generated')
            export=dict()
            export['vec']=vs30_vec
            export['grid']=vs30_grid
            export['x_space']=x_space
            export['y_space']=y_space

            return export
            
    def adjustMeteoLevels(self,nLevels):
        
        
        
        meteo0=dict()
        meteo0['levels']=nLevels
        meteo0['data']=dict()
        
        for d in len(self):
            oLevels=self[d]['meteo']['levels']
            for key in self[d]['meteo']['data'].keys():
                meteoOld=self[d]['meteo']['data'][key]
                meteoNew=np.zeros((len(nLevels),
                                       len(meteoOld[0,:,0]),len(meteoOld[0,0,:])))
                for n in range(len(nLevels)):
                    
                    
                    
                    needInterp=True
                    #Check if same pressure level is already available
                    for o in range(len(oLevels)):
                        if oLevels[o]==oLevels[n]:
                            meteoNew[n,:,:]=meteoOld[o,:,:]
                            needInterp=False
                    #if not available perform simple linear interpolation between 
                    #2 closest pressure levels
                    if needInterp==True:
                        for o in range(len(oLevels)-1):
                            if (nLevels[n]>=oLevels[o]) & (nLevels[n]<=oLevels[o+1]):
                                ix=[o,o+1]
                                
                        dxB=np.abs(nLevels[n]-oLevels[ix[0]]) #distance to lower level
                        dxF=np.abs(nLevels[n]-oLevels[ix[1]]) #distance to upper level
                
                        dx=np.abs(oLevels[ix[1]]-oLevels[ix[0]])
                        meteoNew[n,:,:]=meteoOld[ix[0],:,:]*dxF/dx+meteoOld[ix[1],:,:]*dxB/dx
                    meteo0['data'][key]=meteoNew
        self[d]['meteo']=meteo0
        
        return meteo0
    #Functions Missing:
    #Import Domain (from folder)
    #Add Landuse
    #Add CoastMask
    #Plot grids
    #Export grids
    #Save results
    
if __name__ == "__main__":
    pass




def GridCrop(Domain,xlim,ylim,datejump,kind):
    dx,dy=Domain['Spacing']
    x_space=np.arange(Domain['Longitude'][1]+dx/2,Domain['Longitude'][0]+dx/2,dx)
    y_space=np.arange(Domain['Latitude'][1]+dy/2,Domain['Latitude'][0]+dy/2,dy)
        
    y_space=np.array(sorted(y_space,reverse=True)) #sort the list ascending w.r.t to geotiff orientation
    if datejump==False:
        y2=np.max(np.where(y_space>=ylim[0]))+1
        y1=np.min(np.where(y_space<=ylim[1])) 
        x1=np.min(np.where(x_space>=xlim[0]))
        x2=np.max(np.where(x_space<=xlim[1]))+1
    
        grid=Domain['grids'][kind][y1:y2,x1:x2].astype(float)
                
        x_space=x_space[x1:x2]
        y_space=y_space[y1:y2]
        numx=len(x_space) # number of elements in longitude
        numy=len(y_space) # number of elements in latitude
    elif datejump==True:
        y2=np.max(np.where(y_space>=ylim[0]))+1
        y1=np.min(np.where(y_space<=ylim[1])) 
        x1=np.min(np.where(x_space>=xlim[0]))
        x2=np.max(np.where(x_space<=xlim[1]))+1
        grid1=Domain['grids'][kind][y1:y2,x1:len(x_space)].astype(float)
        grid2=Domain['grids'][kind][y1:y2,0:x2].astype(float)
        grid=np.concatenate((grid1,grid2), axis=1)
        
        x_space1=x_space[x1:len(x_space)-1]
        x_space2=x_space[0:x2]
        x_space=np.concatenate((x_space1,x_space2))
        y_space=y_space[y1:y2]
        numx=len(x_space) # number of elements in longitude
        numy=len(y_space) # number of elements in latitude
                
    phi=y_space*np.pi/180
    Domain['grids']['phi']=phi
    Domain['Longitude']=np.append(xlim,numx) 
    Domain['Latitude']=np.append(ylim,numy) 
    Domain['grids'][kind]=grid
    Domain['x_space']=x_space
    Domain['y_space']=y_space

    return Domain
