# -*- coding: utf-8 -*-
"""
Created on Wed May 27 14:34:04 2015

@author: aschaefer
"""

import numpy as np
from osgeo import gdal, osr

#import matplotlib.pyplot as plt
#import os
#from . import GeoC_Code#cRasterDist, cVectorMatrixAssign, cFindCoastline, cCoast, cGridStretch#, cGridShrink
from .utils import shapefile as shp


from scipy import ndimage


def ImportGeoTiff(fid,kind='grid'):
    """Import of GeoTiff file. Compiled into python-dict

    Args:
        fid (str): Path to GeoTiff-file e.g. 'C:/myfile.tif'
                 
    Returns:
        geodict (dict): Dictionary with the following arguments:
            dict: {
            'longitude' (list): boundary points by longitude
            'latitude' (list):  boundary points by latitude
            'spacing' (list):   raster size
            'grid' (float):     raster array
            }

    """
    ds= gdal.Open(fid) #import geotiff file
    #convert geotiff data information into numpy array
    grid=np.array(ds.GetRasterBand(1).ReadAsArray())
    #retrieve geoinformation, top left corner of tif_file
    gt=ds.GetGeoTransform() 
    #maximum and minimum longitude
    longitude=(gt[0]+len(grid[0,::])*gt[1]+len(grid[::,0])*gt[2], gt[0]) 
    #maximum and minimum latitude
    latitude=(gt[3], gt[3]+len(grid[0,::])*gt[4]+len(grid[::,0])*gt[5]) 
    spacing = (np.abs(gt[1]),np.abs(gt[5])) #x-spacing, y-spacing

    #store geotiff information into dictionary
    geodict={'Longitude': longitude, 'Latitude': latitude, 
    'Spacing': spacing} 
    geodict['grids']=dict()
    geodict['grids'][kind]=grid
    return geodict
    
def ImportShapefile(fid):
    """Import of ESRI-GIS Shapefile file. Compiled into python-dict

    Args:
        fid (str): Path to shp-file e.g. 'C:/myfile.shp'
                 
    Returns:
        geodict (dict): Dictionary with the following arguments:
            dict: {
            'pts' (list): point / edge / vertex information 
            'data' (list): shape data
            
            'fields' (list): list of (str) of column headers
            'shapetype' (float): raster array
            }

    """
    sf=shp.Reader(fid) #Read-In Shapefile
    shapes=sf.shapes() #get geometry
    num_shapes=sf.numRecords #Number of ID-Sources
    pts=np.empty((1,3))

    rec=sf.records() #get data records

    for i in range(num_shapes):
        pts_i=shapes[i].points #points of current data source
        num_points=len(pts_i) #number of points of data source
        pts_j=np.zeros((num_points,3)) #initialize export array
        for j in range(num_points):
            pts_j[j,1]=pts_i[j][0] #longitude
            pts_j[j,2]=pts_i[j][1] #latitude
            pts_j[j,0]=i+1 #ID
        #Combine point array into total array
        pts=np.concatenate((pts,pts_j), axis=0)
      
    pts=np.delete(pts,(0),axis=0) #remove dummy values
 
      
    #Check for various data fields and read them out (data columns)
    num_fields=len(sf.fields)-1
    fields=[]
    shapetype=0 #Unknown shapetype
    #Determine the shapetype, point set, polyline set or polygons
    for i in range(num_fields):
        fields.append(sf.fields[i+1][0])
    if sf.shapeType==1:
        shapetype='Point'
    if sf.shapeType==3:
        shapetype='Polyline'
    if sf.shapeType==5:
        shapetype='Polygon'   
    
    geodict={'data':rec , 'pts':pts, 'fields': fields, 'type':shapetype, 'sf':sf} #arrange export dictionaray

    return geodict
    
def ExportShapefile(fid,df,pts,shapetype):
    """Export of ESRI-GIS Shapefile file to specified path

    Args:
        fid (str):              Path to shp-file e.g. 'C:/myfile.shp'
        df (pandas.DataFrame):  Tabular format of shapefile data
        pts (float):            3xN Array with column 0 as shape index
        shapetype (str):        Description of shape format: Point / Polyline / Polygon

    """
    headers=df.columns
    
    if shapetype=='Point':
        w = shp.Writer(shp.POINT)
    elif shapetype=='Polyline':
        w = shp.Writer(shp.POLYLINE)
    elif shapetype=='Polygon':
        w = shp.Writer(shp.POLYGON)
        
    #w.autoBalance = 1
    #Create Fields and Data-list
    temp=[]

    for row in df.iterrows():
        index, data = row
        temp.append(data.tolist())
    
    for i in range(len(data)):
        if type(data[i])==str:
            #Character field
            w.field(headers[i],'C',80,0)
        else:
            #Get precision
            precision=len(str(data[i]))-len(str(int(data[i])))
            #Numerical field
            w.field(headers[i],'N',80,precision)
    
    #Add data
    for i in range(len(temp)):
        w.records.append(temp[i])
    
    #Adding Shapes
    if shapetype=='Point':
        for i in range(len(pts[:,0])):
            w.point(pts[i,0],pts[i,1])
            
    elif ((shapetype=='Polyline') | (shapetype=='Polygon')):
        ids=np.unique(pts[:,0])
        for i in range(len(ids)):
            index=np.where(pts[:,0]==ids[i])[0]
            pt_list=list()
            for j in range(len(index)):
                pt=[pts[index[j],1],pts[index[j],2]]
                pt_list.append(pt)
            #print(pt_list)
            w.poly(shapeType=w.shapeType,parts=[pt_list])

    #Save Shapefile
    w.save(fid)
    

def ExportGeotiff(fid,grid,x_min,y_max,dx,dy,dtype='Float32',CRS='WGS 84'):
    """Export of GeoTiff file to specified path

    Args:
        fid (str):              Path to shp-file e.g. 'C:/myfile.shp'
        grid (float NxN):       Raster data array
        x_min (float):          3xN Array with column 0 as shape index
        y_min (float):          Description of shape format: Point / Polyline / Polygon
        dx (float):             Spacing along longitude
        dy (float):             Spacing along latitude
        dtype (str ,optional):  datatype precision to save raster, default='Float32'
        CRS (str ,optional):    coordinate reference system (CRS), default='WGS 84'

    """
    #Dimensions of raster
    x_pixels=len(grid[0,::])
    y_pixels=len(grid[::,0])

    driver = gdal.GetDriverByName("GTiff")
    
    
    
    #If domain doesn't contain a date jump (Mid-Pacific)
    if x_min+x_pixels*dx<=180:
        print('Export Single geotiff-File')
        #Create geotiff file
        fid=fid+'.tif'
        print(fid, x_pixels, y_pixels)
        if dtype=='Float32':
            
            dst_ds = driver.Create(fid, x_pixels, y_pixels, 1, gdal.GDT_Float32 )
        elif dtype=='Int':
            dst_ds = driver.Create(fid, x_pixels, y_pixels, 1, gdal.GDT_Int16 )
        elif dtype=='UInt':
            dst_ds = driver.Create(fid, x_pixels, y_pixels, 1, gdal.GDT_UInt16 )
        elif dtype=='Byte':
            dst_ds = driver.Create(fid, x_pixels, y_pixels, 1, gdal.GDT_Byte )
        #Set CoordinateFrame
        dst_ds.SetGeoTransform( [ x_min, dx, 0, y_max, 0, -dy ] )  
        #Determine CoordinateSystem  
        srs = osr.SpatialReference()
        srs.SetWellKnownGeogCS("WGS 84")
        dst_ds.SetProjection( srs.ExportToWkt() )
 
        #Write Array
        dst_ds.GetRasterBand(1).WriteArray(grid)
        #Set Coordinate Projection
        dst_ds.SetProjection( srs.ExportToWkt() )
    
        dst_ds = None
    elif x_min+x_pixels*dx>180:
        print('geotiff-File has been split into eastern and western part (datejump)')
        #Create two tif files
        x_min1=x_min #leftern most longitude coordinate of Western grid
        x_min2=-180 #leftern most longitude coordinate of Eastern grid
        cut_index=int(np.round(abs((180-x_min)/dx))) #Grid-Index where to cut
        
        fid1=fid+'West'+'.tif' #FileName1
        fid2=fid+'East'+'.tif' #FileName2
        grid1=grid[:,0:cut_index] #Grid of Western Segment
        grid2=grid[:,cut_index::] #Grid of Eastern Segment
        
        #------------------------------------------
        #Grid Number Two: Western Pacific
        
        if dtype=='Float32':
            dst_ds = driver.Create(fid1, len(grid1[0,::]), y_pixels, 1, gdal.GDT_Float32 )
        elif dtype=='Int':
            dst_ds = driver.Create(fid1, len(grid1[0,::]), y_pixels, 1,  gdal.GDT_Int16  )
        elif dtype=='UInt':
            dst_ds = driver.Create(fid1, len(grid1[0,::]), y_pixels, 1,  gdal.GDT_UInt16  )
        elif dtype=='Byte':
            dst_ds = driver.Create(fid1, len(grid1[0,::]), y_pixels, 1,  gdal.GDT_Byte  )
        #Set CoordinateFrame
        dst_ds.SetGeoTransform( [ x_min1, dx, 0, y_max, 0, -dy ] )  
        #Determine CoordinateSystem  
        srs = osr.SpatialReference()
        #srs.ImportFromEPSG('WGS 84')
        srs.SetWellKnownGeogCS("WGS 84")
        dst_ds.SetProjection( srs.ExportToWkt() )
        #Write Array
        dst_ds.GetRasterBand(1).WriteArray(grid1)
        #Set Coordinate Projection
        dst_ds.SetProjection( srs.ExportToWkt() )
        #Clear Memory
        dst_ds = None
        #------------------------------------------
        #Grid Number Two: Eastern Pacific
        if dtype=='Float32':
            dst_ds = driver.Create(fid2, len(grid2[0,::]), y_pixels, 1, gdal.GDT_Float32 )
        elif dtype=='Int':
            dst_ds = driver.Create(fid2, len(grid2[0,::]), y_pixels, 1,  gdal.GDT_Int16  )
        elif dtype=='UInt':
            dst_ds = driver.Create(fid2, len(grid2[0,::]), y_pixels, 1,  gdal.GDT_UInt16  )
        elif dtype=='Byte':
            dst_ds = driver.Create(fid2, len(grid2[0,::]), y_pixels, 1,  gdal.GDT_Byte  )
        #Set CoordinateFrame
        dst_ds.SetGeoTransform( [ x_min2, dx, 0, y_max, 0, -dy ] )  
        #Determine CoordinateSystem  
        srs = osr.SpatialReference()
        srs.SetWellKnownGeogCS("WGS 84")
        dst_ds.SetProjection( srs.ExportToWkt() )
        #Write Array
        dst_ds.GetRasterBand(1).WriteArray(grid2)
        #Set Coordinate Projection
        dst_ds.SetProjection( srs.ExportToWkt() )
    
        dst_ds = None
        
def RasterProfile(Domain,pts,ds=10,**args):
    """Export of GeoTiff file to specified path

    Args:
        Domain (tsupy.Domain class):    Path to shp-file e.g. 'C:/myfile.shp'
        pts (float Nx2):                point in x-y coordinates along which raster is created
        ds (float,optional):            Number of sampling points along profile
    Returns:
        vec (float Nx4):                1D vector of values of raster between pts-data
    """
    if 'gridname' in args: gridname=Domain['grids'].keys()[0]
    else: gridname=args['gridname']


    x_space=Domain['x_space']  
    y_space=Domain['y_space'] 
    grid=Domain['grids']['data'][gridname]
    num_seg=len(pts[:,0])-1 #Numer of individual profile segments
    
    aloc=0

    for i in range(num_seg):
            
        tr0=np.array([pts[i,:],pts[i+1,:]])
        #Get Angle of Crosssection

        dy=tr0[0,0]-tr0[1,0]
        dx=tr0[0,1]-tr0[1,1]
        ds=np.sqrt(dx**2+dy**2)

        
        vec=np.zeros((num_seg*ds,4))
        tr=np.copy(tr0)
        #Transform coordinates into grid-indizes
        for i in range(len(tr0)):
            
            dx=np.abs(tr0[i,1]-x_space)
            dy=np.abs(tr0[i,0]-y_space)
            ix,iy=np.where(dx==np.min(dx))[0],np.where(dy==np.min(dy))[0]
            tr[i,:]=iy,ix
        #Create along-line index coordinates
        x=np.linspace(tr[0,1],tr[1,1],ds)
        y=np.linspace(tr[0,0],tr[1,0],ds)
        #Map raster values along polyline
        profile0=ndimage.map_coordinates(grid,np.vstack((y,x)))
        #Create index vector
        x0=np.linspace(0+aloc,ds+aloc,len(profile0))
        aloc=np.max(x0) #update starting index
        
        #Update profile vector
        vec[i*ds:(i+1)*ds,0]=x0 #Along-line distance
        vec[i*ds:(i+1)*ds,1]=np.linspace(pts[i,0],pts[i+1,0],ds) #Longitude
        vec[i*ds:(i+1)*ds,2]=np.linspace(pts[i,1],pts[i+1,1],ds) #Latitude      
        vec[i*ds:(i+1)*ds,3]=profile0
    return vec
                
   
  
def vs30(Domain,regime='active',output='both',null=-32768):
    """Create Vs30 data from digitial elevation model

    Args:
        Domain (tsupy.Domain class):    Tsupy.Domain Class containing 'DEM'-grid
        regime (str, optional):         Definition of tectonic regime, selects conversion function
                                        active | stable
        output (str, optional):         Option of output format, either as 'grid','vector' or 'both' (same order)               
        null (float, optional):         Definition of zero-elevation or null-elevation, 
                                        values smaller null are ignored for vs30 and set to 180 mm/s
    Returns:
        vs30_grid:                      2D raster w.r.t. 'DEM'-grid with vs30-values
        vs30_vec:                       1D vector of values of raster between pts-data
    """
    dem_grid=Domain['grid']['DEM']
    dx=np.mean(Domain['spacing'])*100*1000
    index0=np.where(dem_grid<null)
    dem_grid[index0]=0
    vs30_grid=dem_grid/dx
    grad=np.gradient(vs30_grid)
    grad=np.sqrt(grad[0]**2+grad[1]**2)
    #Boundaries of vs30 values
    bound=[180,240,300,360,490,620,760]
    #Tectonic regimes after Allen and Wald 2009: 
    #On the Use of High-Resolution Topographic Data as a Proxy
    #for Seismic Site Conditions (VS30)
    if regime=='active':
        conv=[0.0003,0.0035,0.01,0.018,0.05,0.10,0.14]
    elif regime=='stable':
        conv=[0.000006,0.002,0.004,0.0072,0.013,0.018,0.025]

    index=np.where(grad<=conv[0])
    vs30_grid[index]=180
    index=np.where(grad>=conv[6])
    vs30_grid[index]=760
    for i in range(len(bound)-1):
        index=np.where((grad>conv[i]) & (grad<=conv[i+1]))
        vs30_grid[index]=bound[i]+(grad[index]-conv[i])/(conv[i+1]-conv[i])*(bound[i+1]-bound[i])
    
    #Smooth vs30-grid
    vs30_grid=ndimage.filters.gaussian_filter(vs30_grid,1)
    vs30_grid[index0]=1
    dx=np.mean(Domain['spacing'])
    x_space=np.linspace(Domain['longitude'][0]+dx/2,Domain['longitude'][1]-dx/2,len(vs30_grid[0,:]))
    y_space=np.flipud(np.linspace(Domain['latitude'][0]+dx/2,Domain['latitude'][1]-dx/2,len(vs30_grid[:,0])))
    
    
    vs30_vec=np.zeros((len(x_space)*len(y_space),3))

    for i in range(len(x_space)):
        vs30_vec[i*len(y_space):(i+1)*len(y_space),0]=x_space[i]
        vs30_vec[i*len(y_space):(i+1)*len(y_space),1]=y_space

        vs30_vec[i*len(y_space):(i+1)*len(y_space),2]=vs30_grid[:,i]

    print('Vs30-grid generated')
    
    if output=='both': return vs30_grid,vs30_vec
    if output=='grid': return vs30_grid
    if output=='vec':  return vs30_vec


def getCountryOutline(countries):
    if type(countries)==str:
        countries=[countries]
    
    cnt=ImportShapefile('..\\RLODIN_support\\GIS\\World.shp')
    num_shapes=len(cnt['data'])
    k=0
    for country in countries:
        for i in range(num_shapes):
  
            if cnt['data'][i][2]==country:
            
                index=np.where(cnt['pts'][:,0]==i+1)[0]
                if k==0:
                    cntry=cnt['pts'][index,:]
                    k+=1
                else:
                    cntry=np.vstack((cntry,cnt['pts'][index,:]))

    return cntry
    