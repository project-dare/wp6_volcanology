# -*- coding: utf-8 -*-
"""
Created on Tue Jun 02 14:46:25 2015

@author: schae
python setup.py build_ext --inplace
"""



#cython: boundscheck=False
#cython: wraparound=False

import numpy as np
cimport numpy as np


def cVectorMatrixAssign(np.ndarray[double, ndim=2] x, np.ndarray[double, ndim=2] y, np.ndarray[double, ndim=1] xx, np.ndarray[double, ndim=1] xy, np.ndarray[int, ndim=1] index, double res):
    cdef unsigned int i,j,k
    cdef float dx, dy
    for i in xrange(index[2],index[3]):
        for j in xrange(index[0], index[1]):
            for k in xrange(0,y.shape[0]-1):
                dx=abs(y[k,0]-xx[j])
                dy=abs(y[k,1]-xy[i])
                if dx<res and dy<res:
                    x[i,j]=y[k,2]
                    
    return x

def cFindCoastline(np.ndarray[double, ndim=2] c,np.ndarray[double, ndim=2] coast):
    cdef unsigned int i,j
    for i in xrange(1,c.shape[0]-1):
        for j in xrange(1, c.shape[1]-1):
            coast[i,j]=c[i,j]+0.25*(c[i-1,j]+c[i+1,j]+c[i,j-1 ]+c[i,j+1])
    
    return coast

def cCoast(np.ndarray[double, ndim=2] coast, np.ndarray[double, ndim=2] depth, np.ndarray[double, ndim=2] coastvector, np.ndarray[double, ndim=1] x_space, np.ndarray[double, ndim=1] y_space):
    cdef unsigned int i,j,k
    k=0
    for i in xrange(1,coast.shape[0]-1):
        for j in xrange(1, coast.shape[1]-1):
            if coast[i,j]>0:
                coastvector[k,0]=x_space[j]
                coastvector[k,1]=y_space[i]
                coastvector[k,2]=depth[i,j]
                k=k+1
    return coastvector

def cGridStretch(np.ndarray[double, ndim=2] grid_new, np.ndarray[double, ndim=2] grid_old, np.ndarray[double, ndim=1] x_space_new, np.ndarray[double, ndim=1] y_space_new, np.ndarray[double, ndim=1] x_space_old, np.ndarray[double, ndim=1] y_space_old, double r):
    cdef unsigned int i,j
    for i in xrange(0,grid_old.shape[0]):
        for j in xrange(0, grid_old.shape[1]):
            for k in xrange(0,int(r)):
                for l in xrange(0,int(r)):
                    grid_new[min(int(i*r)+k,grid_new.shape[0]-1),min(int(j*r)+l,grid_new.shape[1]-1)]=grid_old[i,j]
                    
    return grid_new


def cGridShrink(np.ndarray[double, ndim=2] grid_new, np.ndarray[double, ndim=2] grid_old, np.ndarray[double, ndim=1] x_space_new, np.ndarray[double, ndim=1] y_space_new, np.ndarray[double, ndim=1] x_space_old, np.ndarray[double, ndim=1] y_space_old, double r):
    cdef unsigned int i,j
    cdef float r0
    cdef float tmp
    for i in xrange(0,grid_new.shape[0]):
        for j in xrange(0, grid_new.shape[1]):
            tmp=0
            r0=0
            for k in xrange(-int(r/2),int(r/2)):
                for l in xrange(-int(r/2),int(r/2)):
                    tmp=tmp+grid_old[min(grid_old.shape[0]-1,max(0,int(i*r)+k)),min(grid_old.shape[1]-1,max(0,int(j*r)+l))]/max(1,((k**2+l**2)**0.5))
                    r0=r0+1.0/max(1,(k**2+l**2)**0.5)
            grid_new[i,j]=tmp/max(1,r0)
            #grid_new[i,j]=grid_old[i*r,j*r]
                    

    return grid_new




def cRasterDist(np.ndarray[double, ndim=2] dist,np.ndarray[double, ndim=1] x_space,np.ndarray[double, ndim=1] y_space, double x0, double y0):
    cdef unsigned int i,j
    for i in xrange(0,dist.shape[0]):
        for j in xrange(0, dist.shape[1]):
            dist[i,j]=np.sqrt((x0-x_space[j])**2+(y0-y_space[i])**2)
    
    return dist

