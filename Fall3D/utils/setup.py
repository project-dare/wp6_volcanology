# -*- coding: utf-8 -*-
"""
Created on Sun May 31 18:36:20 2015

@author: Andreas Schaefer
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy

ext = Extension("GeoC_Code", ["GeoC_Code.pyx"],
    include_dirs = [numpy.get_include()])
                
setup(ext_modules=[ext],
      cmdclass = {'build_ext': build_ext})