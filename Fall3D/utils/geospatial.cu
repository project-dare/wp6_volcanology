__global__ void GaussianBlur(float *h, float *h0, float ratio, float dx, float dy, int nx, int ny)
{
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = threadIdx.y + blockIdx.y * blockDim.y;
  
  int I = iy*nx+ix;

  
  int half = 15;
  float w_sum= 0;
  
  int index_x = I%nx;
  int index_y = I/nx -index_x/nx;
  int half1 = min(index_x,half);
  int half2 = min(nx-index_x-1,half);
  int half3 = min(index_y,half);
  int half4 = min(ny-index_y-1,half);
  float dist = 0;
  float weight = 0;
  float r = ratio;
  float x = index_x*dx;
  float y = index_y*dx;
  
  h[I]=0;
  if (I>=nx*ny){return;}
  if (h0[I]<=2.5) {
  for (int i = -half1; i <= half2; ++i)
  {
    for (int j=-half3; j <= half4; ++j)
    {
      dist=sqrt(pow(x-(x+i*dx),2)+pow(y-(y+j*dy),2));
      
      weight=2/(3.141*r)*exp(-2*dist*dist/r);
      
      w_sum=w_sum+weight;
      
      h[I]=h[I]+h0[I+i+nx*j]*weight;
      
    }
  }
  h[I]=h[I]/w_sum;
  } else{h[I]=h0[I];}
  
  
}

__global__ void GridLayering90M(float *grid1, float *grid2, int nx, int ny)
{
  
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = threadIdx.y + blockIdx.y * blockDim.y;
  
  int I = iy*nx+ix;
  
  if (I>=nx*ny){return;}
  
  if (grid1[I]>=-1) { grid1[I]=-1.0;}
  
  if (grid2[I]>0) {grid1[I]=grid2[I];}
  /*
  grid1[I]=grid2[I];
  */
}

__global__ void GridDamping(float *grid1, int nx, int ny)
{
  
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = threadIdx.y + blockIdx.y * blockDim.y;
  
  int I = iy*nx+ix;
  
  if (I>=nx*ny){return;}
  if (grid1[I]>=+25) { grid1[I]=25.0;}
  if (grid1[I]>=-25) { grid1[I]=grid1[I]-25.0*pow((grid1[I]+25)/50.0,2);}
  
  /*
  //grid1[I]=grid2[I];
  */
}

__global__ void GridShrink(float *grid_new, float *grid_old, float *x_space_new, float *y_space_new, float *x_space_old, float *y_space_old, float r, int nx, int ny, int nxo, int nyo)
{
  //Decrease resolution of grid_old to grid_new with ratio
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = threadIdx.y + blockIdx.y * blockDim.y;
  
  int I = iy*nx+ix;
  float r0=0.0;
  float tmp=0.0;
  int ij=0;
  int l=0;
  int k=0;
  if (I>=nx*ny){return;}
  
  
  for (int i = -r/2; i <= r/2; ++i)
  {
    for (int j=-r/2; j <= r/2; ++j)
    {
      k=I/nx*r+i; l=(I%nx)*r+j;
      ij=min(nyo,max(0,k))*nxo+min(nxo,max(0,l));
      tmp=tmp+grid_old[ij]/
        max(1.0,sqrt(pow(x_space_new[I%nx]-x_space_old[l],2)+pow(y_space_new[I/nx]-y_space_old[k],2)));
      r0=r0+1.0/(max(1.0,sqrt(pow(x_space_new[I%nx]-x_space_old[l],2)+pow(y_space_new[I/nx]-y_space_old[k],2))));
      
    }
  }
  grid_new[I]=tmp/r0;
}

__global__ void GridStretch(float *grid_new, float *grid_old, float r, int nx, int ny, int nxo, int nyo)
{
  //Increase resolution of grid_old to grid_new with resolution ratio r
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = threadIdx.y + blockIdx.y * blockDim.y;
  
  int I = iy*nx+ix;
  int ij=0;
  int l=0;
  int k=0;
  if (I>=nx*ny){return;}
  
  
  for (int i = 0; i <= r; ++i)
  {
    for (int j=0; j <= r; ++j)
    {
      k=I/nx*r+i; l=(I%nx)*r+j;
      ij=min(ny,max(0,k))*nx+min(nx,max(0,l));
      grid_new[ij]=grid_old[I];
    }
  }
  
}

__global__ void distance_pt2pt(float *dist, float *ptx, float *pty, float *x_space, float *y_space, int ex, int nx, int ny)
{
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = blockIdx.y;
  int idx = iy*nx +ix;    
    
  if (idx>=nx*ny){return;}
  
  dist[idx]=sqrt(pow( pty[ex]-x_space[idx%nx],2)+pow(ptx[ex]-y_space[idx/nx],2))*100;

}
  
__global__ void GaussSmooth(float *grid, float *dist, float r, int nx, int ny)
{
  unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int iy = blockIdx.y;
  int idx = iy*nx +ix;  
    
  if (idx>=nx*ny){return;}
    
  grid[idx]=2/(3.141*r*r)*exp(-2*dist[idx]*dist[idx]/(r*r));
}


