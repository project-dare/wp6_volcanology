# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 09:02:35 2018

@author: Regina

Program: SetSRC in Fall3D
         Python/Cython Version 2018
         
Purpose: This programm generates the source files needed by Fall3D
         (Corresponding to SetSrc.F90)         
"""

# import modules
import numpy as np
import os
import pickle
from Fall3D.source import Source

# create object of class source
s1 = Source(input_file, pathFall3D)

# loop over time instants (as in SetSrc.f90)
erumass = 0

for idt in range(s1['ndt']):

    # set start and end time of this eruptive phase  
    if s1['ndt'] == 1:
        iebeg1 = s1['erupt_start']
        ieend1 = s1['erupt_end']

    else:
        iebeg1 = s1['erupt_start'][idt]

        if (idt < s1['ndt'] - 1):
            # end of the eruptive phase is the start of the next eruptive phase
            ieend1 = s1['erupt_start'][idt + 1]
        else:
            ieend1 = np.amax(s1['erupt_end'])

    if (s1['source_type'] == 'POINT') or (s1['source_type'] == 'HAT') or (s1['source_type'] == 'SUZUKI'):
        if (s1['est_mfr'] == 'NONE') or (s1['est_mfr'] == 'MASTIN'):

            # No plume-wind coupling

            if s1['ndt'] > 1:
                m0 = s1['m0_dt'][idt]
                Hplume = s1['hplume'][idt]
            else:
                m0 = s1['m0_dt']
                Hplume = s1['hplume']

            # solve for source types
            if s1['source_type'] == 'POINT':
                s1.solvepoint(m0, Hplume)
            elif s1['source_type'] == 'HAT':
                s1.solvehat(m0, Hplume, idt)
            elif s1['source_type'] == 'SUZUKI':
                s1.solvesuzuki(m0, Hplume, idt)

            # write source file during a time step(getsrc.f90)
            s1.getsrc(idt)

            erumass = erumass + m0 * (ieend1 - iebeg1)


        elif (s1['est_mfr'] == 'WOODHOUSE') or (s1['est_mfr'] == 'DEGRUYTER'):

            if s1['ndt'] > 1:
                Hplume = s1['hplume'][idt]
            else:
                Hplume = s1['hplume']

            # plume-wind coupling

            # TODO: Improve for daily data (as in SetSRC.f90)
            s1.readmeteo(0, pathFall3D, input_file)

            # mass flow rate for Degruyter/Woodhouse
            s1.merwind(Hplume)

            m0 = s1['m0']

            # solve for source types
            if s1['source_type'] == 'POINT':
                s1.solvepoint(m0, Hplume)
            elif s1['source_type'] == 'HAT':
                s1.solvehat(m0, Hplume, idt)
            elif s1['source_type'] == 'SUZUKI':
                s1.solvesuzuki(m0, Hplume, idt)

            # write source file during a time step(getsrc.f90)
            s1.getsrc(idt)

            erumass = erumass + m0 * (ieend1 - iebeg1)

    if s1['source_type'] == 'RESUSPENSION':
        # read meteo data
        s1.readmeteo(pathFall3D, input_file)

        # solve for resuspension
        s1.par_ABL()
        s1.getsrc_resus(pathFall3D, input_file, idt)

        erumass = erumass + s1['MFR'] * s1['runtime']
        # TODO: check source term for resuspension

    if s1['source_type'] == 'PLUME':
        print('Source type plume not implemented yet.')

    # save source file as dict
f = open(os.path.join(os.getcwd(), "Source_Object.pkl"), 'wb')
pickle.dump(s1, f)
f.close()
