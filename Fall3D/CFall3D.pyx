# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 14:13:45 2018

@author: Regina
"""
# Type in console to cythonize this code: python setup.py build_ext --inplace

# import modules
import numpy as np
import scipy as sp
cimport numpy as np
import math
cimport cython
from libc.math cimport sin, cos, acos, exp, sqrt


def scalingfac(simulation,source):
    
    # function to calculate the scaling factor Hm1
    
    cdef double [:,:] Hm1 = np.zeros((simulation['ny'],simulation['nx']))
    cdef int j
    
    for j in range(simulation['ny']):
        lat   = simulation['latmin'] + j*source['dlat']    # index 0 is also southernmost point
        colat = (90-lat)*math.pi/180                       # colatitude in rad
        Hm1[j,:] = math.sin(colat)  
        
    return np.asarray(Hm1)




def get_par_ABL(u,v,nx,ny,z1,z0,Tz1,Tz0,Thz0,Thz1):
    
    # function to calculate parameters of planetary boundary layer
    
    cdef double K = 0.4       # Karman constant
    cdef double g = 9.8066
    
    # Monin Obukhov length
    cdef double  [:,:] rmonin = np.zeros((ny,nx))
        
    #friction velocity
    cdef double [:,:] ustar  = np.zeros((ny,nx))
    
    cdef int ix,iy
    
    cdef double umod,Ri,Gm,Gh,ustar1,thstar,rmonin1
    
    print('FALL3D: Calculate ABL parameters.')
    
    # calculate friction velocity and Monin Obhukov length
    for ix in range(nx):        
        
        for iy in range(ny):
                 
            umod = max(sqrt(u[iy,ix]*u[iy,ix]+ v[iy,ix]*v[iy,ix]),math.e**(-4))                 
                
            # bulk Richardson number
            Ri = g*(z1-z0)*(Thz1[iy,ix]-Thz0[iy,ix])/(umod*umod*0.5*(Tz0[iy,ix]+Tz1[iy,ix]))
                 
            # stable/unstable ABL       
                    
            if Ri >= 0:          # stable
                Gm = 1 + 4.7*Ri
                Gm = 1/(Gm*Gm)
                Gh = Gm
                     
            else:                # unstable
                Gm = math.log(z1/z0)
                Gm = 1 + (70*K*K*sqrt(abs(Ri)*z1/z0))/(Gm*Gm)
                Gh = 1 + (50*K*K*sqrt(abs(Ri)*z1/z0))/(Gm*Gm)
                Gm = 1 - ((9.4*Ri)/Gm)
                Gh = 1 - ((9.4*Ri)/Gh)  
                     
            # friction velocity    
            ustar1 = math.log(z1/z0)
            ustar1 = K*umod*sqrt(Gm)/ustar1 
                
            # thstar (Pr=1 assumed)
            thstar = math.log(z1/z0)
            thstar1 = K*K*umod*(Thz1[iy,ix]-Thz0[iy,ix])*Gh/(ustar1*thstar*thstar)
            thstar = max(thstar1,math.e**(-4))
                 
            # Monin Obukhow length
            rmonin1 = ustar1*ustar1*0.5*(Thz0[iy,ix]+Thz1[iy,ix])/(K*g*thstar)
                
            # write to 2D array
            rmonin[iy,ix] = rmonin1
            ustar[iy,ix]  = ustar1
                 
    print('FALL3D: ABL parameters calculated.')
    return (np.asarray(rmonin),np.asarray(ustar))




def diff_coeff_v(simulation,modkv,nx,ny,nz,vx,vy,vptemp,ustar,rmonin,zlayer,dz,zi): 
    
    # function to calculate vertical diffusion coefficients
    
    cdef double vkarm =  0.4                # von karman constant
    cdef double beta1 =  9.2                # empirical parameter from Ulke(2000)
    cdef double beta2 =  13.0               # empirical parameter from Ulke(2000)
    cdef double gi    =  9.81               # gravity
    cdef double alfa  =  0.28               # Smagorinsky parameter in L.E. Kh formula
    cdef double rlamb =  30.0               # parameter in turb. above ABL
    cdef double rkhmin=  100.0              # Minimum K_h value
    cdef double rkvmin=  1.0                # Minimum K_v value above the ABL
    cdef double hinf  =  5.0                # lowest  h value  (proof value)
    cdef double hsup  =  5000.0             # highest h value  (proof value)
    cdef double Khf   =  8000.0             # eddy diffusivity at a fixed resolution (CMAQ)
    
    cdef double [:,:,:] rkver = np.zeros((nz,ny,nx))    
    cdef int k,i,j
    
    if(modkv == 0):                       # constant        
        
        for k in range(nz):
            #print(k)
            for j in range(ny):
                for i in range(nx):
                    rkver[k,j,i] = simulation['rkv0']
                            
                            
    elif(modkv == 1):                     # similarity 
        
        for k in range(1,nz):
            for j in range(ny):
                for i in range(nx):
                                
                    if (k < nz-1):
                        dvdz2 = ((vx[k+1,j,i]-vx[k-1,j,i])/(dz[k]+dz[k-1]))**2 + ((vy[k+1,j,i]-vy[k-1,j,i])/(dz[k]+dz[k-1]))**2
                        dvdz2 = max(dvdz2,math.e**(-10))
                                    
                        rich  = gi*((vptemp[k+1,j,i]-vptemp[k-1,j,i])/(dz[k]+dz[k-1]))/(vptemp[k,j,i]*dvdz2)
                                   
                        h    = min(max(zi[j,i],hinf),hsup)   
                        zeta = zlayer[k]/h
                                
                        # inside the ABL
                        if (zeta < 1):
                            if (h/rmonin[j,i] >= 0):
                                # stable
                                rkver[k,j,i] = vkarm*ustar[j,i]*zlayer[k]*(1-zeta)/(1+beta1*zlayer[k]/rmonin[j,i])
                            else:
                                # unstable
                                rkver[k,j,i] = vkarm*ustar[j,i]*zlayer[k]*(1-zeta)*sqrt(1-beta2*zlayer[k]/rmonin[j,i])
                                
                        # above the ABL        
                        else:
                            lc = 1/((1/(vkarm*zlayer[k]))+(1/rlamb))
                                    
                            if rich >= 0:               # stable
                                rkver[k,j,i] = lc*lc*sqrt(dvdz2)/(1 + 10*rich*(1+8*rich))
                                rkver[k,j,i] = max(rkver[k,j,i],rkvmin)
                            else:                       # unstable
                                rkver[k,j,i] = lc*lc*sqrt(dvdz2)*sqrt(1-18*rich)
                                rkver[k,j,i] = max(rkver[k,j,i],rkvmin)
                                
                                
                                
    elif(modkv == 2):  # surface layer

        gh    = 11.6              # Stability constant
        betah =  7.8              # Stability constant
        prt   =  0.95             # Turbulent Prandtl number
        
        for k in range(1,nz):
            for j in range(ny):
                for i in range(nx):
                                
                    zeta = zlayer[k]/rmonin[j,i]
                                
                    if zeta < 0:   # unstable
                        rkver[k,j,i] = vkarm*ustar[j,i]*zlayer[k]*sqrt(1-gh*zeta)/prt
                    elif zeta > 0: # stable
                        rkver[k,j,i] = vkarm*ustar[j,i]*zlayer[k]/(prt+betah*zeta)
                    else:          # neutral (zeta = 0)
                        rkver[k,j,i] = vkarm*ustar[j,i]*zlayer[k]/prt
                                    
                    h = min(max(zi[j,i],hinf),hsup)
                    zeta = zlayer[k]/h
                                
                    if zeta > 1:
                        zeta = 1
                                
                    # above surface layer no diffusion 
                    rkver[k,j,i] =   rkver[k,j,i]*(1-zeta)
                    
    print('FALL3D: Vertical diffusion coefficient  \n Max: %s \n Min: %s  \n Avg: %s' %(np.amax(rkver), np.amin(rkver), np.sum(rkver)/(nx*ny*nz))) 
                  
    return np.asarray(rkver) 




def diff_coeff_h(simulation,modkh,nx,ny,nz,vx,vy,Hm1,dX1,dX2,dlon,dlat):
    
    #print('Horizontal diffusion coefficient.')
    
    cdef double vkarm =  0.4                # von karman constant
    cdef double beta1 =  9.2                # empirical parameter from Ulke(2000)
    cdef double beta2 = 13.0                # empirical parameter from Ulke(2000)
    cdef double gi    = 9.81                # gravity
    cdef double alfa  =  0.28               # Smagorinsky parameter in L.E. Kh formula
    cdef double rlamb = 30.0                # parameter in turb. above ABL
    cdef double rkhmin=  100                # Minimum K_h value
    cdef double rkvmin=  1.0                # Minimum K_v value above the ABL
    cdef double hinf  =  5.0                # lowest  h value  (proof value)
    cdef double hsup  =  5000               # highest h value  (proof value)
    cdef double Khf   = 8000                # eddy diffusivity at a fixed resolution (CMAQ)
    
    cdef double [:,:,:] rkhor = np.zeros((nz,ny,nx))
    
    cdef int k,i,j
    
    # horizontal component                
    if(modkh == 0):          # constant
                   
        for k in range(nz):            
            for j in range(ny):
                for i in range(nx):
                    rkhor[k,j,i] = simulation['rkh0']
                    
                    
    
    elif(modkh == 1):          # RAMS
        
        Csh = simulation['Csh']
                    
        for k in range(1,nz):
            for j in range(ny):
                for i in range(nx):
                                
                    dxx = Hm1[j,i]*dX1
                    
                    if i == 0:
                        dvxdx = (vx[k,j,1] - vx[k,j,0])/dxx 
                        dvydx = (vy[k,j,1] - vy[k,j,0])/dxx 
                                    
                    elif (i == nx-1):
                        dvxdx = (vx[k,j,nx-1] - vx[k,j,nx-2])/dxx
                        dvydx = (vy[k,j,nx-1] - vy[k,j,nx-2])/dxx
                                    
                    else:
                        dvxdx = (vx[k,j,i+1] - vx[k,j,i-1])/(2.0*dxx)
                        dvydx = (vy[k,j,i+1] - vy[k,j,i-1])/(2.0*dxx)
                                    
                                
                    if j==0:
                        dvxdy = (vx[k,1,i] - vx[k,0,i])/dX2
                        dvydy = (vy[k,1,i] - vy[k,0,i])/dX2
                                    
                    elif j == ny-1:
                        dvxdy = (vx[k,ny-1,i] - vx[k,ny-2,i])/dX2
                        dvydy = (vy[k,ny-1,i] - vy[k,ny-2,i])/dX2
                                    
                    else:
                        dvxdy = (vx[k,j+1,i] - vx[k,j-1,i])/(2.0*dX2)
                        dvydy = (vy[k,j+1,i] - vy[k,j-1,i])/(2.0*dX2)
                                    
                    # Parameter in RAMS formula
                    Kmh   = 0.075*((dxx*dX2)**(2.0/3.0))
                    rkhor[k,j,i] = Csh*Csh*dxx*dX2*sqrt((dvxdy+dvydx)**2 + 2.0*(dvxdx**2+dvydy**2))
                                
                    #  assume Pr=1
                    rkhor[k,j,i] = 1.0*max(rkhor[k,j,i],Kmh)
                    rkhor[k,j,i] = max(rkhor[k,j,i],rkhmin)
                                                
                                  
                                        
    elif (modkh == 2):            # CMAQ
        # For large grid sizes counteract the numerical diffusion
        # It is assumed Kf=8000 m2/s for Dx=4km = 3.6d-2 deg of meridian
        #  (3.6d-2/sin(colat) deg of parallel)
                    
        Khn0 = 3.6 *10.0**(-2)  # in deg
                    
        for k in range(1,nz):
            for j in range(ny):
                for i in range(nx):
                                
                    dxx = Hm1[j,i]*dX1
                                
                    if i == 0:
                        dvxdx = (vx[k,j,1] - vx[k,j,0])/dxx 
                        dvydx = (vy[k,j,1] - vy[k,j,0])/dxx                                    
                                    
                    elif (i == nx-1):
                        dvxdx = (vx[k,j,nx-1] - vx[k,j,nx-2])/dxx
                        dvydx = (vy[k,j,nx-1] - vy[k,j,nx-2])/dxx                                    
                                    
                    else:
                        dvxdx = (vx[k,j,i+1] - vx[k,j,i-1])/(2.0*dxx)
                        dvydx = (vy[k,j,i+1] - vy[k,j,i-1])/(2.0*dxx)
                                    
                    if j==0:
                        dvxdy = (vx[k,1,i] - vx[k,0,i])/dX2
                        dvydy = (vy[k,1,i] - vy[k,0,i])/dX2
                                    
                    elif j == ny-1:
                        dvxdy = (vx[k,ny-1,i] - vx[k,ny-2,i])/dX2
                        dvydy = (vy[k,ny-1,i] - vy[k,ny-2,i])/dX2
                                    
                    else:
                        dvxdy = (vx[k,j+1,i] - vx[k,j-1,i])/(2.0*dX2)
                        dvydy = (vy[k,j+1,i] - vy[k,j-1,i])/(2.0*dX2)
                                    
                    Kht = alfa**2*dxx*dX2*sqrt( (dvxdx-dvydy)**2 + (dvydx+dvxdy)**2 )
                    Khn = Khf*(Khn0/Hm1[j,i]/dlon)*(Khn0/dlat)                            # warum hier Hm1 wenn man deg betrachtet?                                
                                
                    rkhor[k,j,i] = (1.0/Kht) + (1.0/Khn)
                    rkhor[k,j,i] = max(1/rkhor[k,j,i],rkhmin)
    
    print('FALL3D: Horizontal diffusion coefficient  \n Max: %s \n Min: %s  \n Avg: %s' \
          %(np.amax(rkhor), np.amin(rkhor), np.sum(rkhor)/(nx*ny*nz))) 
    
    return np.asarray(rkhor)



# settling velocity fastening
def set_vsett(simulation,npar,nx,ny,nz,nc):
    
    tgsd = simulation['tgsd']
    modv = simulation['modv']
    topog= simulation['topog']
    dz   = simulation['dz']
    
    temp = simulation['temp']
    rho  = simulation['rho']
    
    vsett = set_vset_c(temp,rho,tgsd,modv,topog,dz,npar,nc,nx,ny,nz)    
    
    # correct aggregated class
    if simulation['aggregation']:
        
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                    
                    vsett[k,j,i,npar-1] =  vsett[k,j,i,npar-1]*simulation['vset_aggr']
                    
    # sign criteria
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                for ic in range(nc):
    
                    vsett[k,j,i,ic] = -vsett[k,j,i,ic]
                    
                    
    vset_max = round(np.amax(np.asarray(vsett)),3)
    vset_min = round(np.amin(np.asarray(vsett)),3)
    vset_av  = round(np.sum(np.asarray(vsett))/(nx*ny*nz*nc),3)
            
    print('Max: %s \n Min: %s \n Avg: %s' %(vset_max, vset_min,vset_av))

    return np.asarray(vsett)     

   
# C function    
cdef double [:,:,:,:] set_vset_c(double [:,:,:] temp, double [:,:,:] rho, double [:,:] tgsd, double modv, double [:,:] topog, double [:] dz, int npar,int nc, int nx, int ny, int nz):

    # Standard air properies at sea level  
    cdef double visc0 = 1.827*10.0**(-5)  # reference viscosity
    cdef double Tair0 = 291.15            # reference temperature 
    cdef double g = 9.81
    
    cdef double[:,:,:,:] vsett = np.zeros((nz,ny,nx,nc))
    
    cdef int k,i,j,ic
    cdef double rk1,rk2
    cdef double visc,cd,vset,vold,rey  
    
    cdef double atol = 10.0**(-12)            # Absolute tolerance
    cdef double rtol = 10.0**(-6)             # Relative tolerance
    cdef int    maxit= 2000                   # Maximum number of iterations
    
    cdef int counter = 0
    
    for ic in range(npar):             # gasses have zero settling velocities
        # read particle class properties
        
        diam = tgsd[ic,0]
        
        diam = diam/1000                # convert to m
        
        rhop = tgsd[ic,1]               # in kg/m^3
        
        psi  = tgsd[ic,2]               # shape factor
        
        for i in range(nx):
            for j in range(ny):
                
                z = topog[j,i] - dz[0]
                
                # loop on the layers
                for k in range(nz):
                    
                    z = z + dz[k]   # ground level for k=0, actually this is not needed here
                    T = temp[k,j,i]
                    rhoa = rho[k,j,i]  
                    
                    # Sutherland´s law
                    visc = visc0*((Tair0+120.0)/(T+120.0))*((T/Tair0)**1.5)    # T in K
                    
                    cd   = 1.0                      # Guess value
                    
                    # Arastoopour et al. 1982
                    if modv == 1:
                                   
                        vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                        vold=vset
                        
                        for it in range(maxit):
                            rey=rhoa*vset*diam/visc
                                        
                            if(rey <= 988.947): # This is the actual transition point
                                cd=24.0/rey*(1.0+0.15*rey**0.687)
                                
                            else:
                                cd = 0.44                               
                                
                            vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                            
                            if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                reynolds = rey
                                cdrag = cd
                                break
                                        
                            vold = vset
                            
                            if it == maxit - 1:
                                counter = counter + 1
                    
                        vsett[k,j,i,ic] = vset
                        
                        
                    # Ganser 1993
                    if modv == 2:
                        
                        vset=sqrt(4.0*g*diam*(rhop-rhoa)/(3.0*cd*rhoa))  
                        vold=vset                       
                        
                        
                        for it in range(maxit):
                            
                            rey=rhoa*vset*diam/visc
                            rk1=3.0/(1.0+2.0/sqrt(psi))                 # Stokes' shape factor
                            rk2=10.0**(1.8148*(-np.log10(psi))**0.5743) # Newton's shape factor
                            
                                        
                            cd=24.0/(rey*rk1)*(1.0+0.1118*(rey*rk1*rk2)**0.6567) + 0.4305*rk2/(1.0+3305.0/(rey*rk1*rk2))
                            
                            vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                            
                            if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                reynolds = rey
                                cdrag = cd
                                break
                                            
                            vold = vset 
                            
                            if it == maxit - 1:
                                counter = counter + 1
                        
                        vsett[k,j,i,ic] = vset
                        
                        
                    # Wilson & Huang 1979        
                    if modv == 3: 
                        
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                             
                             cd=24.0/rey*psi**(-0.828)+2.0*sqrt(1.07-psi)
                             
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag = cd
                                 break
                                           
                             vold = vset
                             
                             if it == maxit - 1:
                                counter = counter + 1
                         
                         vsett[k,j,i,ic] = vset
                         
                         
                         
                    # Dellino et al. 2005
                    if modv == 4:
                        
                         Ar   = g*diam**3*(rhop-rhoa)*rhoa/visc**2 # Archimedes number
                         vset = 1.2065*visc/(rhoa*diam)*(Ar*psi**1.6)**0.5206
                         
                         vsett[k,j,i,ic] = vset
                         
                         # old implementation
                         #vset = ((diam**3*g*rhop*rhoa*(psi**1.6))/(visc**2))**0.5206
                         #vset = (1.2065*visc*vset)/(diam*rhoa)
                           
                         
                    # Pfeiffer et al. 2005    
                    if modv == 5:
                        
                         # not noted in the manual
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         cd100=0.24*psi**(-0.828)+2.0*sqrt(1.07-psi) # cd at rey=100
                         cd1000=1.0                                       # cd at rey>=1000 (from Pfeiffer et al., 2005)
                         # cd1000=1.0-0.56*psi
                         
                         a=(cd1000-cd100)/900.0
                         b=cd1000-1000.0*a
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                                        
                             if(rey <= 100.0):
                                 cd=24.0/rey*psi**(-0.828)+2.0*sqrt(1.07-psi)
                             elif (rey > 100.0 and rey < 1000.0):
                                 cd=a*rey+b
                             else: 
                                 cd = cd1000
                                 
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))  
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag = cd
                                 break
                                 
                             vold = vset
                             
                             if it == maxit - 1:
                                counter = counter + 1
                             
                         vsett[k,j,i,ic] = vset   
                         
                         
                     # Dioguardi et al. 2017
                    if modv == 6:
                         
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                             
                             # drag coefficient of a sphere (Re < 30000)
                             cd = 24.0*(1.0 + 0.15*rey**0.687)/rey + 0.42/(1.0 + 42500.0 /rey**1.16)
                             # Note: the Reynolds number is limited to 10^-2 in the exponent
                             # to avoid the strong divergence near Re=0. This limit is compatible
                             # with the range of Reynolds numbers investigated in the experiments
                             # of Dioguardi et al., 2017.  psi is the sphericity
                             
                             cd = 0.74533*rey**0.146012*cd / psi**(0.5134/max(rey,0.01)**0.2)
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag =cd
                                 break
                                 
                             vold = vset
                             
                             if it == maxit - 1:
                                counter = counter + 1
                             
                         vsett[k,j,i,ic] = vset
                         
                         
                    # Dioguardi et al. 2018
                    if modv == 7:
                         
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                             
                             cd = 24.0/rey*(((1.0-psi)/rey+1.0)**0.25) + 24.0/rey*(0.1806*rey**0.6459)*psi**(-rey**0.08) + 0.4251/(1.0 + 6880.95/rey*psi**5.05)
                                        
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag = cd
                                 break
                                            
                             vold = vset
                             
                             if it == maxit - 1:
                                counter = counter + 1
                             
                         vsett[k,j,i,ic] = vset     
                        
    print('FALL3D: No divergence achieved in %s of %s calculations (%s %%)' %(counter,nx*ny*nz*npar,round(counter/(nx*ny*nz*npar)*100,1)))                    
    return vsett



def set_vset(simulation,npar,nx,ny,nz,nc):
    
    # calculate settling velocity (old version)
    
    # Standard air properies at sea level  
    cdef double visc0 = 1.827*10.0**(-5)  # reference viscosity
    cdef double Tair0 = 291.15          # reference temperature 
    cdef double g = 9.81
    
    cdef double[:,:,:,:] vsett = np.zeros((nz,ny,nx,nc))
    
    cdef int k,i,j,ic,maxit
    cdef double rk1,rk2
    cdef double visc,atol,rtol,cd,vset,vold,rey
    
    
    for ic in range(npar):             # gasses have zero settling velocities
        print(ic)
         
        # read particle class properties           
        tgsd = simulation['tgsd']
        
        diam = tgsd[ic,0]
        
        diam = diam/1000                # convert to m
        
        rhop = tgsd[ic,1]
        
        psi  = tgsd[ic,2]
        
        
        
        for i in range(nx):
            for j in range(ny):
                            
                z = simulation['topog'][j,i] - simulation['dz'][0]
                            
                # loop on the layers
                for k in range(nz):
                    
                    z = z + simulation['dz'][k]
                    T = simulation['temp'][k,j,i]
                    rhoa = simulation['rho'][k,j,i]                    
                
                    
                    # Sutherland´s law
                    visc = visc0*((Tair0+120.0)/(T+120.0))*((T/Tair0)**1.5)
                    
                    atol = math.e**(-12)            # Absolute tolerance
                    rtol = math.e**(-6)             # Relative tolerance
                    maxit= 1000                     # Maximum number of iterations
                    cd   = 1.0                      # Guess value
                    
                    # Arastoopour et al. 1982
                    if simulation['modv'] == 1:
                        #print(rhoa)            
                        vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                        vold=vset
                        
                        for it in range(maxit):
                            rey=rhoa*vset*diam/visc
                                        
                            if(rey <= 988.947):
                                cd=24.0/rey*(1.0+0.15*rey**0.687)
                                
                            else:
                                cd = 0.44                               
                                
                            vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                            
                            if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                reynolds = rey
                                cdrag = cd
                                        
                            vold = vset
                    
                        vsett[k,j,i,ic] = vset
                    
                    
                    # Ganser 1993
                    if simulation['modv'] == 2:
                        
                        vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))  
                        vold=vset                       
                        
                        
                        for it in range(maxit):
                            rey=rhoa*vset*diam/visc
                            rk1=3.0/(1.0+2.0/sqrt(psi))            # Stokes' shape factor
                            rk2=10.0**(1.8148*(-np.log10(psi))**0.5743) # Newton's shape factor
                            
                            #print(psi)
                            #print(rey)
                            #print(rk1)
                            #print(rk2)
                                        
                            cd=24.0/(rey*rk1)*(1.0+0.1118*(rey*rk1*rk2)**0.6567) + 0.4305*rk2/(1.0+3305.0/(rey*rk1*rk2))
                            
                            vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                            
                            if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                reynolds = rey
                                cdrag = cd
                                            
                            vold = vset    
                        
                        vsett[k,j,i,ic] = vset
                        
                        
                        
                    # Wilson & Huang 1979        
                    if simulation['modv'] == 3: 
                        
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                             cd=24.0/rey*psi**(-0.828)+2.0*sqrt(1.07-psi)
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag = cd
                                           
                             vold = vset
                         
                         vsett[k,j,i,ic] = vset
                         
                     
                    # Dellino et al. 2005
                    if simulation['modv'] == 4:
                        
                         Ar   = g*diam**3*(rhop-rhoa)*rhoa/visc**2 #Archimedes number
                         vset = 1.2065*visc/(rhoa*diam)*(Ar*psi**1.6)**0.5206
                         
                         vsett[k,j,i,ic] = vset
                         #vset = ((diam**3*g*rhop*rhoa*(psi**1.6))/(visc**2))**0.5206
                         #vset = (1.2065*visc*vset)/(diam*rhoa)
                                    
                         # TODO: Implementation of model (not done yet for Fall3D)
                         #reynolds = rey
                         #cdrag = cd
                         #print('Dellino model not implemented in Fall3D yet. Please choose other model.')
                         
                       
                        
                        
                    # Pfeiffer et al. 2005    
                    if simulation['modv'] == 5:
                         # not noted in the manual
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         cd100=0.24*psi**(-0.828)+2.0*sqrt(1.07-psi) # cd at rey=100
                         cd1000=1.0                                       # cd at rey>=1000 (from Pfeiffer et al., 2005)
                         # cd1000=1.0-0.56*psi
                         
                         a=(cd1000-cd100)/900.0
                         b=cd1000-1000.0*a
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                                        
                             if(rey <= 100.0):
                                 cd=24.0/rey*psi**(-0.828)+2.0*sqrt(1.07-psi)
                             elif (rey > 100.0 and rey < 1000.0):
                                 cd=a*rey+b
                             else: 
                                 cd = cd1000
                                 
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))  
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag = cd
                                 
                             vold = vset
                             
                         vsett[k,j,i,ic] = vset
                         
                         
                    # Dioguardi et al. 2017
                    if simulation['modv'] == 6:
                         
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                                         
                             cd = 24.0*(1.0 + 0.15*rey**0.687)/rey + 0.42/(1.0 + 42500.0 /rey**1.16)
                             # Note: the Reynolds number is limited to 10^-2 in the exponent
                             # to avoid the strong divergence near Re=0. This limit is compatible
                             # with the range of Reynolds numbers investigated in the experiments
                             # of Dioguardi et al., 2017.  psi is the sphericity
                             
                             cd = 0.74533*rey**0.146012*cd / psi**(0.5134/max(rey,0.01)**0.2)
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag =cd
                                 
                             vold = vset
                             
                         vsett[k,j,i,ic] = vset
                         
                         
                         
                    # Dioguardi et al. 2018
                    if simulation['modv'] == 7:
                        
                         psi = 0.83*psi # transform sphericity to particle shape factor
                         
                         vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                         vold=vset
                         
                         for it in range(maxit):
                             rey=rhoa*vset*diam/visc
                             
                             cd = 24.0/rey*(((1.0-psi)/rey+1.0)**0.25 + 24.0/rey*(0.1806*rey**0.6459)*psi**(-rey**0.08)) + 0.4251/(1.0 + 6880.95/rey*psi**5.05)
                                        
                             vset=sqrt(4.0*g*diam*rhop/(3.0*cd*rhoa))
                             
                             if(abs(vset-vold) <= atol+rtol*abs(vset)):
                                 reynolds = rey
                                 cdrag = cd
                                            
                             vold = vset
                             
                         vsett[k,j,i,ic] = vset 
                         
                         
    if simulation['aggregation']:
        
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                    
                    vsett[k,j,i,npar-1] =  vsett[k,j,i,npar-1]*simulation['vset_aggr']
                    
    # sign criteria
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                for ic in range(nc):
    
                    vsett[k,j,i,ic] = -vsett[k,j,i,ic]

    vset_max = round(np.amax(np.asarray(vsett)),3)
    vset_min = round(np.amin(np.asarray(vsett)),3)
    vset_av  = round(np.sum(np.asarray(vsett))/(nx*ny*nz*nc),3)
            
    print(' Max: %s \n Min: %s \n Avg: %s' %(vset_max, vset_min,vset_av))

    return np.asarray(vsett)     









def set_vdry(simulation,imode):
    # dry settling velocity
   
    # Standard air properies at sea level  
    cdef double visc0 = 1.827*10.0**(-5)  # reference viscosity
    cdef double Tair0 = 291.15            # reference temperature 
    cdef double g = 9.81
    cdef double vkarm = 0.4
    
    ustar = simulation['ustar']
    temp  = simulation['temp']  # in K
    rho   = simulation['rho']   # in kg/m^3
    
    dz = simulation['dz']
    nx = simulation['nx']
    ny = simulation['ny']
    nc = simulation['nc']
    
    a = simulation['a']
    b = simulation['b']
    
    cdef double [:,:,:] vdep = np.zeros((ny,nx,nc))
    
    cdef int iy,ix,ic
    cdef int iland
    
    cdef double ra,T,visc,Re,Vd1,Vd
    
    
    # loop over domain
    for iy in range(ny):
        for ix in range(nx):
                        
            iland = 6 #TODO: implement real data (Land use)
                        
            # Compute aerodynamic resistance. Since this is computed at
            # z=z0 we make no disctintion between stable, neutral and unstable
                        
            if ustar[iy,ix] > 0:
                ra = 1.0/(vkarm*ustar[iy,ix])
            else:
                ra = 10.0**(20)   # prevent overflow
                
            # Air viscosity (Sutherland's law)
            T = 0.5*(temp[0,iy,ix]+temp[1,iy,ix])
            visc = visc0*((Tair0+120.0)/(T+120.0))*((T/Tair0)**1.5)
            Re = ustar[iy,ix]*dz[0]*0.5*(rho[0,iy,ix]+rho[1,iy,ix])/visc
            
#            if ix == 5 and iy == 5:
#                print(T)
#                print(visc)
#                print(Re)
#                print(ra) 
                
            # Fist term of deposition velocity
            Vd1 = -0.5*((Re-40300.0)/15330.0)*((Re-40300.0)/15330.0)
            Vd1 = 0.0226*ustar[iy,ix]*exp(Vd1)
            
#            if ix == 5 and iy == 5:
#                print(Vd1)
                
            # Loop over classes
            for ic in range(nc):
                
                if imode[ic] > 0:
                    Vd = Vd1 + a[iland-1,int(imode[ic])-1]*(ustar[iy,ix]**b[iland-1,int(imode[ic])-1])
                    Vd = ra + 1.0/Vd
                    
#                    if ix == 5 and iy == 5:
#                        print(-1/Vd)
                    
                    
                    vdep[iy,ix,ic] = -1.0/Vd   # sign criteria
                                
                        
    return np.asarray(vdep)



def apply_scaling(simulation):
    
    # apply scaling factors to arrays (vx,rho,rkh1)
    
    cdef int i,j,k
    
    nx = simulation['nx']
    ny = simulation['ny']
    nz = simulation['nz']
    
    Hm1 = simulation['Hm1']
    
    cdef double [:,:,:] rkh1 = np.zeros((nz,ny,nx))

    vx    = simulation['vx']
    rho   = simulation['rho']
    rkhor = simulation['rkhor']       
    
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                
                vx[k,j,i] = vx[k,j,i]/Hm1[j,i]
                rho[k,j,i] = rho[k,j,i]*Hm1[j,i]
                
                rkh1[k,j,i] = rkhor[k,j,i]/(Hm1[j,i]*Hm1[j,i])
                
                
    return (np.asarray(vx), np.asarray(rho),np.asarray(rkh1))


def add_radial_wind(simulation,time):
    
    # add radial wind (for gravity currrent option)
    # not tested yet
    
    cdef int i,k,j
    
    Hm1 = simulation['Hm1']
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    vx = simulation['vx']
    vy = simulation['vy'] 
    
    zs = simulation['zs']
    
    zvent = simulation['zvent']
    
    xorigr = simulation['lonmin']
    yorigr = simulation['latmin']
    
    dlon = simulation['dlon']
    dlat = simulation['dlat']
    
    c_flow_rate =  simulation['c_flow_rate']
    k_entrain   =  simulation['k_entrain']
    brunt_vaisala =  simulation['brunt_vaisala']
    lambda_grav =  simulation['lambda_grav']
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    zlayer = simulation['zlayer']
    
    cdef double min_radius,vol_flow_rate,c_gravr,radius_grav,c_gravu,c_grav_factor,th_grav    
    
    min_radius = 1.5*sqrt(Hm1[0,0]*dX1*Hm1[0,0]*dX1+dX2*dX2)    # Proportional to diagonal of the cell
                
    wind_center_x = simulation['xvent']
    wind_center_y = simulation['yvent']
    
    time_eruption = time - simulation['beg_time']*3600 # in s
    
    mass_flow_rate = np.sum(simulation['tmrat'])
    
    # check if source term is on
    if mass_flow_rate > 0.1:
        vol_flow_rate = c_flow_rate*sqrt(k_entrain)*mass_flow_rate**0.75/(brunt_vaisala**0.625)
                   
        c_gravr     = 3.0*lambda_grav*brunt_vaisala*vol_flow_rate/(2.0*math.pi)   # constant
        radius_grav = (c_gravr*time_eruption**2)**(0.33333)                  # Radius(t) of the front
        radius_grav = max(radius_grav,min_radius)                            # Avoid division by zero
        
        c_gravu       = sqrt(2.0*lambda_grav*brunt_vaisala*vol_flow_rate/(3.0*math.pi))
        c_grav_factor = 0.75*c_gravu*sqrt(radius_grav)
        th_grav       = c_gravu/(sqrt(radius_grav)*lambda_grav*brunt_vaisala)     # Thickness
        
        # read column height
        for isrc in range(simulation['plume_ns']):
            zs[isrc] = zs[isrc] - zvent
        
        h_tot  = np.amax(zs)                     # Maximum column height (top of source)
        h_umbr = h_tot/1.32                      # Height of NBL (empirical). This is consistent with the plume model in SetSrc
         
        hmin   = max(h_umbr-0.5*th_grav,0.5*h_tot) # Higher that H_tot/2 (empirical)
        hmax   = min(h_umbr+0.5*th_grav,h_tot)     # Lower than total column height
         
        # Find nodes of the center of the umbrella
        i_wind = int((wind_center_x - simulation['lonmin'])/simulation['dlon']) #TODO: momentan wird hier abgerundet
        j_wind = int((wind_center_y - simulation['latmin'])/simulation['dlat'])
        i_wind = max(1,i_wind)
        j_wind = max(1,j_wind)         
         
        k_wind = 0
        store  = simulation['zlayer'][nz-1]  # A large value
         
        for k in range(nz):
            dist = abs(zlayer[k] - h_umbr)
                            
            if(dist <= store):
                k_wind = k
                store  = dist
                 
        # horizontal wind at NBL above the crater
        v_wind  = sqrt(vx[k_wind,j_wind,i_wind]**2 + vy[k_wind,j_wind,i_wind]**2)
         
        # Radius_grav(Tp) where Tp=64/27*c_gravr/v^3. Transition at Ri=025 for passive transport transition
         
        Tp = 64.0/27.0*c_gravr/max(v_wind**3,0.1)       # Fully passive time scale (s)
        Tb = Tp/8.0                                     # density driven time scale (s)
        max_radius = 1.7777*c_gravr/max(v_wind**2,0.1)         
         
        # Find kmin and kmax (lower and upper z-index of the umbrella region)
                    
        kmin = 0
        store = zlayer[nz-1]  #  A large value
                    
        for k in range(nz):
            dist = abs(zlayer[k] - hmin)
            if(dist <= store):
                kmin = k
                store = dist
                 
        kmax = nz-1
        store = zlayer[nz-1]        
                 
        for k in range(nz):
            dist = abs(zlayer[k] - hmax)
            if(dist <= store):
                kmax = k
                store = dist
                 
         
        # add radial wind
        for j in range(ny):
            for i in range(nx):
                deltax = dX1*Hm1[j,i]*((xorigr + i*dlon)-wind_center_x)/dlon       # distance (in m)
                deltay = dX2         *((yorigr + j*dlat)-wind_center_y)/dlat
                radius = sqrt(deltax*deltax + deltay*deltay)                  # in meters
                 
                if(abs(radius) <= min_radius ): continue                           # Skip for R <= Rmin
                if(abs(radius) >  max_radius ): continue                           # Skip for R >  Rmin
                 
                rvel = c_grav_factor/radius*(1.0+radius**2 /(3.0*radius_grav**2))
                 
                for k in range(kmin,kmax+1):
                     
                    vx[k,j,i] =  vx[k,j,i] + rvel*deltax/radius/Hm1[j,i]
                    vy[k,j,i] =  vy[k,j,i] + rvel*deltay/radius
                     
                     
    return (np.asarrax(vx),np.asarray(ny))



def setdivu(simulation):
    
    # calculate divergence of velocity field
    
    cdef int i,j,k

    # velocity field divergence (Nabla * u)
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx'] 

    vx = simulation['vx']
    vy = simulation['vy']
    vz = simulation['vz']  

    Hm1 = simulation['Hm1']
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']

    dz = simulation['dz']                   
                 
    cdef double [:,:,:] divu = np.zeros((nz,ny,nx))  
    
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                
                vi = vx[k,j,i]
                vi_1 = vx[k,j,max(0,i-1)]
                vj = vy[k,j,i]
                vj_1 = vy[k,max(0,j-1),i]
                vk = vz[k,j,i]
                vk_1 = vz[max(0,k-1),j,i]
                
                divu[k,j,i] = (vi - vi_1)/(Hm1[j,i]*dX1) + (vj - vj_1)/dX2 + (vk - vk_1)/dz[k]
                
    return (np.asarray(divu))  

def finddt(simulation):
    
    cdef int i,j,k,ic
    
    #TODO: integrate in input file?
    safety_factor  = 0.7
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx'] 
    nc = simulation['nc']
    
    rkhor = simulation['rkhor']
    rkver = simulation['rkver']
    rkh1  = simulation['rkh1']
    
    vx = simulation['vx']
    vy = simulation['vy']
    vz = simulation['vz']
    
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    rkh1 = simulation['rkh1']
    
    dz = simulation['dz'] 
    
    vsett = simulation['vset']

    cdef double dtcn = 0
    cdef double R8min = 10.0**(-13)   

    for ic in range(nc):
        for k in range(nz):
            if k == 0:
                ddz = dz[0]
            else:
                ddz = min(dz[k-1],dz[k])
                
            for j in range(ny):
                for i in range(nx):
                    if(k == 0):
                        dvsetdz = (vsett[1,j,i,ic]-vsett[0,j,i,ic])/(dz[0]) 
                    elif (k == nz-1):
                        dvsetdz = (vsett[nz-1,j,i,ic]-vsett[nz-2,j,i,ic])/(dz[nz-2])
                    else:
                        dvsetdz = (vsett[k+1,j,i,ic]-vsett[k-1,j,i,ic])/(dz[k]+dz[k-1])
                        
                    dtcni = (2.0*rkh1[k,j,i]/(dX1**2.0))+(abs(vx[k,j,i])/dX1)   + \
                            (2.0*rkhor[k,j,i]/(dX2**2.0))+(abs(vy[k,j,i])/dX2)  + \
                            (2.0*rkver[k,j,i]/(ddz**2.0))                     + \
                            (abs((vz[k,j,i]+vsett[k,j,i,ic]))/ddz)            + \
                            abs(dvsetdz) 
                            
                    dtcn=max(dtcn,dtcni)         
                
    dtcn=max(dtcn,R8min)
    
    # critical time step
    dtc = 1.0/dtcn     

    # time step
    dt = safety_factor*dtc
    
    return dt






def agrpar(simulation):
    
    # subroutine agrpar.f90 
    
    iaggr = simulation['iaggr']
    npar  = simulation['npart'] 
    ngas  = simulation['ngas'] 
    
    dz = simulation['dz']
    fc = simulation['tgsd'][:,3]
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    Hm1 = simulation['Hm1']
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    tplume = simulation['tplume']
    cdef double [:,:,:] temp   = simulation['temp']
    rho    = simulation['rho']
    qv     = simulation['qv']
    pres   = simulation['pres']
    vx     = simulation['vx']
    vy     = simulation['vy']
    vz     = simulation['vz']
    
    vsett = simulation['vset']
    
    c = simulation['c']
    
    tgsd = simulation['tgsd']
    
    vset_factor = simulation['vset_factor']
    
    
    vsett,Alagr,Abagr,Asagr,Adagr,Atagr = agrpar_c(vset_factor,tgsd,c,vsett,vx,vy,vz,pres,qv,rho,temp,tplume,dX2,dX1,Hm1,nz,ny,nx,fc,dz,iaggr,npar,ngas)
    
    return (np.asarray(vsett),np.asarray(Alagr),np.asarray(Abagr),np.asarray(Asagr),np.asarray(Adagr),np.asarray(Atagr))
    
    




cdef agrpar_c(double vset_factor, double [:,:] tgsd, double[:,:,:,:] c, double [:,:,:,:] vsett, double [:,:,:] vx, double [:,:,:] vy, double [:,:,:] vz, \
              double [:,:,:] pres, double [:,:,:] qv, double [:,:,:] rho, double [:,:,:] temp, double [:,:,:] tplume, \
              double dX2, double dX1, double [:,:] Hm1, int nz, int ny, int nx, double [:] fc, double [:] dz, int iaggr, \
              int npar, int ngas):    
    
    
    # Inizialisations    
    cdef double kb    = 1.38*10.0**-23             # Boltzman constant
    cdef double visc0 = 1.827*10.0**-5             # reference viscosity
    
    cdef float  Tair0 = 291.15                   # reference temperature
    cdef double bb    = 0.44
    cdef double B     = (72.0*4.8*10.0**(-4))/math.pi
    cdef double Stcr  = 1.3                      # Critical Stokes number
    cdef double qq    = 0.8
    cdef double psi3  = 6.0/math.pi
    cdef double psi4  = psi3*((6.0/math.pi)**(1.0/3.0))
    cdef double g     = 9.8066
    cdef float  S     = 104.5                    # Sutherland temperature
    
    cdef double Alagrmax_ice = 0.0
    cdef double Alagrmax_wat = 0.0
    cdef double Alagrmin_ice = 10.0**20
    cdef double Alagrmin_wat = 10.0**20
    
    cdef double sumfc = 0
    #cdef float visc
    
    cdef int ix,iy,iz,ic   
    
    
    
    # sum of fc for only aggregated particles (npar not included)
    for ic in range(iaggr,npar-1):
        sumfc = sumfc + fc[ic]
        
    cdef double sumff = 0
        
    for ic in range(iaggr,npar-1):         # only particles
        for jc in range(iaggr,npar-1):
            sumff = sumff + fc[ic]*fc[jc]
            
    rhop = tgsd[:,1]
    diam = tgsd[:,0]      

    # mean density of primary aggregating particles
    cdef double rhopmean = 0    
        
    for ic in range(iaggr,npar-1):
        rhopmean = rhopmean + fc[ic]*rhop[ic]
            
    rhopmean = rhopmean/sumfc 
    
    cdef double [:,:,:] Abagr = np.zeros((nz,ny,nx))
    cdef double [:,:,:] Asagr = np.zeros((nz,ny,nx))
    cdef double [:,:,:] Adagr = np.zeros((nz,ny,nx))
    cdef double [:,:,:] Atagr = np.zeros((nz,ny,nx))
    cdef double [:,:,:] Alagr = np.zeros((nz,ny,nx))
    
    cdef double [:,:,:] visco = np.zeros((nz,ny,nx))
    
    
    for iz in range(nz):
            for iy in range(ny):
                for ix in range(nx):
                    
                    # TODO: das wurde anders gelöst, aber tplume konnte nicht berechnet werden, wird aber vorausgesetzt???
                    if tplume[iy,ix,iz] > 0:
                        T = tplume[iy,ix,iz]
                            # ???
                    else:
                        T = temp[iz,iy,ix]
                        
                    # air density    
                    rhoa = rho[iz,iy,ix]
                    
                    # air viscosity
                    visc = visc0*((Tair0+S)/(T+S))*((T/Tair0)**1.5)   # Sutherland's law                   
                    
                    visco[iz,iy,ix] = visc
                    
#                    if visc == 0:
#                        print('T = %s' %T)
                    
                    # water viscosity
                    viscl = 2.414*10.0**(-5)*(10.0**(247.7/(T-140.0)))
                    
                    # check whether we are in the plume
                    if tplume[iy,ix,iz] > 0:
                         # In the Plume it is assumed that
                         # T < 255K       ice
                         # 373 > T > 255  water
                         # T > 373        vapor
                         
                         if T < 255:
                             wphase = 'ice'
                         elif T < 373:
                             wphase = 'water'
                         else:
                             wphase = 'vapor'
                             
                    else: 
                        # In the cloud
                        
                        # If there is some gas component it is assumed that
                        # the first component npar+1 is (magmatic) water
                        
                        #TODO: momentan gibt es nur SO2
                        
                        if ngas > 0:
                            wcloud = c[iz,iy,ix,npar]/Hm1[iy,ix]
                        else:
                            wcloud = 0
                            
                        wcloud = wcloud/rhoa # mixing ratio in kg/kg
                        
                        # compute water phase
                        w = qv[iz,iy,ix]/(1 - qv[iz,iy,ix])
                        
                        # add the water cloud contribution
                        w = w + wcloud
                        
                        # vapor pressure
                        e = pres[iz,iy,ix]*w/(w+0.622)
                        
                        # saturation vapour pressure with respect to water
                        ew = 6.112*exp(17.67*(T-273.16)/(T-273.16 + 243.5))
                        ew = 100*ew     # hPa --> Pa
                        
                        # saturation vapour pressure with respect to ice
                        ei = -9.09718*(273.16/T - 1.0) - 3.56654*np.log10(273.16/T) + 0.876793*(1.0-T/273.16) + np.log10(6.1071)
                        ei = 100*(10.0**ei)   # hPa --> Pa
                        
                        if e > ew:
                            if T > 255:
                                # value of 255 K based on Durant 2009
                                wphase = 'water'
                            else:
                                wphase = 'ice'
                                
                        else:
                            if e > ei:
                                wphase = 'ice'
                            else:
                                wphase = 'vapor'
                                
                    # compute a Brownian function at each point
                    Abagr[iz,iy,ix] = -4*kb*T/(3*visc)
                    
#                    if np.isnan(Abagr[iz,iy,ix]):
#                        print('Params:')
#                        print(T)
#                        print(visc) # visc ist 0
                    
                    # Computes A Shear at each point
                    nu = visc/rhoa    # Local kinematic viscosity
                    
                    # Compute Gamma_s according to: Laminar:    Gamma_s = abs( dU/dz ), Turbulent:  Gamma_s = sqrt( 1.3 SijSij)
                        
                    dXX = Hm1[iy,ix]*dX1 # scaled
                    
                    # calculate gradients
                    if ix == 0:
                        dudx=(vx[iz,iy,1]-vx[iz,iy,0])/dXX
                        dvdx=(vy[iz,iy,1]-vy[iz,iy,0])/dXX
                        dwdx=(vz[iz,iy,1]-vz[iz,iy,0])/dXX
                    elif ix == nx-1:
                        dudx=(vx[iz,iy,nx-1]-vx[iz,iy,nx-2])/dXX
                        dvdx=(vy[iz,iy,nx-1]-vy[iz,iy,nx-2])/dXX
                        dwdx=(vz[iz,iy,nx-1]-vz[iz,iy,nx-2])/dXX
                    else:
                        dudx = (vx[iz,iy,ix+1] - vx[iz,iy,ix-1])/(2*dXX)
                        dvdx = (vy[iz,iy,ix+1] - vy[iz,iy,ix-1])/(2*dXX)
                        dwdx = (vz[iz,iy,ix+1] - vz[iz,iy,ix-1])/(2*dXX)
                        
                    if iy == 0:
                        dudy=(vx[iz,1,ix]-vx[iz,0,ix])/dX2
                        dvdy=(vy[iz,1,ix]-vy[iz,0,ix])/dX2
                        dwdy=(vz[iz,1,ix]-vz[iz,0,ix])/dX2
                    elif iy == ny-1:
                        dudy=(vx[iz,ny-1,ix]-vx[iz,ny-2,ix])/dX2
                        dvdy=(vy[iz,ny-1,ix]-vy[iz,ny-2,ix])/dX2
                        dwdy=(vz[iz,ny-1,ix]-vz[iz,ny-2,ix])/dX2
                    else:
                        dudy=(vx[iz,iy+1,ix]-vx[iz,iy-1,ix])/dX2
                        dvdy=(vy[iz,iy+1,ix]-vy[iz,iy-1,ix])/dX2
                        dwdy=(vz[iz,iy+1,ix]-vz[iz,iy-1,ix])/dX2
                        
                    if iz == 0:
                        dudz=(vx[1,iy,ix]-vx[0,iy,ix])/dz[0]
                        dvdz=(vy[1,iy,ix]-vy[0,iy,ix])/dz[0]
                        dwdz=(vz[1,iy,ix]-vz[0,iy,ix])/dz[0]
                    elif iz == nz-1:
                        dudz=(vx[nz-1,iy,ix]-vx[nz-2,iy,ix])/dz[0]
                        dvdz=(vy[nz-1,iy,ix]-vy[nz-2,iy,ix])/dz[0]
                        dwdz=(vz[nz-1,iy,ix]-vz[nz-2,iy,ix])/dz[0]
                    else:
                        dudz=(vx[iz+1,iy,ix]-vx[iz-1,iy,ix])/dz[0]
                        dvdz=(vy[iz+1,iy,ix]-vy[iz-1,iy,ix])/dz[0]
                        dwdz=(vz[iz+1,iy,ix]-vz[iz-1,iy,ix])/dz[0]
                        
                    S11 = dudx
                    S12 = 0.5*(dvdx+dudy)
                    S13 = 0.5*(dwdx+dudz)
                    S21 = S12
                    S22 = dvdy
                    S23 = 0.5*(dwdy+dvdz)
                    S31 = S13
                    S32 = S23
                    S33 = dwdz
                    
                    # Snorm = epsilon/nu
                    Snorm = S11*S11+S12*S12+S13*S13 + S21*S21+S22*S22+S23*S23 +  S31*S31+S32*S32+S33*S33
                    
                    Cs=0.16 # Smagorinsky constant   
                    
                    Lscale = (dXX*dX2*dz[iz])**0.33333
                      
                    nut = (Cs*Lscale)**2*sqrt(Snorm)      # Turbulent viscosity
                    epsilon = nut*Snorm
                    
                    gammas = sqrt(1.3*Snorm)
                        
                    Asagr[iz,iy,ix] = -2.0*gammas*psi3/3.0
                        
                    # compute A differential settling velocity at each point
                    Adagr[iz,iy,ix] = -math.pi*(rhopmean-rhoa)*g*psi4/(48.0*visc)
                       
                    # Compute A for turbulent inertial kernel at each point
                    Atagr[iz,iy,ix]  = math.pi*(epsilon**0.75)*Adagr[iz,iy,ix]/(g*nu**0.25) # already with negative sign
                    
                    # Compute the mean sticking efficiency (alfa mean) at each point
                    Alagr[iz,iy,ix] = 0
                    
                    if wphase == 'ice':
                        Alagr[iz,iy,ix] = 0.09
                        if Alagr[iz,iy,ix] > Alagrmax_ice:
                            Alagrmax_ice = Alagr[iz,iy,ix]
                        if Alagr[iz,iy,ix] < Alagrmin_ice:  
                            Alagrmin_ice = Alagr[iz,iy,ix]
                            
                            
                    if wphase == 'water':
                        
                        for ic in range(iaggr,npar-1): #TODO: checken ob indizes stimmen
                            for jc in range(iaggr,npar-1):
                                
                                vij = abs(vsett[iz,iy,ix,ic]-vsett[iz,iy,ix,jc])- \
                                Abagr[iz,iy,ix]/(2.0*math.pi*diam[ic]*diam[jc])-  \
                                Asagr[iz,iy,ix]*diam[ic]*diam[jc]/(4*math.pi)
                                
                                
                                w    =  fc[ic]*fc[jc]/sumff
                                 
                                Stij =  8*rhopmean*vij*diam[ic]*diam[jc]/ \
                                        (9*viscl*(diam[ic]+diam[jc]))
                                
                                alfa = 1 + ((Stij/Stcr)**qq)
                                alfa = 1/alfa
                                Alagr[iz,iy,ix] = Alagr[iz,iy,ix] + w*alfa
                                
                        if Alagr[iz,iy,ix] > Alagrmax_wat:
                            Alagrmax_wat = Alagr[iz,iy,ix]
                        if Alagr[iz,iy,ix] < Alagrmin_wat:
                            Alagrmin_wat = Alagr[iz,iy,ix]    
                            
    #print(np.asarray(visco))                        
    print(np.amax(np.asarray(visco)))
    
                        
    # for parameter definitions see log file in agrsrc.f90
    Alagrmax = np.amax(np.asarray(Alagr))
    Alagrmin = np.amin(np.asarray(Alagr))
    Alagrave = np.sum(np.asarray(Alagr))/(nx*ny*nz)
  
    Abagrmax = np.amax(np.asarray(Abagr))
    Abagrmin = np.amin(np.asarray(Abagr))
    Abagrave = np.sum(np.asarray(Abagr))/(nx*ny*nz)
                        
    Asagrmax = np.amax(np.asarray(Asagr))
    Asagrmin = np.amin(np.asarray(Asagr))
    Asagrave = np.sum(np.asarray(Asagr))/(nx*ny*nz)
                        
    Adagrmax = np.amax(np.asarray(Adagr))
    Adagrmin = np.amin(np.asarray(Adagr))
    Adagrave = np.sum(np.asarray(Adagr))/(nx*ny*nz)
                      
    Atagrmax = np.amax(np.asarray(Atagr))
    Atagrmin = np.amin(np.asarray(Atagr))
    Atagrave = np.sum(np.asarray(Atagr))/(nx*ny*nz) 

    # minimum and maximum particle diameter of aggregated particles
    dmin = diam[npar-2]
    dmax = diam[iaggr]

    Kbagrmax = Alagrmax*np.amax(-np.asarray(Abagr))*(4)
    kbagrmin = Alagrmin*np.amin(-np.asarray(Abagr))*(4)
        
    ksagrmax = Alagrmax*np.amax(-np.asarray(Asagr))*((2*dmax)**3)
    ksagrmin = Alagrmin*np.amin(-np.asarray(Asagr))*((2*dmin)**3)
        
    kdagrmax = Alagrmax*(np.amax(-np.asarray(Adagr)))*(3*dmax**4)
    kdagrmin = Alagrmin*(np.amin(-np.asarray(Adagr)))*(3*dmin**4)   

    
    #TODO: vset_factor wurde oben schon angewendet?
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                vsett[k,j,i,iaggr]  = vset_factor*vsett[k,j,i,iaggr]
                
    #print('Alagr = %s' % np.amax(np.asarray(Alagr)))
    #print('Abagr = %s' % np.amax(np.asarray(Abagr)))
    #print('Asagr = %s' % np.amax(np.asarray(Asagr)))
    #print('Adagr = %s' % np.amax(np.asarray(Adagr)))
    #print('Atagr = %s' % np.amax(np.asarray(Atagr)))                                    

    return (vsett, Alagr,Abagr,Asagr,Adagr,Atagr)   









def agrsrc(simulation,dt):

    # account for aggregation (particles only) --> agrsrc.f90
    Df   = simulation['Df']
    nc   = simulation['nc']
    ngas = simulation['ngas']
    
    cdef double ka   = 1.0                # Fractal prefactor
    cdef double expb = 2.0                # Brownian     ntot exponent
    cdef double exps = 2.0-(3.0/Df)       # Shear        ntot exponent
    cdef double expd = 2.0-(4.0/Df)       # Differential ntot exponent                     
                                
    cdef double sumNi = 0
    cdef double [:] d3 = np.zeros((nc))
    cdef double [:] Ni = np.zeros((nc))
    
    cdef int ix,iy,iz
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    dz = simulation['dz']    
    
    Hm1 = simulation['Hm1']
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    c = simulation['c']
    
    iaggr = simulation['iaggr']
    npar  = simulation['npart'] 
    
    ndtagr = simulation['ndtagr']
    
    rhop = simulation['tgsd'][:,1]
    diam = simulation['tgsd'][:,0]
    fc   = simulation['tgsd'][:,3]
    
    Abagr = simulation['Abagr']
    Adagr = simulation['Adagr']
    Atagr = simulation['Atagr']
    Asagr = simulation['Asagr']
    Alagr = simulation['Alagr']
    
    tmrat = simulation['tmrat']
    nsou  = simulation['nsou']
    xsrc  = simulation['xsrc']
    ysrc  = simulation['ysrc']
    
    cdef double [:,:,:,:] tarat = np.zeros((nz,ny,nx,nc))
    
    
    
    for ic in range(iaggr,npar-1):
        d3[ic] = math.pi*rhop[ic]*diam[ic]*diam[ic]*diam[ic]
        Ni[ic] = ka*( (diam[npar-1]/diam[ic])**Df )
        sumNi = sumNi + Ni[ic]
        
    counter = 1   
    
    # loop over domain
    for iz in range(nz):
        for iy in range(ny):
            for ix in range(nx):
                
                ntot = 0
                fi = 0
                
                for ic in range(iaggr,npar-1):
                    fi = fi + c[iz,iy,ix,ic]/rhop[ic]
                    ntot = ntot + 6.0*c[iz,iy,ix,ic]/d3[ic]
                    
                ntot = ntot/Hm1[iy,ix]        #concentration is scaled
                fi   = fi/Hm1[iy,ix]
                
                dntot = Abagr[iz,iy,ix]*(ntot**expb) + Asagr[iz,iy,ix]*(ntot**exps)*(fi**(3.0/Df)) + \
                        (Adagr[iz,iy,ix]+Atagr[iz,iy,ix])*(ntot**expd)*(fi**(4.0/Df))
                        
#                if np.isnan(dntot) and counter < 2:
#                    print('Abagr: %s' % Abagr[iz,iy,ix])
#                    print('Asagr: %s' % Asagr[iz,iy,ix])
#                    print('Adagr: %s' % Adagr[iz,iy,ix])
#                    print('Atagr: %s' % Atagr[iz,iy,ix])
#                    counter = counter + 1
        
                dntot = Alagr[iz,iy,ix]*dntot
                
                # Calculates source (particles that aggregate only)
                        
                for ic in range(iaggr,npar-1):
                    
                    # Scale source term (in kg/(m3*s)) and sign criteria
                            
                    # Number of particles that aggragate per unit volume and unit time
                    dnj = dntot*Ni[ic]/sumNi
                    
                    tarat[iz,iy,ix,ic] = dnj*d3[ic]/6.0
                    
                    # class mass conservation
                    if (c[iz,iy,ix,ic] + dt*tarat[iz,iy,ix,ic] < 0):
                        tarat[iz,iy,ix,ic] = -c[iz,iy,ix,ic]/dt        # positive sign
                        
                    tarat[iz,iy,ix,npar-1] = tarat[iz,iy,ix,npar-1] - tarat[iz,iy,ix,ic]    
    
    
    # print('Ende: %s' %dntot)
                   
                    
                    
    # account for aggregated particles in concentration                
    for ic in range(nc):
        for iz in range(nz): 

            if iz == 0:
                dzeta = dz[0]
            else:
                dzeta = 0.5*(dz[iz-1] + dz[iz])

            for iy in range(ny):
                for ix in range(nx):
                    c[iz,iy,ix,ic] = c[iz,iy,ix,ic] + ndtagr*dt*tarat[iz,iy,ix,ic]   # TODO: volumenelement --> einheiten
    
    
    
    # accumulated mass of aggregates. This is stored to visualize where the aggregates form         
    for iz in range(nz):
        if iz == 0:
            dzeta = dz[0]
        else:
            dzeta = 0.5*(dz[iz-1] + dz[iz])                    

        for iy in range(ny):
            for ix in range(nx):
                simulation['Maggr'][iz,iy,ix] = simulation['Maggr'][iz,iy,ix] + ndtagr*dt*tarat[iz,iy,ix,npar-1] * Hm1[iy,ix]*dX1*dX2*dzeta                        
                    
                        
    # class mass balance
            
    # mass of the source
    for isou in range(nsou):
        for ic in range(nc):
            simulation['mclass'][ic,0] =  simulation['mclass'][ic,0] + ndtagr*dt*tmrat[isou,ic]/Hm1[int(ysrc[isou]),int(xsrc[isou])] # source mass 

    # mass of the aggregates        
    for ic in range(nc):
        for iz in range(nz):
                    
            if iz == 0:
                dzeta = dz[0]
            else:
                dzeta = 0.5*(dz[iz-1] + dz[iz])
                
            for iy in range(ny):
                for ix in range(nx):
                    simulation['mclass'][ic,1] =  simulation['mclass'][ic,1] + ndtagr*dt*tarat[iz,iy,ix,ic] * Hm1[iy,ix]*dX1*dX2*dzeta # Mass of aggragates (= c*v)    
               
                
    return (np.asarray(c), np.asarray(simulation['Maggr']),np.asarray(simulation['mclass'])) 



def setboundaries(simulation,ic):
    
    vz    = simulation['vz']
    vy    = simulation['vy']
    vx    = simulation['vx']
    vsett = simulation['vset']
    vdep  = simulation['vdep']
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']

    c = simulation['c'][:,:,:,ic] # concentration of class
    
    
    ###############################################
    # Damping layer
    ###############################################
    
    cdef int ix,iy,iz
    cdef double perc = 3   # percent of layers that are damped
    cdef double dampfac = 0.6
    
    dampnum_x = max(int(nx*perc/100),1)   # number of pixels at rim that are damped (at least 1)
    dampnum_y = max(int(ny*perc/100),1)
    dampnum_z = max(int(nz*perc/100),1)
    
    for ix in range(nx+2):
        for iy in range(ny+2):
            for iz in range(nz+2):
                if (ix < dampnum_x -1) or (ix > nx-dampnum_x) or (iy < dampnum_y -1) or  (iy > ny-dampnum_y) or (iz > nz-dampnum_z):
                    c[iz,iy,ix] = dampfac*c[iz,iy,ix]
    
    
    

    # bottom
    for iy in range(ny):
        for ix in range(nx):
                
            if (vz[0,iy,ix] + vsett[0,iy,ix,ic] >= 0):
                c[-1,iy,ix] = 0
            else:
                c[-1,iy,ix] = c[0,iy,ix]/(1 + vdep[iy,ix,ic] / vsett[0,iy,ix,ic]) # dry deposition mechanism                           
                    
                        
    # top
    for iy in range(ny):
        for ix in range(nx):
            if (vz[nz-1,iy,ix] + vsett[nz-1,iy,ix,ic] <= 0):
                c[nz,iy,ix] = 0
            else: 
                c[nz,iy,ix] = c[nz-1,iy,ix] 
                
    # left/right (x)
    for iz in range(nz):
        for iy in range(ny):
                
            if (vx[iz,iy,0] >= 0): # west
                c[iz,iy,-1] = 0
            else:
                c[iz,iy,-1] = c[iz,iy,0]
                    
            if (vx[iz,iy,nx-1] <= 0):
                c[iz,iy,nx] = 0
            else:
                c[iz,iy,nx] = c[iz,iy,nx-1]            
                    
    # up/down(y)
    for iz in range(nz):
        for ix in range(nx):
            if (vy[iz,0,ix] >= 0): # south
                c[iz,-1,ix] = 0
            else:
                c[iz,-1,ix] = c[iz,0,ix]
                    
            if (vy[iz,ny-1,ix] <= 0):  # north
                c[iz,ny,ix] = 0
            else:
                c[iz,ny,ix] = c[iz,ny-1,ix]
                
    # corners                
    c[-1,-1,-1] = c[0,0,0]
    c[-1,-1,nx] = c[0,0,nx-1]
    c[-1,ny,-1] = c[0,ny-1,0]
    c[-1,ny,nx] = c[0,ny-1,nx-1]
        
    c[nz,-1,-1] = c[nz-1,0,0]
    c[nz,-1,nx] = c[nz-1,0,nx-1]
    c[nz,ny,-1] = c[nz-1,ny-1,0]
    c[nz,ny,nx] = c[nz-1,ny-1,nx-1] 

    return np.asarray(c) 

    


def advectz(simulation,c,dt,ic):
    
    # calculates advection in z direction
    
    vz     = simulation['vz']
    vsett = simulation['vset'] # always negative
    
    cdef int nz = simulation['nz']
    cdef int ny = simulation['ny']
    cdef int nx = simulation['nx']
    
    cdef double [:] dz = simulation['dz']
    
    vz_ges = vz + vsett[:,:,:,ic] # total vertical velocity
    
    # call pure C function
    c = advectz_c2(c,dt,ic,vz_ges,nx,ny,nz,dz)
    
    return np.asarray(c)
    




# C function uniform grid
cdef double [:,:,:] advectz_c2(double [:,:,:] c, double dt, int ic, double [:,:,:] vz_ges, int nx, int ny, int nz, double [:] dez):

    dz = dez[1] # z increment    
    
    cdef double auxv = dt/dz
    cdef double [:,:] work = np.zeros((nz+1,2))
    
    cdef int iz
    cdef int ix
    cdef int iy
    
    for ix in range(nx):
        for iy in range(ny):
            for iz in range(nz):
                
                vj    = vz_ges[iz,iy,ix]
                vj_1  = vz_ges[max(0,iz-1),iy,ix]
                vj_12 = 0.5*(vj+vj_1)            # average 
                
                work[iz,0] = auxv* ( vj* c[iz,iy,ix] - vj_1 * c[iz-1,iy,ix])# first order backward FD
                
                deltar = ( c[iz+1,iy,ix] - c[iz,iy,ix])/dz
                deltal = ( c[iz,iy,ix] - c[iz-1,iy,ix])/dz
                
                if deltar < 0: # this replaces Fortran's sign function
                    a = -1
                else: 
                    a = 1    
                        
                if deltal < 0:
                    b = -1
                else:
                    b = 1
                    
                sigma = 0.5 * (a + b) *min(abs(deltar),abs(deltal))   
                
                Crj       = auxv*abs(vj_12)                      # Courant 
                
                work[iz,1] = dt*0.5*(1 - Crj)*sigma*abs(vj_12)
                
            work[nz,0] = auxv*vj*(c[nz,iy,ix] - c[nz-1,iy,ix])
            
            for iz in range(nz):
                if vz_ges[iz,iy,ix] > 0:
                    c[iz,iy,ix] =  c[iz,iy,ix] - work[iz,0] - (work[iz,1] - work[max(0,iz-1),1])
                else:
                    c[iz,iy,ix] =  c[iz,iy,ix] - work[iz+1,0] - ( work[min(nz-1,iz+1),1] - work[iz,1])
    
    return c    


     
    
# C function
cdef double [:,:,:] advectz_c(double [:,:,:] c, double dt, int ic, double [:,:,:] vz, double [:,:,:,:] vsett, int nx, int ny, int nz, double [:] dz):
    

    cdef double [:,:,:,:] work = np.zeros((nz+1,ny,nx,2))

    
    for iz in range(nz):
        
        if iz > 0:
            dzz   = 2.0/(dz[iz]+dz[iz-1])
            dzk_1 = 1.0/dz[iz-1]
        else:
            dzz = dz[0]
            dzk_1 = 1.0/dz[0]  
            
        dzk   = 1.0/dz[iz]
        kmax  = max(0,iz-1)
        
        for iy in range(ny):
            for ix in range(nx):
                    
                vk = vz[iz,iy,ix] + vsett[iz,iy,ix,ic]
                vk_1  = vz[kmax,iy,ix] + vsett[kmax,iy,ix,ic]
                vk_12 = 0.5*(vk+vk_1)
                
        work[iz,iy,ix,0] = dt*( vk * c[iz,iy,ix] - vk_1*c[iz-1,iy,ix])*dzz   # first order backward difference
                    
        deltar = (c[iz+1,iy,ix] - c[iz,iy,ix])*dzk
        deltal = (c[iz,iy,ix] - c[iz-1,iy,ix])*dzk_1  
        
        if deltar < 0: # this replaces Fortran's sign function
            a = -1
        else: 
            a = 1
            
        if deltal < 0:
            b = -1
        else:
            b = 1
            
        sigma  = 0.5*( a + b )*min(abs(deltar),abs(deltal))
                    
        Crk    = abs(vk_12)*dt*dzk_1
        
        if iz == 0:
            work[iz,iy,ix,1] = dt*0.5*dz[0]*(1.0-Crk)*sigma*abs(vk_12)*dzz
        else:
            work[iz,iy,ix,1] = dt*0.5*dz[iz-1]*(1.0-Crk)*sigma*abs(vk_12)*dzz
            
    # upper boundary        
    dznz = 1.0/dz[nz-1]

    for iy in range(ny):
        for ix in range(nx):
            vk  = vz[nz-1,iy,ix] + vsett[nz-1,iy,ix,ic]
            work[nz,iy,ix,0] = dt*vk*( c[nz,iy,ix] - c[nz-1,iy,ix] )*dznz
            
    for iz in range(nz):
        kmax = max(0,iz-1)
        kmin = min(nz-1,iz+1) #TODO: kmin größer als kmax???
        
        for iy in range(ny):
            for ix in range(nx):
                
                vk = vz[iz,iy,ix] + vsett[iz,iy,ix,ic]
                
                if vk > 0:
                    c[iz,iy,ix] =  c[iz,iy,ix] - work[iz,iy,ix,0]  - (work[iz,iy,ix,1] - work[kmax,iy,ix,1])
                else:
                    c[iz,iy,ix] = c[iz,iy,ix] - work[iz+1,iy,ix,0] - (work[kmin,iy,ix,1] - work[iz,iy,ix,1])
    
    # einmal auf Fehler überprüft
    
    # end of advctzc.f90
    return c   
            
            
        
            
def advectx(simulation,c,dt):
    
    Hm1 = simulation['Hm1']
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    vx    = simulation['vx']
    vsett = simulation['vset']
    vdep  = simulation['vdep']
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    c = advectx_c(nx,ny,nz,vdep,vsett,vx,dX1,dX2,Hm1,c,dt)
    
    return np.asarray(c)
    
    
    
cdef double [:,:,:] advectx_c(int nx, int ny, int nz, double [:,:,:] vdep, double [:,:,:,:] vsett, double [:,:,:] vx, \
                              double dX1, double dX2, double [:,:] Hm1, double [:,:,:] c, \
                              double dt):    
    
    cdef double [:,:] work = np.zeros((nx+1,2))
    
    cdef double auxv = dt/dX1
    cdef int ix,iy,iz
    
    for iz in range(nz):
        for iy in range(ny):
            for ix in range(nx):
                
                vi    = vx[iz,iy,ix]
                vi_1  = vx[iz,iy,max(0,ix-1)]                 
                vi_12 = 0.5*(vi+vi_1)
                
                work[ix,0] = auxv*(vi*c[iz,iy,ix] - vi_1*c[iz,iy,ix-1])   # first order backward difference
                
                deltar = (c[iz,iy,ix+1] - c[iz,iy,ix])/dX1
                deltal = (c[iz,iy,ix] - c[iz,iy,ix-1])/dX1
                
                if deltar < 0: # this replaces Fortran's sign function
                    a = -1
                else: 
                    a = 1 
                    
                if deltal < 0:
                    b = -1
                else:
                    b = 1    
                    
                
                sigma = 0.5 * (a + b) * min(abs(deltar),abs(deltal))
                        
                Cri  = auxv*abs(vi_12)
                        
                work[ix,1] = dt*0.5*(1 - Cri)* sigma* abs(vi_12)
               
                
            work[nx,0] = auxv * vi * (c[iz,iy,nx] - c[iz,iy,nx-1]) 
            
            
            for ix in range(nx):
                if vx[iz,iy,ix] > 0:
                    c[iz,iy,ix] = c[iz,iy,ix] - work[ix,0] - (work[ix,1] - work[max(0,ix-1),1])
                else:
                    c[iz,iy,ix] =  c[iz,iy,ix] - work[ix+1,0] - (work[min(nx-1,ix+1),1] - work[ix,1])
    
    # einmal geprüft
                        
    return c



def advecty(simulation,c,dt):
    
    # advection in y direction
    
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    vy    = simulation['vy']    
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    c = advecty_c(c,dt,dX1,dX2,vy,nz,ny,nx)
    
    return np.asarray(c)
    

cdef double [:,:,:] advecty_c(double [:,:,:] c, double dt, double dX1, double dX2, double [:,:,:] vy, int nz, int ny, int nx):    
    
    cdef double auxv = dt/dX2
    cdef double [:,:] work = np.zeros((ny+1,2))
    
    cdef int iz
    cdef int ix
    cdef int iy
    
    for iz in range(nz):
        for ix in range(nx):
            for iy in range(ny):
                
                vj    = vy[iz,iy,ix]
                vj_1  = vy[iz,max(0,iy-1),ix]
                vj_12 = 0.5*(vj+vj_1)            # average 
                
                work[iy,0] = auxv* ( vj* c[iz,iy,ix] - vj_1 * c[iz,iy-1,ix])   # first order backward FD
                
                deltar = ( c[iz,iy+1,ix] - c[iz,iy,ix])/dX2
                deltal = ( c[iz,iy,ix] - c[iz,iy-1,ix])/dX2
                
                if deltar < 0: # this replaces Fortran's sign function
                    a = -1
                else: 
                    a = 1    
                        
                if deltal < 0:
                    b = -1
                else:
                    b = 1
                    
                sigma = 0.5 * (a + b) *min(abs(deltar),abs(deltal))    
                        
                Crj       = auxv*abs(vj_12)                      # Courant 
                
                work[iy,1] = dt*0.5*(1 - Crj)*sigma*abs(vj_12)   
                
            work[ny,0] = auxv*vj*(c[iz,ny,ix] - c[iz,ny-1,ix])
            
            for iy in range(ny):
                if vy[iz,iy,ix] > 0:
                    c[iz,iy,ix] =  c[iz,iy,ix] - work[iy,0] - (work[iy,1] - work[max(0,iy-1),1])
                    
                else:
                    c[iz,iy,ix] = c[iz,iy,ix] - work[iy+1,0] - ( work[min(ny-1,iy+1),1] - work[iy,1])
    

    # einfach geprüft
                
    return c 





def diffz(simulation,c,dt):

    # diffusion in z-direction
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    dz = simulation['dz']
    
    rho   = simulation['rho']
    rkver = simulation['rkver']
    
    c = diffz_c(c,dt,dz,rho,rkver,nz,ny,nx)
    
    return np.asarray(c)

    
cdef double [:,:,:] diffz_c(double [:,:,:] c, double dt, double [:] dz, double [:,:,:] rho, double [:,:,:] rkver, \
                            int nz, int ny, int nx):    
    
    cdef double [:] l_dz = np.zeros((nz+1))
    cdef double [:] l_dzdz = np.zeros((nz))
    cdef double [:] work = np.zeros((nz))
    
    cdef int iz
    cdef int ix
    cdef int iy
    
    for iz in range(nz):
        l_dz[iz+1] = 1/dz[iz]
    
    
    l_dz[0] = 1/dz[0]
    
    for iz in range(nz):
        l_dzdz[iz] = 2.0/(dz[iz]+dz[max(0,iz-1)])
        
    for ix in range(nx):
        for iy in range(ny): 
            
            # Bottom boundary
            rkst    = rkver[1,iy,ix]*rho[1,iy,ix] # K*rho
            rksb    = rkver[0,iy,ix]*rho[0,iy,ix]
            
            gradt   = rkst*(( c[1,iy,ix]/rho[1,iy,ix]) - (c[0,iy,ix]/rho[0,iy,ix]))*l_dz[0]
            gradb   = rksb*((c[0,iy,ix]/rho[0,iy,ix]) - (c[-1,iy,ix]/rho[0,iy,ix]))*l_dz[0]
            
            gradb   = max(0,gradb)
                
            work[0] = dt*(gradt-gradb)*l_dzdz[0]
            
            # loop over middle
            for iz in range(1,nz-1):
                rkst = 0.5* (rho[iz+1,iy,ix]*rkver[iz+1,iy,ix] + rho[iz,iy,ix]*rkver[iz,iy,ix])
                rksb = 0.5* (rho[iz,iy,ix]*rkver[iz,iy,ix] + rho[iz-1,iy,ix]*rkver[iz-1,iy,ix])
                
                gradt = rkst*((c[iz+1,iy,ix]/rho[iz+1,iy,ix]) - (c[iz,iy,ix]/rho[iz,iy,ix])) * l_dz[iz]
                gradb = rksb*((c[iz,iy,ix]/rho[iz,iy,ix]) - (c[iz-1,iy,ix]/rho[iz-1,iy,ix])) * l_dz[iz-1]
                work[iz] = dt*(gradt-gradb)*l_dzdz[iz]
                
            # Top boundary
            rkst  = rkver[nz-1,iy,ix]*rho[nz-1,iy,ix]
            rksb  = rkver[nz-2,iy,ix]*rho[nz-2,iy,ix]
            gradt = rkst*((c[nz,iy,ix]/rho[nz-1,iy,ix]) - (c[nz-1,iy,ix]/rho[nz-1,iy,ix]))*l_dz[nz]
            gradb = rksb*((c[nz-1,iy,ix]/rho[nz-1,iy,ix]) - (c[nz-2,iy,ix]/rho[nz-2,iy,ix]))*l_dz[nz-1] 
            
            gradt = min(0,gradt)
            work[nz-1] = dt*(gradt - gradb)*l_dz[nz]
            
            for iz in range(nz):
                c[iz,iy,ix] =  c[iz,iy,ix] + work[iz]
       
    # einfach geprüft (Regina)    
    return c





def diffx(simulation,c,dt):
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    rho   = simulation['rho']
    rkhor = simulation['rkh1'] # scaled diffusivity
    
    c = diffx_c(c,dt,rho,rkhor,dX1,dX2,nz,ny,nx)
    
    return np.asarray(c)
    


    
cdef double [:,:,:] diffx_c(double [:,:,:] c, double dt, double [:,:,:] rho, double [:,:,:] rkhor, double dX1, double dX2, \
                            int nz, int ny, int nx):    
    
    cdef double dtdx2=dt/dX1**2
    cdef double [:] work = np.zeros((nx))
    cdef double [:] l_rho = np.zeros((nx))
    
    cdef int iz
    cdef int iy
    cdef int ix
    
    for iz in range(nz):
        for iy in range(ny):
            
            for ix in range(nx):
                l_rho[ix] = 1/rho[iz,iy,ix]
                    
            # left boundary
            rksr = rkhor[iz,iy,1]*rho[iz,iy,1]
            rksl = rkhor[iz,iy,0]*rho[iz,iy,0]
            
            gradr = rksr*(c[iz,iy,1]*l_rho[1] - c[iz,iy,0]*l_rho[0])
            gradl = rksl*(c[iz,iy,0]*l_rho[0] - c[iz,iy,-1]*l_rho[0])
                    
            gradl   = max(0,gradl)
            work[0] = dtdx2*(gradr - gradl) # change term
            
            # loop over middle
            for ix in range(1,nx-1):
                rksr = 0.5*( rho[iz,iy,ix+1] * rkhor[iz,iy,ix+1] + rho[iz,iy,ix]* rkhor[iz,iy,ix])
                rksl = 0.5*(rho[iz,iy,ix] * rkhor[iz,iy,ix] + rho[iz,iy,ix-1] * rkhor[iz,iy,ix-1])
                
                gradr = rksr*(c[iz,iy,ix+1]*l_rho[ix+1] - c[iz,iy,ix]*l_rho[ix])
                gradl = rksl*(c[iz,iy,ix]*l_rho[ix] - c[iz,iy,ix-1]*l_rho[ix-1])
                
                work[ix] = dtdx2*(gradr - gradl)
                
            # right boundary
            rksr  = rkhor[iz,iy,nx-1]*rho[iz,iy,nx-1]
            rksl  = rkhor[iz,iy,nx-2]*rho[iz,iy,nx-2]
                    
            gradr = rksr*(c[iz,iy,nx]*l_rho[nx-1] - c[iz,iy,nx-1]*l_rho[nx-1])
            gradl = rksl*(c[iz,iy,nx-1]*l_rho[nx-1] - c[iz,iy,nx-2]*l_rho[nx-2]) 
                    
            gradr = min(0,gradr)
            work[nx-1] = dtdx2*(gradr - gradl)  # change term 
            
            # update concentration
            for ix in range(nx):
                c[iz,iy,ix] = c[iz,iy,ix] + work[ix]
    
    # einfach geprüft (Regina)            
    return c
            
    
def diffy(simulation,c,dt):
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    Hm1 = simulation['Hm1']
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    rho   = simulation['rho']
    rkhor = simulation['rkhor']
    
    c = diffy_c(c,dt,rho,rkhor,Hm1,dX1,dX2,nz,ny,nx)
    
    return np.asarray(c)
    
    
    
cdef double [:,:,:] diffy_c(double [:,:,:] c, double dt, double [:,:,:] rho, double [:,:,:] rkhor, double [:,:] Hm1, \
                            double dX1, double dX2, int nz, int ny, int nx):    
    
    cdef int iz
    cdef int iy
    cdef int ix
    
    cdef double dtdy2=dt/dX2**2
            
    cdef double [:] work  = np.zeros((ny))
    cdef double [:] l_rho = np.zeros((ny))
    
    for iz in range(nz):
        for ix in range(nx):
            for iy in range(ny):
                l_rho[iy] = 1/rho[iz,iy,ix]
                
    
    
    for iz in range(nz):
        for ix in range(nx):
                    
            #l_rho = 1/rho[iz,:,ix]
            
#            for iy in range(ny):
#                l_rho[iy] = 1/rho[iz,iy,ix]
                
               
                    
            # Down boundary
            rksu = rkhor[iz,1,ix]*rho[iz,1,ix]
            rksd = rkhor[iz,0,ix]*rho[iz,0,ix]
                    
            gradu = rksu*(c[iz,1,ix]*l_rho[1] - c[iz,0,ix]*l_rho[0])
            gradd = rksd*(c[iz,0,ix]*l_rho[0] - c[iz,-1,ix]*l_rho[0])
            
            gradd   = max(0,gradd)
            work[0] = dtdy2*(gradu - gradd)
            
            # loop over middle
            for iy in range(1,ny-1):
                rksu = 0.5*(rho[iz,iy+1,ix]*rkhor[iz,iy+1,ix] + rho[iz,iy,ix]*rkhor[iz,iy,ix])
                rksd = 0.5*(rho[iz,iy,ix]*rkhor[iz,iy,ix] + rho[iz,iy-1,ix]*rkhor[iz,iy-1,ix])
                        
                gradu = rksu*(c[iz,iy+1,ix]*l_rho[iy+1] - c[iz,iy,ix]*l_rho[iy])
                gradd = rksd*(c[iz,iy,ix]*l_rho[iy] - c[iz,iy-1,ix]*l_rho[iy-1])
                        
                work[iy] = dtdy2*(gradu-gradd)
    
            # Up boundary
            rksu  = rkhor[iz,ny-1,ix]* rho[iz,ny-1,ix]
            rksd  = rkhor[iz,ny-2,ix]* rho[iz,ny-2,ix]
                    
            gradu = rksu*(c[iz,ny,ix]*l_rho[ny-1] - c[iz,ny-1,ix]*l_rho[ny-1])
            gradd = rksd*(c[iz,ny-1,nx]*l_rho[ny-1] - c[iz,ny-2,ix]*l_rho[ny-2])
                    
            gradu = min(0,gradu)
            work[ny-1] = dtdy2*(gradu-gradd)
            
            # update
            for iy in range(ny):
                c[iz,iy,ix] = c[iz,iy,ix] + work[iy]
    
    # geprüft von Regina
    return c



def wetdep(simulation,c,ic,dt):

    # Computes wet deposition according to Jung and Shao (2006)
        
        #    dC/dt = -L*C = a*(P**b)*C
        #    a = 8.4d-5
        #    b = 0.79
        #    P precipitation rate in mmh-1
    
        # A critical cut-off size is assumed. As a first approach, wet deposition
        # is assumed below the PBL only. This is done because there is no
        # informtion about the height of the precipitation (only the total
        # rate is known).    

    
    cdef int nz = simulation['nz']
    cdef int ny = simulation['ny']
    cdef int nx = simulation['nx']
    
    cdef double [:,:] zi = simulation['zi'] # height of ABL
    
    cdef double p_rate = simulation['p_rate']
    
    cdef long [:] zlayer = simulation['zlayer']
    cdef double [:] dz     = simulation['dz']
    
    cdef double [:,:,:] cwet = simulation['cwet']
    
    
    cdef double diam = simulation['tgsd'][ic,0]                 # diam in mm
    
    c,cwet = wetdep_c(c,dt,ic,nz,ny,nx,zi,p_rate,zlayer,dz,cwet,diam)
    
    return (np.asarray(c),np.asarray(cwet))
    
    
    
    
    
cdef wetdep_c(double [:,:,:] c, double dt, int ic, int nz, int ny, int nx, double [:,:] zi, \
                                            double p_rate, long [:] zlayer, double [:] dz, double [:,:,:] cwet, double diam):
    
    
    cdef double diam_max = 100*10.0**-6           # cut-off size in m (100 um)
    cdef double a = 8.4*10.0**-5
    cdef double b = 0.79
    
    cdef double lambdah
    
    cdef int ix
    cdef int iy
    cdef int iz
    
    # Check if wet deposition applies for this particle size
    if diam*10.0**-3 < diam_max: 

        for ix in range(nx):
            for iy in range(ny):
                
                lambdah = min(dt*a*(p_rate**b),1)   #TODO: eigentlich prate(iy,ix)
                
                for iz in range(nz):
                    
                    # wet deposition only in boundary layer
                    if zlayer[iz] <= zi[iy,ix]:
                        cwet[iy,ix,ic] = cwet[iy,ix,ic] + dz[iz]*lambdah*c[iz,iy,ix]
                        c[iz,iy,ix]= c[iz,iy,ix]*(1-lambdah)
                     
    return c,cwet

                            
                
                
    
def accum(simulation,dt,ic,c):
    
    # Dry deposition and sedimentation
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    
    dz = simulation['dz']
    
    vdep = simulation['vdep']
    cdry = simulation['cdry']
    vsett = simulation['vset']
    
#    cdry,c = accum_c(dt,c,vdep,cdry,vsett,ic,nz,nx,ny,dz)
#    
#    
#    return (np.asarray(cdry),np.asarray(c))
#    
#    
#cdef double [:,:,:] accum_c(double dt, double [:,:,:] c, double [:,:,:] vdep, double [:,:,:] cdry, double [:,:,:,:] vsett, \
#                            int ic, int nz, int nx, int ny, double [:] dz):    
    
    cdef int ix
    cdef int iy

    # Accumulation from dry deposition and sedimentation (accum.f90)    
    for ix in range(nx):
        for iy in range(ny):
            
            #cdry[iy,ix,ic] = cdry[iy,ix,ic] - dt*(c[0,iy,ix]*min(0,vsett[0,iy,ix,ic]+ vdep[iy,ix,ic]))
            delta_cdry = dt*0.5*(c[0,iy,ix] * min(0,vsett[0,iy,ix,ic]) + c[-1,iy,ix]* min(0,vsett[0,iy,ix,ic] + vdep[iy,ix,ic]))
            
            cdry[iy,ix,ic] = cdry[iy,ix,ic] - delta_cdry              # this is in original Fortran code
            
            # correct concentration at ground because of the deposit of mass
            #c[0,iy,ix] = c[0,iy,ix] + delta_cdry/dz[1] # delta_cdry is negative
            
            
    return (np.asarray(cdry),np.asarray(c))
            
            
    
    
    
def cmass2d(simulation,dt):
    
    # mass lost at boundaries
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    nc = simulation['nc']
    
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    
    vx = simulation['vx']
    vy = simulation['vy']
    vz = simulation['vz']
    dz = simulation['dz']
    
    vsett = simulation['vset']
    
    c = simulation['c']
    
    mymass = cmass2d_c(dt,dX1,dX2,vx,vy,vz,dz,vsett,c,nc,nx,ny,nz)
    
    return mymass


cdef double cmass2d_c(double dt, double dX1, double dX2, double [:,:,:] vx, double [:,:,:] vy, double [:,:,:] vz, double [:] dz, \
                      double [:,:,:,:] vsett, double [:,:,:,:] c, int nc, int nx, int ny, int nz):

    cdef double mymass = 0
    
    cdef int ix
    cdef int iy
    cdef int iz
    cdef double faty
    cdef double area
     
    # Left/Right (X). Sign criteria: influx < 0
    for ic in range(nc):
        for iy in range(ny):
            
            faty = 0
            
            if(iy == 0) or (iy == ny-1):
                faty=0.5
                
            for iz in range(nz):
                    
                area = faty*dX2*0.5*(dz[iz]+dz[max(0,iz-1)])
                    
                velo    = vx[iz,iy,0]
                conc    = c[iz,iy,-1,ic]
                    
                mymass = mymass - dt*area*velo*conc  # Left. outflux > 0
                    
                velo = vx[iz,iy,nx-1]
                conc = c[iz,iy,nx,ic]
                    
                mymass = mymass + dt*area*velo*conc  # Right. outflux > 0    

           
    # Up/Down
    for ic in range(nc):
        for ix in range(nx):
            
            fatx = 1.0
            
            if(ix == 0) or (ix == nx-1):
                fatx = 0.5
                
            for iz in range(nz):
                
                area    = fatx*dX1*0.5*(dz[iz]+dz[max(0,iz-1)])
                
                velo    = vy[iz,0,ix]
                conc    = c[iz,-1,ix,ic]
                
                mymass = mymass - dt*area*velo*conc  # Down. outflux > 0
                
                
                velo    = vy[iz,ny-1,ix]
                conc    = c[iz,ny,ix,ic]
                
                mymass = mymass + dt*area*velo*conc  # Up. outflux > 0
                
                
    # Top (Z)
    for ic in range(nc):
        for iy in range(ny):
            
            faty = 1.0
            
            if(iy == 0 or iy == ny-1):
                faty=0.5
                
            for ix in range(nx):
                
                fatx=1.0
                
                if(ix == 0) or (ix == nx-1):
                    fatx=0.5
                
                area = fatx*faty*dX1*dX2
                
                velo    = vz[nz-1,iy,ix] + vsett[nz-1,iy,ix,ic]
                conc    = c[nz,iy,ix,ic]
                
                mymass = mymass + dt*area*velo*conc  # Top. outflux > 0            
                
                 
    return mymass     
         
    
    
    
    
    
def calcmass_c(simulation,dt,c_classes):
    
    nz = simulation['nz']
    ny = simulation['ny']
    nx = simulation['nx']
    nc = simulation['nc']
    npar = simulation['npart']
    
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']    
    
    cload = simulation['cload']
    cwet  = simulation['cwet']
    cdry  = simulation['cdry']
    
    dez = simulation['dz']
    
    
    

    massvol  = 0
    massdep  = 0
    massdry  = 0
    masswet  = 0
    
    cdef int ix,iy,iz
    cdef double fatx,faty,zmass
    
#    for ix in range(nx):
#        fatx = 1
#        if ix == 0 or ix == nx-1:
#            fatx = 0.5
#            
#        for iy in range(ny):
#            faty = 1
#            if iy == 0 or iy == ny-1:
#                faty = 0.5    
#
#            for ic in range(nc):
#                zmass = 0
#
#                for iz in range(nz-1):
#                    zmass = zmass + 0.5 * dz[iz]* (c[iz,iy,ix,ic] + c[iz+1,iy,ix,ic])
#                # whole domain    
#                massvol = massvol + fatx*faty*zmass                               
#
#                             
#            # deposit: only particles    
#            massdep = massdep + fatx*faty*np.sum(cload[iy,ix,0:npar])           
#            massdry = massdry + fatx*faty*np.sum(cdry[iy,ix,0:npar]) 
#            masswet = masswet + fatx*faty*np.sum(cwet[iy,ix,0:npar])  
#
#
#    massvol    = massvol*dX1*dX2      
#    massdep    = massdep*dX1*dX2      
#    massdry    = massdry*dX1*dX2      
#    masswet    = masswet*dX1*dX2
#
#    return (massvol,massdep,massdry,masswet)


    # Reginas Funktion
    
    #print(np.amax(c_classes))
    
    
    for ix in range(nx):
        
        dx = dX1
        if ix == 0 or ix == nx-1:
            dx = 0.5*dX1
            
        for iy in range(ny):
            dy = dX2
            if iy == 0 or iy == ny-1:
                dy = 0.5*dX2
                
            
            massdep = massdep + np.sum(cload[iy,ix,0:npar])*dx*dy
            massdry = massdry + np.sum(cdry[iy,ix,0:npar])*dx*dy 
            masswet = masswet + np.sum(cwet[iy,ix,0:npar])*dx*dy 
            
            
            for iz in range(nz):
                dz = dez[1]
                if iz == 0 or iz == nz-1:
                    dz = 0.5*dez[1]
                 
                
                   
                massvol = massvol + c_classes[iz,iy,ix]*dx*dy*dz
                

    
    return (np.asarray(massvol),np.asarray(massdep),np.asarray(massdry),np.asarray(masswet))


def depthick(simulation):
    
    # computes thickness of ash deposits
    rhop = simulation['tgsd'][:,1] # in kg/m^3
    cload = simulation['cload']
    
    # correct cload with Hm1
    Hm1 = simulation['Hm1']

    
    npar = simulation['npart']
    
    ny = simulation['ny']
    nx = simulation['nx']
    nc = simulation['nc']
    
    cdef double [:,:] thickdep = np.zeros((ny,nx))
    
    
    
    cdef int ix,iy,ic
    
    for ix in range(nx):
        for iy in range(ny):
            
            sumrho = 0
            #summ = np.sum(cload[iy,ix,0:npar])
            
            for ic in range(npar):
                #sumrho = sumrho + cload[iy,ix,ic]/(rhop[ic]*Hm1[iy,ix])
                sumrho = sumrho + cload[iy,ix,ic]/(1000*Hm1[iy,ix]) # default deposit density of 1000 kg/m^3
                
            thickdep[iy,ix] = sumrho   # in m , depositing factor is chosen 1
                
    return np.asarray(thickdep)
    

                
                         
                     
                             
                         
                     
                         
def divcorr(simulation,erumass,dt):
    # Correct the mass unbalance due to the nonnull divergence
    
    ny = simulation['ny']
    nx = simulation['nx']
    nz = simulation['nz']
    nc = simulation['nc']

    massvol = simulation['massvol']
    outmass = simulation['outmass']
    massdep = simulation['massdep']
    
    c = simulation['c']
    divu = simulation['divu']
    
    compmass = massvol + outmass + massdep
    
    # use function
    c = divcorr_c(c,divu,compmass,erumass,dt,nx,ny,nz,nc)
    
    return np.asarray(c)
 

    
cdef double [:,:,:,:] divcorr_c(double [:,:,:,:] c, double [:,:,:] divu, double compmass, double erumass, double dt, int nx, int ny, int nz, int nc):

    cdef double eps = 0.005
    cdef double eps1 = 10.0*eps
    
    cdef int ix,iy,iz,ic

    corrdiv = max(min((erumass-compmass)/(erumass),eps1),-eps1)  
    
    for ix in range(nx):
        for iy in range(ny):
            for iz in range(nz):
                dive = divu[iz,iy,ix]
                
                if abs(dive*dt) > eps:
                    
                    for ic in range(nc):
                        
                        c[iz,iy,ix,ic] = c[iz,iy,ix,ic]*(1 + corrdiv)
                
    return c            
                
    
    
                             
def corvver(simulation):
    
    #    Correct vertical velocity. This is a transformation
    #    from the cartesian (x,y,z) to the terrain following
    #    coordinate system (X;Y;Z). Note that J=1 is assumed because
    #    we do not use the classical sigma mapping but Z = z - zo
    #    (i.e. the factor H/(H-zo) is not present)

    cdef int ix,iy,iz
    cdef double zx,zy,vs,dyinv
    
    dX1 = simulation['dX1']
    dX2 = simulation['dX2']
    Hm1 = simulation['Hm1']
    
    ny = simulation['ny']
    nx = simulation['nx']
    nz = simulation['nz']
    
    vx = simulation['vx']
    vy = simulation['vy']
    vz = simulation['vz']
    
    topog = simulation['topog']

    dyinv = 1.0/dX2 

    for ix in range(nx):
        for iy in range(ny):
            
            dxinv = 1.0/(Hm1[iy,ix]*dX1)
            
            zx    = dxinv*(topog[iy,min(ix+1,nx-1)] -topog[iy,ix])
            zy    = dyinv*(topog[min(iy+1,ny-1),ix] -topog[iy,ix]) 
            
            for iz in range(1,nz): # not in lowest layer
                
                vs = zx*vx[iz,iy,ix]+zy*vy[iz,iy,ix]
                
                vz[iz,iy,ix] = vz[iz,iy,ix] - vs
                
    return np.asarray(vz)            
            

                    
                    
                    
                    
                    
                    
                    
# for interpolation

def interpolate1D(data_matrix, data_height_matrix, zlayer,f):
    print(f)
    
    a = np.shape(data_height_matrix)
   
    
    
    if (len(a)) == 4:
        
        returnval = np.zeros((int(a[0]),len(zlayer),int(a[2]),int(a[3])))
        
        if f != 'p_lvls':
            for it in range(np.shape(data_matrix)[0]): # loop over nmet
                returnval[it,:,:,:] = interpolate1D_c(data_matrix[it,:,:,:], data_height_matrix[it,:,:,:],zlayer,b = 0)
                
                if f == 'qv':
                    print(it)
        else: 
            for it in range(np.shape(data_height_matrix)[0]): # loop over nmet
                
                returnval[it,:,:,:] = interpolate1D_c(data_matrix[:,:,:], data_height_matrix[it,:,:,:],zlayer,b = 0)
                
    else:
        
        b = 0
        
        if f == 'qv':
            b = 1
            
        returnval = interpolate1D_c(data_matrix,data_height_matrix,zlayer,b)
        #print(np.amax(returnval))
    
    # print('%s finished with maximum value: %s' % (f,np.amax(np.asarray(returnval))))
     
    return np.asarray(returnval)    
        
        
        
        
    
cdef double [:,:,:] interpolate1D_c(double [:,:,:] data, double [:,:,:] datapoints, long [:] newpoints, double b):
    
    cdef double [:,:,:] interpol = np.zeros((len(newpoints),np.shape(data)[1],np.shape(data)[2]))
    
    cdef int ix,iy,i,j
    
    
    
    for ix in range(np.shape(data)[2]):
        
        if b == 1:
            
            print('%s/%s' %(ix,np.shape(data)[2]))
            
        for iy in range(np.shape(data)[1]):
            
            interp = np.zeros((len(newpoints)))
            
            values = data[:,iy,ix]
            pts    = datapoints[0:len(values),iy,ix]
            
            for i in range(len(newpoints)):
                for j in range(len(pts)-1):
                    
                    if newpoints[i] == pts[j]:
                        interp[i] = values[j]
                        
                    elif pts[j] <= newpoints[i] and pts[j+1] >= newpoints[i]:
                        dx1 = newpoints[i] - pts[j]
                        dx2 = pts[j+1] - newpoints[i]
                        interp[i] = (values[j]*dx2 + values[j+1]*dx1)/(dx1 + dx2) 
                        
                    # extrapolation    
                    elif (newpoints[i] >= pts[len(pts)-1]):
                        interp[i] = values[len(values)-1]
                    elif (newpoints[i] <= pts[0]):
                        interp[i] = values[0]    
            
            for iz in range(len(newpoints)):
                interpol[iz,iy,ix] = interp[iz]
            
    return interpol                           
                    
                    
                    
                    
                    
                    
                    
def ter2asl(array3D,topog,zlayer,nx,ny):
    
    cdef int ix,iy,iz,izz
    
    nz = len(zlayer)

    # Converts a 3d array from terrain following coordinates to absolute coordinates
    res = array3D[0:nz,0:ny,0:nx]
    
    for ix in range(nx):
        for iy in range(ny):
            
            zo = max(0,topog[iy,ix])
            
            for iz in range(nz):
                
                zasl = zlayer[iz]    # this is the point to find
                
                if(zasl < (zo+zlayer[0])):
                    array3D[iz,iy,ix] = 0
                    
                else:
                    found = False
                    izz   = -1
                    
                    while not found:
                        
                        izz = izz + 1
                        
                        if( (zasl >= (zo+zlayer[izz])) and (zasl <= (zo+zlayer[izz+1])) ):
                            
                            found = True
                            s = (zo+zlayer[izz+1]-zasl)/(zlayer[izz+1]-zlayer[izz])
                            
                            array3D[iz,iy,ix] = s*res[izz,iy,ix] + (1-s)*res[izz+1,iy,ix]
                            
                        if( (izz+1 == nz-1) and (found == False)):
                            
                            array3D[iz,iy,ix] = 0
                            found = True
                            
    return np.asarray(array3D)        
                            
                            
                        
                        
                
                
def correct_divergence(vx,vy,vz,nx,ny,nz,dX1,dX2,Hm1,dz):

    # Cython function to eliminate divergence
    result = correct_divergence_c(vx,vy,vz,nx,ny,nz,dX1,dX2,Hm1,dz)
    
    vx = result[0:nz,0:ny,0:nx]
    vy = result[nz:2*nz,0:ny,0:nx]
    
    return (np.asarray(vx),np.asarray(vy))
    
    
    
cdef double [:,:,:] correct_divergence_c(double [:,:,:] vx, double [:,:,:] vy, double [:,:,:] vz, int nx, int ny, int nz, double dX1, double dX2, double [:,:] Hm1, double [:] dz):    

    cdef double epsilon = 10.0**-3 # threshold value
    cdef double max_div = 10.0
    
    cdef double [:,:,:] divergence
    
    cdef double [:,:,:] result = np.zeros((2*nz,ny,nx))
    
    cdef int iteration = 1
    
    cdef int maxiter = 100
    
    # iterative procedure as in CALMET
    while max_div > epsilon:
        
        
        
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                
                    vi = vx[k,j,i]
                    vi_1 = vx[k,j,max(0,i-1)]
                    vj = vy[k,j,i]
                    vj_1 = vy[k,max(0,j-1),i]
                    vk = vz[k,j,i]
                    vk_1 = vz[max(0,k-1),j,i]
                    
                    # divergence at this point
                    divu = (vi - vi_1)/(Hm1[j,i]*dX1) + (vj - vj_1)/dX2 + (vk - vk_1)/dz[k]
                    
                    # correction factor 
                    vx_corr = - divu*Hm1[j,i]*dX1/2
                    vy_corr = -divu*dX2/2
                    
                    # apply correcting
                    vx[k,j,i] =  vx[k,j,i] - vx_corr
                    vx[k,j,max(0,i-1)] = vx[k,j,max(0,i-1)] + vx_corr
                    
                    vy[k,j,i] = vy[k,j,i] - vy_corr
                    vy[k,max(0,j-1),i] = vy[k,max(0,j-1),i] + vy_corr
                    
        # calculate divergence after this iteration
        divergence = np.zeros((nz,ny,nx))
        
        max_div = 0
        
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                    
                    divergence[k,j,i] = vi = vx[k,j,i]
                    vi_1 = vx[k,j,max(0,i-1)]
                    vj = vy[k,j,i]
                    vj_1 = vy[k,max(0,j-1),i]
                    vk = vz[k,j,i]
                    vk_1 = vz[max(0,k-1),j,i]
                    
                    # divergence at this point
                    divergence[k,j,i] = (vi - vi_1)/(Hm1[j,i]*dX1) + (vj - vj_1)/dX2 + (vk - vk_1)/dz[k]
                    
                    if sqrt(divergence[k,j,i]*divergence[k,j,i]) > max_div:
                        max_div = divergence[k,j,i]
                    
          
         
        if iteration == 1:
            print('Creating non-divergent wind field. Starting with a maximum divergence of %s per second.' % round(max_div,2))             
        
        iteration = iteration + 1
        
        # stop after number of iterations
        if iteration > maxiter:
            max_div = 0
            print('Warning: Maximum number of iterations reached.')
        
    print('Non-divergent wind field created. Within %s iterations a maximum divergence of %s was reached.' \
          %(iteration-1,round(max_div,2)))

    result[0:nz,0:ny,0:nx] = vx
    result[nz:2*nz,0:ny,0:nx] = vy
        
    return result
                    
            
                        
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
    
    
    
        