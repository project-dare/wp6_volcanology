"""
 FALL3D: 3-D Advection-Diffusion-Sedimentation model for tephra fallout
 
 Authors:         Arnau Folch, Antonio Costa, Giovanni Macedonio
 
                  Translated to Python/Cython by Regina Beckmann 2018
                  Modified for DARE by Michael Grund, Andre Gemuend 2019/2020
                  
                  
 Please note:     To do a simulation in the framework of the DARE VC use-case 
                  run this file according to the given instructions


"""

import timeit

import numpy as np

from Fall3D.AshfallSimulation import AshfallSimulation

start = timeit.default_timer()

#######################################
# Initialize the run (setup.f90)
#######################################

# Initialization of variables (default values)

dt = 1.0
erumass = 0  # Total erupted mass
outmass = 0  # Output mass
g = 9.81  # gravitational acceleration
k = 0.4  # Karman constant
Rearth = 6356 * 10 ** 3  # earth´s radius (m)
n = 0  # needed for plotting

# TODO: User gives input_file as input
# input_file = 'EruptionExample'

# find path of Fall3D program
# pathFall3D = path.abspath(path.join(__file__ ,"../.."))

# create simulation class object
simu = AshfallSimulation(name=input_file, path=pathFall3D)

time = simu['beg_time'] * 3600
sourcetime = True
meteotime = True
go_on = True

# index of current eruptive phase
source_ind = 0
iiter = 1

if simu['source_type'] == 'RESUSPENSION':
    end_erupt = simu['run_end'] * 3600
else:
    if simu['ndt'] > 1:
        end_erupt = simu['erupt_end'][source_ind] * 3600
    else:
        end_erupt = simu['erupt_end'] * 3600

print('FALL3D: Simulation starting')

while go_on:

    ##########################################################################
    # read source term
    ##########################################################################

    if time >= end_erupt:
        sourcetime = True
        # set to true if imported eruptive phase/meteo phase is over
        print('FALL3D: Eruptive phase ended. ')

    if (sourcetime):

        if (simu['source_time'] * 3600 <= time) and time < end_erupt:

            # import current sresuspensionource term
            simu.readsource(source_ind, pathFall3D, input_file)

            sourcetime = False

        elif time > end_erupt:

            if source_ind == simu['ndt'] - 1:
                # last eruptive phase is over: set source term to zero
                simu['tmrat'] = np.zeros((simu['nsou'], simu['nc']))
                simu['flowrate'] = 0
                sourcetime = False
                # set erupt_end to high value
                end_erupt = 10 ** 20
                print('FALL3D: End of last eruptive phase. Source term switched off.')

            else:
                # read source data from next step if necessary
                source_ind = source_ind + 1
                end_erupt = simu['erupt_end'][source_ind] * 3600
                simu['source_time'] = simu['erupt_start'][source_ind]

                # check whether next eruptive phase is already continuing
                if (simu['source_time'] * 3600 <= time) and time < end_erupt:

                    # YES
                    print('FALL3D: (Re-) importing source term at iteration %s (after %s h)' % (
                        iiter, round(time / 3600 - simu['beg_time'], 2)))
                    simu.readsource(source_ind, pathFall3D, input_file)

                    sourcetime = False

                else:
                    # NO
                    simu['tmrat'] = np.zeros((simu['nsou'], simu['nc']))
                    simu['flowrate'] = 0
                    # check at every iteration whether next eruptive phase started
                    sourcetime = True

    if time >= (simu['meteo_time'] + simu['dtdbs']) * 3600:
        # increase meteo index by 1
        simu['meteo_ind'] = simu['meteo_ind'] + 1
        simu['meteo_time'] = simu['ibdbs'] + simu['meteo_ind'] * simu['dtdbs']
        meteotime = True

    # read meteo data
    if (simu['meteo_time'] * 3600 <= time):  # should be fulfilled now
        if meteotime:  # data not read if there is no change

            print('FALL3D: (Re-) importing climate data at iteration %s (after %s h)' % (
                iiter, round(time / 3600 - simu['beg_time'], 2)))
            simu.readmeteo(simu['meteo_ind'], pathFall3D, input_file)

            # calculate settling velocity
            simu.calc_vset()

            # calculate dry deposition velocity + correct vertical velocity
            simu.calc_vdry()

            # add radial wind field if enabled
            simu.gravity_current(time)

            # calculate dt
            dt = simu.critical_dt()

            # safety factor
            dt = 0.9 * dt

            # correct divergence of wind field 
            # simu.correct_divergence()

            meteotime = False

    # calculate aggregation parameters
    if simu['aggregationtime']:
        simu.calc_aggr_par()

        # increase time
    time = time + dt

    iiter = iiter + 1

    erumass = erumass + simu['flowrate'] * dt  # total mass including gas species

    # set source term
    simu.setsrc(dt)

    # Loop on particles classes to solve PDE (sizloop.f90)
    for ic in range(simu['nc']):
        simu.solveads(ic, dt, iiter)

        # mass lost at boundaries
    simu.lostmass(dt, erumass)

    # compute ground load/concentration every dt_images hours       
    if time - simu['beg_time'] * 3600 > simu['dt_images'] * 3600 * n or iiter == 2:

        # mass inside domain
        simu.calcmass(dt)

        simu.giveinfo(time, dt, iiter, erumass)

        n = n + 1

        if simu['massdep'] / erumass > 0.97 or simu['massvol'] / erumass < 0.01:
            go_on = False

    # -MG- comment to ingore this plotting routine, was commented by Regina originally    
    # simu.accumulation_time(pathFall3D,input_file,iiter,time)

    if iiter % 50 == 0:
        print('Iteration number %s.' % iiter)

    # end loop 
    if time > simu['run_end'] * 3600:
        go_on = False

###########################
# RUNEND
###########################      

simu.saveres()

simu.giveinfo(time, dt, iiter=2.5, erumass=erumass)

print('FALL3D: End of run.')
# time needed for run    
stop = timeit.default_timer()
print('Runtime: %s min' % round((stop - start) / 60, 2))
print('Number of iterations: %s' % iiter)
# average time per iteration
print('Average time per iteration needed: %s s' % (round((stop - start) / (iiter - 1), 1)))
