# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 14:37:14 2018

@author: Regina

Purpose: This file generates a granulometric distribution in the 
         Fall3D input file format (Corresponding to SetTgsd.F90)
         
         
modified: 2020-06-10 
    plotting some parameters is currently not supported in the framework of 
    DARE (via matplotlib), if that should be changed uncomment all plt.* lines 
    in the current script and ensure to have matplotlib==3.1.3 in the docker 
    file         
         
"""
# from datetime import datetime
import numpy as np
import math
import json
from scipy import stats
import os
# import matplotlib.pyplot as plt

##############################################################################
# how is sampling chosen?

mass_consideration = True
# if true, the sampling of particle classes is done so that every class holds 100%/nc of total mass (recommended)

##############################################################################

verbose = True  # print process comments in console

# import JSON Input file
file = input_file

json_file = open(file)
f = json_file.read()
indata = json.loads(f)

# read parameters needed
fimax = indata["GRANULOMETRY"]["FI_RANGE"]["FIMAX"]
fimin = indata["GRANULOMETRY"]["FI_RANGE"]['FIMIN']
fi_mean = indata["GRANULOMETRY"]["FI_MEAN"]
fi_disp = indata["GRANULOMETRY"]['FI_DISP']

# error checks
try:
    if  fi_mean < 0:
        raise ValueError('Value smaller than zero!')
except ValueError:
        print('Value error: Select a value equal or larger than 0 for fi_mean!')
        raise




if mass_consideration == True:
    nc = 1000
    nc_final = indata["GRANULOMETRY"]['NUMBER_CLASSES']
else:
    nc = indata["GRANULOMETRY"]['NUMBER_CLASSES']

rhomax = indata["GRANULOMETRY"]["DENSITY_RANGE"]['RHOMAX']
rhomin = indata["GRANULOMETRY"]["DENSITY_RANGE"]['RHOMIN']
shape_max = indata["GRANULOMETRY"]["SHAPE_RANGE"]['SHAPE_MAX']
shape_min = indata["GRANULOMETRY"]["SHAPE_RANGE"]['SHAPE_MIN']

# Compute particle diameter (in mm) as in setfrac.f90
results = np.zeros((nc, 4))  # diameter, density, shapefactor - psi, fraction

fi = np.linspace(fimin, fimax, nc)
deltafi = fi[1] - fi[0]

# Compute Diameters
results[:, 0] = (2.0) ** (-fi)

# Density rhop(ic) and shape factor psi(nc)
fimin0 = -1.0  # as in Bonadonna and Phillips (2003)
fimax0 = 6.0  # as in Bonadonna and Phillips (2003)

# Calculate Rho and Psi   
for ic in range(nc):
    results[ic, 1] = (rhomax - rhomin) * (fi[ic] - fimin) / (
                fimax - fimin) + rhomin  # max(min((rhomax-rhomin)*(fi[ic]-fimin0)/(fimax0-fimin0)+rhomin,rhomax),rhomin)   # variables are missing
    results[ic, 2] = (shape_max - shape_min) * (fi[ic] - fimin) / (fimax - fimin) + shape_min

"""
Calculate fractions fc of the particle classes
"""

DISTRIBUTION = indata["GRANULOMETRY"]["DISTRIBUTION"]

if (DISTRIBUTION == 'GAUSSIAN' or DISTRIBUTION == 'WEIBULL'):
    ng = 1
    # warning if fimin/fimax define too narrow range
    if fimax < fi_mean[0] + 2 * fi_disp[0] or fimin > fi_mean[0] - 2 * fi_disp[0]:
        print('TGSD: Warning. Extend total size range to include all defined diameters.')
elif (DISTRIBUTION == 'BIGAUSSIAN' or DISTRIBUTION == 'BIWEIBULL'):
    ng = 2
else:
    if verbose: print('TGSD: Type of distribution not implemented')
    quit()

"""
Computes the mass for a given value of fi between fimin and fimax
(mass between fi-deltafi/2 and fi+deltafi/2) for a Gaussian/Weibull distribution
The ultimate values fimin and fimax are extended to +- infinity
to ensure mass conservation.
"""

# allocate array for values of fc
work = np.zeros((nc, ng))

distributions = ['GAUSSIAN', 'BIGAUSSIAN']

# print(ng)
if (DISTRIBUTION in distributions):
    for ig in range(ng):
        for ic in range(nc):
            fi_c = fi[ic]
            if fi_c >= fimax: fi_c -= deltafi / 2
            if fi_c <= fimin: fi_c += deltafi / 2

            work[ic, ig] = stats.norm.cdf(fi[ic], fi_mean[ig] - deltafi / 2, fi_disp[ig])

"""
Fraction fc(ic) (weibull or biweibull normalized --> Note: variable fimean 
stores the Weibull scale factor (Lambda) and variable fidisp stores 
the Weibull shape factor k          
"""

distributions = ['WEIBULL', 'BIWEIBULL']

if (DISTRIBUTION == 'WEIBULL' or DISTRIBUTION == 'BIWEIBULL'):

    if min(fi) < 0:
        a = abs(min(fi))
    else:
        a = 0

    for ig in range(ng):
        for ic in range(nc):
            fi_c = fi[ic]
            if fi_c >= fimax: fi_c -= deltafi / 2
            if fi_c <= fimin: fi_c += deltafi / 2
            work[ic, ig] = stats.weibull_min.cdf(fi[ic] + 2 * a, fi_mean[ig] - deltafi / 2, fi_disp[ig])

###Normalize distribution
work0 = np.zeros_like(work)
for ig in range(ng):
    work[:, ig] = work[:, ig] / np.max(work[:, ig])
    work0[:, ig] = work[:, ig]
    for ic in range(nc):
        if ic > 0:
            work0[ic, ig] = abs(work[ic, ig] - work[ic - 1, ig])

work = work0
del work0

pweight = np.ones(ng) / ng
# define pweight as in readinp.f90
if (DISTRIBUTION == 'BIWEIBULL' or DISTRIBUTION == 'BIGAUSSIAN'):
    if (math.isnan(indata["GRANULOMETRY"]["MIXING_FACTOR"]) == False):
        pweight[0] = indata["GRANULOMETRY"]["MIXING_FACTOR"]
        pweight[1] = 1.0 - indata["GRANULOMETRY"]["MIXING_FACTOR"]

    # calculate mass fraction with weight function (for Bigaussian and Biweibull relevant)
fc = np.zeros(nc)

for ic in range(nc):
    results[ic, 3] = 0.0
    for ig in range(ng):
        results[ic, 3] = results[ic, 3] + pweight[ig] * work[ic, ig]

results[:, 3] = results[:, 3] / deltafi  # % of total

if mass_consideration == True:

    print('TGSD: adaption enabled to ensure equal mass fraction of particle classes.')

    results_new = np.zeros((nc_final, 4))
    index_border = np.zeros((nc_final))
    # sum up classes to higher classes

    fc_class_fraction = 1 / nc_final  # uniform mass fraction

    # find index of classes that belong together

    results[:, 3] = results[:, 3] * deltafi  # fraction of 1

    ind_start = 0

    total_nc = 0

    for ic in range(0, nc_final):
        # sum of mass fractions in this class       

        summ = 0

        # loop over all initial classes
        for i in range(ind_start, nc):

            summ = summ + results[i, 3]

            if summ >= fc_class_fraction:

                results_new[ic, 3] = summ  # fraction of 1

                if ic == nc_final - 1:
                    i = nc

                if ic < nc_final - 1:
                    index_border[ic] = i

                # weight properties of old classes with fc
                results_new[ic, 0] = np.sum(results[ind_start:i, 0] * results[ind_start:i, 3]) / np.sum(
                    results[ind_start:i, 3])
                results_new[ic, 1] = np.sum(results[ind_start:i, 1] * results[ind_start:i, 3]) / np.sum(
                    results[ind_start:i, 3])
                results_new[ic, 2] = np.sum(results[ind_start:i, 2] * results[ind_start:i, 3]) / np.sum(
                    results[ind_start:i, 3])

                # print('Class %s includes %s perc of starting classes. Total mass fraction: %s perc.' %(ic+1,100*(i-ind_start)/nc,round(summ*100,0)))
                total_nc = total_nc + (i - ind_start)

                ind_start = i

                break

    fi_new = - np.log10(results_new[:, 0]) / np.log10(2)  # transform to Phi vector

    ### plotting not supported in DARE framework
    # fig = plt.figure()
    # ax3 = plt.subplot(223)
    # plt.plot(fi[1:len(fi)], results[1:len(fi), 3] * 100 * deltafi, color='g')
    # for xv in index_border:
    #     plt.axvline(x=fi[int(xv)], color='k', linestyle=':', linewidth=1)
    # plt.ylabel('Total mass fraction/%')
    # plt.xlabel('Phi')
    # ax4 = plt.subplot(224)
    # plt.plot(fi, results[:, 0], color='r')
    # plt.scatter(fi_new, results_new[:, 0], color='r')
    # for xv in index_border:
    #     plt.axvline(x=fi[int(xv)], color='k', linestyle=':', linewidth=1)
    # plt.ylabel('Diameter/mm')
    # plt.xlabel('Phi')
    # ax1 = plt.subplot(221, sharex=ax3)
    # plt.plot(fi, results[:, 1], color='b')
    # plt.scatter(fi_new, results_new[:, 1], color='b')
    # for xv in index_border:
    #     plt.axvline(x=fi[int(xv)], color='k', linestyle=':', linewidth=1)
    # plt.ylabel('Density/kg/m$^3$')
    # ax2 = plt.subplot(222, sharex=ax4)
    # plt.plot(fi, results[:, 2], color='orange')
    # plt.scatter(fi_new, results_new[:, 2], color='orange')
    # for xv in index_border:
    #     plt.axvline(x=fi[int(xv)], color='k', linestyle=':', linewidth=1)
    # plt.ylabel('Shape factor')

    # compare with original sampling
    #    fi_original = np.linspace(fimin,fimax,nc_final)
    ##
    #    plt.figure()
    #    plt.plot(fi[1:len(fi)],results[1:len(fi),3]*100*deltafi,color='k',linestyle = ':') # distribution
    #    for xv in range(len(fi_original)):
    #        plt.axvline(x=fi_original[int(xv)],color='k',linestyle='-',linewidth=1)
    #    plt.xlabel(r'$\Phi$')
    #    plt.ylabel('Total mass fraction/%')
    # plt.legend(name = 'bla')
    #    for xv in range(nc_final):
    #        plt.axvline(x=fi_original[int(xv)],color='b',linestyle='-',linewidth=2)
    #

    # correct to 1.0
    fc_tot = np.sum(results_new[:, 3])

    results_new[:, 3] = results_new[:, 3] / fc_tot

    deltafi_new = (fimax - fimin) / (nc_final - 1)

    results_new[:, 3] = results_new[:, 3] / deltafi_new

    fid = os.path.join(os.getcwd(), "GranulometryTGSD.txt")
    np.savetxt(fid, results_new, fmt='%5.6f', delimiter=' ', header='Diameter Density Psi Faction', comments='')

else:

    # total mass fraction (should be approx. 1) --> integral
    fc_tot = np.sum(results[:, 3]) * deltafi

    # write results to file in same directory as input file
    fid = os.path.join(os.getcwd(), "GranulometryTGSD.txt")
    np.savetxt(fid, results, fmt='%5.6f', delimiter=' ', header='Diameter Density Psi Faction', comments='')

    ### plotting not supported in DARE framework
    # fig = plt.figure()
    # ax3 = plt.subplot(223)
    # plt.plot(fi, results[:, 3] * 100 * deltafi, '-o', color='g')
    # plt.ylabel('Total mass fraction/%')
    # plt.xlabel('Phi')
    # ax4 = plt.subplot(224)
    # plt.plot(fi, results[:, 0], '-o', color='r')
    # plt.ylabel('Diameter/mm')
    # plt.xlabel('Phi')
    # ax1 = plt.subplot(221, sharex=ax3)
    # plt.plot(fi, results[:, 1] / 1000, '-o', color='b')
    # plt.ylabel('Density/t/m$^3$')
    # ax2 = plt.subplot(222, sharex=ax4)
    # plt.plot(fi, results[:, 2], '-o', color='orange')
    # plt.ylabel('Shape factor')

##------------------------------------------------------------
## Weibull distributions: define the distribution parameters to be plotted
# k_values = [0.5, 5, 5, 5]
# lam_values = [1, 1, 2,3]
# linestyles = ['-', '--', ':', '-.', '--']
# mu = 0
# x = np.linspace(-10, 10, 1000)
#
##------------------------------------------------------------
## plot the distributions
# fig, ax = plt.subplots(figsize=(5, 3.75))
#
# for (k, lam, ls) in zip(k_values, lam_values, linestyles):
#    dist = dweibull(k, mu, lam)
#    plt.plot(x, dist.pdf(x), ls=ls, c='black',
#             label=r'$k=%.1f,\ \lambda=%i$' % (k, lam))
#
# plt.xlim(0, 5)
# plt.ylim(0, 0.6)
#
# plt.xlabel('$-ln_2(d)$')
# plt.ylabel(r'$fc(d|k,\lambda)$')
# plt.title('Weibull Distribution')
#
# plt.legend()
# plt.show()
#
