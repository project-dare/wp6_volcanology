# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 10:27:51 2018

@author: rbeck

modified: 2020-06-10 
    plotting of source term for control reasons and distribution 
    are currently not supported in the framework of DARE (via matplotlib),
    if that should be changed uncomment all plt.* lines in the current script
    and ensure to have matplotlib==3.1.3 in the docker file

"""
import os
import json
import math
import pickle

#import matplotlib.pyplot as plt
import numpy as np

from Fall3D import Csource


class Source(dict):

    def __init__(self, name, path=None):
        self.name = name
        self.path = path

        # if path!=None:
        self.sourceinfo(path, name)  # wird immer ausgeführt
        self.modgran(path, name)
        self.erupinfo(path, name)
        self.sourcetypeinfo(path, name)
        self.getgrid(path, name)

    def sourceinfo(self, path, name):
        # reads information about the source from the input file

        # inp_file file path
        file = name

        json_file = open(file)
        f = json_file.read()
        inp_file = json.loads(f)

        # information on source and data type
        self['analysis_type'] = inp_file["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"]
        self['source_type'] = inp_file["SOURCE"]["SOURCE_TYPE"]
        self['data_type'] = inp_file["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"]

        # aggregation
        self['aggregation'] = inp_file["AGGREGATION"]["AGGREGATION"]
        self['aggr_type'] = inp_file["AGGREGATION"]["AGGREGATION_MODEL"]

        # information on TGSD
        self['fimax'] = inp_file["GRANULOMETRY"]["FI_RANGE"]["FIMAX"]
        self['fimin'] = inp_file["GRANULOMETRY"]["FI_RANGE"]["FIMIN"]
        self['nc'] = inp_file["GRANULOMETRY"]["NUMBER_CLASSES"]

        if self['analysis_type'] == 'NCEP':
            self['analysis_type'] = 'NCAR'  # naming issues

        if (self['source_type'] != 'PLUME') and (self['source_type'] != 'SUZUKI') and (
                self['source_type'] != 'HAT') and (self['source_type'] != 'POINT') and (
                self['source_type'] != 'RESUSPENSION'):
            print('SetSRC: Source type not implemented.')
            quit()

        # no aggregation for resuspension source
        if (self['aggregation'] == 'RESUSPENSION') or (self['aggregation'] == False):
            self['aggr_type'] = 'NONE'
        else:
            if (self['aggr_type'] != 'CORNELL') and (self['aggr_type'] != 'PERCENTAGE') and (
                    self['aggr_type'] != 'COSTA'):
                print('SetSRC: Aggregation type not implemented. Aggregation set to none.')
                self['aggr_type'] = 'NONE'
                self['aggregation'] = False

        # Costa model only compatible with plume source
        if (self['aggr_type'] == 'COSTA') and (self['selftype'] != 'PLUME'):
            print('SetSRC: COSTA aggregation model is compatible only with the PLUME source option')
            self['aggr_type'] = 'NONE'

            # read aggregation block
        if (self['aggregation']):
            self['phi_aggr'] = inp_file["AGGREGATION"]["FI_AGGREGATES"]
            self['diam_aggr'] = 2.0 ** (-self['phi_aggr'])  # diameter in mm
            self['diam_aggr'] = 10 ** (-3) * self['diam_aggr']  # diameter in m
            self['rho_aggr'] = inp_file["AGGREGATION"]["DENSITY_AGGREGATES"]
            self['vset_aggr'] = inp_file["AGGREGATION"]["VSET_FACTOR"]  # set to 1.0 if not defined!

            if (self['aggr_type'] == 'PERCENTAGE'):
                self['perc'] = inp_file["AGGREGATION"]["PERCENTAGE"] * 10 ** (-2)
            if (self['aggr_type'] == 'COSTA'):
                self['Df'] = inp_file["AGGREGATION"]["FRACTAL_EXPONENT"]
                if (np.isnan(self['Df'])): self['Df'] = 2.99

        # sulfur dioxide cloud
        self['SO2'] = inp_file["SO2"]["SO2"]

        if self['SO2'] == True:
            self['SO2_percentage'] = inp_file["SO2"]["SO2_PERCENTAGE"]

        # total number of particles
        self['npart'] = inp_file["GRANULOMETRY"]["NUMBER_CLASSES"]

        # add one class if aggregation is considered
        if (self['aggregation']):
            self['npart'] = self['npart'] + 1

            # number of gas classes (+1 if SO2 enabled)
        self['ngas'] = 0

        if (self['SO2']):
            self['ngas'] = self['ngas'] + 1

            # total number of classes
        self['nc'] = self['npart'] + self['ngas']

        return self.get_dict()
        # return self

    def modgran(self, path, name):

        # reads the granulometry file from SetTGSD
        file = os.path.join(os.getcwd(), 'GranulometryTGSD.txt')

        f = open(file, 'r+')
        self['tgsd'] = np.loadtxt(f, delimiter=' ', comments='Diameter Density Psi Faction')

        # allocate arrays
        fc = np.zeros((self['nc']))
        rhop = np.zeros((self['nc']))
        diam = np.zeros((self['nc']))
        fip = np.zeros((self['nc']))
        psi = np.zeros((self['nc']))

        # set parameters for aggregates
        if (self['aggregation']):

            # diameter of aggreates (in mm)
            self['diam_aggr'] = 2 ** (-self['phi_aggr'])
            diam[self['npart'] - 1] = self['diam_aggr']
            rhop[self['npart'] - 1] = self['rho_aggr']
            psi[self['npart'] - 1] = 1.0
            fc[self['npart'] - 1] = 0.0

            fi = np.linspace(self['fimin'], self['fimax'], self['npart'] - 1)

        else:
            fi = np.linspace(self['fimin'], self['fimax'], self['npart'])

        # check that fc sums 1
        self['epsilon'] = 10 ** (-2)

        deltafi = fi[1] - fi[0]

        self['tgsd'][:, 3] = self['tgsd'][:, 3] * deltafi

        if (np.sum(self['tgsd'][:, 3]) < 1 - self['epsilon']) or (np.sum(self['tgsd'][:, 3]) > 1 + self['epsilon']):
            print('SetSRC: Imported particle mass fraction not normalized.')

            Sum = np.sum(self['tgsd'][:, 3])
            print('Sum is %s' % Sum)

        if self['aggregation']:
            diam[0:self['npart'] - 1] = self['tgsd'][:, 0]
            rhop[0:self['npart'] - 1] = self['tgsd'][:, 1]
            psi[0:self['npart'] - 1] = self['tgsd'][:, 2]
            fc[0:self['npart'] - 1] = self['tgsd'][:, 3]

        else:
            diam[0:self['npart']] = self['tgsd'][:, 0]
            rhop[0:self['npart']] = self['tgsd'][:, 1]
            psi[0:self['npart']] = self['tgsd'][:, 2]
            fc[0:self['npart']] = self['tgsd'][:, 3]

        # calculate phi (only for solid particles)
        fip[0:self['npart']] = -np.log(diam[0:self['npart']]) / np.log(2.0)

        """
        Aerosol properties. For the gas aerosol components values for density, rho and
        shape are assumed. Approximation can be justifyed by the low
        mass fraction. This is used to generate a column with gas
        distribuion (e.g. H2O or SO2)
        """

        if (self['ngas'] > 0):
            # until now only SO2 is possible
            diam[self['npart']:self['npart'] + self['ngas']] = 10 ** (-3)  # in mm (assuming 1 micrometer)
            rhop[self['npart']:self['npart'] + self['ngas']] = 10 ** 3  # TODO: check density value
            psi[self['npart']:self['npart'] + self['ngas']] = 1

            if (self['SO2']):
                fc[self['npart']] = self['SO2_percentage'] / 100

        # calculate averaged particle density (without aggregates)
        rho_mean = 0

        if (self['aggregation']):
            for ic in range(self['npart'] - 1):
                rho_mean = rho_mean + rhop[ic] * fc[ic]
        else:
            for ic in range(self['npart']):
                rho_mean = rho_mean + rhop[ic] * fc[ic]

        self['rho_mean'] = rho_mean

        ##############################################################################
        # Computes aggregation if set & corrects TGSD
        ##############################################################################

        if self['aggregation']:
            if self['aggr_type'] == 'PERCENTAGE':
                fc = Csource.percentage(self, fc, diam)
            elif self['aggr_type'] == 'CORNELL':
                fc = Csource.cornell(self, fc, diam)
            elif self['aggr_type'] == 'COSTA':
                Csource.costa(self, fc, diam)

        results = np.zeros((self['nc'], 4))

        results[:, 0] = diam
        results[:, 1] = rhop
        results[:, 2] = psi
        results[:, 3] = fc

        self['tgsd'] = results

        # write adapted granulometry file (aggregation and aerosols)
        file = os.path.join(os.getcwd(), 'GranulometrySRC.txt')
        np.savetxt(file, results, fmt='%5.6f', delimiter=' ',
                   header='Diameter(mm) Density(kg/m^3) Psi Mass Fraction(%)', comments='')

        return self.get_dict()

    def erupinfo(self, path, name):
        # Get properties for eruption mode (readinp_erup.f90)

        # inp_file file path
        file = name

        json_file = open(file)
        f = json_file.read()
        inp_file = json.loads(f)

        self['erupt_start'] = inp_file["TIME"]["ERUPTION_START"]
        self['erupt_end'] = inp_file["TIME"]["ERUPTION_END"]
        self['run_end'] = inp_file["TIME"]["RUN_END"]
        self['meteo_start'] = inp_file["TIME"]["BEGIN_METEO_DATA"]
        self['meteo_end'] = inp_file["TIME"]["END_METEO_DATA"]
        self['year_erupt'] = inp_file["TIME"]["YEAR"]
        self['month_erupt'] = inp_file["TIME"]["MONTH"]
        self['day_erupt'] = inp_file["TIME"]["DAY"]

        if inp_file["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] == "MONTHLY_MEAN":
            # set very large
            self['meteo_inc'] = 10 ** 9
        else:
            self['meteo_inc'] = inp_file["TIME"]["TIME_STEP_METEO_DATA"]

            # find number of eruptive phases
        if hasattr(self['erupt_start'], "__len__"):

            self['ndt'] = len(self['erupt_start'])

            if self['ndt'] == 0:
                print('SetSRC: Empty value in eruption start.')
                quit()

            for idt in range(self['ndt']):
                if self['erupt_start'][idt] > np.amax(self['erupt_end']):
                    print('SetSRC: Inconsistency between eruption intervals and eruption end.')
                    quit()

        else:
            self['ndt'] = 1

            if self['source_type'] != 'RESUSPENSION':
                if self['erupt_start'] > self['erupt_end']:
                    print('SetSRC: Inconsistency between eruption start and eruption end.')
                    quit()

            elif self['source_type'] == 'RESUSPENSION':
                if self['erupt_start'] > self['run_end']:
                    print('SetSRC: Inconsistency between eruption start and run end.')
                    quit()

        # set begin time of run
        if self['ndt'] == 1:
            self['beg_time'] = self['erupt_start']
        elif self['ndt'] > 1:
            if self['source_type'] == 'RESUSPENSION':
                self['beg_time'] = self['erupt_start']
            else:
                self['beg_time'] = self['erupt_start'][0]

                # type of source
        if self['source_type'] == 'POINT':
            print('SetSRC: Point source assumed.')
            self['plume_ns'] = 1
            self['plume_np'] = 0
        elif self['source_type'] == 'SUZUKI':
            print('SetSRC: Suzuki source assumed.')
            self['plume_ns'] = 100
            self['plume_np'] = 0
        elif self['source_type'] == 'HAT':
            print('SetSRC: Hat source assumed.')
            self['plume_ns'] = 100
            self['plume_np'] = 0
        elif self['source_type'] == 'PLUME':
            print('SetSRC: Plume source assumed.')
            self['plume_ns'] = 300
            self['plume_np'] = 200
        else:
            print('SetSRC: Resuspension assumed as source.')
            # find number of resuspension phases
            run_beg = self['erupt_start']  # this should be one value (h)
            self['runtime'] = run_beg - self['run_end']  # in h

            # number of weather data sets needed (at least 1)
            if self['data_type'] == 'MONTHLY_MEAN':
                self['ndt'] = 1
            else:
                self['ndt'] = int(self['runtime'] / self['meteo_inc']) + 1

        return self.get_dict()

    def sourcetypeinfo(self, path, name):
        # read data depending on source type

        # inp_file file path
        file = name

        json_file = open(file)
        f = json_file.read()
        inp_file = json.loads(f)

        # Estimation type of MFR
        self['est_mfr'] = inp_file["SOURCE"][self['source_type']]["ESTIMATE_MFR"]

        # Point source
        if self['source_type'] == 'POINT':

            self['hplume'] = inp_file["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"]
            self['MFR'] = inp_file["SOURCE"]["POINT"]["MASS_FLOW_RATE"]

            if (hasattr(self['erupt_start'], "__len__")) and (hasattr(self['hplume'], "__len__")):
                if len(self['erupt_start']) != len(self['hplume']):
                    print('Check consitency of number of eruptive phases and plume heights.')
                    quit()

            if self['MFR'] == 'Est':

                # self['est_mfr'] = inp_file["SOURCE"]["POINT"]["ESTIMATE_MFR"]

                # more than 1 eruptive phase
                if self['ndt'] > 1:
                    self['m0_dt'] = np.zeros((len(self['erupt_start'])))

                    if self['est_mfr'] == 'MASTIN':
                        mfr_wind_coupling = False

                        for idt in range(self['ndt']):
                            self['m0_dt'][idt] = self['rho_mean'] * (
                                    (0.5 * self['hplume'][idt] / 1000) ** (1.0 / 0.241))

                    elif self['est_mfr'] == 'WOODHOUSE':
                        mfr_wind_coupling = True
                    elif self['est_mfr'] == 'DEGRUYTER':
                        mfr_wind_coupling = True

                        # only 1 eruptive phase
                if self['ndt'] == 1:
                    if self['est_mfr'] == 'MASTIN':
                        self['m0_dt'] = self['rho_mean'] * ((0.5 * self['hplume'] / 1000) ** (1.0 / 0.241))
                    elif self['est_mfr'] == 'WOODHOUSE':
                        mfr_wind_coupling = True
                    elif self['est_mfr'] == 'DEGRUYTER':
                        mfr_wind_coupling = True

            else:
                if self['ndt'] > 1:

                    self['m0_dt'] = np.zeros((len(self['erupt_start'])))

                    for idt in range(self['ndt']):
                        self['m0_dt'][idt] = self['MFR'][idt]

                if self['ndt'] == 1:
                    self['m0_dt'] = self['MFR']

        # Suzuki source
        if (self['source_type'] == 'SUZUKI'):

            self['hplume'] = inp_file["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"]
            self['MFR'] = inp_file["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"]
            self['A'] = inp_file["SOURCE"]["SUZUKI"]["A"]
            self['L'] = inp_file["SOURCE"]["SUZUKI"]["L"]

            if (hasattr(self['erupt_start'], "__len__")):

                if (hasattr(self['hplume'], "__len__") == False) or (hasattr(self['A'], "__len__") == False) or (
                        hasattr(self['L'], "__len__") == False):
                    print('SetSRC: Inconsistency between eruption intervals and source data.')
                    quit()
                else:
                    if ((len(self['hplume'])) != len(self['erupt_start'])) or (
                            (len(self['A'])) != len(self['erupt_start'])) or (
                            (len(self['L'])) != len(self['erupt_start'])):
                        print('SetSRC: Inconsitency between eruption intervals and source data.')
                        quit()

            if (self['MFR'] == 'Est'):

                # self['est_mfr'] = inp_file["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"]

                # more than 1 eruptive phase
                if self['ndt'] > 1:
                    self['m0_dt'] = np.zeros((len(self['erupt_start'])))

                    if self['est_mfr'] == 'MASTIN':
                        mfr_wind_coupling = False

                        for idt in range(self['ndt']):
                            self['m0_dt'][idt] = self['rho_mean'] * (
                                    (0.5 * self['hplume'][idt] / 1000) ** (1.0 / 0.241))

                    elif self['est_mfr'] == 'WOODHOUSE':
                        mfr_wind_coupling = True
                    elif self['est_mfr'] == 'DEGRUYTER':
                        mfr_wind_coupling = True

                        # only 1 eruptive phase
                if self['ndt'] == 1:
                    if self['est_mfr'] == 'MASTIN':
                        self['m0_dt'] = self['rho_mean'] * ((0.5 * self['hplume'] / 1000) ** (1.0 / 0.241))
                    elif self['est_mfr'] == 'WOODHOUSE':
                        mfr_wind_coupling = True
                    elif self['est_mfr'] == 'DEGRUYTER':
                        mfr_wind_coupling = True

            else:
                if self['ndt'] > 1:

                    self['m0_dt'] = np.zeros((len(self['erupt_start'])))

                    for idt in range(self['ndt']):
                        self['m0_dt'][idt] = self['MFR'][idt]

                if self['ndt'] == 1:
                    self['m0_dt'] = self['MFR']

        # Hat source
        if (self['source_type'] == 'HAT'):

            self['thick'] = np.asarray(inp_file["SOURCE"]["HAT"]["THICKNESS"])
            self['hplume'] = np.asarray(inp_file["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"])
            self['MFR'] = inp_file["SOURCE"]["HAT"]["MASS_FLOW_RATE"]

            if (hasattr(self['erupt_start'], "__len__")):
                if (hasattr(self['hplume'], "__len__") == False) or (hasattr(self['thick'], "__len__") == False):
                    print('SetSRC: Inconsistency between eruption intervals and source data.')
                    quit()
                else:
                    if ((len(self['hplume'])) != len(self['erupt_start'])) or (
                            (len(self['thick'])) != len(self['erupt_start'])):
                        print('SetSRC: Inconsitency between eruption intervals and source data.')
                        quit()

            else:
                if (hasattr(self['hplume'], "__len__") == True) or (hasattr(self['thick'], "__len__") == True):
                    print('SetSRC: Inconsistency between eruption intervals and source data.')

            if (self['MFR'] == 'Est'):

                self['est_mfr'] = inp_file["SOURCE"]["HAT"]["ESTIMATE_MFR"]

                # more than 1 eruptive phase
                if self['ndt'] > 1:
                    self['m0_dt'] = np.zeros((len(self['erupt_start'])))

                    if self['est_mfr'] == 'MASTIN':
                        mfr_wind_coupling = False

                        for idt in range(self['ndt']):
                            self['m0_dt'][idt] = self['rho_mean'] * (
                                    (0.5 * self['hplume'][idt] / 1000) ** (1.0 / 0.241))


                    elif self['est_mfr'] == 'WOODHOUSE':
                        mfr_wind_coupling = True
                    elif self['est_mfr'] == 'DEGRUYTER':
                        mfr_wind_coupling = True

                        # only 1 eruptive phase
                else:
                    if self['est_mfr'] == 'MASTIN':
                        self['m0_dt'] = self['rho_mean'] * ((0.5 * self['hplume'] / 1000) ** (1.0 / 0.241))
                    elif self['est_mfr'] == 'WOODHOUSE':
                        mfr_wind_coupling = True
                    elif self['est_mfr'] == 'DEGRUYTER':
                        mfr_wind_coupling = True

            else:
                self['m0_dt'] = self['MFR']

        if (self['source_type'] == 'PLUME'):

            # plume solving strategy
            self['solve_plume_for'] = inp_file["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"]

            if (self['solve_plume_for'] == 'MFR'):
                self['search_range'] = inp_file["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"]
                self['hplume'] = inp_file["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"]

            elif (self['solve_plume_for'] == 'HEIGHT'):
                self['m0_dt'] = inp_file["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"]

            else:
                print('SetSRC: Please specify the type of plume solving strategy.')

            # read parameters
            self['u0'] = inp_file["SOURCE"]["PLUME"]["EXIT_VEL"]
            self['T01'] = inp_file["SOURCE"]["PLUME"]["EXIT_TEM"]
            self['w0'] = inp_file["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] / 100

            self['plume_wind_coupling'] = inp_file["SOURCE"]["PLUME"]["WIND_COUPLING"]
            self['plume_moist_air'] = inp_file["SOURCE"]["PLUME"]["AIR_MOISTURE"]
            self['plume_latent_heat'] = inp_file["SOURCE"]["PLUME"]["LATENT_HEAT"]
            self['plume_reentrainment'] = inp_file["SOURCE"]["PLUME"]["REENTRAINMENT"]
            self['plume_type_as'] = inp_file["SOURCE"]["PLUME"]["A_S"]
            self['plume_type_av'] = inp_file["SOURCE"]["PLUME"]["A_V"]

            self['plume_umbrella_model'] = 'SPARKS1986'  # default model

            if (hasattr(self['plume_type_as'], "__len__")):
                plume_a_s_jet = self['plume_type_as'][0]
                plume_a_s_plume = self['plume_type_as'][1]
            else:
                print('SetSRC: Please specify two values for A_s.')

            if (self['ndt'] > 1) and (hasattr(self['u0'], "__len__") == False) or (
                    hasattr(self['T01'], "__len__") == False) or (hasattr(self['w0'], "__len__") == False):
                print('SetSRC: Inconsistency between eruption intervals and source data.')

        # Resuspension source (not implemented yet)    
        if (self['source_type'] == 'RESUSPENSION'):

            self['emission_scheme'] = inp_file["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"]
            self['emission_factor'] = inp_file["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"]
            self['moisture_correction'] = inp_file["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"]

            if self['emission_scheme'] == 'WESTPHAL':
                self['tust_cte'] = inp_file["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"]
            elif self['emission_scheme'] == 'MARTICORENA' or self['emission_scheme'] == 'SHAO':
                self['tust_cte'] = 0
            else:
                print('SetSRC: Incorrect emission scheme type')

            self['z_emission'] = inp_file["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"]
            self['mindep'] = inp_file["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"]
            self['diam_max'] = inp_file["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] * 10 ** (
                -3)  # convert to mm (given in micrometer)

        return self.get_dict()

    def getgrid(self, path, name):
        # read vent coordinates & topography

        # inp_file file path
        file = name

        json_file = open(file)
        f = json_file.read()
        inp_file = json.loads(f)

        # should be lon-lat
        self['coord_sys'] = inp_file["GRID"]["COORDINATES"]

        if (self['coord_sys'] != 'LON-LAT'):
            print('SetSRC: Inocrrect system of coordinates. Please use LON-LAT.')
            quit()

        # Position of vent
        self['X0'] = inp_file["GRID"]["LON_VENT"]
        self['Y0'] = inp_file["GRID"]["LAT_VENT"]
        self['Z0'] = inp_file["GRID"]["VENT_HEIGHT"]

        self['lonmin'] = inp_file["GRID"]["LONMIN"]
        self['lonmax'] = inp_file["GRID"]["LONMAX"]
        self['latmin'] = inp_file["GRID"]["LATMIN"]
        self['latmax'] = inp_file["GRID"]["LATMAX"]

        # resolution in km
        self['res'] = inp_file["GRID"]["TOPO_RESOL"]

        long = [self['lonmin'], self['lonmax']]
        lat = [self['latmin'], self['latmax']]

        # import topography
        self.gettopo(self['res'], long, lat)

        # number of vertical layers
        self['zmin'] = inp_file["GRID"]["ZLAYER"]["MIN"]
        self['zmax'] = inp_file["GRID"]["ZLAYER"]["MAX"]
        self['zinc'] = inp_file["GRID"]["ZLAYER"]["INCREMENT"]

        self['dlon'] = (self['lonmax'] - self['lonmin']) / (self['nx'] - 1)
        self['dlat'] = (self['latmax'] - self['latmin']) / (self['ny'] - 1)

        self['zlayer'] = np.arange(self['zmin'], self['zmax'] + self['zinc'], self['zinc'])
        self['nz'] = len(self['zlayer'])

        return self.get_dict()

    def get_dict(self):
        # needed for almost all functions
        export = dict()
        for key in self.keys(): export[key] = self[key]
        return export

    def gettopo(self, res, long, lat):

        #        path_rlodin = self.path + '/rlodin_small/'
        #        sys.path.insert(0, path_rlodin)
        #        from rlodin.Geometry import Domain
        from Fall3D import Domain

        topopath = self.path + '/TsupyDEM/'

        sl = [[long[0], long[1], lat[0], lat[1], res, 'domainName']]
        d = Domain.Domain(sl, kind='DEM', DEMfolder=topopath)

        # read topography
        self['topog'] = d[0]['grids']['DEM']
        self['lon_topo'] = d[0]['x_space']
        self['lat_topo'] = d[0]['y_space']

        self['nx'] = len(self['lon_topo'])
        self['ny'] = len(self['lat_topo'])

        # flip topography so that index 0 corresponds to western/southern rim
        self['topog'] = np.flip(self['topog'], 0)

        self['topog'][self['topog'] < 0] = 0.1

        return self.get_dict()

    def solvepoint(self, m0, Hplume):
        # solve for point source (not recommended)

        fc = self['tgsd'][:, 3]

        self['Xplum'] = np.ones((self['plume_ns'])) * self['X0']
        self['Yplum'] = np.ones((self['plume_ns'])) * self['Y0']
        self['Zplum'] = np.ones((self['plume_ns'])) * (self['Z0'] + Hplume)

        # mass flow rate of different classes
        Mplum = np.zeros((self['nc'], 1))
        Mplum[:, 0] = m0 * fc

        self['Mplum'] = Mplum

        return self.get_dict()

    def solvehat(self, m0, Hplume, idt):
        # hat source

        Mplum = np.zeros((self['nc'], self['plume_ns']))

        if self['ndt'] > 1:
            # thickness of hat
            Thick = self['thick'][idt]
        else:
            Thick = self['thick']

        self['Xplum'] = np.ones((self['plume_ns'])) * self['X0']
        self['Yplum'] = np.ones((self['plume_ns'])) * self['Y0']

        # Vertical position and total mass according to hat distribution
        plume_ns = self['plume_ns']

        S = np.zeros((plume_ns))
        Zplum = np.zeros((plume_ns))

        deltaz = Thick / (plume_ns - 1)  # from the bottom to Hplum

        for inds in range(plume_ns):
            z = Hplume - Thick + inds * deltaz
            S[inds] = 1.0
            Zplum[inds] = self['Z0'] + z

        # normalization
        S[0:plume_ns] = m0 * S[0:plume_ns] / plume_ns

        fc = self['tgsd'][:, 3]

        # mass distribution
        for inds in range(plume_ns):
            for ic in range(self['nc']):
                Mplum[ic, inds] = fc[ic] * S[inds]

        self['Mplum'] = Mplum
        self['Zplum'] = Zplum
        self['S'] = S

        return self.get_dict()

    def solvesuzuki(self, m0, Hplume, idt):

        # Suzuki parameters 
        if self['ndt'] == 1:
            asuzu = self['A']
            lsuzu = self['L']
        else:
            asuzu = self['A'][idt]
            lsuzu = self['L'][idt]

        self['Xplum'] = np.ones((self['plume_ns'])) * self['X0']
        self['Yplum'] = np.ones((self['plume_ns'])) * self['Y0']

        plume_ns = self['plume_ns']

        S = np.zeros((plume_ns))
        Zplum = np.zeros((plume_ns))
        Mplum = np.zeros((self['nc'], plume_ns))

        summ = 0.0

        deltaz = Hplume / plume_ns

        # from the bottom to Hplum                 
        for inds in range(plume_ns):
            z = inds * deltaz
            S[inds] = ((1.0 - z / Hplume) * np.exp(asuzu * (z / Hplume - 1.0))) ** lsuzu
            Zplum[inds] = self['Z0'] + z
            summ = summ + S[inds]

            # normalization to MFR (SUM=MFR)
        S[0:plume_ns] = m0 * S[0:plume_ns] / summ
        fc = self['tgsd'][:, 3]

        # mass distribution
        for inds in range(plume_ns):
            for ic in range(self['nc']):
                Mplum[ic, inds] = fc[ic] * S[inds]

                # print(np.sum(fc))
        # print(np.sum(Mplum))
        self['S'] = S
        self['Mplum'] = Mplum
        self['Zplum'] = Zplum

        return self.get_dict()

    def getsrc(self, idt):
        # calculate mass generation array

        nx = self['nx']
        ny = self['ny']
        nz = self['nz']
        nc = self['nc']
        plume_ns = self['plume_ns']
        Xplum = self['Xplum']
        Yplum = self['Yplum']
        Zplum = self['Zplum']
        lonmin = self['lonmin']
        lonmax = self['lonmax']
        latmin = self['latmin']
        latmax = self['latmax']
        dlon = self['dlon']
        dlat = self['dlat']
        zlayer = self['zlayer']
        Mplum = self['Mplum']

        src = np.zeros((nc, ny, nx, nz))

        for inds in range(plume_ns):

            # source position in x
            x = Xplum[inds]

            found = False

            for i in range(nx - 1):
                if (x >= (lonmin + i * dlon)) and (x <= (lonmin + (i + 1) * dlon)):
                    found = True
                    ix = i

            if (found == False):
                print('SetSRC: Source position not found in x.')

            s = abs((x - (lonmin + ix * dlon)) / dlon)  # parameter s in (0,1)
            s = 2.0 * s - 1.0

            # source position in y
            y = Yplum[inds]

            found = False

            for i in range(ny - 1):
                if (y >= (latmin + i * dlat)) and (y <= (latmin + (i + 1) * dlat)):
                    found = True
                    iy = i

            if (found == False):
                print('SetSRC: Source position not found in y.')

            t = abs((y - (latmin + iy * dlat)) / dlat)  # parameter t in (0,1)
            t = 2.0 * t - 1.0  # parameter t in (-1,1)

            # function getshape.f90

            # Evaluates the shape function for 4-nodes a brick at the point (s,t)
            shape = np.zeros((4))

            sm = 0.5 * (1.0 - s)
            tm = 0.5 * (1.0 - t)
            sp = 0.5 * (1.0 + s)
            tp = 0.5 * (1.0 + t)

            shape[0] = sm * tm
            shape[1] = sp * tm
            shape[2] = sp * tp
            shape[3] = sm * tp

            # Interpolation on a (x,y) plane. This is necessary because each of the
            # four points (ix,iy),(ix+1,iy),(ix+1,iy+1),(ix,iy+1) may have a different
            # vertical interpolation duo to topography (i.e. interpolation on a simple
            # 8-nodes brick is not correct)

            zp = np.zeros((4))
            iz = np.zeros((4))
            w = np.zeros((4))

            if self['res'] >= 0.1:
                # topography spacing is too great, assume vent elevation
                zp[:] = self['Z0']
            else:
                zp[0] = self['topog'][iy, ix]  # Point (ix  ,iy  ) elevation
                zp[1] = self['topog'][iy, ix + 1]  # Point (ix+1,iy  ) elevation
                zp[2] = self['topog'][iy + 1, ix + 1]  # Point (ix+1,iy+1) elevation
                zp[3] = self['topog'][iy + 1, ix]  # Point (ix  ,iy+1) elevation

            z = Zplum[inds]

            # find source position in z
            for i in range(4):
                iz[i] = -1
                go_on = True

                while go_on:

                    iz[i] = iz[i] + 1

                    if ((z - zp[i]) < zlayer[0]):
                        go_on = False
                        w[i] = 0

                    elif ((z - zp[i]) >= zlayer[int(iz[i])]) and ((z - zp[i]) < zlayer[int(iz[i]) + 1]):
                        go_on = False
                        w[i] = (z - zp[i] - zlayer[int(iz[i])]) / (
                                zlayer[int(iz[i]) + 1] - zlayer[int(iz[i])])  # parameter w in (0,1)

                    elif iz[i] == nz - 1:
                        go_on = False
                        print(
                            'SetSRC: Source position not found in z. Choose higher domain height or better topography resolution.')

            for ic in range(nc):
                # calculate source term for eruptive phase idt
                src[ic, iy, ix, int(iz[0])] = src[ic, iy, ix, int(iz[0])] + (1.0 - w[0]) * shape[0] * Mplum[ic, inds]
                src[ic, iy, ix, int(iz[0]) + 1] = src[ic, iy, ix, int(iz[0]) + 1] + w[0] * shape[0] * Mplum[ic, inds]

                src[ic, iy, ix + 1, int(iz[1])] = src[ic, iy, ix + 1, int(iz[1])] + (1.0 - w[1]) * shape[1] * Mplum[
                    ic, inds]
                src[ic, iy, ix + 1, int(iz[1]) + 1] = src[ic, iy, ix + 1, int(iz[1]) + 1] + w[1] * shape[1] * Mplum[
                    ic, inds]

                src[ic, iy + 1, ix + 1, int(iz[2])] = src[ic, iy + 1, ix + 1, int(iz[2])] + (1.0 - w[2]) * shape[2] * \
                                                      Mplum[ic, inds]
                src[ic, iy + 1, ix + 1, int(iz[2]) + 1] = src[ic, iy + 1, ix + 1, int(iz[2]) + 1] + w[2] * shape[2] * \
                                                          Mplum[ic, inds]

                src[ic, iy + 1, ix, int(iz[3])] = src[ic, iy + 1, ix, int(iz[3])] + (1.0 - w[3]) * shape[3] * Mplum[
                    ic, inds]
                src[ic, iy + 1, ix, int(iz[3]) + 1] = src[ic, iy + 1, ix, int(iz[3]) + 1] + w[3] * shape[3] * Mplum[
                    ic, inds]

                # get number of sources
        nsrc = 0

        for iz in range(nz):
            for ix in range(nx):
                for iy in range(ny):
                    found = False
                    for ic in range(nc):
                        if src[ic, iy, ix, iz] > 0:
                            found = True

                    if found:
                        nsrc = nsrc + 1

        # extract only source points
        tmrat = np.zeros((nsrc, nc))
        xsrc = np.zeros((nsrc))
        ysrc = np.zeros((nsrc))
        zsrc = np.zeros((nsrc))

        ind = 0

        for iz in range(nz):
            for ix in range(nx):
                for iy in range(ny):
                    found = False

                    for ic in range(nc):
                        if src[ic, iy, ix, iz] > 0:
                            found = True
                            tmrat[ind, ic] = src[ic, iy, ix, iz]

                    if found:
                        xsrc[ind] = ix
                        ysrc[ind] = iy
                        zsrc[ind] = iz
                        ind = ind + 1

        self['tmrat_%s' % idt] = tmrat
        self['xsrc_%s' % idt] = xsrc
        self['ysrc_%s' % idt] = ysrc
        self['zsrc_%s' % idt] = zsrc
        self['nsrc_%s' % idt] = nsrc
        self['zs_%s' % idt] = self['Zplum']

        print('Mass flow rate (kg/s) for eruptive phase %s \n Max: %s \n Min: %s  \n Avg: %s \n Sum: %s' % (
            idt + 1, np.amax(src), np.amin(src), np.sum(src) / (nx * ny * nz * nc), np.sum(src)))

        # plot source term for control reasons
        src_classes = np.sum(src, axis=0)
        src_above = src_classes[:, :, int(zsrc[0])]

        # plt.figure()
        # plt.imshow(src_above, extent=(lonmin, lonmax, latmin, latmax), origin='lower', cmap='binary')
        # plt.xlabel('Longitude (deg)')
        # plt.ylabel('Latitude (deg)')
        # plt.title('Source term - Eruptive phase %s' % (idt + 1))
        # plt.colorbar(label='Mass flow rate/kg/s')
        # plt.annotate('x Vent', xy=(int(xsrc[0]), int(ysrc[0])))

        # height distribution
        src_height = np.sum(src_classes, axis=0)
        src_height = np.sum(src_height, axis=0)

        # # plot distribution
        # if self['source_type'] == 'SUZUKI' or self['source_type'] == 'HAT':
        #     plt.figure()
        #     plt.plot(self['S'] / (Zplum[1] - Zplum[0]), (Zplum - self['Z0']) / 1000, label='Distribution function')
        #     plt.plot(src_height / (self['zlayer'][1] - self['zlayer'][0]), (self['zlayer']) / 1000, label='Source term')
        #     plt.xlabel('Density of mass flow rate/kg/s/m')
        #     plt.ylabel('Height above vent/km')
        #     plt.legend()
        #     # plt.title('Eruptive phase %s' % (idt+1))       

        return self.get_dict()

    def readmeteo(self, mind, path, name):

        #        path_rlodin = self.path + '/Fall3D/rlodin_small/'
        #
        #        sys.path.insert(0, path_rlodin)
        #        from rlodin.Geometry import Domain
        from Fall3D import Domain

        topopath = self.path + '/TsupyDEM/'

        sl = [[self['lonmin'], self['lonmax'], self['latmin'], self['latmax'], self['res'], 'VolcanoDomain']]
        d = Domain.Domain(sl, DEMfolder=topopath)
        # TODO change the path below
        folder = path + '/MeteoDataBase/' + self['analysis_type'] + '/' + self['data_type']

        # TODO: für entsprechende Eruptionszeitraum passende Meteo-Daten einlesen
        data_meteo = d.AddMeteo(folder, source=self['analysis_type'], year=self['year_erupt'],
                                month=self['month_erupt'], \
                                topography=self['topog'], zlayer=self['zlayer'], day=self['day_erupt'],
                                hour=self['beg_time'], \
                                run_end=self['run_end'], data_type=self['data_type'], path_C='None')

        # extract meteorological data from dict data_meteo        
        if self['data_type'] == 'MONTHLY_MEAN':

            u = data_meteo['data']['u']
            v = data_meteo['data']['v']
            z = data_meteo['data']['w']
            temp = data_meteo['data']['temp']
            hum = data_meteo['data']['hum']
            rho = data_meteo['data']['ro']
            vptemp = data_meteo['data']['Tp']  # potential temperature
            pres = data_meteo['data']['p_lvls']
            qv = data_meteo['data']['qv']  # specific humidity

            tsurf = data_meteo['data_pressure']['t2m']
            u10 = data_meteo['data_pressure']['u10']
            v10 = data_meteo['data_pressure']['v10']

            # Flip meteo data so that iy = 0 is south and ix = 0 west (which is the case) 
            meteovar = [u, v, z, temp, hum, rho]

            for f in meteovar:
                f = np.flip(f, 1)

            meteovar2D = [tsurf, u10, v10]

            for f in meteovar2D:
                f = np.flip(f, 0)  # axis 0 is latitude for 2D data

            self['vx'] = u
            self['vy'] = v
            self['vz'] = z
            self['temp'] = temp
            self['hum'] = hum
            self['rho'] = rho
            self['pres'] = pres
            self['tsurf'] = tsurf

        elif self['data_type'] == 'DAILY':

            # extract meteorological data from dict    
            u = data_meteo['data']['u'][mind, :, :, :]  # eastward wind
            v = data_meteo['data']['v'][mind, :, :, :]  # northward wind
            z = data_meteo['data']['w'][mind, :, :, :]  # vertical wind from omega
            temp = data_meteo['data']['temp'][mind, :, :, :]  # temperature in K
            hum = data_meteo['data']['hum'][mind, :, :, :]
            rho = data_meteo['data']['ro'][mind, :, :, :]
            vptemp = data_meteo['data']['Tp'][mind, :, :, :]  # potential temperature
            pres = data_meteo['data']['p_lvls'][mind, :, :, :]
            qv = data_meteo['data']['qv'][mind, :, :, :]  # specific humidity

            # temperature at surface           
            tsurf = data_meteo['data_pressure']['t2m'][mind, :, :]

            # velocities at surface
            u10 = data_meteo['data_pressure']['u10'][mind, :, :]
            v10 = data_meteo['data_pressure']['v10'][mind, :, :]

            # read boundary layer height
            if self['analysis_type'] == 'ERA5':
                zi = data_meteo['data_pressure']['blh'][mind, :, :]
            else:
                print('SetSRC: No boundary layer information available. Assuming a height of 1,500 m.')
                zi = np.ones((self['ny'], self['nx'])) * 1500

        tol = 10 ** -10

        nx = self['nx']
        ny = self['ny']

        # find index of vent position
        for i in range(nx):
            if abs(self['lonmin'] + i * self['dlon'] - self['X0']) <= self['dlon'] / 2 + tol:
                indx_vent = i

        for j in range(ny):
            if abs(self['latmin'] + j * self['dlat'] - self['Y0']) <= self['dlat'] / 2 + tol:
                indy_vent = j

        self['indx_vent'] = indx_vent
        self['indy_vent'] = indy_vent

        # horizontal wind speed at vent
        nz = self['nz']
        Vair = np.zeros((nz))

        for iz in range(nz):
            Vair[iz] = math.sqrt(
                u[iz, indy_vent, indx_vent] * u[iz, indy_vent, indx_vent] + v[iz, indy_vent, indx_vent] * v[
                    iz, indy_vent, indx_vent])

        self['Vair'] = Vair
        self['Rair0'] = rho[0, indy_vent, indx_vent]
        self['Tair0'] = tsurf[indy_vent, indx_vent]

        # air direction
        Aair = np.zeros((len(self['zlayer'])))
        PSIa = 0
        z = 0
        g = 9.8066

        # loop over all height levels
        for iz in range(len(self['zlayer'])):

            # TODO: for daily meteo data this has to be improved
            u1 = u[iz, indy_vent, indx_vent]
            v1 = v[iz, indy_vent, indx_vent]

            if (abs(u1) > 10 ** (-8)):
                angle = math.atan2(v1, u1) * 180 / math.pi
            else:
                if (v1 > 10 ** (-8)):
                    angle = 90
                elif (v1 < -10 ** (-8)):
                    angle = 270
                else:
                    angle = 0

            Aair[iz] = angle

            if (Aair[iz] < 0.0):
                Aair[iz] = 360.0 + Aair[iz]  # Angle in deg. (0 to 360)

            Aair[iz] = Aair[iz] * math.pi / 180  # Angle in Rad. (0 to 2pi)

            PSIa = PSIa + angle * (self['zlayer'][iz] - z)
            z = self['zlayer'][iz]

            # averaged wind direction
        PSIa = PSIa / self['zlayer'][len(self['zlayer']) - 1]
        if (PSIa < 0.0):
            PSIa = 360.0 + PSIa
        PSIa = PSIa * math.pi / 180.0

        Tair0 = tsurf[indy_vent, indx_vent]

        Cao = 998  # specific heat capacity at constant pressure of dry air (J kg^-1 K^-1)

        # compute squared buoyancy frequency
        Nair = np.zeros((len(self['zlayer'])))

        for iz in range(len(self['zlayer'])):

            if (iz == 0):
                dTdz = (temp[1, indy_vent, indx_vent] - temp[0, indy_vent, indx_vent]) / (
                        self['zlayer'][1] - self['zlayer'][0])
            elif (iz == len(self['zlayer'])):
                dTdz = (temp[len(self['zlayer']), indy_vent, indx_vent] - temp[
                    len(self['zlayer']) - 1, indy_vent, indx_vent]) / (self['zlayer'][1] - self['zlayer'][0])

            else:
                dTdz = (temp[iz, indy_vent, indx_vent] - temp[iz - 2, indy_vent, indx_vent]) / (
                        self['zlayer'][iz] - self['zlayer'][iz - 2])

            Nair[iz] = g * g * (1 + Cao * dTdz / g) / (Cao * Tair0)

        # Buoyancy frequency (Brunt-Vaisälä)    
        self['Nair'] = Nair

        return self.get_dict()

    def merwind(self, Hplume):

        if (self['est_mfr'] == 'DEGRUYTER'):

            # estimates MER as in Degruyter and Bonadonna (2012)

            Cao = 998.0  # specific heat capacity at constant pressure of dry air (J kg^-1 K^-1)
            Co = 1250.0  # specific heat capacity at constant pressure of solids (J kg^-1 K^-1)
            To = 1200.0  # initial plume temperature (K)
            alfa = 0.1  # radial entrainment coefficient
            beta = 0.5  # wind entrainment coefficient
            z1 = 2.8  # maximum non-dimensional height (Morton et al. 1956)
            g = 9.8066

            nz = len(self['zlayer'])

            Tair0 = self['Tair0']
            Rair0 = self['Rair0']
            Vair = self['Vair']
            Nair = self['Nair']
            zlayer = self['zlayer']

            gprime = g * (Co * To - Cao * Tair0) / (Cao * Tair0)

            v_mean = 0.0
            N_mean = 0.0
            jz = 1

            for iz in range(1, nz):
                if (zlayer[iz - 1] <= Hplume):
                    v_mean = v_mean + 0.5 * (Vair[iz - 1] + Vair[iz]) * (zlayer[iz] - zlayer[iz - 1])
                    N_mean = N_mean + 0.5 * (Nair[iz - 1] + Nair[iz]) * (zlayer[iz] - zlayer[iz - 1])
                    jz = iz

            v_mean = v_mean / (zlayer[jz - 1] - zlayer[0])
            N_mean = N_mean / (zlayer[jz - 1] - zlayer[0])
            N_mean = math.sqrt(N_mean)

            M0 = (2.0 ** (5.0 / 2.0)) * alfa * alfa * N_mean * N_mean * N_mean * Hplume * Hplume * Hplume * Hplume / (
                    z1 * z1 * z1 * z1)
            M0 = M0 + beta * beta * N_mean * N_mean * v_mean * Hplume * Hplume * Hplume / 6.0
            m0 = math.pi * Rair0 * M0 / gprime

            self['m0'] = m0

        if (self['est_mfr'] == 'WOODHOUSE'):

            # Estimates MER as in Woodhouse et al. (2013)

            N_mean = 0.0
            jz = 1
            Tair0 = self['Tair0']
            Rair0 = self['Rair0']
            Vair = self['Vair']
            Nair = self['Nair']
            zlayer = self['zlayer']
            nz = len(self['zlayer'])

            for iz in range(1, nz):
                if (zlayer[iz - 1] <= Hplume):
                    N_mean = N_mean + 0.5 * (Nair[iz - 1] + Nair[iz]) * (zlayer[iz] - zlayer[iz - 1])
                    jz = iz

            N_mean = N_mean / (zlayer[jz - 1] - zlayer[0])
            N_mean = math.sqrt(N_mean)

            V1 = Vair[jz - 1]  # reference velocity
            H1 = zlayer[jz - 1] - zlayer[0]  # reference height

            Ws = 1.44 * V1 / (N_mean * H1)
            Ws = 0.318 * (1.0 + 1.373 * Ws) / (1.0 + 4.266 * Ws + 0.3527 * Ws * Ws)
            m0 = (Hplume / 1000 / Ws) ** (1 / 0.253)

            self['m0'] = m0

        return self.get_dict()

    def par_ABL(self):

        Tz1 = self['temp'][1, :, :]
        z0 = 1  # reference level z0 (bottom, 1m)
        z1 = self['zlayer'][1]  # reference level z1 > z0
        Pz0 = self['pres'][0, :, :]  # pressure at levels
        Pz1 = self['pres'][1, :, :]

        u = self['vx'][1, :, :]  # wind speeds at z1
        v = self['vy'][1, :, :]

        ny = self['ny']
        nx = self['nx']

        # potential temperature at z1 and z0 (in K)
        Thz0 = self['tsurf'] * (1.01 * 100000 / Pz0) ** (0.285)
        Thz1 = Tz1 * (1.01 * 100000 / Pz1) ** (0.285)

        Tz0 = self['tsurf']

        # use Cython function
        rmonin, ustar = Csource.get_par_ABL(u, v, nx, ny, z1, z0, Tz1, Tz0, Thz0, Thz1)

        self['rmonin'] = np.asarray(rmonin)
        self['ustar'] = np.asarray(ustar)

        return self.get_dict()

    def getsrc_resus(self, path, name, idt):
        # TODO: testing

        # read deposits file from former run that is located in results folder
        file = os.path.join(os.getcwd(), 'Results.pkl')

        with open(file, 'rb') as f:
            x = pickle.load(f)

        cload = x['Groundload_part']  # ground load for each particle class
        gload = x['Groundload_tot']  # total ground load

        diam = self['tgsd'][:, 0]

        # number of classes in deposits file
        npar_dep = np.shape(cload)[2]

        # number of classes that can be transported
        nc = 0
        for ic in range(npar_dep):
            if diam[ic] < self['diam_max']:  # compare in mm
                nc = nc + 1

        if nc == 0:
            print('SetSRC: The deposit has no particles lower or equal than max size.')

        # calculate threshold friction velocity
        tust = Csource.thres_ust(self, npar_dep)

        if self['moisture_correction'] == True:
            # apply soil moisture correction
            tust = Csource.moist_corr(self, gload, cload, tust, npar_dep)

        # calculate emitted mass
        emis, emis_mas = Csource.calc_emis(self, tust, gload, cload, npar_dep, nc)

        # get number of sources
        nsrc = 0

        # number of emission layers
        nz_emission = 1

        for iz in range(self['nz']):
            if self['zlayer'][iz] <= self['z_emission']:
                nz_emission = iz + 1

        for ix in range(self['nx']):
            for iy in range(self['ny']):
                found = False

                for ic in range(nc):
                    if emis[iy, ix, ic] > 0:
                        found = True

                if found:
                    nsrc = nsrc + 1

                    # total number of sources
        nsrc_tot = nz_emission * nsrc

        # extract only source points with values > 0
        tmrat = np.zeros((nsrc_tot, nc))
        xsrc = np.zeros((nsrc_tot))
        ysrc = np.zeros((nsrc_tot))
        zsrc = np.zeros((nsrc_tot))

        ind = 0

        for ix in range(self['nx']):
            for iy in range(self['ny']):
                found = False

                for ic in range(nc):
                    if emis[iy, ix, ic] > 0:
                        found = True

                        for zs in range(nz_emission):
                            tmrat[ind + nsrc * zs, ic] = emis[iy, ix, ic]

                if found:
                    for zs in range(nz_emission):
                        # write source coordinates
                        xsrc[ind + nsrc * zs] = ix
                        ysrc[ind + nsrc * zs] = iy
                        zsrc[ind + nsrc * zs] = zs

                    ind = ind + 1

        self['MFR'] = np.sum(emis)

        self['tmrat_%s' % idt] = np.asarray(tmrat)
        self['xsrc_%s' % idt] = np.asarray(xsrc)
        self['ysrc_%s' % idt] = np.asarray(ysrc)
        self['zsrc_%s' % idt] = np.asarray(zsrc)
        self['nsrc_%s' % idt] = np.asarray(nsrc)

        return self.get_dict()
