# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 11:25:56 2018

@author: Regina
"""

# import modules
cimport cython
import numpy as np
import scipy as sp
cimport numpy as np
import math

from libc.math cimport sin, cos, acos, exp, sqrt


# Type in console to cythonize this code: python setup.py build_ext --inplace

def interpolate1D(data_matrix, data_height_matrix, zlayer,f):
    
    a = np.shape(data_matrix)
    
    if (len(a)) == 4:
        
        returnval = np.zeros((int(a[0]),int(a[1]),int(a[2]),int(a[3])))
        
        if f != 'p_lvls':
            for it in range(np.shape(data_matrix)[0]): # loop over nmet
                returnval[it,:,:,:] = interpolate1D_c(data_matrix[it,:,:,:], data_height_matrix[it,:,:,:],zlayer)
        else: 
            for it in range(np.shape(data_height_matrix)[0]): # loop over nmet
                returnval[it,:,:,:] = interpolate1D_c(data_matrix[:,:,:], data_height_matrix[it,:,:,:],zlayer)
                
    else:
        
        returnval = interpolate1D_c(data_matrix,data_height_matrix,zlayer)
        
    return np.asarray(returnval)    
        
        
        
        
    
cdef double [:,:,:] interpolate1D_c(double [:,:,:] data, double [:,:,:] datapoints, double [:] newpoints):
    
    cdef double [:,:,:] interpol
    
    cdef int ix,iy,i,j
    
    for ix in range(np.shape(data)[2]):
        for iy in range(np.shape(data)[1]):
            
            interp = np.zeros((len(newpoints)))
            
            values = data[:,iy,ix]
            pts    = datapoints[0:len(values),iy,ix]
            
            for i in range(len(newpoints)):
                for j in range(len(pts)-1):
                    
                    if newpoints[i] == pts[j]:
                        interp[i] = values[j]
                        
                    elif pts[j] <= newpoints[i] and pts[j+1] >= newpoints[i]:
                        dx1 = newpoints[i] - pts[j]
                        dx2 = pts[j+1] - newpoints[i]
                        interp[i] = (values[j]*dx2 + values[j+1]*dx1)/(dx1 + dx2) 
                        
                    # extrapolation    
                    elif (newpoints[i] >= pts[len(pts)-1]):
                        interp[i] = values[len(values)-1]
                    elif (newpoints[i] <= pts[0]):
                        interp[i] = values[0]    
            
            interpol[:,iy,ix] = interp
            
    return interpol        

        