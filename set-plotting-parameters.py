import json
import sys
import os

args = sys.argv
username = args[1]

with open(os.path.join(os.getcwd(), "project_parameters.json"), "r") as f:
    file = json.loads(f.read())
    jsonname = file["producer"][0]["jfilepath"]
# set user-specific plotting parameters 
paras = {}
paras["upload_folder"] = "/home/mpiuser/sfs/{}/uploads".format(username)
# change the gpi_admin (username) if the downloaded data are downloaded by a different user
paras["data_folder"] = "/home/mpiuser/sfs/gpi_admin/uploads"
paras["jfilepath"] = os.path.join(paras["upload_folder"], jsonname)
paras['plot_all'] = True # either set to True to plot all time steps (default) or give series of steps to plot e.g. [0, 1, 3, 10]
paras['figsize'] = (24.0/2.54,16.0/2.54)
paras['figedgecol'] = 'k' # color of figure edges
paras['figfacecol'] = 'w' # color of figure face
paras['format'] = 'png'
paras['dpi'] = 150 # resolution of output file
paras['alpha'] = 0.6 # transparency of plotted distributions
paras['plot_contour'] = True # plot contours of grids on map
paras['linew_contour'] = 1 # linewidth of contour lines
paras['linec_contour'] = 'k' # linecolor of contours
paras['labc_contour'] = 'w' # color of contour labels
paras['fonts_contour'] =10 # fontsize of contour labels
paras['ventcol'] = 'r' # facecolor of vent marker
paras['ventedgecol'] = 'k' # edgecolor of vent marker
paras['ventsize'] = 5 # markersize of vent
paras['cmap'] = 'Oranges' # used colormap for plotting distributions,'Oranges' is default
paras['background'] = 'real' # real or gray shaded background topography
paras['autores'] = True # use automatically best background resolution for area
paras['manures'] = None # define own background resolution (should be less than 10)
paras['cities'] = {}# plot larger cities in given area
paras['cities']['plot'] = True # if False, no cities are plotted
paras['cities']['fcolor'] = 'red' # facecolor of circles at city locations
paras['cities']['ecolor'] = 'k' # edgecolor of circles at city locations
paras['cities']['size'] = 10 # size of scatter circles
paras['cities']['tstyle'] = 'italic' # text style of annotations
paras['cities']['tfcol'] = 'w' # annotation facecolor
paras['cities']['tecol'] = 'k' # annotation edgecolor
paras['cities']['tewidth'] = 1 # annotation edge linewidth
paras['cities']['tfweight'] = 'bold' # annotation font weight

paras['inset'] = {} # plot an inset showing the location of the area in a global view
paras['inset']['plot'] = False # if False, no inset is plotted
paras['inset']['locsiz'] = [0.09, 0.7, 0.3, 0.25] # location and size of inset, [0.09, 0.7, 0.3, 0.25] corresponds roughly to upper left corner
paras['inset']['content'] = 'real' # use shaded ocean and continent background, parameters below are ignored, 'normal' uses the parameters below
paras['inset']['col_contf'] = 'lightgray' # color of continent fill
paras['inset']['col_conte'] = 'black'  # color of continent edges
paras['inset']['linw_conte'] = .75  # linewidth of continent edges
paras['inset']['col_ocean'] = 'powderblue' # color of oceans
paras['inset']['col_rect'] = 'firebrick' # color of rectangle showing the area 

input_dic = {"producer": [ {"plotting_parameters": paras}]}

with open('input_parameters.json', 'w') as fp:
    json.dump(input_dic, fp)
