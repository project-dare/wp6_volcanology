# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 10:57:30 2019

main author: Regina Beckmann (2019)
modified by: Michael Grund (2020/04)

run a full FALL3DPy simulation 

requires: 'project_parameters.json' 
          (stored in the main directory of F3DPy)
  
"""

import json
import os
import matplotlib.pyplot as plt
import json

###############################################################################
# input json file (generated in the beginning of the project workflow)
# is read based on the information given in 'project_parameters.json'

input_data = json.loads(os.environ["INPUT_DATA"])
input_file = input_data['jfilepath']

print('Run simulation for project: ' + input_file + '...')
print(' ')

##############################################################################

#find path of Fall3D program
pathFall3D = input_data["data_folder"]

# execute TGSD program 
exec(open("/home/mpiuser/docker/wp6_volcanology/Fall3D/TGSD.py").read())
plt.close("all")

# execute source master
from Fall3D.source import Source
exec(open("/home/mpiuser/docker/wp6_volcanology/Fall3D/SourceMaster.py").read())
plt.close("all")

# execute FALL3D master
#sys.path.insert(0, './Fall3D')
from Fall3D.AshfallSimulation import AshfallSimulation
exec(open("/home/mpiuser/docker/wp6_volcanology/Fall3D/Fall3DMaster.py").read())

##############################################################################

print(' ')
print('Simulation run finished!')