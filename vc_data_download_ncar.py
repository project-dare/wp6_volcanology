#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 15:42:49 2020

routines for preparation and downloading
meteorological data of NCAR 

@author: mgrund
"""

import os

# NCEP/NCAR (NOAA) meteo data, provided the source urls will not change in future 
def prep_download_ncar(jfile, download_folder):

    # json input required
    yofi = jfile['TIME']['YEAR']
    anatype = jfile['TIME']['TYPE_METEO_DATA']['ANALYSIS_TYPE']
    datatype = jfile['TIME']['TYPE_METEO_DATA']['DATA_TYPE']
    
    # some basic error handling
    try:
        if  anatype != 'NCAR' and anatype != 'NCEP':
            raise NameError('Request could not be completed.')
    except NameError:
        print('Input error: Select NCAR instead of ' +  anatype + ' in your json input file!')
        raise
        
    # url of ftp server where data is stored
    urlbas = 'ftp://ftp.cdc.noaa.gov/Datasets/ncep.reanalysis'
    
    if datatype == 'MONTHLY_MEAN':
        
        path2store = os.path.join(download_folder,
                                  'MeteoDataBase',
                                  'NCAR',
                                  'MONTHLY_MEAN')
        
    
        # file list for monthly data
        filelist = {'air': {'filename': 'air.mon.mean.nc', 
                            'url': urlbas + '.derived/pressure/air.mon.mean.nc'},
                   'air.sig995': {'filename': 'air.sig995.mon.mean.nc', 
                                  'url': urlbas + '.derived/surface/air.sig995.mon.mean.nc'}, 
                   'hgt': {'filename': 'hgt.mon.mean.nc', 
                           'url': urlbas + '.derived/pressure/hgt.mon.mean.nc'},  
                   'omega': {'filename': 'omega.mon.mean.nc', 
                             'url': urlbas + '.derived/pressure/omega.mon.mean.nc'},
                   'rhum': {'filename': 'rhum.mon.mean.nc', 
                            'url': urlbas + '.derived/pressure/rhum.mon.mean.nc'},
                   'uwnd': {'filename': 'uwnd.mon.mean.nc', 
                            'url': urlbas + '.derived/pressure/uwnd.mon.mean.nc'},
                   'uwnd.sig995': {'filename': 'uwnd.sig995.mon.mean.nc', 
                                   'url': urlbas +'.derived/surface/uwnd.sig995.mon.mean.nc'},
                   'vwnd': {'filename': 'vwnd.mon.mean.nc', 
                            'url': urlbas + '.derived/pressure/vwnd.mon.mean.nc'},
                   'vwnd.sig995': {'filename': 'vwnd.sig995.mon.mean.nc', 
                                   'url': urlbas +'.derived/surface/vwnd.sig995.mon.mean.nc'}}

    else:
        
        path2store = os.path.join(download_folder,
                                  'MeteoDataBase',
                                  'NCAR',
                                  'DAILY',
                                  str(yofi))

 
        # file list for daily data
        filelist = {'air.2m': {'filename': 'air.2m.gauss.' + str(yofi) + '.nc', 
                               'url': urlbas + '/surface_gauss/air.2m.gauss.' +str(yofi) + '.nc'},
                   'air': {'filename': 'air.' + str(yofi) + '.nc', 
                           'url': urlbas + '/pressure/air.' + str(yofi) + '.nc'},
                   'hgt': {'filename': 'hgt.' + str(yofi) + '.nc', 
                           'url': urlbas + '/pressure/hgt.' + str(yofi) + '.nc'},
                   'omega': {'filename': 'omega.' + str(yofi) + '.nc', 
                             'url': urlbas + '/pressure/omega.' + str(yofi) + '.nc'},
                   'rhum': {'filename': 'rhum.' + str(yofi) + '.nc', 
                            'url': urlbas + '/pressure/rhum.' + str(yofi) + '.nc'},
                   'uwnd.10m': {'filename': 'uwnd.10m.gauss.' + str(yofi) + '.nc', 
                                'url': urlbas + '/surface_gauss/uwnd.10m.gauss.' + str(yofi) + '.nc'},
                   'uwnd': {'filename': 'uwnd.' + str(yofi) + '.nc', 
                            'url': urlbas + '/pressure/uwnd.' + str(yofi) + '.nc'},
                   'vwnd.10m': {'filename': 'vwnd.10m.gauss.' + str(yofi) + '.nc', 
                                'url': urlbas + '/surface_gauss/vwnd.10m.gauss.' + str(yofi) + '.nc'},
                   'vwnd': {'filename': 'vwnd.' + str(yofi) + '.nc', 
                            'url': urlbas + '/pressure/vwnd.' + str(yofi) + '.nc'}}

    return filelist, path2store

######################################################################################################################################
