"""
Created on Tue May  5 11:18:45 2020

routines for preparation and downloading
meteorological data of ERA5

author: Michael Grund
"""

import os
import pathlib
import platform
import sys


def check4key():
    # search for API key file in home directory 

    if platform.system() == 'Windows':
        
        homepath = os.path.expanduser(os.getenv('USERPROFILE'))

        if not pathlib.Path(os.path.join(homepath,'.cdsapirc')).exists():
                print('File ".cdsapirc" is missing in your home directory! Check Windows instructions at https://cds.climate.copernicus.eu/api-how-to.')
                sys.exit(0)

            
    elif platform.system() == 'Linux':
        
        homepath = os.path.expanduser(os.getenv('HOME'))
        
        if not pathlib.Path(os.path.join(homepath, '.cdsapirc')).exists():
            print('File ".cdsapirc" is missing in your home directory! Check Linux instructions at https://cds.climate.copernicus.eu/api-how-to.')


    elif platform.system() == 'Darwin': #TODO MacOS
        pass
    
    else: # unknown platform, add here if required
        pass
    

#==========================================================================================================================================================================
#==========================================================================================================================================================================
# define file list

def prep_download_era(jfile, download_folder):

    # json input required
    yofi = jfile['TIME']['YEAR']
    mofi = jfile['TIME']['MONTH']
    dofi = jfile['TIME']['DAY']
    datatype = jfile['TIME']['TYPE_METEO_DATA']['DATA_TYPE']
    
    ###########################################################################

    # files required for simulations based on monthly means
    if datatype == 'MONTHLY_MEAN':
    
        path2store = os.path.join(download_folder,
                                  'MeteoDataBase', 
                                  'ERA5',
                                  'MONTHLY_MEAN')

        # define filenames for required data to download (see API call below)
        reqfile1 = '_'.join([str(yofi), str(mofi), 'pressure_lvl.nc']) 
        reqfile2 = '_'.join([str(yofi), str(mofi), 'surface.nc'])
        reqfile3 = '_'.join([str(yofi), str(mofi), 'invariant.nc'])
        
        file1 = ('reanalysis-era5-pressure-levels-monthly-means',
                        {
                            'product_type': 'monthly_averaged_reanalysis',
                            'variable': [
                                'geopotential', 'relative_humidity', 'temperature',
                                'u_component_of_wind', 'v_component_of_wind', 'vertical_velocity',
                            ],
                            'pressure_level': [
                                '1', '2', '3',
                                '5', '7', '10',
                                '20', '30', '50',
                                '70', '100', '125',
                                '150', '175', '200',
                                '225', '250', '300',
                                '350', '400', '450',
                                '500', '550', '600',
                                '650', '700', '750',
                                '775', '800', '825',
                                '850', '875', '900',
                                '925', '950', '975',
                                '1000',
                            ],
                            'year': str(yofi),
                            'month': [str(mofi)],
                            'time': '00:00',
                            'format': 'netcdf',
                        },
                        os.path.join(path2store, reqfile1))
         
         
        file2 = ('reanalysis-era5-single-levels-monthly-means',
                        {
                            'format': 'netcdf',
                            'variable': [
                                '10m_u_component_of_wind', '10m_v_component_of_wind', '2m_temperature',
                                'boundary_layer_height',
                            ],
                            'year': str(yofi),
                            'month': [str(mofi)],
                            'time': '00:00',
                            'product_type': 'monthly_averaged_reanalysis',
                        },
                        os.path.join(path2store, reqfile2))
        
        
        file3 = ('reanalysis-era5-single-levels-monthly-means',
                        {
                            'product_type': 'monthly_averaged_reanalysis',
                            'variable': 'land_sea_mask',
                            'year': [
                                str(yofi)],
                            'month': [str(mofi)],
                            'time': '00:00',
                            'format': 'netcdf',
                        },
                        os.path.join(path2store, reqfile3))
        
        
        filelist = {'erafile1': file1, 
                    'erafile2': file2, 
                    'erafile3': file3}
    
    ###########################################################################
    
    # files required for simulations based on daily means
    # TODO (not used by Regina so far, check which time is should be used?
    # 00:00, 05:00 etc. ????)  
    
    else:
        path2store = os.path.join(download_folder,
                                  'MeteoDataBase',
                                  'ERA5',
                                  'DAILY',
                                  str(yofi))
        
        
        # define filenames for required data to download (see API call below)
        reqfile1 = '_'.join([str(yofi), str(mofi), str(dofi), 'pressure_lvl.nc']) 
        reqfile2 = '_'.join([str(yofi), str(mofi), str(dofi), 'surface.nc']) 
        reqfile3 = '_'.join([str(yofi), str(mofi), str(dofi), 'invariant.nc'])
    
        file1 = ('reanalysis-era5-pressure-levels',
                        {
                            'product_type': 'reanalysis',
                            'format': 'netcdf',
                            'variable': [
                                'geopotential', 'relative_humidity', 'temperature',
                                'u_component_of_wind', 'v_component_of_wind', 'vertical_velocity',
                            ],
                            'pressure_level': [
                                '1', '2', '3',
                                '5', '7', '10',
                                '20', '30', '50',
                                '70', '100', '125',
                                '150', '175', '200',
                                '225', '250', '300',
                                '350', '400', '450',
                                '500', '550', '600',
                                '650', '700', '750',
                                '775', '800', '825',
                                '850', '875', '900',
                                '925', '950', '975',
                                '1000',
                            ],
                            'year': str(yofi),
                            'month': str(mofi),
                            'day': str(dofi),
                            'time': [
                                '00:00',
                            ],
                        },
                         os.path.join(path2store, reqfile1))  
        
        file2 = ('reanalysis-era5-single-levels',
                    {
                        'product_type': 'reanalysis',
                        'format': 'netcdf',
                        'variable': [
                            '10m_u_component_of_wind', '10m_v_component_of_wind', '2m_temperature',
                            'boundary_layer_height',
                        ],
                        'year': str(yofi),
                        'month': str(mofi),
                        'day': str(dofi),
                        'time': '00:00',
                    },
                    os.path.join(path2store, reqfile2))
        
        file3 = ('reanalysis-era5-single-levels',
                    {
                        'product_type': 'reanalysis',
                        'format': 'netcdf',
                        'variable': 'land_sea_mask',
                        'year': str(yofi),
                        'month': str(mofi),
                        'day': str(dofi),
                        'time': '00:00',
                    },
                    os.path.join(path2store, reqfile3))
        
        
        filelist = {'erafile1': file1, 
                    'erafile2': file2, 
                    'erafile3': file3}
        
    return filelist, path2store   
 
