# Before running the Fall3DPy jupyter notebook on your local machine, 
# please follow the instructions listed below

# 2020-05 -MG-

################################################################
################################################################
# packages required (we recommend to use anaconda for managing packages
# and to create a new environment e.g. env_f3dpy)

conda install pandas
conda install cython
conda install GDAL
conda install netcdf4
conda install -c conda-forge cartopy 

################################################################
# further packages required to run the workflows

#===========================================
# dispel4py for parallel computing

# 1. Installing dispel4py
>> git clone https://gitlab.com/project-dare/dispel4py.git
>> cd dispel4py
>> python setup.py install

# 2. Testing your installation using the �simple� (sequential) mapping
>> dispel4py simple dispel4py.examples.graph_testing.pipeline_test -i 10
#===========================================

# cdsapi, for download of ERA weather data (here also a registration 
# is required to request data via the API, for details see 
# https://cds.climate.copernicus.eu/api-how-to)

conda install cdsapi

################################################################
################################################################
# some files need to be cythonized before working on your system 
# CFall3D.pyx
# CDomain.pyx
# CSource.pyx
# the following command will translation them to C using Cython
# and compile them to native python extensions.

python setup.py build_ext --inplace

################################################################
################################################################