"""
Created on Mon May  4 12:42:08 2020

setup a simulation project based on
the given project name and the selected 
case

author: Michael Grund
"""

import sys
import json
import os
import requests

vc_use_cases = requests.get("https://gitlab.com/project-dare/wp6_volcanology/-/raw/master/vc_usecases.py")
with open("vc_usecases.py", "w") as f:
    f.write(vc_use_cases.text)

from vc_usecases import UseCases
from datetime import datetime

args = sys.argv
pjtname = args[1]
case = args[2]
username = args[3]

print('Welcome to the DARE volcanology use-case!')
print('')

# pre-defined example use-cases,
# be sure that the names appearing 
# in this list are correctly implemented 
# in vc_usescases.py
example_cases = ('Stromboli',
                 'Etna',
                 'Vulcano',
                 'Vesuvius',
                 'Eifel',
                 'Tavurvur',
                 'Guntur',
                 'mg_test_stromboli')

now = datetime.now()
pjtn = pjtname + '_' + now.strftime("%Y-%m-%d_%H_%M_%S")

# custom or pre-defined use-cases
if case == 'Custom':
    print('You are defining a custom scenario. Be sure to fill/modify your json file:')
    print('         >>> ' + pjtn + '.json <<<')
    print('before continuing with the remaining workflow steps!')
    jfile = UseCases.gen_json_cust(pjtn)

elif case in example_cases:
    print('Use example case "' + case + '"')
    print('Load corresponding parameters ...')
    jfile = UseCases.gen_json_pre(case, example_cases, pjtn)
    print('Write parameters to file >>> ' + pjtn + '.json <<< ...')

else:
    print('Unknown case! Available cases in FALL3DPy:')
    print('(A) custom')
    print('(B) pre-defined ' + str(example_cases))
    sys.exit(1)

# write json file for record and/or modification by the user
with open(os.path.join(os.getcwd(), pjtn + '.json'), 'w') as outfile:
    json.dump(jfile, outfile, indent=4)
      
print('Done!')

# write content for subsequent dispel4py workflow 
# "jfilepath" defines the path to the project json file created above
# "project" contains the user's defined project name which is used in several 
# of the following steps
# change the gpi_admin (username) if the downloaded data are downloaded by a different user - download_folder param)
input_dic = {"producer": [{"jfilepath": os.path.join('/home/mpiuser/sfs/', username, "uploads", pjtn + '.json'),
                           "download_folder": "/home/mpiuser/sfs/gpi_admin/uploads"}]}

with open('project_parameters.json', 'w') as fp:
    json.dump(input_dic, fp)
