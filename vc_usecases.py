#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 13:52:39 2020

Generate json files for different use-cases

1) empty json file with all parameters needed for simulation to set
2) predefined parameters for use-cases (e.g. Stromboli, Etna, Eifel ... 
   further use-cases can be easily added)

author: Michael Grund
"""


class UseCases:

    def gen_json_cust(pjtname):

        jfile = {}

        jfile["SCENARIO"] = {}
        jfile["SCENARIO"]["TYPE"] = "CUSTOM"
        jfile["SCENARIO"]["SCNAME"] = "CUSTOM"
        jfile["SCENARIO"]["NAME"] = pjtname

        jfile["TIME"] = {}
        jfile["TIME"]["YEAR"] = []
        jfile["TIME"]["MONTH"] = []
        jfile["TIME"]["DAY"] = []
        jfile["TIME"]["TYPE_METEO_DATA"] = {}
        jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = ' '
        jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = ' '
        jfile["TIME"]["BEGIN_METEO_DATA"] = []
        jfile["TIME"]["TIME_STEP_METEO_DATA"] = []
        jfile["TIME"]["END_METEO_DATA"] = []
        jfile["TIME"]["ERUPTION_START"] = []
        jfile["TIME"]["ERUPTION_END"] = []
        jfile["TIME"]["RUN_END"] = []
        jfile["TIME"]["RESTART"] = []

        jfile["GRID"] = {}
        jfile["GRID"]["COORDINATES"] = "LON-LAT"
        jfile["GRID"]["LONMIN"] = []
        jfile["GRID"]["LONMAX"] = []
        jfile["GRID"]["LATMIN"] = []
        jfile["GRID"]["LATMAX"] = []
        jfile["GRID"]["TOPO_RESOL"] = []
        jfile["GRID"]["LON_VENT"] = []
        jfile["GRID"]["LAT_VENT"] = []
        jfile["GRID"]["VENT_HEIGHT"] = []
        jfile["GRID"]["ZLAYER"] = {}
        jfile["GRID"]["ZLAYER"]["MIN"] = []
        jfile["GRID"]["ZLAYER"]["MAX"] = []
        jfile["GRID"]["ZLAYER"]["INCREMENT"] = []

        jfile["GRANULOMETRY"] = {}
        jfile["GRANULOMETRY"]["DISTRIBUTION"] = " "
        jfile["GRANULOMETRY"]["MIXING_FACTOR"] = []
        jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = []
        jfile["GRANULOMETRY"]["FI_MEAN"] = []
        jfile["GRANULOMETRY"]["FI_DISP"] = []
        jfile["GRANULOMETRY"]["FI_RANGE"] = {}
        jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = []
        jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = []
        jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
        jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = []
        jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = []
        jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
        jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = []
        jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = []

        jfile["SOURCE"] = {}
        jfile["SOURCE"]["SOURCE_TYPE"] = " "
        jfile["SOURCE"]["POINT"] = {}
        jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = []
        jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = []
        jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = ' '
        jfile["SOURCE"]["SUZUKI"] = {}
        jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = []
        jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = []
        jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = " "
        jfile["SOURCE"]["SUZUKI"]["A"] = []
        jfile["SOURCE"]["SUZUKI"]["L"] = []
        jfile["SOURCE"]["HAT"] = {}
        jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = []
        jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = []
        jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = " "
        jfile["SOURCE"]["HAT"]["THICKNESS"] = []
        jfile["SOURCE"]["PLUME"] = {}
        jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = " "
        jfile["SOURCE"]["PLUME"]["MFR"] = {}
        jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = []
        jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = []
        jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
        jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = []
        jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = []
        jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = []
        jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = []
        jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = " "
        jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = " "
        jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = " "
        jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = " "
        jfile["SOURCE"]["PLUME"]["A_S"] = " "
        jfile["SOURCE"]["PLUME"]["A_V"] = " "
        jfile["SOURCE"]["RESUSPENSION"] = {}
        jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = " "
        jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = []
        jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = []
        jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = []
        jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = " "
        jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = []
        jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = []
        jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = " "

        jfile["AGGREGATION"] = {}
        jfile["AGGREGATION"]["AGGREGATION"] = " "
        jfile["AGGREGATION"]["AGGREGATION_MODEL"] = " "
        jfile["AGGREGATION"]["FI_AGGREGATES"] = []
        jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = []
        jfile["AGGREGATION"]["VSET_FACTOR"] = []
        jfile["AGGREGATION"]["PERCENTAGE"] = []
        jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = []

        jfile["SO2"] = {}
        jfile["SO2"]["SO2"] = " "
        jfile["SO2"]["SO2_PERCENTAGE"] = []

        jfile["GRAVITY_CURRENT"] = {}
        jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = " "
        jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = []
        jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = []
        jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = []
        jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = []

        jfile["FALL3D"] = {}
        jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = " "
        jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = " "
        jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = []
        jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = " "
        jfile["FALL3D"]["RAMS_CS"] = []
        jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = []
        jfile["FALL3D"]["WET_DEPOSITION"] = " "
        jfile["FALL3D"]["P_RATE"] = []
        jfile["FALL3D"]["FLIGHT_LEVEL"] = []
        jfile["FALL3D"]["SIMULATION"] = " "

        jfile["OUTPUT"] = {}
        jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = []
        jfile["OUTPUT"]["TIME_IMAGES"] = []
        jfile["OUTPUT"]["POINTS_LOAD"] = {}
        jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
        jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
        jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
        jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
        jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "

        return jfile

    #================================================================   
    #================================================================   

    def gen_json_pre(case, example_cases, pjtname):

        if case == example_cases[0]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "STROMBOLI"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 2017
            jfile["TIME"]["MONTH"] = 1
            jfile["TIME"]["DAY"] = 1
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = "NCAR"
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = "MONTHLY_MEAN"
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 4
            jfile["TIME"]["RUN_END"] = 6
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 15
            jfile["GRID"]["LONMAX"] = 18
            jfile["GRID"]["LATMIN"] = 37
            jfile["GRID"]["LATMAX"] = 40
            jfile["GRID"]["TOPO_RESOL"] = 5
            jfile["GRID"]["LON_VENT"] = 15.211242
            jfile["GRID"]["LAT_VENT"] = 38.793356
            jfile["GRID"]["VENT_HEIGHT"] = 926
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.25
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [5.7]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = 0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 11
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 2300
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2700
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.5
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 1

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = 4000
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 5000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 1700000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 5
            jfile["SOURCE"]["SUZUKI"]["L"] = 2
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 8000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 2000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "DIOGUARDI2018"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "
            
        #================================================================    
        elif case == example_cases[1]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "ETNA"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 2018 # last eruption 24/12/2018
            jfile["TIME"]["MONTH"] = 12
            jfile["TIME"]["DAY"] = 24
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = "NCAR"
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = "MONTHLY_MEAN"
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 4
            jfile["TIME"]["RUN_END"] = 6
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 14
            jfile["GRID"]["LONMAX"] = 18
            jfile["GRID"]["LATMIN"] = 35
            jfile["GRID"]["LATMAX"] = 39
            jfile["GRID"]["TOPO_RESOL"] = 5
            jfile["GRID"]["LON_VENT"] = 14.995
            jfile["GRID"]["LAT_VENT"] = 37.755
            jfile["GRID"]["VENT_HEIGHT"] = 3329
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.25
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [5.7]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = 0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 11
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 2300
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2700
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.5
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 1

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = 4000
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 5000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 1700000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 5
            jfile["SOURCE"]["SUZUKI"]["L"] = 2
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 8000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 2000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [100, 200]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "DIOGUARDI2018"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "
             
        #================================================================    
        elif case == example_cases[2]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "VULCANO (AEOLIAN ISLANDS)"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 2017
            jfile["TIME"]["MONTH"] = 1
            jfile["TIME"]["DAY"] = 1
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = "NCAR"
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = "MONTHLY_MEAN"
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 4
            jfile["TIME"]["RUN_END"] = 6
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 14
            jfile["GRID"]["LONMAX"] = 17
            jfile["GRID"]["LATMIN"] = 37
            jfile["GRID"]["LATMAX"] = 40
            jfile["GRID"]["TOPO_RESOL"] = 5
            jfile["GRID"]["LON_VENT"] = 14.979444
            jfile["GRID"]["LAT_VENT"] = 38.391111
            jfile["GRID"]["VENT_HEIGHT"] = 400
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.25
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [5.7]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = 0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 11
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 2300
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2700
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.5
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 1

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = 4000
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 5000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 1700000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 5
            jfile["SOURCE"]["SUZUKI"]["L"] = 2
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 8000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 2000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "DIOGUARDI2018"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "            

        #================================================================    
        elif case == example_cases[3]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "VESUVIUS"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 2000 # last eruption in 03/1944 
            jfile["TIME"]["MONTH"] = 3
            jfile["TIME"]["DAY"] = 1
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = "NCAR"
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = "MONTHLY_MEAN"
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 4
            jfile["TIME"]["RUN_END"] = 6
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 13
            jfile["GRID"]["LONMAX"] = 17
            jfile["GRID"]["LATMIN"] = 39
            jfile["GRID"]["LATMAX"] = 43
            jfile["GRID"]["TOPO_RESOL"] = 5
            jfile["GRID"]["LON_VENT"] = 14.425556
            jfile["GRID"]["LAT_VENT"] = 40.821389
            jfile["GRID"]["VENT_HEIGHT"] = 1281
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.25
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [5.7]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = 0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 11
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 2300
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2700
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.5
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 1

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = 4000
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 5000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 1700000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 5
            jfile["SOURCE"]["SUZUKI"]["L"] = 2
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 8000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 2000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "DIOGUARDI2018"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "             

        #================================================================ 
        elif case == example_cases[4]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "EIFEL (LAACHER SEE VOLCANO)"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 2010 # last eruption 10.930 BC
            jfile["TIME"]["MONTH"] = 6
            jfile["TIME"]["DAY"] = 1
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = "NCAR"
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = "MONTHLY_MEAN"
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 4
            jfile["TIME"]["RUN_END"] = 6
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 5
            jfile["GRID"]["LONMAX"] = 9
            jfile["GRID"]["LATMIN"] = 48
            jfile["GRID"]["LATMAX"] = 52
            jfile["GRID"]["TOPO_RESOL"] = 5
            jfile["GRID"]["LON_VENT"] = 7.269722
            jfile["GRID"]["LAT_VENT"] = 50.410278
            jfile["GRID"]["VENT_HEIGHT"] = 0 # maar, no height
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.25
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [5.7]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = 0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 11
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 2300
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2700
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.5
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 1

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = 4000
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 5000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 1700000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 5
            jfile["SOURCE"]["SUZUKI"]["L"] = 2
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 8000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 2000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "DIOGUARDI2018"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "

        #================================================================
        elif case == example_cases[5]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "TAVURVUR"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 1994
            jfile["TIME"]["MONTH"] = 5
            jfile["TIME"]["DAY"] = 20
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = 'ERAIterim'
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = 'MONTHLY_MEAN'
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 24
            jfile["TIME"]["RUN_END"] = 26
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 151.8
            jfile["GRID"]["LONMAX"] = 152.3
            jfile["GRID"]["LATMIN"] = -4.3
            jfile["GRID"]["LATMAX"] = -3.8
            jfile["GRID"]["TOPO_RESOL"] = 1
            jfile["GRID"]["LON_VENT"] = 152.2094
            jfile["GRID"]["LAT_VENT"] = -4.238
            jfile["GRID"]["VENT_HEIGHT"] = 688
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 7000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 1000

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.5
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [0]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = -4.0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 3.0
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 1600
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2000
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.9
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 0.9

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = [2000, 5000]
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = "Est"
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = "MASTIN"
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 6000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 720000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 2.0
            jfile["SOURCE"]["SUZUKI"]["L"] = 5.0
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 6000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = "Est"
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = "MASTIN"
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 5000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "GANSER"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50.0
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2500
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "

        #================================================================
        elif case == example_cases[6]:

            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "GUNTUR"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 1994
            jfile["TIME"]["MONTH"] = 5
            jfile["TIME"]["DAY"] = 20
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = 'NCAR'
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = 'MONTHLY_MEAN'
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 5
            jfile["TIME"]["RUN_END"] = 5
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 107.6
            jfile["GRID"]["LONMAX"] = 108
            jfile["GRID"]["LATMIN"] = -7.4
            jfile["GRID"]["LATMAX"] = -7.0
            jfile["GRID"]["TOPO_RESOL"] = 1
            jfile["GRID"]["LON_VENT"] = 107.851038
            jfile["GRID"]["LAT_VENT"] = -7.151884
            jfile["GRID"]["VENT_HEIGHT"] = 2250
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 10000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 1000

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.5
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 8
            jfile["GRANULOMETRY"]["FI_MEAN"] = [-1, 5]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = -4.0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 3.0
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 1200
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2500
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.9
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 0.9

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = [2000, 5000]
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = "Est"
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = "MASTIN"
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 6000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 3000000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 4.0
            jfile["SOURCE"]["SUZUKI"]["L"] = 1.0
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 6000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = "Est"
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = "MASTIN"
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 5000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "GANSER"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 100.0
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 1000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 8000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = 2
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = [107.733, -7.318]
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = "Papandayan"
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = [107.817, -7.269]
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = "Cipangligaran"

        #================================================================
        elif case == example_cases[7]:
            # this scenario is only implemented for testing purposes, will be removed later
            jfile = {}

            jfile["SCENARIO"] = {}
            jfile["SCENARIO"]["TYPE"] = "PRE-DEF"
            jfile["SCENARIO"]["SCNAME"] = "STROMBOLI_MG_TESTING"
            jfile["SCENARIO"]["NAME"] = pjtname

            jfile["TIME"] = {}
            jfile["TIME"]["YEAR"] = 2015
            jfile["TIME"]["MONTH"] = 1
            jfile["TIME"]["DAY"] = 1
            jfile["TIME"]["TYPE_METEO_DATA"] = {}
            jfile["TIME"]["TYPE_METEO_DATA"]["ANALYSIS_TYPE"] = "NCAR"
            jfile["TIME"]["TYPE_METEO_DATA"]["DATA_TYPE"] = "DAILY"
            jfile["TIME"]["BEGIN_METEO_DATA"] = 0
            jfile["TIME"]["TIME_STEP_METEO_DATA"] = 6
            jfile["TIME"]["END_METEO_DATA"] = 10000
            jfile["TIME"]["ERUPTION_START"] = 0
            jfile["TIME"]["ERUPTION_END"] = 2
            jfile["TIME"]["RUN_END"] = 2
            jfile["TIME"]["RESTART"] = False

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 15.15
            jfile["GRID"]["LONMAX"] = 15.26
            jfile["GRID"]["LATMIN"] = 38.75
            jfile["GRID"]["LATMAX"] = 38.83
            jfile["GRID"]["TOPO_RESOL"] = 0.9
            jfile["GRID"]["LON_VENT"] = 15.211242
            jfile["GRID"]["LAT_VENT"] = 38.793356
            jfile["GRID"]["VENT_HEIGHT"] = 926
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRID"] = {}
            jfile["GRID"]["COORDINATES"] = "LON-LAT"
            jfile["GRID"]["LONMIN"] = 15
            jfile["GRID"]["LONMAX"] = 17
            jfile["GRID"]["LATMIN"] = 38
            jfile["GRID"]["LATMAX"] = 40
            jfile["GRID"]["TOPO_RESOL"] = 15
            jfile["GRID"]["LON_VENT"] = 15.211242
            jfile["GRID"]["LAT_VENT"] = 38.793356
            jfile["GRID"]["VENT_HEIGHT"] = 926
            jfile["GRID"]["ZLAYER"] = {}
            jfile["GRID"]["ZLAYER"]["MIN"] = 0
            jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            # jfile["GRID"] = {}
            # jfile["GRID"]["COORDINATES"] = "LON-LAT"
            # jfile["GRID"]["LONMIN"] = 15.15
            # jfile["GRID"]["LONMAX"] = 15.26
            # jfile["GRID"]["LATMIN"] = 38.75
            # jfile["GRID"]["LATMAX"] = 38.83
            # jfile["GRID"]["TOPO_RESOL"] = 0.9
            # jfile["GRID"]["LON_VENT"] = 15.211242
            # jfile["GRID"]["LAT_VENT"] = 38.793356
            # jfile["GRID"]["VENT_HEIGHT"] = 926
            # jfile["GRID"]["ZLAYER"] = {}
            # jfile["GRID"]["ZLAYER"]["MIN"] = 0 
            # jfile["GRID"]["ZLAYER"]["MAX"] = 8000
            # jfile["GRID"]["ZLAYER"]["INCREMENT"] = 500

            jfile["GRANULOMETRY"] = {}
            jfile["GRANULOMETRY"]["DISTRIBUTION"] = "GAUSSIAN"
            jfile["GRANULOMETRY"]["MIXING_FACTOR"] = 0.25
            jfile["GRANULOMETRY"]["NUMBER_CLASSES"] = 10
            jfile["GRANULOMETRY"]["FI_MEAN"] = [5.7]
            jfile["GRANULOMETRY"]["FI_DISP"] = [2]
            jfile["GRANULOMETRY"]["FI_RANGE"] = {}
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMIN"] = 0
            jfile["GRANULOMETRY"]["FI_RANGE"]["FIMAX"] = 11
            jfile["GRANULOMETRY"]["DENSITY_RANGE"] = {}
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMIN"] = 2300
            jfile["GRANULOMETRY"]["DENSITY_RANGE"]["RHOMAX"] = 2700
            jfile["GRANULOMETRY"]["SHAPE_RANGE"] = {}
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MIN"] = 0.5
            jfile["GRANULOMETRY"]["SHAPE_RANGE"]["SHAPE_MAX"] = 1

            jfile["SOURCE"] = {}
            jfile["SOURCE"]["SOURCE_TYPE"] = "SUZUKI"
            jfile["SOURCE"]["POINT"] = {}
            jfile["SOURCE"]["POINT"]["HEIGHT_ABOVE_VENT"] = 4000
            jfile["SOURCE"]["POINT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["POINT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"] = {}
            jfile["SOURCE"]["SUZUKI"]["HEIGHT_ABOVE_VENT"] = 5000
            jfile["SOURCE"]["SUZUKI"]["MASS_FLOW_RATE"] = 1700000
            jfile["SOURCE"]["SUZUKI"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["SUZUKI"]["A"] = 5
            jfile["SOURCE"]["SUZUKI"]["L"] = 2
            jfile["SOURCE"]["HAT"] = {}
            jfile["SOURCE"]["HAT"]["HEIGHT_ABOVE_VENT"] = 8000
            jfile["SOURCE"]["HAT"]["MASS_FLOW_RATE"] = 1000000
            jfile["SOURCE"]["HAT"]["ESTIMATE_MFR"] = 'NONE'
            jfile["SOURCE"]["HAT"]["THICKNESS"] = 2000
            jfile["SOURCE"]["PLUME"] = {}
            jfile["SOURCE"]["PLUME"]["SOLVE_PLUME_FOR"] = "HEIGHT"
            jfile["SOURCE"]["PLUME"]["MFR"] = {}
            jfile["SOURCE"]["PLUME"]["MFR"]["SEARCH_RANGE"] = [3, 7]
            jfile["SOURCE"]["PLUME"]["MFR"]["HEIGHT_ABOVE_VENT"] = [3000, 5000]
            jfile["SOURCE"]["PLUME"]["HEIGHT"] = {}
            jfile["SOURCE"]["PLUME"]["HEIGHT"]["MASS_FLOW_RATE"] = [1000, 10000]
            jfile["SOURCE"]["PLUME"]["EXIT_VEL"] = [200, 300]
            jfile["SOURCE"]["PLUME"]["EXIT_TEM"] = [1000, 1070]
            jfile["SOURCE"]["PLUME"]["EXIT_WATER_FRAC"] = [1, 1]
            jfile["SOURCE"]["PLUME"]["WIND_COUPLING"] = True
            jfile["SOURCE"]["PLUME"]["AIR_MOISTURE"] = True
            jfile["SOURCE"]["PLUME"]["REENTRAINMENT"] = True
            jfile["SOURCE"]["PLUME"]["LATENT_HEAT"] = True
            jfile["SOURCE"]["PLUME"]["A_S"] = "KAMINSKI_C"
            jfile["SOURCE"]["PLUME"]["A_V"] = "TATE"
            jfile["SOURCE"]["RESUSPENSION"] = {}
            jfile["SOURCE"]["RESUSPENSION"]["RESUSPENSION"] = True
            jfile["SOURCE"]["RESUSPENSION"]["MAX_RESUSPENSION_SIZE"] = 100
            jfile["SOURCE"]["RESUSPENSION"]["DEPOSIT_THRESHOLD"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["MAX_INJECTION_HEIGHT"] = 1000
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_SCHEME"] = "WESTPHAL"
            jfile["SOURCE"]["RESUSPENSION"]["EMISSION_FACTOR"] = 1.0
            jfile["SOURCE"]["RESUSPENSION"]["THRESHOLD_UST"] = 0.3
            jfile["SOURCE"]["RESUSPENSION"]["MOISTURE_CORRECTION"] = True

            jfile["AGGREGATION"] = {}
            jfile["AGGREGATION"]["AGGREGATION"] = False
            jfile["AGGREGATION"]["AGGREGATION_MODEL"] = "CORNELL"
            jfile["AGGREGATION"]["FI_AGGREGATES"] = 2.0
            jfile["AGGREGATION"]["DENSITY_AGGREGATES"] = 350
            jfile["AGGREGATION"]["VSET_FACTOR"] = 1.0
            jfile["AGGREGATION"]["PERCENTAGE"] = 20.0
            jfile["AGGREGATION"]["FRACTAL_EXPONENT"] = 2.99

            jfile["SO2"] = {}
            jfile["SO2"]["SO2"] = False
            jfile["SO2"]["SO2_PERCENTAGE"] = 2.0

            jfile["GRAVITY_CURRENT"] = {}
            jfile["GRAVITY_CURRENT"]["GRAVITY_CURRENT"] = False
            jfile["GRAVITY_CURRENT"]["C_FLOW_RATE"] = 10000
            jfile["GRAVITY_CURRENT"]["LAMBDA_GRAV"] = 0.2
            jfile["GRAVITY_CURRENT"]["K_ENTRAIN"] = 0.1
            jfile["GRAVITY_CURRENT"]["BRUNT_VAISALA"] = 0.02

            jfile["FALL3D"] = {}
            jfile["FALL3D"]["TERMINAL_VELOCITY_MODEL"] = "DIOGUARDI2018"
            jfile["FALL3D"]["VERTICAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["VERTICAL_DIFFUSION_COEFFICIENT"] = 50
            jfile["FALL3D"]["HORIZONTAL_TURBULENCE_MODEL"] = "CONSTANT"
            jfile["FALL3D"]["RAMS_CS"] = 0.1
            jfile["FALL3D"]["HORIZONTAL_DIFFUSION_COEFFICIENT"] = 2000
            jfile["FALL3D"]["WET_DEPOSITION"] = False
            jfile["FALL3D"]["P_RATE"] = 0
            jfile["FALL3D"]["FLIGHT_LEVEL"] = 5000
            jfile["FALL3D"]["SIMULATION"] = "real"

            jfile["OUTPUT"] = {}
            jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            jfile["OUTPUT"]["TIME_IMAGES"] = 0.5
            jfile["OUTPUT"]["POINTS_LOAD"] = {}
            jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = []
            jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = " "
            jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = " "

            # jfile["OUTPUT"] = {}
            # jfile["OUTPUT"]["POSTPROCESS_TIME_INTERVAL"] = 2.0
            # jfile["OUTPUT"]["TIME_IMAGES"] = 0.25
            # jfile["OUTPUT"]["POINTS_LOAD"] = {}
            # jfile["OUTPUT"]["POINTS_LOAD"]["NUMBER_POINTS"] = 2
            # jfile["OUTPUT"]["POINTS_LOAD"]["P1"] = [16, 39]
            # jfile["OUTPUT"]["POINTS_LOAD"]["P1_NAME"] = "AAAAAA"
            # jfile["OUTPUT"]["POINTS_LOAD"]["P2"] = [15.5, 38.5]
            # jfile["OUTPUT"]["POINTS_LOAD"]["P2_NAME"] = "BBBBBB"           

        return jfile
