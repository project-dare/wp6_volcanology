"""
Created on Tue Apr 14 14:59:34 2020

author: Michael Grund
"""

import json
import os
from datetime import datetime
import sys
import netCDF4 as nc4
import requests
from dispel4py.core import GenericPE
from dispel4py.workflow_graph import WorkflowGraph

path_to_store = os.path.join("/home/mpiuser/sfs/", os.environ["USERNAME"], "runs", os.environ["RUN_DIR"])
sys.path.append(path_to_store)
vc_plotting = requests.get("https://gitlab.com/project-dare/wp6_volcanology/-/raw/master/vc_plotting.py")
with open(os.path.join(path_to_store, "vc_plotting.py"), "w") as f:
    f.write(vc_plotting.text)

import vc_plotting


# ==========================================================================================
class ProducerPE(GenericPE):
    def __init__(self):
        GenericPE.__init__(self)
        self._add_input('input')
        self._add_output('output')

    def _process(self, inputs):
        paras = inputs['plotting_parameters']
        base_path = paras["upload_folder"]
        jfilepath = paras["jfilepath"]

        with open(jfilepath) as file:
            data = json.load(file)  # data contains content of project json file

        # paths to required files
        # path where final figures are saved
        figures_folder = os.path.join(
            "/home/mpiuser/sfs/{}/runs/{}".format(os.environ["USERNAME"], os.environ["RUN_DIR"]), "output", "Figures")
        if not os.path.exists(figures_folder):
            os.mkdir(figures_folder)
        paras['savepath'] = os.path.join(figures_folder)
        # path to cartopy data files etc.
        paras['path2cp'] = os.path.join(paras["data_folder"], 'Cartopy_Data')
        # path to netcdf file that contains all simulation results
        paras['netcdf'] = os.path.join(base_path, 'simres_grids.nc')

        # parameters required to correctly display simulation results
        # get them from project json file
        paras['lonmin'] = data['GRID']['LONMIN']
        paras['lonmax'] = data['GRID']['LONMAX']
        paras['latmin'] = data['GRID']['LATMIN']
        paras['latmax'] = data['GRID']['LATMAX']
        paras['zmin'] = data['GRID']['ZLAYER']['MIN']
        paras['zmax'] = data['GRID']['ZLAYER']['MAX']
        paras['xvent'] = data['GRID']['LON_VENT']
        paras['yvent'] = data['GRID']['LAT_VENT']

        paras['analysis_type'] = data['TIME']['TYPE_METEO_DATA']['DATA_TYPE']
        paras['simulation_type'] = data['FALL3D']['SIMULATION']  # type == real

        # should paras be saved and can be used for provencance?

        ##################################
        # assign parameter names etc. included in results netcdf file and further stuff for plotting
        filelist = {'gload': {'type': 'mapview',
                              'group': 'Groundload_data',
                              'varname': 'Ground_load',
                              'xdim': 'Longitude',
                              'ydim': 'Latitude',
                              'annots': {'title': 'Ground load',
                                         'clabel': 'Ground load in kg/m$^2$',
                                         'filename': 'Ground_load'}},
                    'depoth': {'type': 'mapview',
                               'group': 'Depothick_data',
                               'varname': 'Depo_thick',
                               'xdim': 'Longitude',
                               'ydim': 'Latitude',
                               'annots': {'title': 'Deposit thickness',
                                          'clabel': 'Deposit thickness in mm',
                                          'filename': 'Deposit_thickness'}},
                    'airb': {'type': 'mapview',
                             'group': 'Airborne_data',
                             'varname': 'Airborne_mass',
                             'xdim': 'Longitude',
                             'ydim': 'Latitude',
                             'annots': {'title': 'Total air-borne mass',
                                        'clabel': 'Height averaged concentration in g/m$^3$',
                                        'filename': 'Height_Averaged_Conc'}},
                    'conc': {'type': 'densplot',
                             'group1': 'Concentration_Info_1',
                             'varname1': 'Ground_conc',
                             'xdim1': 'Longitude',
                             'ydim1': 'Latitude',
                             'group2': 'Concentration_Info_2',
                             'varname2': 'Ash_conc',
                             'xdim2': 'Latitude',
                             'ydim2': 'Vertical'}}

        ##################################
        # check how many time steps are included in netcdf file
        filein = paras['netcdf']
        f = nc4.Dataset(filein, 'r')
        tsteps = f.groups['Groundload_data'].variables['Time']

        ##################################
        # plot all time steps or only user-specified ones
        if paras['plot_all'] == True:
            idxs = range(0, len(tsteps) - 1)
        else:
            idxs = paras['plot_all']

            try:
                if not all(isinstance(idx, int) for idx in idxs):
                    raise ValueError('Request could not be completed.')
            except ValueError:
                print('Value error: Only integers allowed in list, e.g. [0,1,4,10].')
                raise

            try:
                if not all(idx <= len(tsteps) - 1 for idx in idxs):
                    raise ValueError('Request could not be completed.')
            except ValueError:
                print(
                    'Value error: Only integers <= ' + str(len(tsteps) - 1) + ' (max. time step) are allowed in list.')
                raise

        ##################################        
        # looping
        for key in filelist:  # loop over keys

            for idx in idxs:  # loop over time steps

                self.write('output', [filelist, paras, key, idx])

                currtstp = datetime.fromtimestamp(tsteps[idx])
                currtstr = currtstp.strftime('%d %b %Y at %H:%M')
                print("sending the %s grid for time step (%s) to be plotted" % (key, currtstr))


# ==========================================================================================
class PlottingPE(GenericPE):
    def __init__(self):
        GenericPE.__init__(self)
        self._add_input('input')
        self._add_output('output')

    def _process(self, inputs):
        filelist, paras, key, idx = inputs['input']

        ##################################
        # load netcdf file 
        filein = paras['netcdf']
        f = nc4.Dataset(filein, 'r')

        paras['nx'] = f.nx
        paras['ny'] = f.ny

        # differentiate between map view and density plot
        if filelist.get(key)['type'] == 'mapview':

            datagrid, xx, yy, time_num = vc_plotting.get_nc_data(f,
                                                                 filelist.get(key)['group'],
                                                                 filelist.get(key)['varname'],
                                                                 filelist.get(key)['xdim'],
                                                                 filelist.get(key)['ydim'])

            timestp = datetime.fromtimestamp(time_num[idx])

            vc_plotting.makeplot_map(paras,
                                     datagrid[idx, :, :],
                                     timestp,
                                     filelist.get(key)['annots'])
        else:
            datagrid1, xx, yy, time_num = vc_plotting.get_nc_data(f,
                                                                  filelist.get(key)['group1'],
                                                                  filelist.get(key)['varname1'],
                                                                  filelist.get(key)['xdim1'],
                                                                  filelist.get(key)['ydim1'])

            datagrid2, xx, yy, time_num = vc_plotting.get_nc_data(f,
                                                                  filelist.get(key)['group2'],
                                                                  filelist.get(key)['varname2'],
                                                                  filelist.get(key)['xdim2'],
                                                                  filelist.get(key)['ydim2'])

            timestp = datetime.fromtimestamp(time_num[idx])
            vc_plotting.makeplot_sub(paras,
                                     datagrid1[idx, :, :],
                                     datagrid2[idx, :, :],
                                     timestp)

        ##################################


# ==========================================================================================

graph = WorkflowGraph()
producer = ProducerPE()
producer.name = 'producer'
plotting = PlottingPE()
graph.connect(producer, 'output', plotting, 'input')
## uncoment this line if you want to produce a plot of the workflow
# write_image(graph, "f3_plotting.png")
