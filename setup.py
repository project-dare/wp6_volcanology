# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 09:15:51 2018

@author: rbeck
"""
# python setup.py build_ext --inplace

from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
    name = 'Fall3D',
    ext_modules=cythonize(["Fall3D/CFall3D.pyx", "Fall3D/CDomain.pyx", "Fall3D/Csource.pyx"]),
    include_dirs=[numpy.get_include()]
)    
