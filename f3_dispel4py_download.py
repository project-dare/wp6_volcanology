"""
Created on Thu Mar 12 13:22:48 2020

download meteorological data of NCAR or ERA5

authors: Michael Grund, Rosa Filgueira
"""

import json
import os
import pathlib
import time
from datetime import datetime
# =====================================================================================
import cdsapi  # see instructions on: https://cds.climate.copernicus.eu/api-how-to
import wget  # use python-wget
import requests
from dispel4py.core import GenericPE
from dispel4py.workflow_graph import WorkflowGraph

download_era = requests.get("https://gitlab.com/project-dare/wp6_volcanology/-/raw/master/vc_data_download_era.py")
path_to_store = os.path.join("/home/mpiuser/sfs/", os.environ["USERNAME"], "runs", os.environ["RUN_DIR"])

import sys
sys.path.append(path_to_store)

with open(os.path.join(path_to_store, "vc_data_download_era.py"), "w") as f:
    f.write(download_era.text)
download_ncar = requests.get("https://gitlab.com/project-dare/wp6_volcanology/-/raw/master/vc_data_download_ncar.py")
with open(os.path.join(path_to_store, "vc_data_download_ncar.py"), "w") as f:
    f.write(download_ncar.text)

from vc_data_download_era import prep_download_era, check4key
from vc_data_download_ncar import prep_download_ncar


# please check if file .cdsapirc exits in your home directory, if not create it and
# add information of your account as described under the link above

# for linux: $HOME/.cdsapirc (in your Unix/Linux environment)
# for windows  %USERPROFILE%\.cdsapirc, where in your windows environment, %USERPROFILE% is usually located at C:\Users\Username folder)
# =====================================================================================

class ProducerPE(GenericPE):
    def __init__(self):
        GenericPE.__init__(self)
        self._add_input('input')
        self._add_output('output')

    def _process(self, inputs):

        # input is the path to the json file of the current project
        # as given in file "project_parameters.json"
        jfilepath = inputs['jfilepath']
        download_folder = inputs["download_folder"]

        with open(jfilepath) as file:
            jfile = json.load(file)

        # ====================================
        # NCAR or ERA5 data
        analysis_type = jfile['TIME']['TYPE_METEO_DATA']['ANALYSIS_TYPE']
        datatype = jfile['TIME']['TYPE_METEO_DATA']['DATA_TYPE']

        print('')
        print('Source of meteorological data: ' + analysis_type)
        print('')

        # get required file list
        if analysis_type == 'NCAR':
            filelist, path2store = prep_download_ncar(jfile, download_folder)
        else:
            # first check for cds API key file
            check4key()
            filelist, path2store = prep_download_era(jfile, download_folder)
            # ====================================

        # create directory for data 
        try:
            os.makedirs(path2store)
        except:
            pass

        # write to output
        lenlist = len(filelist)
        counter = 1
        for key in filelist:
            self.write('output', [filelist, path2store, analysis_type, datatype, counter, lenlist, key])
            print("sending the file %s to be downloaded (%s/%s)" % (key, counter, lenlist))
            counter += 1

        print('')


class DownloadPE(GenericPE):
    def __init__(self):
        GenericPE.__init__(self)
        self._add_input('input')
        self._add_output('output')

    def _process(self, inputs):
        filelist, path2store, analysis_type, datatype, counter, lenlist, key = inputs['input']

        ##################################################################################
        # perform download for NCAR data
        if analysis_type == 'NCAR':

            # if exist file in current directory skip, else download
            if not pathlib.Path(os.path.join(path2store, filelist.get(key)['filename'])).exists():
                if key == list(filelist.keys())[counter - 1]:  # print that only for the first key in list
                    print('No required meteo data found! New data will be stored in: > ' +
                          path2store + ' <.')
                    print('Start download...')
                    try:
                        print('Downloading file ' + str(counter) + '/' + str(lenlist) + ': ' + filelist.get(key)[
                            'filename'] + ' ...')  # progress bar is only shown in shell not in spyder
                        wget.download(filelist.get(key)['url'],
                                      out=os.path.join(path2store, filelist.get(key)['filename']))
                        print(' Download complete!')

                    except:
                        print('Error while downloading file ' + filelist.get(key)['filename'] + '! Skip ...')
                else:
                    print('Error, key %s different from filelist key %s' % (key, list(filelist.keys())[counter - 1]))
            elif pathlib.Path(os.path.join(path2store, filelist.get(key)['filename'])).exists():

                if key == list(filelist.keys())[0]:  # print that only for the first key in list
                    print('Required meteorological data found!')
                    filestats = os.stat(os.path.join(path2store, filelist[key]['filename']))
                    # current date
                    datenow = datetime.now()  # - timedelta(days = 132)
                    # file last time downloaded
                    datelmod = datetime.utcfromtimestamp(filestats.st_mtime)
                    # if difference beween datenow and datelmod is larger than 31 days (maximum of possible days
                    # between updates are available at NCAR webserver) re-download files
                    timediff = datenow - datelmod
                    if abs(timediff.days) > 31:
                        print('Last update was done ' + str(
                            abs(timediff.days)) + ' days ago. Search for newer version of file '
                              + filelist[key]['filename'] + '!')
                        try:
                            print('Re-downloading file ' + str(counter) + '/' + str(lenlist) + ': ' + filelist.get(key)[
                                'filename'] + ' ...')
                            wget.download(filelist.get(key)['url'],
                                          out=os.path.join(path2store, filelist.get(key)['filename']))
                            print('Re-download complete!')
                        except:
                            print('Error while re-downloading file ' + filelist.get(key)['filename'] + '! Skip ...')

                    else:
                        print('Use NCAR data already available in database!')

                else:
                    print('File already exists in current directory! Skip ...')
                    print("last modified: %s" % time.ctime(
                        os.path.getmtime(os.path.join(path2store, filelist.get(key)['filename']))))
                    print("created: %s" % time.ctime(
                        os.path.getctime(os.path.join(path2store, filelist.get(key)['filename']))))
                    print('Search for more recent version of the file ...')

        ##################################################################################            
        # perform download for ERA5 data            
        else:

            if datatype == 'MONTHLY_MEAN':

                if not pathlib.Path(filelist.get(key)[2]).exists():

                    print('No required meteo data found for ' + '/'.join(
                        [str(filelist.get(key)[1]['year']), str(filelist.get(key)[1]['month'])]) + '!')
                    print('New data will be stored in: > ' + path2store + ' <.')

                    c = cdsapi.Client()
                    c.retrieve(*filelist.get(key))  # * allows to unpack tuple here

                elif pathlib.Path(filelist.get(key)[2]).exists():
                    print('')
                    print('Use ERA5 data already available in database for ' + '/'.join(
                        [str(filelist.get(key)[1]['year']), str(filelist.get(key)[1]['month'])]) + '!')
                    print('')
                    pass

            else:  # DAILY MEANS

                # check if year folder for requested data already exists in MeteoDataBase
                try:
                    os.mkdir(path2store)
                except FileExistsError:
                    pass

                # if exist file in current directory skip, else download
                if not pathlib.Path(filelist.get(key)[2]).exists():

                    print('No required meteo data found for ' + '/'.join(
                        [str(filelist.get(key)[1]['year']), str(filelist.get(key)[1]['month']),
                         str(filelist.get(key)[1]['day'])]) + '!')
                    print('New data will be stored in: > ' + path2store + ' <.')
                    print('')
                    c = cdsapi.Client()
                    c.retrieve(*filelist.get(key))

                elif pathlib.Path(filelist.get(key)[2]).exists():
                    print('')
                    print('Use ERA5 data already available in database for ' + '/'.join(
                        [str(filelist.get(key)[1]['year']), str(filelist.get(key)[1]['month']),
                         str(filelist.get(key)[1]['day'])]) + '!')
                    print('')
                    pass


graph = WorkflowGraph()
producer = ProducerPE()
producer.name = 'producer'
download = DownloadPE()
graph.connect(producer, 'output', download, 'input')
## uncoment this line if you want to produce a plot of the workflow
# write_image(graph, "f3.png")
