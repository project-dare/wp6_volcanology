#!/bin/bash
set -x 

export PYTHONPATH=$PYTHONPATH:.

#### Use ONE of THE TWO - By default I have indicated the sequential

### Running the workflow in sequential
#dispel4py simple f3_dispel4py_download.py -d '{"producer": [ {"pjtname":"Eruption_test_Stromboli", "case": "Stromboli"}]}'   

### Running the workflow in Parallel - Multiprocessing - 10 cores
dispel4py multi f3_dispel4py_download.py -n 10 -d '{"producer": [ {"pjtname":"Eruption_test_Stromboli", "case": "Stromboli"}]}'   

