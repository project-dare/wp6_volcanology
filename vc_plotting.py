#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:28:45 2020

plotting routines for DARE volcanology use-case,
generate map and figure output based on Fall3DPy
simulation results

@author: mgrund
"""

import os
import netCDF4 as nc4

import cartopy.crs as ccrs
from cartopy.io.img_tiles import GoogleTiles
import cartopy.feature as cfeature

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.patheffects as PathEffects
from matplotlib import ticker


# ==========================================================================================
# helper function to extract all variables of the netcdf file
def get_nc_data(f, group, varname, xdim, ydim):
    # xdim, ylim formulation allows to use different combinations like, lat-lon, lat-vert, lon-vert 

    # get data group
    datagrp = f.groups[group]

    # preallocate array for data
    data = np.zeros(datagrp.variables[varname].shape)
    data = datagrp.variables[varname][:, :, :]

    xdimvals = datagrp.variables[xdim][:]
    ydimvals = datagrp.variables[ydim][:]

    time_num = datagrp.variables['Time'][:]

    return data, xdimvals, ydimvals, time_num


# ==========================================================================================

def assign_nc_data(paras):
    filein = paras['netcdf']
    f = nc4.Dataset(filein, 'r')

    paras['nx'] = f.nx
    paras['ny'] = f.ny

    ### map views
    group = 'Groundload_data'
    varname = 'Ground_load'
    xdim = 'Longitude'
    ydim = 'Latitude'
    gloadpre, xx, yy, time_num = get_nc_data(f, group, varname, xdim, ydim)

    group = 'Depothick_data'
    varname = 'Depo_thick'
    xdim = 'Longitude'
    ydim = 'Latitude'
    depopre, xx, yy, time_num = get_nc_data(f, group, varname, xdim, ydim)

    group = 'Airborne_data'
    varname = 'Airborne_mass'
    xdim = 'Longitude'
    ydim = 'Latitude'
    airbpre, xx, yy, time_num = get_nc_data(f, group, varname, xdim, ydim)

    ### other views
    group = 'Concentration_Info_1'
    varname = 'Ground_conc'
    xdim = 'Longitude'
    ydim = 'Latitude'
    gconcpre, xx1, yy1, time_num = get_nc_data(f, group, varname, xdim, ydim)

    group = 'Concentration_Info_2'
    varname = 'Ash_conc'
    xdim = 'Latitude'
    ydim = 'Vertical'
    aconcpre, xx2, yy2, time_num = get_nc_data(f, group, varname, xdim, ydim)

    # close netcdf file
    f.close()

    return gloadpre, depopre, airbpre, gconcpre, aconcpre, time_num


# ==========================================================================================
# background shading, real or gray color    
class ShadedReliefESRIReal(GoogleTiles):

    # shaded relief
    def _image_url(self, tile):
        x, y, z = tile
        url = ('https://server.arcgisonline.com/ArcGIS/rest/services/' \
               'World_Imagery/MapServer/tile/{z}/{y}/{x}.jpg').format(
            z=z, y=y, x=x)

        return url


class ShadedReliefESRIGray(GoogleTiles):

    # shaded relief
    def _image_url(self, tile):
        x, y, z = tile
        url = ('https://server.arcgisonline.com/ArcGIS/rest/services/' \
               'World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}.jpg').format(
            z=z, y=y, x=x)

        return url
        # ==========================================================================================


# MAKE MAPVIEW PLOTS
def makeplot_map(paras, datain, timestp, annots):
    # Cities (or other csv files)
    path_cities = os.path.join(paras['path2cp'], 'Cities.csv')
    cities = pd.read_csv(path_cities)
    Name = np.asarray(cities['NAME'])

    lonc = cities['LONGITUDE']
    latc = cities['LATITUDE']
    popc = np.asarray(cities['POP_MAX'])

    indc = np.where((latc <= paras['latmax']) &
                    (latc >= paras['latmin']) &
                    (lonc <= paras['lonmax']) &
                    (lonc >= paras['lonmin']))

    indc = np.asarray(indc)

    # Background image
    if paras['background'] == 'real':  # ESRI object
        tiler = ShadedReliefESRIReal()
    else:
        tiler = ShadedReliefESRIGray()
        # stamen_terrain = cimgt.Stamen('terrain-background')     # terrain background

    long = [paras['lonmin'], paras['lonmax']]
    lati = [paras['latmin'], paras['latmax']]

    states_provinces = cfeature.NaturalEarthFeature(
        category='cultural',
        name='admin_1_states_provinces_lines',
        scale='10m',
        facecolor='none')

    # resolution of background image
    if paras['autores'] == True:

        if abs(paras['lonmin'] - paras['lonmax']) > 5:
            tile_res = 7
        elif abs(paras['lonmin'] - paras['lonmax']) < 1:
            tile_res = 11
        else:
            tile_res = 10

    else:
        tile_res = paras['manures']

    # =======================================
    # Differentiate between different plotting contents
    # variables are already in correct format from main program saved
    # assign nan values (for display)
    if annots['title'] == 'Ground load':

        gmin = min(0.95, 0.01 * np.amax(datain))
        gmax = np.amax(datain)
        datain[datain < gmin] = float('nan')

        if gmax > 100:
            bounds_cb = np.linspace(0, int(gmax / 50) * 50, 7)
            bounds = np.append(1, bounds_cb)
        else:
            bounds_cb = np.linspace(0, gmax, 7)
            bounds = np.linspace(gmin * 1.01, gmax, 7)

        data2plot = datain
        vmax = np.amax(bounds_cb)

    elif annots['title'] == 'Deposit thickness':

        # assign nan values
        thickmax = np.amax(datain)
        thickmin = min(thickmax * 0.005, 0.001)  # in m
        datain[datain < thickmin] = float('nan')

        bounds_cb = np.linspace(0, thickmax, 5)
        bounds = np.linspace(1.1 * thickmin, thickmax, 5)

        data2plot = datain
        vmax = thickmax

    elif annots['title'] == 'Total air-borne mass':

        # min/max values
        mmin = 10 ** (-5)  # 0.01 g/m^3
        mmax = np.amax(datain)

        # assign Nan value so it becomes transparent
        datain[datain < mmin] = float('nan')

        bounds = np.linspace(mmin, mmax, 6)

        data2plot = datain
        vmax = mmax

    # =======================================

    fig1 = plt.figure(num=1,
                      figsize=paras['figsize'],
                      dpi=paras['dpi'],
                      facecolor=paras['figfacecol'],
                      edgecolor=paras['figedgecol'])

    ax = fig1.add_axes([0.15, 0.1, 0.65, 0.8],
                       projection=ccrs.PlateCarree(),
                       label='axmap')

    ax.set_extent([long[0], long[1], lati[0], lati[1]])

    # Background image
    if paras['simulation_type'] == 'real':  # use satellite image from area as background
        ax.add_image(tiler, tile_res)

    else:
        # use normal cartopy content !!! TODO !!!
        ax.coastlines()

    # ax.coastlines(resolution = '10m', color = 'black', linewidth = 1)
    # ax.add_feature(shape_feature, zorder = 10, facecolor = 'None')
    ax.add_feature(states_provinces, zorder=11, edgecolor='gray')

    # mark volcano
    ax.plot(paras['xvent'],
            paras['yvent'],
            paras['ventcol'] + '^',
            markersize=paras['ventsize'],
            markeredgecolor=paras['ventedgecol'],
            transform=ccrs.Geodetic(),
            zorder=24)

    # axis label etc.
    ax.set_xlabel('Longitude in$^\circ$')
    ax.set_ylabel('Latitude in$^\circ$')
    ax.set_yticks(np.linspace(paras['latmin'], paras['latmax'], 3))
    ax.set_xticks(np.linspace(paras['lonmin'], paras['lonmax'], 6))
    ax.set_xlim([paras['lonmin'], paras['lonmax']])
    ax.set_ylim([paras['latmin'], paras['latmax']])

    time_str = timestp.strftime('%d %b %Y at %H:%M')
    time_str_fname = timestp.strftime('%d_%b_%Y_at_%H-%M')

    ax.set_title(time_str + ' ' + annots['title'])

    # ========================================
    # add cities
    if paras['cities']['plot'] == True:

        for i in range(np.size(indc, axis=1)):
            ax.scatter(lonc[indc[0, i]],
                       latc[indc[0, i]],
                       c=paras['cities']['fcolor'],
                       edgecolor=paras['cities']['ecolor'],
                       s=paras['cities']['size'],
                       zorder=70)

            ax.annotate(Name[int(indc[0, i])],
                        xy=(lonc[indc[0, i]],
                            latc[indc[0, i]]),
                        xytext=(3, 3),
                        textcoords='offset points',
                        style=paras['cities']['tstyle'],
                        color=paras['cities']['tfcol'],
                        fontweight=paras['cities']['tfweight'],
                        zorder=45,
                        path_effects=[PathEffects.withStroke(linewidth=paras['cities']['tewidth'],
                                                             foreground=paras['cities']['tecol'])])
            # ========================================

    paras['img1'] = ax.imshow(data2plot,
                              zorder=12,
                              transform=ccrs.PlateCarree(),
                              cmap=paras['cmap'],
                              alpha=paras['alpha'],
                              extent=[paras['lonmin'],
                                      paras['lonmax'],
                                      paras['latmin'],
                                      paras['latmax']],
                              vmax=vmax, vmin=0)

    # colorbar axis
    axb = fig1.add_axes([0.85, 0.1, 0.025, 0.8],
                        label='axcol')

    paras['ax'] = ax
    paras['fig1'] = fig1
    paras['axb'] = axb

    # add colorbar
    cb = paras['fig1'].colorbar(paras['img1'],
                                cax=paras['axb'],
                                format='%.3f',
                                extend='max')

    cb.set_label(annots['clabel'])
    cb.solids.set(alpha=1)  # contours are transparent, remove transparency for colorbar here

    # TODO add manual adjusting of colorbar if required (put another parameter in paras)

    plt.locator_params(nbins=6)
    tick_locator = ticker.MaxNLocator(nbins=6)
    cb.locator = tick_locator
    cb.update_ticks()

    # ========================================
    # add contours
    if paras['plot_contour'] == True:

        nx_inter = 1000
        f_inter = int(nx_inter / paras['nx'])

        lon = np.linspace(paras['lonmin'],
                          paras['lonmax'],
                          paras['nx'] * f_inter)

        lat = np.linspace(paras['latmin'],
                          paras['latmax'],
                          paras['ny'] * f_inter)

        xx1, yy1 = np.meshgrid(lon, lat)

        # contour plot only at last iteration
        paras['contour1'] = paras['ax'].contour(xx1,
                                                yy1,
                                                data2plot,
                                                bounds,
                                                zorder=13,
                                                colors=paras['linec_contour'],
                                                transform=ccrs.PlateCarree(),
                                                alpha=0.8,
                                                linewidths=paras['linew_contour'])

        clabels = plt.clabel(paras['contour1'],
                             fontsize=paras['fonts_contour'],
                             fmt='%.0f',
                             colors=paras['labc_contour'])

        # workaround to plot contour labels on top, 'zorder' may be included
        # in plt.clabel as parameter in future releases (2020/02/13)
        if clabels:
            for clabel in clabels:
                clabel.set_zorder(120)

    # ========================================

    # ========================================
    # add inset showing the area from a global view
    if paras['inset']['plot'] == True:

        latinset = np.median(lat)
        loninset = np.median(lon)

        ax_sub = fig1.add_axes(paras['inset']['locsiz'],
                               projection=ccrs.Orthographic(central_longitude=loninset,
                                                            central_latitude=latinset),
                               label='axsub')

        if paras['inset']['content'] == 'real':

            # Add coastlines
            ax_sub.stock_img()
            ax_sub.coastlines(linewidth=paras['inset']['linw_conte'])

        elif paras['inset']['content'] == 'normal':

            ax_sub.add_feature(cfeature.OCEAN,
                               facecolor=paras['inset']['col_ocean'],
                               zorder=0)

            ax_sub.add_feature(cfeature.LAND,
                               zorder=100,
                               edgecolor=paras['inset']['col_conte'],
                               facecolor=paras['inset']['col_contf'],
                               linewidth=paras['inset']['linw_conte'])

        ax_sub.set_global()
        ax_sub.gridlines(zorder=101)

        # plot rectangle
        def_rect_x = [paras['lonmin'],
                      paras['lonmin'],
                      paras['lonmax'],
                      paras['lonmax']]

        def_rect_y = [paras['latmin'],
                      paras['latmax'],
                      paras['latmax'],
                      paras['latmin']]

        ax_sub.plot(def_rect_x,
                    def_rect_y,
                    paras['inset']['col_rect'],
                    transform=ccrs.Geodetic(),
                    zorder=150)

    # ========================================

    # save figure
    plt.pause(1)

    filename = annots['filename'] + '_' + time_str_fname + '.' + paras['format']
    paras['fig1'].savefig(os.path.join(paras['savepath'], filename),
                          format=paras['format'],
                          dpi=paras['dpi'])

    plt.close('all')


#########################################################################################################################################################
#########################################################################################################################################################
# MAKE DISTRIBUTION PLOT

def makeplot_sub(paras, datain1, datain2, timestp):
    plt.figure(num=2,
               figsize=paras['figsize'],
               dpi=paras['dpi'],
               facecolor=paras['figfacecol'],
               edgecolor=paras['figedgecol'])

    plt.clf()
    plt.subplot(211)
    plt.imshow(datain1,
               extent=[paras['lonmin'],
                       paras['lonmax'],
                       paras['latmin'],
                       paras['latmax']],
               aspect='auto',
               origin='lower',
               cmap=paras['cmap'])

    plt.xlabel('Longitude in$^\circ$')
    plt.ylabel('Latitude in$^\circ$')
    plt.colorbar(label='Ash ground concentration in kg/m$^3$')

    time_str = timestp.strftime('%d %b %Y at %H:%M')
    time_str_fname = timestp.strftime('%d_%b_%Y_at_%H-%M')
    plt.title(time_str + ' Concentration Info ')

    plt.subplot(212)
    plt.imshow(datain2,
               extent=[paras['latmin'],
                       paras['latmax'],
                       paras['zmin'],
                       paras['zmax']],
               aspect='auto',
               origin='lower',
               cmap=paras['cmap'])

    plt.colorbar(label='Ash concentration in kg/m$^3$')

    plt.xlabel('Latitude in$^\circ$')
    plt.ylabel('Height above surface in m')

    filename = 'Concentration_Info_' + time_str_fname + '.' + paras['format']
    plt.savefig(os.path.join(paras['savepath'], filename),
                format=paras['format'],
                dpi=paras['dpi'])

    plt.close('all')

    # ==========================================================================================
